<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'empresasData';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUserEmpresa', 'idSector', 'profilePic', 'contract', 'contactoName', 'contactoEmail', 'contactoPuesto', 'contactoPhone', 'calleNumero', 'cp', 'col', 'delmpo', 'edo', 'pais', 'folder',
    ];
}
