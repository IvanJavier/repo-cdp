<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Controllers\Services\EmailServiceController;
use App\User;
use App\Freelance;
use App\RoleUser;

class Freelance extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'freelances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profilePic', 'user_id', 'direccion', 'cp', 'colonia', 'delmpo', 'edo', 'pais', 'folder',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public static function allUsers(){
        
        $users = User::select('users.*')
                    ->leftJoin('freelances','users.id','=','freelances.user_id')
                    ->whereRaw('freelances.id IS NOT NULL')
                    ->orderBy('users.email', 'asc')
                    ->paginate(10);
        
        return $users;
    }

    public static function createUser($request){
 
        try {
            DB::transaction(function () use($request) {
                $pwd = \App\Helpers\AppHelper::instance()->randomString(15);
                $randomStr = \App\Helpers\AppHelper::instance()->randomString();

                $users = new User();
                $users->name = $request->name;
                $users->lastName = $request->lastName;
                $users->email = $request->email;
                $users->phone = $request->phone;
                $users->password = Hash::make($pwd);
                $users->save();

                $pic_name = 'profile_pic_default.png';
                $directory = str_replace("\\","/",storage_path()).'/app/public/'.$randomStr;
		       	File::makeDirectory($directory);
		       	$old = str_replace("\\","/",public_path()).'/img'.'/'.$pic_name;
		       	$new = $directory.'/'.$pic_name; 
                if(\file_exists($old)){
                    copy($old,$new);
                }

                $user_id = $users->id;
                $freelances = new Freelance();
                $freelances->user_id = $user_id;
                $freelances->profilePic = $randomStr.$pic_name;
                $freelances->direccion = $request->calleNumero;
                $freelances->cp = $request->cp;
                $freelances->colonia = $request->col;
                $freelances->delmpo = $request->delmpo;
                $freelances->edo = $request->edo;
                $freelances->pais = $request->pais;
                $freelances->folder = $randomStr;
                $freelances->save();

                $role_id = 7; //role freelance
                $roles = new RoleUser();
                $roles->role_id = $role_id;
                $roles->user_id = $user_id;
                $roles->save(); 

                if($users->save()){
                    EmailServiceController::sendEmail($request,$pwd);
                }
               
         });
          return \Response::json(array(
                                    'success' => true,
                                    'data' => 'Creado exitosamente'),
                                    200);
        } catch (\PDOException $e) {
             return \Response::json(array(
                                    'error' => false,
                                    'data' => $e),
                                    200);
        }
     }

     //get User by Id
    public static function getUserById($id){
        
       $user = User::select('users.*','freelances.direccion','freelances.cp','freelances.colonia','freelances.delmpo','freelances.edo','freelances.pais', 'freelances.folder','freelances.profilePic')
                    ->leftJoin('freelances','users.id','=','freelances.user_id')
                    ->where('users.id','=',$id)
                    ->whereRaw('freelances.id IS NOT NULL')
                    ->get();
        
        return $user;
     }

     // update user
     public static function updateUser($request, $id){
        try {
            DB::transaction(function () use($request, $id) {

                User::findOrFail($id)->update([
                'name'     => $request->name,
                'lastName' => $request->lastName,
                'email'    => $request->email,
                'phone'    => $request->phone,
                ]);

                Freelance::where('user_id', '=', $id)->update([
                'direccion' => $request->calleNumero,
                'cp' => $request->cp,
                'colonia' => $request->col,
                'delmpo' => $request->delmpo,
                'edo' => $request->edo,
                'pais' => $request->pais,    
                ]);
      
         });
          return \Response::json(array(
                                    'success' => true,
                                    'data' => 'Actualizado exitosamente'),
                                    200);
        } catch (\PDOException $e) {
             return \Response::json(array(
                                    'error' => false,
                                    'data' => $e),
                                    200);
        }
     }
}
