<?php

namespace App\Helpers;

//use GuzzleHttp\Client;
//use Illuminate\Support\Facades\DB;

class AppHelper
{
    public function randomString($length = 10){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
    }
	
	public function trimString($length = 10, $str){
		$trim = substr($str, 0, 10);
	    return $trim;
    }

     public static function instance()
     {
         return new AppHelper();
     }
}
