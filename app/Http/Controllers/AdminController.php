<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Mail;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function userList(Request $request, $role_type)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);

		$_q = $request->input('q');
		$sueldoDesde = $request->input('sueldoDesde');
		$sueldoHasta = $request->input('sueldoHasta');

		$fechaDesde = $request->input('fromDate');
		$fechaHasta = $request->input('toDate');
		$estatus = $request->input('estatus');

		$idSector = $request->input('idSector');

		$sortBy = 'id';
		if($request->input('sortBy')){
			$sortBy = $request->input('sortBy');
		}
		$sort = 'ASC';
		if($request->input('sort')){
			$sort = $request->input('sort');
		}
		
		$sectores;
		$whereArray;
		$orWhereArrayA;
		$orWhereArrayB;
	    
		$userData = NULL;
		if($role_type == 'Candidato'){
			
			if($_q){
				$whereArray[] = ['name','like','%'.$_q.'%'];
				$orWhereArrayA[] = ['lastName','like','%'.$_q.'%'];
				$orWhereArrayB[] = ['email','like','%'.$_q.'%'];
				
				$users = User::select('*',
						DB::raw("(SELECT candidatosData.folder FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS folder"),
						DB::raw("(SELECT candidatosData.puesto FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS puesto"),
						DB::raw("(SELECT candidatosData.sueldoDesde FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS sueldoDesde"),
						DB::raw("(SELECT candidatosData.sueldoHasta FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS sueldoHasta"),
						DB::raw("(SELECT candidatosData.idSector FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS idSector"),
						DB::raw("(SELECT sectores.nombre FROM sectores WHERE sectores.id IN (SELECT idSector FROM candidatosData WHERE users.id = candidatosData.idUSerCandidato) LIMIT 0,1) AS sector")
					)
					->whereRaw("`id` IN(SELECT `idUserCandidato` FROM `candidatosData` WHERE `candidatosData`.`puesto` LIKE '%".$_q."%')")
					->orWhere($whereArray)
					->orWhere($orWhereArrayA)
					->orWhere($orWhereArrayB)
					->whereHas(
						'roles', function($q) use ($role_type){
							$q->where('name', $role_type);
						}
					)
					->orderBy($sortBy,$sort)
					->paginate(50);
					
					//return $users;
			}else{
				$users = User::select('*',
						DB::raw("(SELECT candidatosData.folder FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS folder"),
						DB::raw("(SELECT candidatosData.puesto FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS puesto"),
						DB::raw("(SELECT candidatosData.sueldoDesde FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS sueldoDesde"),
						DB::raw("(SELECT candidatosData.sueldoHasta FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS sueldoHasta"),
						DB::raw("(SELECT candidatosData.divisa FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS divisa"),
						DB::raw("(SELECT candidatosData.idSector FROM candidatosData WHERE candidatosData.idUserCandidato = users.id) AS idSector"),
						DB::raw("(SELECT sectores.nombre FROM sectores WHERE sectores.id IN (SELECT idSector FROM candidatosData WHERE users.id = candidatosData.idUSerCandidato) LIMIT 0,1) AS sector")
					)
					->whereHas(
						'roles', function($q) use ($role_type){
							$q->where('name', $role_type);
						}
					)
						->orderBy($sortBy,$sort)
						->paginate(50);
			}
			
			foreach($users as $key => $user){

				$uid = $user->id;
				
				/*$folder = DB::table('candidatosData')
					->select('folder', 'puesto', 'sueldoDesde', 'sueldoHasta', 'idSector',
						DB::Raw('(SELECT nombre FROM sectores WHERE sectores.id = candidatosData.idSector) AS sector')
					)
					->where('idUserCandidato','=',$uid)
					->first();*/
					
				if($user->folder == NULL){
					$folderName = \App\Helpers\AppHelper::instance()->randomString();
					$uData = DB::table('candidatosData')->insert([
						'idUserCandidato' => $uid,
						'folder' => $folderName,
					]);
					$user->folder = $folderName;
					/*$folder = DB::table('candidatosData')
						->select('folder', 'puesto', 'sueldoDesde', 'sueldoHasta', 'idSector',
							DB::Raw('(SELECT nombre FROM sectores WHERE sectores.id = candidatosData.idSector) AS sector')
						)
						->where('idUserCandidato','=',$uid)
						->first();*/
				}
				
				//$user->folder = $folder->folder;
				//$user->puesto = $folder->puesto;
				//$user->sueldoDesde = $folder->sueldoDesde;				
				//$user->sueldoHasta = $folder->sueldoHasta;
				//$user->idSector = $folder->idSector;
				//$user->sector = $folder->sector;
				
				//foreach($users as $key =>$user){
					if($idSector != 0 && $idSector != NULL){
						if($user->idSector != $idSector){
							//$user->delete();
							$users->forget($key);
						}
					}
					if($user->sueldoDesde < $sueldoDesde && $sueldoDesde != NULL){
						//$user->delete();
						$users->forget($key);
					}
					if($user->sueldoHasta > $sueldoHasta && $sueldoHasta != NULL){
						//$user->delete();
						$users->forget($key);
					}
				//}
				
				$vacantes = DB::table('vacantes')
					->select('vacantes.id','vacantes.idUserEmpresa','vacantes.titulo','vacantes.estatus','vacantes.created_at',
						DB::raw("(SELECT vacantesRelation.idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idVacante = vacantes.id) as ownerId")
					)
					->join('vacantesCandidatos', function ($join) use ($uid) {
						$join->on('vacantesCandidatos.idVacante', '=', 'vacantes.id')
							->where('vacantesCandidatos.idUserCandidato','=',$uid)
							->where('vacantesCandidatos.added', '=', '1');
					})
					->get();
					
				foreach($vacantes as $key => $vacante){
					$eid = $vacante->idUserEmpresa;
					$empresas = DB::table('users')
						->select('name')
						->where('id', '=', $eid)
						->first();
							
					if($vacante->estatus == 'Abierta'){
						$vacante->empresa = $empresas->name;
					}else{
						$hired = DB::table('vacantesCandidatos')
							->select('hired_at')
							->where([['idVacante','=',$vacante->id],['idUserCandidato','=',$uid]])
							->first();
							
						if($hired->hired_at == NULL){
							$vacantes->forget($key);
						}else{
							$vacante->empresa = $empresas->name;
						}
					}
				}
				
				$bloqueado = DB::table('vacantesCandidatos')
					->select('bloqueado')
					->where([['idUserCandidato','=',$uid],['bloqueado','=','1']])
					->count();
				$colocado = DB::table('vacantesCandidatos')
					->select('colocado')
					->where([['idUserCandidato','=',$uid],['colocado','=','1']])
					->count();
				
				$user->bloqueado = $bloqueado;
				$user->colocado = $colocado;
				$user->vacantes = $vacantes;
				
			}
			//return $users;
			
			$sectores = DB::table('sectores')
				->orderBy('nombre','ASC')
				->get();

			/*if($sort == 'ASC'){
				$users = $users->sortBy($sortBy);
			}else{
				$users = $users->sortByDesc($sortBy);
			}*/
			//return $users;
			return view('admins.users.list.candidatos')
				->with('users',$users)
				->with('sectores',$sectores)
				->with('_q',$_q)
				->with('role_type', $role_type)
				->with('sueldoDesde', $sueldoDesde)
				->with('sueldoHasta', $sueldoHasta)
				->with('idSector', $idSector)
				->with('sortBy',$sortBy)
				->with('sort',$sort);
		}
		if($role_type == 'Empresa'){
			
			if($_q){
				$whereArray[] = ['name','like','%'.$_q.'%'];
				$orWhereArrayA[] = ['lastName','like','%'.$_q.'%'];
				$orWhereArrayB[] = ['email','like','%'.$_q.'%'];
				
				$users = User::where($whereArray)
					->orWhere($orWhereArrayA)
					->orWhere($orWhereArrayB)
					->whereHas(
						'roles', function($q) use ($role_type){
							$q->where('name', $role_type);
						}
					)
					->get();
				
				foreach($users as $key =>$user){
					if(!$user->hasRole('Empresa')){
						$users->forget($key);
					}
				}
				
			}else{
				$users = User::whereHas(
					'roles', function($q) use ($role_type){
						$q->where('name', $role_type);
					}
				)
				->get();
			}

			foreach($users as $user){
				$uid = $user->id;
				
				$folder = DB::table('empresasData')
					->select('folder','idSector',
						DB::Raw('(SELECT nombre FROM sectores WHERE sectores.id = empresasData.idSector) AS sector')
					)
					->where('idUserEmpresa','=',$uid)
					->first();

				$user->folder = $folder->folder;
				$user->idSector = $folder->idSector;
				$user->sector = $folder->sector;
				
				$vacantes = DB::table('vacantes')
					->select('vacantes.id','vacantes.idUserEmpresa','vacantes.titulo','vacantes.estatus','vacantes.created_at',
						DB::raw("(SELECT vacantesRelation.idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idVacante = vacantes.id) as ownerId")
					)
					->where('idUserEmpresa','=',$uid)
					->get();
				
				$user->vacantes = $vacantes;
			}
			
			$sectores = DB::table('sectores')
				->orderBy('nombre','ASC')
				->get();

			if($sort == 'ASC'){
				$users = $users->sortBy($sortBy);
			}else{
				$users = $users->sortByDesc($sortBy);
			}
			
			return view('admins.users.list.empresas')
				->with('users',$users)
				->with('role_type', $role_type)
				->with('sectores', $sectores)
				->with('idSector', $idSector)
				->with('sortBy',$sortBy)
				->with('sort',$sort);
		}
		if($role_type == 'Reclutador'){
			
			$users = User::whereHas(
				'roles', function($q) use ($role_type){
					$q->where('name', $role_type);
				}
			)
			->get();
			
			foreach($users as $user){
				$uid = $user->id;
				
				$folder = DB::table('reclutadoresData')
					->select('folder')
					->where('idUserReclutador','=',$uid)
					->first();
				$user->folder = $folder->folder;
				
				$vacantesWhere = array(
					array('vacantesRelation.idOwnerReclutador','=',$uid)
				);
				if($fechaDesde){
					array_push($vacantesWhere, array('created_at','>=',$fechaDesde));
				}
				if($fechaHasta){
					array_push($vacantesWhere, array('created_at','<=',$fechaHasta));
				}
				if($estatus && $estatus != 'all'){
					array_push($vacantesWhere, array('estatus','=',$estatus));
				}
				//return $vacantesWhere;
				$vacantes = DB::table('vacantes')
					->select('id','titulo','estatus','created_at','tarifa')
					->join('vacantesRelation', function ($join) use ($vacantesWhere) {
						/*$join->on('vacantesRelation.idVacante', '=', 'vacantes.id')
							->where('vacantesRelation.idOwnerReclutador','=',$uid);*/
						$join->on('vacantesRelation.idVacante', '=', 'vacantes.id')
							->where($vacantesWhere);
					})
					->get();
				
				$user->bloqueado = NULL;
				$user->colocado = NULL;
				$user->vacantes = $vacantes;
			}
			//return $users;
			return view('admins.users.list.reclutadores')
				->with('users',$users)
				->with('role_type', $role_type);
		}
		if($role_type == 'Admin' || $role_type == 'Vendedor'){
			return redirect('dashboard');
		}
		
        
    }
    public function userProfile(Request $request, $id)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Candidato', 'Empresa', 'Vendedor']);

		if($request->user()->hasRole('Candidato') || $request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $id){
				return 'Acción no permitida.';
			}
		}
		$user = User::where('id','=',$id)
			->first();
		
		$user_data = NULL;
		$reclutador = NULL;
		$vacantes = NULL;

		if($user->hasRole('Reclutador')){
			$user_data = DB::table('reclutadoresData')
			  ->where('idUserReclutador', '=', $id)
			  ->get();
		
			return view('admins.users.profiles.reclutador')
				->with('user',$user)
				->with('user_data', $user_data)
				->with('reclutador', $reclutador)
				->with('vacantes', $vacantes);
		}
		
		if($user->hasRole('Candidato')){
			$user_data = DB::table('candidatosData')
				->select("candidatosData.*",
					DB::raw("(SELECT candidatosCompensaciones.completed FROM candidatosCompensaciones WHERE candidatosCompensaciones.idUserCandidato = $id) AS compensaciones"),
					DB::raw("(SELECT sectores.nombre FROM sectores WHERE sectores.id = candidatosData.idSector) as sector")
				)
				->where('idUserCandidato', '=', $id)
				->get();

			if(!$user_data){
				$folder = \App\Helpers\AppHelper::instance()->randomString();
				$uData = DB::table('candidatosData')->insert([
					'idUserCandidato' => $id,
					'folder' => $folder,
				]);
				$user_data = DB::table('candidatosData')
					->select("candidatosData.*",
						DB::raw("(SELECT candidatosCompensaciones.completed FROM candidatosCompensaciones WHERE candidatosCompensaciones.idUserCandidato = $id) AS compensaciones"),
						DB::raw("(SELECT sectores.nombre FROM sectores WHERE sectores.id = candidatosData.idSector) as sector")
					)
					->where('idUserCandidato', '=', $id)
					->get();
			}
			
			$idReclutador = DB::table('usersRelations')
				->select('idOwnerReclutador')
				->where('idUserCandidato', '=', $id)
				->first();
				
			if($idReclutador == NULL){
				$userOwner = DB::table('usersRelations')->insert([
					'idUserCandidato' => $user->id,
					'idOwnerReclutador' => Auth::user()->id,
				]); 
				
				$idReclutador = DB::table('usersRelations')
					->select('idOwnerReclutador')
					->where('idUserCandidato', '=', $id)
					->first();
			}
				
			$reclutador = DB::table('users')
				->select('name','lastName','email')
				->where('id','=',$idReclutador->idOwnerReclutador)
				->first();
			
			$vacantes = DB::table('vacantes')
				->select('id','idUserEmpresa','titulo', 'confidencial')
				->join('vacantesCandidatos', function ($join) use ($id) {
					$join->on('vacantesCandidatos.idVacante', '=', 'vacantes.id')
						->where('vacantesCandidatos.idUserCandidato','=',$id)
						->where('vacantesCandidatos.added', '=', '1');
				})
				->get();
				
			foreach($vacantes as $vacante){
				$eid = $vacante->idUserEmpresa;
				$empresas = DB::table('users')
					->select('name')
					->where('id', '=', $eid)
					->first();
				$vacante->empresa = $empresas->name;
				
				$vid = $vacante->id;
				$secFits = DB::table('candidatosSecFits')
					->select('completed')
					->where([['idUserCandidato', '=', $id],['idVacante', '=', $vid],['completed', '=', '1']])
					->count();
				$vacante->secFits = $secFits;
				
				$estatus = DB::table('vacantesCandidatos')
					->select('bloqueado','colocado')
					->where([['idUserCandidato', '=', $id],['idVacante', '=', $vid]])
					->first();
				$vacante->bloqueado = $estatus->bloqueado;
				$vacante->colocado = $estatus->colocado;
					
			}
			
			if($request->user()->hasRole('Candidato')){
				if($user_data[0]->profilePic == NULL){
					return redirect('/dashboard/users/edit/'.Auth::user()->id);
				}else{
					return view('users.profiles.candidato')
						->with('user',$user)
						->with('user_data', $user_data)
						->with('reclutador', $reclutador)
						->with('vacantes', $vacantes);
				}
			}else{
				return view('admins.users.profiles.candidato')
					->with('user',$user)
					->with('user_data', $user_data)
					->with('reclutador', $reclutador)
					->with('vacantes', $vacantes);
			}
		}
		
		if($user->hasRole('Empresa')){
			$user_data = DB::table('empresasData')
				->select('empresasData.*',
					DB::raw("(SELECT sectores.nombre FROM sectores WHERE sectores.id = empresasData.idSector) as sector")
				)
				->where('idUserEmpresa', '=', $id)
				->get();
			 
			 $vacantes = DB::table('vacantes')
				->select('id','estatus','created_at','titulo',
					DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluName"),
					DB::raw("(SELECT users.lastName FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluLastName"),
					DB::raw("(SELECT users.email FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluEmail"),
					DB::raw("(SELECT users.phone FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluPhone"),
					DB::raw("(SELECT vacantesRelation.idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idVacante = vacantes.id) as ownerId")
				)
				->where('idUserEmpresa','=',$id)
				->get();
			
			foreach($vacantes as $keyVac => $vacante){
				$vid = $vacante->id;
				$candidatos = DB::select(
					DB::raw("SELECT users.id, users.name, users.lastName, users.email FROM users WHERE id IN (SELECT idUserCandidato FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $vid AND added = '1')")
				);
				
				/*foreach($candidatos as $keyCand => $candidato){
					$candidatoData = DB::table('candidatosData')
						->select('cv','video','ltr')
						->where('idUserCandidato','=',$candidato->id)
						->first();
					
					$candidatoRefs = DB::table('candidatosReferencias')
						->where([['idUserCandidato','=',$candidato->id]])
						->count();
						
					$comp = 0;
					$compensaciones = DB::table('candidatosCompensaciones')
						->select('completed')
						->where('idUserCandidato','=', $candidato->id)
						->first();
					
					if(count($compensaciones) == 0){
						$comp = 0;
					}else{
						$comp = $compensaciones->completed;
					}
					
					$secFit = 0;
					$secFits = DB::table('candidatosSecFits')
						->select('completed')
						->where([['idUserCandidato','=', $candidato->id],['idVacante','=', $vid]])
						->first();
					
					if(count($secFits) == 0){
						$secFit = 0;
					}else{
						$secFit = $secFits->completed;
					}
					
					if($candidatoData->cv == NULL || $secFit == 0 || $candidatoData->ltr == NULL || $comp == 0 || $candidatoData->video == NULL || $candidatoRefs == 0){
						$candidato->completed = 0;
					}else{
						$candidato->completed = 1;
					}
				}*/

				$vacante->candidatos = $candidatos;
			}
			
			if($request->user()->hasRole('Empresa')){
				return view('users.profiles.empresa')
					->with('user',$user)
					->with('user_data', $user_data)
					->with('reclutador', $reclutador)
					->with('vacantes', $vacantes);
			}else{
				return view('admins.users.profiles.empresa')
					->with('user',$user)
					->with('user_data', $user_data)
					->with('reclutador', $reclutador)
					->with('vacantes', $vacantes);
			}
		}
	
		/*return view('admins.users.userProfile')
			->with('user',$user)
			->with('user_data', $user_data)
			->with('reclutador', $reclutador)
			->with('vacantes', $vacantes);*/
    }
    // USER EDIT
    public function showEditForm(Request $request, $id)
    {
        $request->user()->authorizeRoles(['SuperAdmin','Admin','Reclutador','Candidato','Empresa', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato') || $request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $id){
				return 'Acción no permitida.';
			}
		}
		
        $user = User::where('id','=',$id)
            ->first();
        
        $user_data = NULL;
		$reclutadores = NULL;
		$idReclutador = NULL;
		$sectores = NULL;

        if($user->hasRole('Reclutador')){
            $user_data = DB::table('reclutadoresData')
              ->where('idUserReclutador', '=', $id)
              ->first();
        }
        if($user->hasRole('Candidato')){
            $user_data = DB::table('candidatosData')
				->select("candidatosData.*",
					DB::Raw('(SELECT id FROM sectores WHERE sectores.id = candidatosData.idSector) AS idSector')
				)
				->where('idUserCandidato', '=', $id)
				->first();
			
			if(!$user_data){
				$folder = \App\Helpers\AppHelper::instance()->randomString();
				$uData = DB::table('candidatosData')->insert([
					'idUserCandidato' => $id,
					'folder' => $folder,
				]);
				$user_data = DB::table('candidatosData')
					->select("candidatosData.*",
						DB::Raw('(SELECT id FROM sectores WHERE sectores.id = candidatosData.idSector) AS idSector')
					)
					->where('idUserCandidato', '=', $id)
					->first();
			}
			
			
			$role_reclutador = 'Reclutador';
			$reclutadores = User::whereHas(
				'roles', function($q) use ($role_reclutador){
					$q->where('name', $role_reclutador);
				}
			)
			->select('id','name', 'lastName')
			->orderBy('name', 'ASC')
			->get();
			
			$idReclutador = DB::table('usersRelations')
				->select('idOwnerReclutador')
              	->where('idUserCandidato', '=', $id)
              	->first();
			
			$sectores = DB::table('sectores')
				->orderBy('nombre','ASC')
				->get();
			
        }
        if($user->hasRole('Empresa')){
            $user_data = DB::table('empresasData')
				->where('idUserEmpresa', '=', $id)
				->first();
			
			$sectores = DB::table('sectores')
				->orderBy('nombre','ASC')
				->get();
        }
        //print_r( $user_data );
        return view('admins.users.userEdit')
            ->with('user',$user)
            ->with('user_data', $user_data)
			->with('sectores', $sectores)
			->with('reclutadores', $reclutadores)
			->with('idReclutador', $idReclutador);
    }
    
    protected function update(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Candidato', 'Empresa', 'Vendedor']);
		$data = request()->all();
		
		if($request->user()->hasRole('Candidato') || $request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $data['id']){
				return 'Acción no permitida.';
			}
		}
        
        $userRole = $data['role_type'];

		if($data['updatePwdRadio'] == '1'){
			$pwd = Hash::make($data['password_repeat']);
			DB::table('users')
				->where('id', $data['id'])
				->update(
				[
					'name' => $data['name'],
					'lastName' => $data['lastName'],
					'email' => $data['email'],
					'phone' => $data['phone'],
					'password' => $pwd
				]
			);
		}else{
			DB::table('users')
				->where('id', $data['id'])
				->update(
				[
					'name' => $data['name'],
					'lastName' => $data['lastName'],
					'email' => $data['email'],
					'phone' => $data['phone']
				]
			);
		}
		
		if($userRole != 'Admin' && $userRole != 'Vendedor'){
			// PROFILE PIC
			$folder = $data['folder'];
			if($folder == NULL) {
				$folder = \App\Helpers\AppHelper::instance()->randomString();
			}
			
			$imageName = NULL;
			if( array_key_exists('profilePicOld', $data) ){
				$imageName = $data['profilePicOld'];
			}
	
			if( array_key_exists('profilePic', $data) ){
	
				$ramdomStr = \App\Helpers\AppHelper::instance()->randomString();
		
				$imageName = time() . '-' . $ramdomStr . '.' . $data['profilePic']->getClientOriginalExtension();
				
				$data['profilePic']->move(storage_path('app/public/'.$folder), $imageName);
			}
		}

        if($userRole == 'Reclutador'){
            DB::table('reclutadoresData')
                ->where('idUserReclutador', $data['id'])
                ->update(
                [
                    'profilePic' => $imageName,
                    'calleNumero' => $data['calleNumero'],
                    'cp' => $data['cp'],
                    'col' => $data['col'],
                    'delmpo' => $data['delmpo'],
                    'edo' => $data['edo'],
                    'pais' => $data['pais'],
					'folder' => $folder,
                ]
            );
			// SEND EMAIL
			$mailData = array(
				'mailto' => $data['email'],
				'subject' => 'Levu Talent - Datos actualizados',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster'
			);
	
			Mail::send('emails.reclutadores.profileUpdate', [
					'candidatoName' => $data['name'].' '.$data['lastName'],
				], function ($message) use ($mailData){
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->to($mailData['mailto']);
					$message->subject($mailData['subject']);
					$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
				}
			);
        }
        if($userRole == 'Candidato'){
            DB::table('candidatosData')
                ->where('idUserCandidato', $data['id'])
                ->update(
                [
					'idSector' => $data['idSector'],
                    'profilePic' => $imageName,
                    'puesto' => $data['puesto'],
                    'escolaridad' => $data['escolaridad'],
                    'sueldoDesde' => $data['sueldoDesde'],
					'sueldoHasta' => $data['sueldoHasta'],
					'divisa' => $data['divisa'],
                    'calleNumero' => $data['calleNumero'],
                    'cp' => $data['cp'],
                    'col' => $data['col'],
                    'delmpo' => $data['delmpo'],
                    'edo' => $data['edo'],
                    'pais' => $data['pais'],
					'folder' => $folder,
                ]
            );
			DB::table('usersRelations')
				->where('idUserCandidato', $data['id'])
				->update(
				[
					'idOwnerReclutador' => $data['idOwnerReclutador'],
				]
			);
			
			// SEND EMAIL
			$mailData = array(
				'mailto' => $data['email'],
				'subject' => 'Levu Talent - Datos actualizados',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster'
			);
	
			Mail::send('emails.candidatos.profileUpdate', [
					'candidatoName' => $data['name'].' '.$data['lastName'],
				], function ($message) use ($mailData){
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->to($mailData['mailto']);
					$message->subject($mailData['subject']);
					$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
				}
			);
		}
        if($userRole == 'Empresa'){

        	$contractName = NULL;
			if( array_key_exists('contractOld', $data) ){
				$contractName = $data['contractOld'];
			}
	
			if( array_key_exists('contract', $data) ){
	
				$ramdomStr = \App\Helpers\AppHelper::instance()->randomString();
		
				$contractName = time() . '-' . $ramdomStr . '.' . $data['contract']->getClientOriginalExtension();
				
				$data['contract']->move(storage_path('app/public/'.$folder), $contractName);
			}

            DB::table('empresasData')
                ->where('idUserEmpresa', $data['id'])
                ->update(
                [
					'idSector' => $data['idSector'],
                    'profilePic' => $imageName,
                    'contract' => $contractName,
                    'contactoName' => $data['contactoName'],
                    'contactoEmail' => $data['contactoEmail'],
                    'contactoPuesto' => $data['contactoPuesto'],
                    'contactoPhone' => $data['contactoPhone'],
                    
                    'calleNumero' => $data['calleNumero'],
                    'cp' => $data['cp'],
                    'col' => $data['col'],
                    'delmpo' => $data['delmpo'],
                    'edo' => $data['edo'],
                    'pais' => $data['pais'],
					'folder' => $folder,
                ]
            );
			
			// SEND EMAIL
			$mailData = array(
				'mailto' => $data['email'],
				'subject' => 'Levu Talent - Datos actualizados',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster',
				'cc' => $data['contactoEmail']
			);
	
			Mail::send('emails.empresas.profileUpdate', [
					'empresaEmail' => $data['email'],
					'empresaName' => $data['name'],
					'contactoName' => $data['contactoName'],
					'contactoEmail' => $data['contactoEmail']
				], function ($message) use ($mailData){
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->to($mailData['mailto']);
					$message->subject($mailData['subject']);
					$message->cc($mailData['cc']);
					$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
				}
			);
        }
		if($request->user()->hasRole('Candidato') || $request->user()->hasRole('Empresa')){
        	return redirect('/dashboard/users/profile/'.$data['id']);
		}else{
			return redirect('dashboard/users/'.$userRole);
		}
    }
	// USER DELETE
    protected function userDelete(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();

		if($data['role'] == 'Admin'){
			DB::table('users')
				->where('id', '=', $data['id'])
				->where('email', '=', $data['email'])
				->delete();
		}else if($data['role'] == 'Reclutador'){
			DB::table('vacantesRelation')
				->where('idOwnerReclutador', $data['id'])
				->update(
				[
					'idOwnerReclutador' => $data['id_newReclutador']
				]
			);
			
			DB::table('usersRelations')
				->where('idOwnerReclutador', $data['id'])
				->update(
				[
					'idOwnerReclutador' => $data['id_newReclutador']
				]
			);
				
			DB::table('reclutadoresData')
				->where('idUserReclutador', '=', $data['id'])
				->delete();
				
			DB::table('users')
				->where('id', '=', $data['id'])
				->where('email', '=', $data['email'])
				->delete();
		}else if($data['role'] == 'Empresa'){
				
			// OBTENER PRIMERO TODAS LAS VACANTES DE LA EMPRESA A ELIMINAR
			$vacantes = DB::table('vacantes')
				->select('id')
				->where('idUserEmpresa', '=', $data['id'])
				->get();
			
			foreach($vacantes as $vacante){
				// ELIMINAR TODAS LOS CANDIADTOS ASIGNADOS A LAS VACANTES DE LA EMPRESA
				DB::table('vacantesCandidatos')
					->where('idVacante', '=', $vacante->id)
					->delete();
				// ELIMINAR TODAS LAS VACANTES DE LOS RECLUTADORES
				DB::table('vacantesRelation')
					->where('idVacante', '=', $vacante->id)
					->delete();
			}
			// ELMINAR TODAS LAS VACANTES DE LA EMPRESA
			DB::table('vacantes')
				->where('idUserEmpresa', '=', $data['id'])
				->delete();
			// ELMINAR DATOS DE LA EMPRESA
			DB::table('empresasData')
				->where('idUserEmpresa', '=', $data['id'])
				->delete();
			// ELIMINAR EMPRESA
			DB::table('users')
				->where('id', '=', $data['id'])
				->where('email', '=', $data['email'])
				->delete();
					
		}else if($data['role'] == 'Candidato'){
			DB::table('usersRelations')
				->where('idUserCandidato', '=', $data['id'])
				->delete();
				
			DB::table('vacantesCandidatos')
				->where('idUserCandidato', '=', $data['id'])
				->delete();
			
			DB::table('users')
				->where('id', '=', $data['id'])
				->where('email', '=', $data['email'])
				->delete();
		}
		$folder = $data['folder'];
		if($folder != NULL){
			Storage::deleteDirectory(storage_path('app/public/'.$folder));
		}
        return redirect($data['_returnPath']);
    }
    // USER REGISTRATION
    public function showRegistrationForm(Request $request, $role_type)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		
		if($role_type == 'SuperAdmin'){
			return 'Acción no permitida.';
		}
		
		if($request->user()->hasRole('Reclutador')){
			if($role_type == 'Empresa' || $role_type == 'Admin' || $role_type == 'Reclutador'){
				return 'Acción no permitida.';
			}
		}
		
		$reclutadores = NULL;
		$sectores = NULL;
		
		if($role_type == 'Candidato'){
			$role_reclutador = 'Reclutador';
			$reclutadores = User::whereHas(
				'roles', function($q) use ($role_reclutador){
					$q->where('name', $role_reclutador);
				}
			)
			->select('id','name', 'lastName')
			->orderBy('name', 'ASC')
			->get();
			
			$sectores = DB::table('sectores')
				->where('active','=','1')
				->orderBy('slug','ASC')
				->get();
		}
		
		if($role_type == 'Empresa'){
			$sectores = DB::table('sectores')
				->where('active','=','1')
				->orderBy('slug','ASC')
				->get();
		}
			
        return view('admins.users.userRegister')
            ->with('role_type', $role_type)
			->with('reclutadores', $reclutadores)
			->with('sectores', $sectores);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();
		$userRole = $data['role'];
		
		if($request->user()->hasRole('Reclutador')){
			if($userRole != 'Candidato'){
				return 'Acción no permitida';
			}
		}
		$pwd = \App\Helpers\AppHelper::instance()->randomString(15);
		try{
			$user = User::create([
				'name' => $data['name'],
				'lastName' => $data['lastName'],
				'email' => $data['email'],
				'phone' => $data['phone'],
				'password' => Hash::make($pwd),
			]);
			$userId = $user->id;
			// CÓDIGO PARA ASIGNAR UN ROL A UN NUEVO USUARIO
			
			$user
				->roles()
				->attach(Role::where('name', $userRole)->first());
	
			// REVISAR QUE TIPO DE USUARIO ES PARA METER LOS DATOS A LA TABLA CORRESPONDIENTE
			$imageName = NULL;
			$contractName = NULL;
			$folder = \App\Helpers\AppHelper::instance()->randomString();
			if( array_key_exists('profilePic', $data) ){
	
				$ramdomStr = \App\Helpers\AppHelper::instance()->randomString();
		
				$imageName = time() . '-' . $ramdomStr . '.' . $data['profilePic']->getClientOriginalExtension();
				
				$data['profilePic']->move(storage_path('app/public/'.$folder), $imageName);
			}
	
			if($userRole == 'Reclutador'){
				$userData = DB::table('reclutadoresData')->insert([
					'idUserReclutador' => $user->id,
					'profilePic' => $imageName,
					'calleNumero' => $data['calleNumero'],
					'cp' => $data['cp'],
					'col' => $data['col'],
					'delmpo' => $data['delmpo'],
					'edo' => $data['edo'],
					'pais' => $data['pais'],
					'folder' => $folder,
				]);  
				
				// SEND WELCOME EMAIL
				$mailData = array(
					'mailto' => $data['email'],
					'subject' => 'Levu Talent - Bienvenido',
					'from' => 'no-reply@levutalent.com', 
					'from_name' => 'Levu Webmaster'
				);
		
				Mail::send('emails.reclutadores.bienvenido', [
						'recluEmail' => $data['email'],
						'recluName' => $data['name'].' '.$data['lastName'],
						'recluPwd' => $pwd,
					], function ($message) use ($mailData){
						$message->from( $mailData['from'], $mailData['from_name'] );
						$message->to($mailData['mailto']);
						$message->subject($mailData['subject']);
						$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
					}
				);
			}
			if($userRole == 'Candidato'){
	
				$userData = DB::table('candidatosData')->insert([
					'idUserCandidato' => $userId,
					'idSector' => $data['idSector'],
					'profilePic' => $imageName,
					
					'puesto' => $data['puesto'],
					'escolaridad' => $data['escolaridad'],
					'sueldoDesde' => $data['sueldoDesde'],
					'sueldoHasta' => $data['sueldoHasta'],
					'divisa' => $data['divisa'],
	
					'calleNumero' => $data['calleNumero'],
					'cp' => $data['cp'],
					'col' => $data['col'],
					'delmpo' => $data['delmpo'],
					'edo' => $data['edo'],
					'pais' => $data['pais'],
	
					'folder' => $folder,
				]);
				
				$userOwner = DB::table('usersRelations')->insert([
					'idUserCandidato' => $userId,
					'idOwnerReclutador' => $data['idOwnerReclutador'],
				]);  
				
				// SEND WELCOME EMAIL
				$mailData = array(
					'mailto' => $data['email'],
					'subject' => 'Levu Talent - Bienvenido',
					'from' => 'no-reply@levutalent.com', 
					'from_name' => 'Levu Webmaster',
				);

				Mail::send('emails.candidatos.bienvenido', [
						'candidatoName' => $data['name'].' '.$data['lastName'],
						'candidatoEmail' => $data['email'],
						'candidatoPwd' => $pwd,
					], function ($message) use ($mailData){
						$message->from( $mailData['from'], $mailData['from_name'] );
						$message->to($mailData['mailto']);
						$message->subject($mailData['subject']);
						$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
						$message->attach(public_path('pdf/LEVU_manual.pdf'));
					}
				);
			}
			if($userRole == 'Empresa'){
				if( array_key_exists('contract', $data) ){
	
					$ramdomStr = \App\Helpers\AppHelper::instance()->randomString();
			
					$contractName = time() . '-' . $ramdomStr . '.' . $data['contract']->getClientOriginalExtension();
					
					$data['contract']->move(storage_path('app/public/'.$folder), $contractName);
				}

				$userData = DB::table('empresasData')->insert([
					'idUserEmpresa' => $user->id,
					'idSector' => $data['idSector'],
					'profilePic' => $imageName,
					'contract' => $contractName,
					
					'contactoName' => $data['contactoName'],
					'contactoEmail' => $data['contactoEmail'],
					'contactoPuesto' => $data['contactoPuesto'],
					'contactoPhone' => $data['contactoPhone'],
	
					'calleNumero' => $data['calleNumero'],
					'cp' => $data['cp'],
					'col' => $data['col'],
					'delmpo' => $data['delmpo'],
					'edo' => $data['edo'],
					'pais' => $data['pais'],
					
					'folder' => $folder,
				]);   
				
				// SEND WELCOME EMAIL
				$mailData = array(
					'mailto' => Auth::user()->email,
					'subject' => 'Levu Talent - Nueva empresa',
					'from' => 'no-reply@levutalent.com', 
					'from_name' => 'Levu Webmaster',
					'cc' => 'igarza@levutalent.com'
				);
		
				Mail::send('emails.empresas.bienvenido', [
						'empresaEmail' => $data['email'],
						'empresaName' => $data['name'],
						'empresaPwd' => $pwd,
						'contactoName' => Auth::user()->name,
						'contactoEmail' => $data['contactoEmail']
					], function ($message) use ($mailData){
						$message->from( $mailData['from'], $mailData['from_name'] );
						$message->to($mailData['mailto']);
						$message->subject($mailData['subject']);
						$message->cc($mailData['cc']);
						$message->bcc('icarrillo@levutalent.com');
					}
				);
			}
			
			if($userRole == 'Admin'){
				// SEND WELCOME EMAIL
				$mailData = array(
					'mailto' => $data['email'],
					'subject' => 'Levu Talent - Bienvenido',
					'from' => 'no-reply@levutalent.com', 
					'from_name' => 'Levu Webmaster',
				);
		
				Mail::send('emails.candidatos.bienvenido', [
						'candidatoName' => $data['name'].' '.$data['lastName'],
						'candidatoEmail' => $data['email'],
						'candidatoPwd' => $pwd,
					], function ($message) use ($mailData){
						$message->from( $mailData['from'], $mailData['from_name'] );
						$message->to($mailData['mailto']);
						$message->subject($mailData['subject']);
						$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
					}
				);
				return redirect('dashboard');
			}
			
			return redirect('dashboard/users/'.$userRole );
			
		} catch(\Illuminate\Database\QueryException $e){
            $errorCode = $e->errorInfo[1];
			//dd($e);
            if($errorCode == '1062'){
                //dd('Duplicate Entry');
				$url = '';
				foreach($data as $key=>$getParam){
					if($key != '_token' && $key != 'password'){
						$url .= '&'.$key.'='.$getParam;
					}
				}
				return redirect('/dashboard/users/registration/'.$data['role'].'?exist'.$url);
            }
			
        }
    }
	
	// USER COMPENSACIONES
	public function showCompensaciones(Request $request, $id)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato', 'Empresa','Reclutador', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $id){
				return 'Acción no permitida.';
			}
		}
		
		$user = User::where('id','=',$id)
			->select('id','name','lastName')
            ->first();
			
		$compensaciones = DB::table('candidatosCompensaciones')
            ->where('idUserCandidato','=', $id)
            ->first();
			
        return view('admins.users.userCompensaciones')
            ->with('user',$user)
            ->with('compensaciones', $compensaciones);
    }
	
	public function editCompensaciones(Request $request, $id)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $id){
				return 'Acción no permitida.';
			}
		}
		
		$user = User::where('id','=',$id)
			->select('id','name','lastName')
            ->first();
		
		$compensaciones = DB::table('candidatosCompensaciones')
            ->where('idUserCandidato','=', $id)
            ->first();
		
		if(empty($compensaciones)){
			$lastId = DB::table('candidatosCompensaciones')->insertGetId([
				'idUserCandidato' => $id,
			]);
			$compensaciones = DB::table('candidatosCompensaciones')
				->where('idUserCandidato','=', $id)
				->first();
		}
			
        return view('admins.users.userCompensacionesEdit')
			->with('user',$user)
            ->with('compensaciones', $compensaciones);
    }
	public function updateCompensaciones(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
		$data = request()->all();
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $data['id']){
				return 'Acción no permitida.';
			}
		}
		
		DB::table('candidatosCompensaciones')
            ->where('idUserCandidato', $data['id'])
            ->update(
            [
                'empresaActual' => $data['empresaActual'],
                'antiguedad' => $data['antiguedad'],
                'salarioBruto' => json_encode($data['salarioBruto']),
				'comisiones' => json_encode($data['comisiones']),
				'bonoMensualPorObjetivos' => json_encode($data['bonoMensualPorObjetivos']),
				'bonoAnualPorObjetivos' => json_encode($data['bonoAnualPorObjetivos']),
				
				'aguinaldo' => json_encode($data['aguinaldo']),
				'primaVacacional' => json_encode($data['primaVacacional']),
				'fondoAhorro' => json_encode($data['fondoAhorro']),
				'fondoRetiro' => json_encode($data['fondoRetiro']),
				'valesDespensa' => json_encode($data['valesDespensa']),
				'valesRestaurante' => json_encode($data['valesRestaurante']),
				'valesGasolina' => json_encode($data['valesGasolina']),
				'ptu' => json_encode($data['ptu']),
				'otro' => json_encode($data['otro']),
				'total' => json_encode($data['total']),
				
				'diasVacaciones' => $data['diasVacaciones'],
				'automovil' => json_encode($data['automovil']),
				'gastosAutomovil' => json_encode($data['gastosAutomovil']),
				'seguroVida' => json_encode($data['seguroVida']),
				'gastosMedMenores' => json_encode($data['gastosMedMenores']),
				'gastoMedMayores' => json_encode($data['gastoMedMayores']),
				'vivienda' => json_encode($data['vivienda']),
				'apoyosEducativos' => json_encode($data['apoyosEducativos']),
				
				'stockNumeroAcciones' => json_encode($data['stockNumeroAcciones']),
				//'stockValorPresente' => json_encode($data['stockValorPresente']),
				'otrasPrestaciones' => $data['otrasPrestaciones'],
				'completed' => $data['completed'],
				'divisa' => $data['divisa']
            ]
        );
		return redirect($data['_returnPath']);
    }
	
	// USER REFERENCIAS
	public function showReferencias(Request $request, $uid)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato', 'Empresa','Reclutador', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $uid){
				return 'Acción no permitida.';
			}
		}

		$userData = DB::table('users')
			->select('name','lastName')
			->where('id','=',$uid)
			->first();
		
		/*$vacanteData = DB::table('vacantes')
			->select('vacantes.*',
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid)) AS cliente"),
				DB::raw("(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid) AS idUserEmpresa")
			)
			->where('id','=',$vid)
			->first();
		
		if($request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $vacanteData->idUserEmpresa){
				return 'Acción no permitida.';
			}
		}*/
			
        $referencias = DB::table('candidatosReferencias')
            ->where('idUserCandidato','=', $uid)
			->get();

        return view('admins.users.referencias')
            ->with('referencias', $referencias)
			->with('userData', $userData)
			->with('idUserCandidato', $uid);
    }
	public function newReferencias(Request $request, $uid)
    {
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $uid){
				return 'Acción no permitida.';
			}
		}
		
		/*$userData = User::with('roles')
			->where('id','=',$uid)
            ->first();*/
		$userData = DB::table('users')
			->select('name', 'lastName')
            ->where('id','=', $uid)
			->first();

		/*$vacanteData = DB::table('vacantes')
			->select("vacantes.*",
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid)) AS cliente")
			)
            ->where('id','=', $vid)
			->first();*/
		
		$preguntas = DB::table('referenciasPreguntas')
			->select('pregunta')
			->get();

		return view('admins.users.referenciasNew')
			->with('userData', $userData)
			//->with('vacanteData', $vacanteData)
			->with('idUserCandidato', $uid)
			->with('preguntas', $preguntas);
			//->with('idVacante', $vid);
    }
	public function createReferencias(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
		$data = request()->all();
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $data['idUserCandidato']){
				return 'Acción no permitida.';
			}
		}
		$fechaEntrevista = NULL;
		if(in_array('fechaEntrevista',$data)){
			$fechaEntrevista = $data['fechaEntrevista'];
		}
		
		DB::table('candidatosReferencias')->insert([
			'idUserCandidato' => $data['idUserCandidato'],
			//'idVacante' => $data['idVacante'],
			'entrevistaA' => $data['entrevistaA'],
			'puesto' => $data['puesto'],
			'empresa' => $data['empresa'],
			'telefono' => $data['telefono'],
			'correo' => $data['correo'],
			'fechaEntrevista' => $fechaEntrevista,
			'respuestas' => json_encode($data['respuestas']),
			'completed' => '0',
		]);
		
        return redirect($data['_returnPath']);
    }
    
	public function editReferencias(Request $request, $rid, $uid)
    {
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $uid){
				return 'Acción no permitida.';
			}
		}
		
        $referencias = DB::table('candidatosReferencias')
			->select("candidatosReferencias.*",
				DB::raw("(SELECT users.name FROM users WHERE users.id = $uid) AS candidatoName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id = $uid) AS candidatoLastName")
			)
            ->where([['id','=', $rid],['idUserCandidato','=', $uid]])
			->first();
			
        return view('admins.users.referenciasEdit')
            ->with('referencias', $referencias)
			->with('idUserCandidato', $uid);
    }
	public function updateReferencias(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
        $data = request()->all();
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $data['idUserCandidato']){
				return 'Acción no permitida.';
			}
		}
		
        DB::table('candidatosReferencias')
            ->where([
				['id', $data['id']],
				['idUserCandidato',$data['idUserCandidato']]
			])
            ->update(
            [
                'entrevistaA' => $data['entrevistaA'],
                'puesto' => $data['puesto'],
                'empresa' => $data['empresa'],
                'telefono' => $data['telefono'],
                'correo' => $data['correo'],
                'fechaEntrevista' => $data['fechaEntrevista'],
                'respuestas' => json_encode($data['respuestas']),
				'completed' => $data['completed'],
            ]
        );
		
        return redirect($data['_returnPath']);
    }
	public function viewReferencias(Request $request, $rid, $uid)
    {
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato', 'Empresa','Reclutador', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $uid){
				return 'Acción no permitida.';
			}
		}
		
        $referencias = DB::table('candidatosReferencias')
			->select("candidatosReferencias.*",
				DB::raw("(SELECT users.name FROM users WHERE users.id = $uid) AS candidatoName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id = $uid) AS candidatoLastName")
			)
            ->where([['id','=', $rid],['idUserCandidato','=', $uid]])
			->first();
			
		if($request->user()->hasRole('Empresa')){
			/*if(Auth::user()->id != $referencias->idUserEmpresa){
				return 'Acción no permitida.';
			}*/
		}
		
        return view('admins.users.referenciasView')
            ->with('referencias', $referencias)
			->with('idUserCandidato', $uid);
    }
	public function deleteReferencias(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Candidato','Reclutador', 'Vendedor']);
		$data = request()->all();
		
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $data['uid']){
				return 'Acción no permitida.';
			}
		}
		
		DB::table('candidatosReferencias')
				->where('id', '=', $data['rid'])
				->where('idUserCandidato', '=', $data['uid'])
				->delete();
			
		if($request->user()->hasRole('Empresa')){
			/*if(Auth::user()->id != $referencias->idUserEmpresa){
				return 'Acción no permitida.';
			}*/
		}
		
        return redirect($data['_returnPath']);
    }

    // SUCURSALES Y SECTORES
    public function sucursalesSectores(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin']);

		$sucursales = DB::table('sucursales')
			->get();

		$sectores = DB::table('sectores')
			->orderBy('nombre','ASC')
			->get();
				
        return view('admins.sucursalesSectores.home')
        	->with('sucursales',$sucursales)
        	->with('sectores',$sectores);
    }

    public function sucursalesUpload(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin']);
		$data = request()->all();

		DB::table('sucursales')->insert([
			'name' => $data['name'],
			'active' => 1,
		]);

				
        return redirect('dashboard/sucursalesSectores');
    }
    public function sucursalesUpdate(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin']);
		$data = request()->all();

		DB::table('sucursales')
			->where('id', $data['id'])
			->update(
			[
				'active' => $data['active']
			]
		);
				
        return redirect('dashboard/sucursalesSectores');
    }

    public function sectorUpload(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin']);
		$data = request()->all();

		DB::table('sectores')->insert([
			'nombre' => $data['nombre'],
			'slug' => str_slug($data['nombre'], '-'),
		]);

				
        return redirect('dashboard/sucursalesSectores');
    }
    public function sectorUpdate(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin']);
		$data = request()->all();

		DB::table('sectores')
			->where('id', $data['id'])
			->update(
			[
				'active' => $data['active']
			]
		);
				
        return redirect('dashboard/sucursalesSectores');
    }

}
