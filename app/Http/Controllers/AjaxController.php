<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class AjaxController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    protected function sepomex($cp)
    {
        $sepomex = DB::table('sepomex')
          ->where('cp', '=', $cp)
          ->get();

        return $sepomex;
    }

    protected function empresasData($id)
    {
        $data = DB::table('empresasData')
            ->select('contactoName','contactoEmail','contactoPhone',
				DB::Raw('(SELECT nombre FROM sectores WHERE sectores.id = empresasData.idSector) AS giroEmpresa'),
				DB::Raw('(SELECT email FROM users WHERE users.id = '.$id.') AS empresaEmail'),
				DB::Raw('(SELECT name FROM users WHERE users.id = '.$id.') AS empresaName'),
				'calleNumero','cp','col','delmpo','edo','pais'
			)
            ->where('idUserEmpresa', '=', $id)
            ->get();

        return $data;
    }

    protected function candidatosList($_userId, $_roleId, $_ownerId, $_vacanteId)
    {
        if($_roleId == 1 || $_roleId == 2 || $_roleId == 3){
			// OBTENER CANDIDATOS SOLAMENTE DEL RECLUTADOR
			/*$role_type = 'Candidato';
			$candidatos = DB::table('users')
			->select('id','name','lastName','email')
			->join('usersRelations', function ($join) use ($_ownerId) {
				$join->on('users.id', '=', 'usersRelations.idUserCandidato')
					 ->where('usersRelations.idOwnerReclutador', '=', $_ownerId);
			})
			->orderBy('name', 'ASC')
			->get();*/
			
			$role = 'Candidato';
			$candidatos = User::whereHas(
				'roles', function($q) use ($role){
					$q->where('name', $role);
				}
			)
			->get();
		
			foreach($candidatos as $candidato){
				$added = DB::table('vacantesCandidatos')
					->select('added')
					->where([
						['idVacante', $_vacanteId],
						['idUserCandidato', $candidato->id]
					])
					->first();
				
				if($added){
					$candidato->added = $added->added;
				}else{
					$candidato->added = 0;
				}
				
				$bloqueado = DB::table('vacantesCandidatos')
					->select('bloqueado')
					->where([
						['idUserCandidato', $candidato->id],
						['bloqueado', '1']
					])
					->count();
				
				if($bloqueado > 0){
					$candidato->bloqueado = '1';
				}else{
					$candidato->bloqueado = '0';
				}
				
				$colocado = DB::table('vacantesCandidatos')
					->select('colocado')
					->where([
						['idUserCandidato', $candidato->id],
						['colocado', '1']
					])
					->count();
				
				if($colocado > 0){
					$candidato->bloqueado = '1';
					$candidato->colocado = '1';
				}else{
					$candidato->colocado = '0';
				}
				
			}
        }
		$candidatos = $candidatos->sortByDesc('added');
        return $candidatos->values()->all();
    }
	
	protected function reclutadorRelations($id)
    {
        $role = 'Reclutador';
		$reclutadores = User::whereHas(
            'roles', function($q) use ($role){
                $q->where('name', $role);
            }
        )
		->where('id', '!=', $id)
		->get();
		
		
		$totalCandidatos = DB::table('usersRelations')
			->where('idOwnerReclutador', '=', $id)
			->count();
		
		$totalVacantes = DB::table('vacantesRelation')
			->where('idOwnerReclutador', '=', $id)
			->count();
			
        return [$totalCandidatos, $totalVacantes, $reclutadores];
    }
	
	protected function empresasRelations($id)
    {
        $totalVacantes = DB::table('vacantes')
			->where('idUserEmpresa', '=', $id)
			->count();
		
        return $totalVacantes;
    }
}
