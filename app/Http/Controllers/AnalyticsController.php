<?php
/**
 * @author Iván Carrillo
 * @email icarrillo@levutalent.com
 * @create date 2020-06-15 11:58:13
 * @modify date 2020-06-16 15:22:58
 * @desc [description]
 */
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class AnalyticsController extends Controller
{
    /**
     * Show the view of index
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          return view('dashboard.admin.charts');
    }

    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $openVacants
     */
    public function getOpenVacants(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $openVacants = DB::table('users as a')->select('a.name as nombre','b.idUserReclutador as recluiter_id',DB::raw('count(c.idVacante) as countVacantes'), DB::raw('sum(d.tarifa) as tarifa'))
                ->leftJoin('reclutadoresData as b','a.id','=','b.idUserReclutador')
                ->leftJoin('vacantesRelation as c','b.idUserReclutador','=','c.idOwnerReclutador')
                ->leftJoin('vacantes as d','c.idVacante','=','d.id')
                ->whereRaw('c.idVacante IS NOT NULL')
                ->where('d.estatus','=','Abierta')
                ->whereBetween('d.created_at', array($from, $to))
                ->groupBy('c.idOwnerReclutador')
                ->orderBy('b.idUserReclutador')
                ->get();

        return json_encode($openVacants);
    }

    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $closedVacants
     */
    public function getClosedVacants(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $closedVacants = DB::table('users as a')->select('a.name as nombre','b.idUserReclutador as recluiter_id',DB::raw('count(c.idVacante) as countVacantes'), DB::raw('sum(d.tarifa) as tarifa'))
        ->leftJoin('reclutadoresData as b','a.id','=','b.idUserReclutador')
        ->leftJoin('vacantesRelation as c','b.idUserReclutador','=','c.idOwnerReclutador')
        ->leftJoin('vacantes as d','c.idVacante','=','d.id')
        ->whereRaw('c.idVacante IS NOT NULL')
        ->where('d.estatus','=','Cerrada')
        ->whereBetween('d.created_at', array($from, $to))
        ->groupBy('c.idOwnerReclutador')
        ->orderBy('b.idUserReclutador')
        ->get();

        return json_encode($closedVacants);
    }

    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $vacantsClosedWithHiring
     */
    public function getVacantsClosedWithHiring(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');        
        $vacantsClosedWithHiring = DB::table('users as a')->select('a.name as nombre','b.idUserReclutador as recluiter_id',DB::raw('count(c.idVacante) as countVacantes'), DB::raw('sum(d.tarifa) as tarifa'))
        ->leftJoin('reclutadoresData as b','a.id','=','b.idUserReclutador')
        ->leftJoin('vacantesRelation as c','b.idUserReclutador','=','c.idOwnerReclutador')
        ->leftJoin('vacantes as d','c.idVacante','=','d.id')
        ->whereRaw('c.idVacante IS NOT NULL')
        ->where('d.estatus','=','Cerrada con contratación')
        ->whereBetween('d.created_at', array($from, $to))
        ->groupBy('c.idOwnerReclutador')
        ->orderBy('b.idUserReclutador')
        ->get();
        
        return json_encode($vacantsClosedWithHiring);
    }

    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $vacantsClosedWithOutHiring
     */
    public function getVacantsClosedWithOutHiring(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $vacantsClosedWithOutHiring = DB::table('users as a')->select('a.name as nombre','b.idUserReclutador as recluiter_id',DB::raw('count(c.idVacante) as countVacantes'), DB::raw('sum(d.tarifa) as tarifa'))
            ->leftJoin('reclutadoresData as b','a.id','=','b.idUserReclutador')
            ->leftJoin('vacantesRelation as c','b.idUserReclutador','=','c.idOwnerReclutador')
            ->leftJoin('vacantes as d','c.idVacante','=','d.id')
            ->whereRaw('c.idVacante IS NOT NULL')
            ->where('d.estatus','=','Cerrada sin contratación')
            ->whereBetween('d.created_at', array($from, $to))
            ->groupBy('c.idOwnerReclutador')
            ->orderBy('b.idUserReclutador')
            ->get();

        return json_encode($vacantsClosedWithOutHiring);
    }

    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $sellers
     */
    public function getSaleAmount(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $status = $request->input('status');
        if($status == 1){
            $content = '"Abierta"';
        }elseif($status == 2){
            $content = '"Cerrada"';    
        }elseif($status == 3){
            $content = '"Cerrada con contratación"';
        }elseif($status == 4){
            $content = '"Cerrada sin contratación"';
        }elseif($status == 5){
            $content = '"Abierta"'.','.'"Cerrada"'.','.'"Cerrada con contratación"'.','.'"Cerrada sin contratación"';
        }
        $sellers = DB::table('users as a')->select('a.name as nombre','a.id as recluiter_id',DB::raw('count(b.idVacante) as countVacantes'), DB::raw('sum(c.tarifa) as tarifa'))
            ->leftJoin('vacantesRelation as b','a.id','=','b.idVendedor')
            ->leftJoin('vacantes as c','b.idVacante','=','c.id')
            ->whereRaw('c.tarifa IS NOT NULL')
            ->whereRaw('c.estatus IN('.$content.')')
            ->whereRaw('a.id NOT IN(1,128)')
            ->whereBetween('c.created_at', array($from, $to))
            ->groupBy('b.idVendedor')
            ->orderBy('a.id')
            ->get();

        return json_encode($sellers);
    }

    public function viewChart()
    {
          return view('dashboard.chart');
    }

    public function getTotalSecfits(Request $request)
    {
        $id = $request->id;
        $calif = DB::table('users')
		->select("users.id","users.name", DB::raw("round(candidatosSecFits.calificacion) as calificacion"),"candidatosSecFits.completed")
		->join('vacantesCandidatos', function ($join) use ($id) {
			$join->on('users.id', '=', 'vacantesCandidatos.idUserCandidato')
				 ->where([
				 	['vacantesCandidatos.idVacante', '=', $id],
					['vacantesCandidatos.added', '=', '1']
				]);
		})
        ->join('candidatosSecFits', function ($join) use ($id) {
			$join->on('users.id', '=', 'candidatosSecFits.idUserCandidato')
            ->where([
				 	['candidatosSecFits.idVacante', '=', $id],
					['candidatosSecFits.completed', '=', '1']
				]);
        })
        ->groupBy('candidatosSecFits.id')
		->orderByRaw('round(calificacion) asc')
		->get();
        
        return json_encode($calif);
    }
        
    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $vacants
     */
    public function getVacantsWithAssurance(Request $request)
    {
        $from = $request->input('from');
        $to = $request->input('to');
        $content = '"Abierta"'.','.'"Cerrada"'.','.'"Cerrada con contratación"'.','.'"Cerrada sin contratación"';

        $vacants = DB::table('users as a')->select('a.name as nombre','b.idUserReclutador as recluiter_id',DB::raw('count(c.idVacante) as countVacantes'), DB::raw('sum(d.tarifa) as tarifa'))
            ->leftJoin('reclutadoresData as b','a.id','=','b.idUserReclutador')
            ->leftJoin('vacantesRelation as c','b.idUserReclutador','=','c.idOwnerReclutador')
            ->leftJoin('vacantes as d','c.idVacante','=','d.id')
            ->where('d.garantia', 1)
            ->whereRaw('c.idVacante IS NOT NULL')
            ->whereRaw('d.tarifa IS NOT NULL')
            ->whereRaw('d.estatus IN('.$content.')')
            ->whereBetween('d.created_at', array($from, $to))
            ->groupBy('c.idOwnerReclutador')
            ->orderBy('b.idUserReclutador')
            ->get();
        return json_encode($vacants);
    }

    /**
     * Return data to make a chart
     *
     * @param  \Illuminate\Http\Request  $request
     * @return $vacancy
     */
    public function getVacantsByEnterprise(Request $request)
    {
       $vacancy = DB::table('users as a')
        ->select('a.id','a.name',DB::raw('COUNT(c.id) as noVacantes'), DB::raw('SUM(c.tarifa) as tarifa'))
        ->leftJoin('empresasData as b','a.id','=','b.idUserEmpresa')
        ->leftJoin('vacantes as c','b.idUserEmpresa','=','c.idUserEmpresa')
        ->where('c.estatus','Abierta')
        ->groupBy('a.id')
        ->orderByRaw('SUM(tarifa) DESC')
        ->get();
        
        return json_encode($vacancy);
    }
}
