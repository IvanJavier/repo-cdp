<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class CsrfController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    protected function csrf_()
    {
		return csrf_token();
    }
}
