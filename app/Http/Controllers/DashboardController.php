<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Candidato', 'Empresa', 'Vendedor']);
		
		if($request->user()->hasRole('Candidato')){
			
			$profilePic = DB::table('candidatosData')
					->select('profilePic','folder')
					->where('idUserCandidato','=',Auth::user()->id)
					->first();
			session()->put('profilePic', $profilePic->profilePic);
			session()->put('folder', $profilePic->folder);

			return redirect('/dashboard/users/profile/' . Auth::user()->id);
			
		}elseif($request->user()->hasRole('Empresa')){
			
			$profilePic = DB::table('empresasData')
					->select('profilePic','folder')
					->where('idUserEmpresa','=',Auth::user()->id)
					->first();
			session()->put('profilePic', $profilePic->profilePic);
			session()->put('folder', $profilePic->folder);

			return redirect('/dashboard/users/profile/' . Auth::user()->id);
			
		}else{
		
			$users = User::with('roles')->orderBy('id','desc')->get();
			
			foreach($users as $user){
				$uid = $user->id;
				if($user->hasRole('Candidato')){
					$folder = DB::table('candidatosData')
						->select('folder')
						->where('idUserCandidato','=',$uid)
						->first();
					
					$postulaciones = DB::table('vacantesCandidatos')
						->select('bloqueado','colocado','added')
						->where([['idUserCandidato','=',$uid],['added','=','1']])
						->get();
					
					if($folder != NULL){
						$user->folder = $folder->folder;
					}else{
						$user->folder = NULL;
					}
					$user->bloqueado = 0;
					foreach($postulaciones as $postulacion){
						if($postulacion->bloqueado == 1 || $postulacion->colocado == 1){
							$user->bloqueado = 1;
							break;
						}
					}
					$user->postulaciones = count($postulaciones);
					$user->vacantes = NULL;
					
				}else if($user->hasRole('Empresa')){
					$folder = DB::table('empresasData')
						->select('folder')
						->where('idUserEmpresa','=',$uid)
						->first();
					
					$vacantes = DB::table('vacantes')
						->where('idUserEmpresa','=',$uid)
						->count();
					
					$user->folder = $folder->folder;
					$user->bloqueado = NULL;
					$user->postulaciones = NULL;
					$user->vacantes = $vacantes;
					
				}else if($user->hasRole('Reclutador')){
					$folder = DB::table('reclutadoresData')
						->select('folder')
						->where('idUserReclutador','=',$uid)
						->first();
					
					$vacantes = DB::table('vacantesRelation')
						->where('idOwnerReclutador','=',$uid)
						->whereRaw("idVacante IN(SELECT vacantes.id FROM vacantes WHERE estatus = 'Abierta')")
						->count();
						
					$user->folder = $folder->folder;
					$user->bloqueado = NULL;
					$user->postulaciones = NULL;
					$user->vacantes = $vacantes;
					
				}else if($user->hasRole('Admin') || $user->hasRole('SuperAdmin')){
					$user->folder = NULL;
					$user->bloqueado = NULL;
					$user->postulaciones = NULL;
					$user->vacantes = NULL;
				}
				
			}
			
			if($request->user()->hasRole('Reclutador')){
				$reclutador = Auth::user()->id;
				$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = $reclutador)";
			}else{
				$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador != '0')";
			}
			
			$vacantes = DB::table('vacantes')
				->select("vacantes.*",
					DB::raw("(SELECT users.name FROM users WHERE users.id = vacantes.idUserEmpresa) as empresa"),
					DB::raw("(SELECT COUNT(vacantesCandidatos.idUserCandidato) FROM vacantesCandidatos WHERE vacantesCandidatos.idVacante = vacantes.id AND added = '1') as candidatos"))
				->whereRaw($whereRaw)
				->orderBy('id', 'DESC')
				->limit(5)
				->get();
				
	
			return view('dashboard')
				->with('users', $users)
				->with('vacantes', $vacantes);
		}
    }
	
	public function getFile($folder,$filename)
	{
		return response()->download(storage_path('app/public/'.$folder.'/'.$filename), null, [], null);
	}
}
