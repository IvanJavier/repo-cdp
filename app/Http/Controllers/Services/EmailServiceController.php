<?php

namespace App\Http\Controllers\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class EmailServiceController extends Controller
{
    public static function sendEmail($request,$pwd)
    {
        $contact_subject = 'Levu';
        $email_from = 'no_reply@levutalent.com';
        $email_from_name  = 'Levu WebMaster';
        $email_cc = 'icarrillo@levutalent.com';
        $mailData = array(
            'mailto' => $request->email,
            'subject' => $contact_subject,
            'from' => $email_from, 
            'from_name' => $email_from_name,
            'cc' => $email_cc, 
            );

        Mail::send('emails.reclutadores.bienvenido', [
                'recluEmail' => $request->email,
                'recluName' => $request->name.' '.$request->lastName,
                'recluPwd' => $pwd,
            ], function ($message) use ($mailData){
                $message->from( $mailData['from'], $mailData['from_name'] );
                $message->to($mailData['mailto']);
                $message->subject($mailData['subject']);
                $message->bcc($mailData['cc']);
            }
        );

    }
}
