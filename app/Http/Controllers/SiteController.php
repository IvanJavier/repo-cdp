<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;

class SiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }
    
    public function sendResume(Request $request)
    {
		$data = request()->all();
        $attach = $request->file('cv-file');
		$mailData = array(
			'mailto' => 'jobs@levutalent.com',
			'subject' => 'Levu Talent - Resume',
			'from' => 'no-reply@levutalent.com', 
			'from_name' => 'Levu Webmaster',
		);

		Mail::send('emails.resume', [
				'contactName' => $data['cv-name'],
				'contactEmail' => $data['cv-email']
			], function ($message) use ($mailData, $attach){
				$message->from( $mailData['from'], $mailData['from_name'] );
				$message->to($mailData['mailto']);
				$message->subject($mailData['subject']);
				$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
				$message->attach(
							$attach->getRealPath(), array(
							'as' 	=> $attach->getClientOriginalName(), 
        					'mime' 	=> $attach->getMimeType())
						);
			}
		);
		return redirect('/?sent=r#contact-form');
    }
	public function sendContact(Request $request)
    {
		$data = request()->all();
        
		$mailData = array(
			'mailto' => 'jobs@levutalent.com',
			'subject' => 'Levu Talent - Contact',
			'from' => 'no-reply@levutalent.com', 
			'from_name' => 'Levu Webmaster',
		);

		Mail::send('emails.contact', [
				'contactName' => $data['contact-name'],
				'contactEmail' => $data['contact-email'],
				'contactPhone' => $data['contact-phone'],
				'contactMsg' => $data['contact-message'],
			], function ($message) use ($mailData){
				$message->from( $mailData['from'], $mailData['from_name'] );
				$message->to($mailData['mailto']);
				$message->subject($mailData['subject']);
				$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
			}
		);
		return redirect('/?sent=c#contact-form');
    }

}
