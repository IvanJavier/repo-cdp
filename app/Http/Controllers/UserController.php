<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function myProfile(Request $request)
    {
        $request->user()->authorizeRoles(['Reclutador', 'Candidato', 'Empresa']);
        $user_id = Auth::user()->id;
        $user = User::where('id','=',$user_id)
            ->first();
        
        return view('myProfile')
            ->with('user',$user);
    }

}
