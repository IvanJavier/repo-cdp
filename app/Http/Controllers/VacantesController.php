<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Mail;

class VacantesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		
		$order = $request->input('order');
		if($order){
			switch($order){
				case 'folioAsc':
					$orderType = 'id';
					$orderOrder = 'asc';
				break;
				case 'folioDesc':
					$orderType = 'id';
					$orderOrder = 'desc';
				break;
				case 'tituloAsc':
					$orderType = 'titulo';
					$orderOrder = 'asc';
				break;
				case 'tituloDesc':
					$orderType = 'titulo';
					$orderOrder = 'desc';
				break;
				case 'fechaAsc':
					$orderType = 'created_at';
					$orderOrder = 'asc';
				break;
				case 'fechaDesc':
					$orderType = 'created_at';
					$orderOrder = 'desc';
				break;
				case 'reclutadorAsc':
					$orderType = 'recluName';
					$orderOrder = 'asc';
				break;
				case 'reclutadorDesc':
					$orderType = 'recluName';
					$orderOrder = 'desc';
				break;
				case 'empresaAsc':
					$orderType = 'empresa';
					$orderOrder = 'asc';
				break;
				case 'empresaDesc':
					$orderType = 'empresa';
					$orderOrder = 'desc';
				break;
				case 'estatusAsc':
					$orderType = 'estatus';
					$orderOrder = 'asc';
				break;
				case 'estatusDesc':
					$orderType = 'estatus';
					$orderOrder = 'desc';
				break;
			}
		}else{
			$orderType = 'id';
			$orderOrder = 'asc';
		}
		$estatus = $request->input('estatus');
		$from = $request->input('from');
		$to = $request->input('to');
		
		$empresa = $request->input('empresa');
		$titulo = $request->input('titulo');
		$whereArray = array();
		$whereRaw = "";
		
		if($request->user()->hasRole('SuperAdmin') || $request->user()->hasRole('Admin') || $request->user()->hasRole('Vendedor')){
			$reclutador = $request->input('reclutador');
			if($reclutador){
				$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = $reclutador)";
			}else{
				$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador != '0')";
			}
		}elseif($request->user()->hasRole('Reclutador')){
			$reclutador = Auth::user()->id;
			$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = $reclutador)";
		}elseif($request->user()->hasRole('Vendedor')){
			$reclutador = Auth::user()->id;
			$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idVendedor = $reclutador)";
		}
		
		if($estatus){
			$whereArray[] = ['estatus','=',$estatus];
		}
		if($from){
			$whereArray[] = ['created_at','>=', $from.' 00:00:00'];
		}
		if($to){
			$whereArray[] = ['created_at','<=',$to.' 23:59:59'];
		}
		

		if($empresa){
			$whereArray[] = ['idUserEmpresa','=',$empresa];
		}
        //return $whereArray;
		$vacantes = DB::table('vacantes')
            ->select("vacantes.*",
				DB::raw("(SELECT users.id FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluID"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluLastName"),
				DB::raw("(SELECT users.phone FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS recluPhone"),
                DB::raw("(SELECT users.id FROM users WHERE users.id = vacantes.idUserEmpresa) as empresaID"),
				DB::raw("(SELECT users.name FROM users WHERE users.id = vacantes.idUserEmpresa) as empresa"),
                DB::raw("(SELECT empresasData.contactoName FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoName"),
                DB::raw("(SELECT empresasData.contactoEmail FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoEmail")
			)
			->where($whereArray)
			->whereRaw($whereRaw)
			->orderBy($orderType,$orderOrder)
            ->paginate(50);
			
		foreach($vacantes as $vacante){
			$vid = $vacante->id;
			$candidatos = DB::select(
				DB::raw("SELECT users.id, users.name, users.lastName, users.email FROM users WHERE id IN (SELECT idUserCandidato FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $vid AND added = '1')")
			);
			$vacante->candidatos = $candidatos;
		}
		
		$role_reclutador = 'Reclutador';
		$reclutadores = User::whereHas(
			'roles', function($q) use ($role_reclutador){
				$q->where('name', $role_reclutador);
			}
		)
		->select('id','name','lastName')
		->whereRaw("id IN (SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id)")
		->orderBy('name','ASC')
		->get();
		
		$role_empresas = 'Empresa';
		$empresas = User::whereHas(
			'roles', function($q) use ($role_empresas){
				$q->where('name', $role_empresas);
			}
		)
		->select('id','name')
		->whereRaw("id IN (SELECT idUserempresa FROM vacantes WHERE vacantes.idUserempresa = users.id)")
		->orderBy('name')
		->get();
		
		return view('admins.vacantes.list')
            ->with('vacantes', $vacantes)
			->with('order',$order)
			->with('estatus',$estatus)
			->with('from',$from)
			->with('to',$to)
			->with('reclutador',$reclutador)
			->with('empresa',$empresa)
			->with('reclutadores',$reclutadores)
			->with('empresas',$empresas);
    }
	
	public function indexExport(Request $request)
    {
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		
		$estatus = $request->input('estatus');
		$from = $request->input('from');
		$to = $request->input('to');
		$reclutador = $request->input('reclutador');
		$empresa = $request->input('empresa');
		$whereArray = array();
		$whereRaw = "";
		
		if($estatus){
			$whereArray[] = ['estatus','=',$estatus];
		}
		if($from){
			$whereArray[] = ['created_at','>=', $from.' 00:00:00'];
		}
		if($to){
			$whereArray[] = ['created_at','<=',$to.' 23:59:59'];
		}
		if($reclutador){
			$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = $reclutador)";
		}else{
			$whereRaw = "id IN(SELECT idVacante FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador != '0')";
		}
		if($empresa){
			$whereArray[] = ['idUserEmpresa','=',$empresa];
		}
        //return $whereArray;

		$vacantes = DB::table('vacantes')
            ->select("vacantes.id AS folio",
			        "vacantes.created_at AS fechaDeCreacion",
			        "vacantes.tarifa",
			        "vacantes.valorAnticipo",
			        "vacantes.valorCierre",
			        "vacantes.costoInterno",
			        "vacantes.bonoReclutador",
			        "vacantes.bonoReclutadorPayed",
			        "vacantes.bonoVendedor",
			        "vacantes.bonoVendedorPayed",
			        "vacantes.bonoAdministrador",
			        "vacantes.bonoAdministradorPayed",
			        "vacantes.convenioTarifa",
			        "vacantes.convenioAnticipo",
			        "vacantes.titulo",
			        "vacantes.jefeVacantenombre",
			        "vacantes.jefeVacantePuesto",
			        "vacantes.entrevistadorUno",
			        "vacantes.entrevistadorDos",
			        "vacantes.calleNumero",
			        "vacantes.cp",
			        "vacantes.col",
			        "vacantes.delmpo",
			        "vacantes.edo",
			        "vacantes.pais",
			        "vacantes.reemplazo",
			        "vacantes.nuevaCreacion",
			        "vacantes.confidencial",
			        "vacantes.cantidadDeVacantes",
			        "vacantes.edadDesde",
			        "vacantes.edadHasta",
			        "vacantes.sueldoMensual",
			        "vacantes.sueldoMensualHasta",
			        "vacantes.sueldoTipo",
			        "vacantes.divisa",
			        "vacantes.sexo",
			        "vacantes.horarioLaboral",
			        "vacantes.escolaridad",
			        "vacantes.compensacionVariable",
			        "vacantes.prestaciones",
			        "vacantes.experiencia",
			        "vacantes.viajesAlAnio",
			        "vacantes.puestosQueLaReportan",
			        "vacantes.habilidadesNecesarias",
			        "vacantes.sistemas",
			        "vacantes.mision",
			        "vacantes.funciones",
			        "vacantes.indicadoresDesempenio",
			        "vacantes.entregables",
			        "vacantes.problemasActuales",
			        "vacantes.resultadosCorto",
			        "vacantes.resultadosMediano",
			        "vacantes.motivosRechazo",
			        "vacantes.empresasSimilares",
			        "vacantes.planInternoReferidos",
			        "vacantes.descripcionColaboradorIdeal",
			        "vacantes.viaComunicacionLevu",
			        "vacantes.preguntasClave",
			        "vacantes.elementosEstrategia",
			        "vacantes.culturaDeLaEmpresa",
			        "vacantes.logosYsimbolos",
			        "vacantes.normasYpatronesConducta",
			        "vacantes.fundamentosYvalores",
			        "vacantes.politicaInterna",
			        "vacantes.estatus",
			        "vacantes.resumenComparativo",
			        "vacantes.comentarios",
			        "vacantes.fechaPrimerosCandidatos",
				DB::raw("(SELECT users.name FROM users WHERE users.id = vacantes.idUserEmpresa) as empresa"),
				
				DB::raw("(SELECT sectores.nombre FROM sectores WHERE id IN(SELECT idSector FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa)) as sector"),
				DB::raw("(SELECT sucursales.name FROM sucursales WHERE sucursales.id = vacantes.idSucursal) as sucursal"),
				
                DB::raw("(SELECT empresasData.contactoName FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoNombre"),
                DB::raw("(SELECT empresasData.contactoEmail FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoEmail"),
				DB::raw("(SELECT empresasData.contactoPhone FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoTelefono"),
				
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS reclutadorNombre"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idOwnerReclutador = users.id AND vacantes.id = vacantesRelation.idvacante) LIMIT 0,1) AS reclutadorApellido"),
				
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idVendedor FROM vacantesRelation WHERE idVacante = vacantes.id)) AS vendedorName")
			)
			->where($whereArray)
			->whereRaw($whereRaw)
            ->get();

		return $vacantes->downloadExcel(
		    'vacantes.xlsx',
		    $writerType = null,
		    $headings = true
		);
		
    }

    public function view(Request $request, $id)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Candidato', 'Empresa', 'Vendedor']);

        $vacante = DB::table('vacantes')
            ->select("vacantes.*",
				DB::raw("(SELECT users.id FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS ownerId"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS recluName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS recluLastName"),
				DB::raw("(SELECT users.email FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS recluEmail"),
				DB::raw("(SELECT users.phone FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS recluPhone"),
				
				DB::raw("(SELECT users.id FROM users WHERE users.id IN(SELECT idVendedor FROM vacantesRelation WHERE idVacante = $id)) AS vendedorId"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idVendedor FROM vacantesRelation WHERE idVacante = $id)) AS vendedorName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id IN(SELECT idVendedor FROM vacantesRelation WHERE idVacante = $id)) AS vendedorLastName"),
                
				DB::raw("(SELECT users.name FROM users WHERE users.id = vacantes.idUserEmpresa) as empresa"),
                DB::raw("(SELECT empresasData.contactoName FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoName"),
                DB::raw("(SELECT empresasData.contactoEmail FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoEmail"),
                DB::raw("(SELECT empresasData.contactoPhone FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoPhone"),
                DB::raw("(SELECT sucursales.name FROM sucursales WHERE vacantes.idSucursal = sucursales.id) as sucursalName")
			)
            ->where('id','=', $id)
            ->first();
         
		
		$giroEmpresa = DB::table('sectores')
			->select('nombre')
			->whereRaw('id IN (SELECT idSector FROM empresasData WHERE idUserEmpresa = '.$vacante->idUserEmpresa.')')
			->first();
			
		$vacante->giroEmpresa = $giroEmpresa->nombre;

		$role_type = 'Candidato';
			if($request->user()->hasRole('SuperAdmin') || $request->user()->hasRole('Admin') || $request->user()->hasRole('Reclutador') || $request->user()->hasRole('Vendedor')){
			$candidatos = DB::table('users')
			->select("users.id","users.name","users.lastName","users.email",
				DB::raw("(SELECT vacantesCandidatos.estatus FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS estatus"),
				DB::raw("(SELECT vacantesCandidatos.bloqueado FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS bloqueado"),
				DB::raw("(SELECT vacantesCandidatos.colocado FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS colocado"),
				DB::raw("(SELECT vacantesCandidatos.assigned_at FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS assigned_at"),
				DB::raw("(SELECT candidatosData.is_hidden from candidatosData WHERE candidatosData.idUserCandidato = users.id LIMIT 0,1) as is_hidden")
			)
			->join('vacantesCandidatos', function ($join) use ($id) {
				$join->on('users.id', '=', 'vacantesCandidatos.idUserCandidato')
					->where([
						['vacantesCandidatos.idVacante', '=', $id],
						['vacantesCandidatos.added', '=', '1']
					]);
			})
			->join('candidatosData', function ($join) use ($id){
				$join->on('users.id', '=', 'candidatosData.idUserCandidato');
			})
			->whereRaw('candidatosData.is_hidden IN (0,1)')
			->orderBy('assigned_at', 'ASC')
			->get();
		}else{
			$candidatos = DB::table('users')
			->select("users.id","users.name","users.lastName","users.email",
				DB::raw("(SELECT vacantesCandidatos.estatus FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS estatus"),
				DB::raw("(SELECT vacantesCandidatos.bloqueado FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS bloqueado"),
				DB::raw("(SELECT vacantesCandidatos.colocado FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS colocado"),
				DB::raw("(SELECT vacantesCandidatos.assigned_at FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS assigned_at"),
				DB::raw("(SELECT candidatosData.is_hidden from candidatosData WHERE candidatosData.idUserCandidato = users.id LIMIT 0,1) as is_hidden")
			)
			->join('vacantesCandidatos', function ($join) use ($id) {
				$join->on('users.id', '=', 'vacantesCandidatos.idUserCandidato')
					->where([
						['vacantesCandidatos.idVacante', '=', $id],
						['vacantesCandidatos.added', '=', '1']
					]);
			})
			->join('candidatosData', function ($join) use ($id){
				$join->on('users.id', '=', 'candidatosData.idUserCandidato');
			})
			->where('candidatosData.is_hidden','=',0)
			->orderBy('assigned_at', 'ASC')
			->get();
		}
		$colocadoCount = DB::table('vacantesCandidatos')
			->where([
				['colocado','=','1'],
				['idVacante', '=', $id]
			])
			->count();
		//dd($candidatos);
		foreach($candidatos as $candidato){
			$candidatoData = DB::table('candidatosData')
				->select('cv','video','ltr','folder','is_hidden')
				->where('idUserCandidato','=',$candidato->id)
				->first();
			//dd(['candidato' => $candidato, 'candidatoData' => $candidatoData]);
			$candidatoRefs = DB::table('candidatosReferencias')
				->where([['idUserCandidato','=',$candidato->id]])
				->count();
				
			$comp = 0;
			$compensaciones = DB::table('candidatosCompensaciones')
				->select('completed')
				->where('idUserCandidato','=', $candidato->id)
				->first();
			
			if($compensaciones){
				$comp = $compensaciones->completed;
			}
			
			$secFit = 0;
			$secFits = DB::table('candidatosSecFits')
				->select('completed')
				->where([['idUserCandidato','=', $candidato->id],['idVacante','=', $id]])
				->first();
			
			if($secFits){
				$secFit = $secFits->completed;
			}
			
			$candidato->folder = $candidatoData->folder;
			$candidato->cv = $candidatoData->cv;
			$candidato->video = $candidatoData->video;
			$candidato->ltr = $candidatoData->ltr;
			$candidato->compensaciones = $comp;
			$candidato->secFits = $secFit;
			$candidato->referencias = $candidatoRefs;
		}

		if($request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $vacante->idUserEmpresa){
				return 'Acción no permitida.';
			}
		}
		//dd($candidatos);
        return view('admins.vacantes.view')
            ->with('vacante', $vacante)
			->with('candidatos', $candidatos)
			->with('colocadoCount',$colocadoCount);
    }
    
	public function imprimir(Request $request, $id)
    {
        $request->user()->authorizeRoles(['Empresa']);

        $vacante = DB::table('vacantes')
            ->select("vacantes.*",
				DB::raw("(SELECT users.id FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS ownerId"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS recluName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id IN(SELECT idOwnerReclutador FROM vacantesRelation WHERE idVacante = $id)) AS recluLastName"),
                DB::raw("(SELECT users.name FROM users WHERE users.id = vacantes.idUserEmpresa) as empresa"),
                DB::raw("(SELECT empresasData.contactoName FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoName"),
                DB::raw("(SELECT empresasData.contactoEmail FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoEmail"),
                DB::raw("(SELECT empresasData.contactoPhone FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoPhone")
			)
            ->where('id','=', $id)
            ->first();
		
		$giroEmpresa = DB::table('sectores')
			->select('nombre')
			->whereRaw('id IN (SELECT idSector FROM empresasData WHERE idUserEmpresa = '.$vacante->idUserEmpresa.')')
			->first();
			
		$vacante->giroEmpresa = $giroEmpresa->nombre;

		$role_type = 'Candidato';
		$candidatos = DB::table('users')
		->select("users.id","users.name","users.lastName","users.email",
			DB::raw("(SELECT vacantesCandidatos.entrevistar FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS entrevistar"),
			DB::raw("(SELECT vacantesCandidatos.bloqueado FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS bloqueado"),
			DB::raw("(SELECT vacantesCandidatos.colocado FROM vacantesCandidatos WHERE vacantesCandidatos.idUserCandidato = users.id AND vacantesCandidatos.idVacante = $id LIMIT 0,1) AS colocado")
		)
		->join('vacantesCandidatos', function ($join) use ($id) {
			$join->on('users.id', '=', 'vacantesCandidatos.idUserCandidato')
				 ->where([
				 	['vacantesCandidatos.idVacante', '=', $id],
					['vacantesCandidatos.added', '=', '1']
				]);
		})
		->orderBy('name', 'ASC')
		->get();
		
		$colocadoCount = DB::table('vacantesCandidatos')
			->where([
				['colocado','=','1'],
				['idVacante', '=', $id]
			])
			->count();
		
		foreach($candidatos as $candidato){
			$candidatoData = DB::table('candidatosData')
				->select('cv','video','ltr','folder')
				->where('idUserCandidato','=',$candidato->id)
				->first();
			
			$candidatoRefs = DB::table('candidatosReferencias')
				->where([['idUserCandidato','=',$candidato->id]])
				->count();
				
			$comp = 0;
			$compensaciones = DB::table('candidatosCompensaciones')
				->select('completed')
				->where('idUserCandidato','=', $candidato->id)
				->first();
			
			if($compensaciones){
				$comp = $compensaciones->completed;
			}
			
			$secFit = 0;
			$secFits = DB::table('candidatosSecFits')
				->select('completed')
				->where([['idUserCandidato','=', $candidato->id],['idVacante','=', $id]])
				->first();
			
			if($secFits){
				$secFit = $secFits->completed;
			}
			
			$candidato->folder = $candidatoData->folder;
			$candidato->cv = $candidatoData->cv;
			$candidato->video = $candidatoData->video;
			$candidato->ltr = $candidatoData->ltr;
			$candidato->compensaciones = $comp;
			$candidato->secFits = $secFit;
			$candidato->referencias = $candidatoRefs;
		}

		if($request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $vacante->idUserEmpresa){
				return 'Acción no permitida.';
			}
		}
        return view('admins.vacantes.print')
            ->with('vacante', $vacante)
			->with('candidatos', $candidatos)
			->with('colocadoCount',$colocadoCount);
    }

    public function showCreationForm(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $role_empresas = 'Empresa';
        $empresas = User::whereHas(
            'roles', function($q) use ($role_empresas){
                $q->where('name', $role_empresas);
            }
        )
            ->select('id','name')
            ->orderBy('name', 'ASC')
            ->get();
		
		$role_reclutador = 'Reclutador';
		$reclutadores = User::whereHas(
            'roles', function($q) use ($role_reclutador){
                $q->where('name','=','Reclutador')
					->orWhere('name','=','Admin')
					->orWhere('name','=','SuperAdmin');
            }
        )
            ->select('id','name', 'lastName')
            ->orderBy('name', 'ASC')
            ->get();

        $role_vendedor = 'Vendedor';
        $vendedores = User::whereHas(
            'roles', function($q) use ($role_vendedor){
                $q->where('name','=','Reclutador')
					->orWhere('name','=','Vendedor')
					->orWhere('name','=','Admin')
					->orWhere('name','=','SuperAdmin');
            }
        )
            ->select('id','name', 'lastName')
            ->orderBy('name', 'ASC')
            ->get();

        $sucursales = DB::table('sucursales')
        	->where('active','=',1)
            ->orderBy('name', 'ASC')
            ->get();

        return view('admins.vacantes.create')
            ->with('empresas', $empresas)
			->with('reclutadores', $reclutadores)
			->with('vendedores', $vendedores)
			->with('sucursales',$sucursales);
    }
    public function create(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		$folder = \App\Helpers\AppHelper::instance()->randomString();
        $data = request() -> all();
		
        $lastId = DB::table('vacantes')->insertGetId([
            'idUserEmpresa' => $data['idUserEmpresa'],
            'idSucursal' => $data['idSucursal'],

            'titulo' => $data['titulo'],
			
			'tarifa' => $data['tarifa'],
			'valorAnticipo' => $data['valorAnticipo'],
			'valorCierre' => $data['valorCierre'],
			'bonoReclutador' => $data['bonoReclutador'],
			
			'costoInterno' => $data['costoInterno'],
			'bonoVendedor' => $data['bonoVendedor'],
			'bonoAdministrador' => $data['bonoAdministrador'],

			'convenioTarifa' => $data['convenioTarifa'],
			'convenioAnticipo' => $data['convenioAnticipo'],
			
            'jefeVacantenombre' => $data['jefeVacantenombre'],
            'jefeVacantePuesto' => $data['jefeVacantePuesto'],
            'entrevistadorUno' => $data['entrevistadorUno'],
            'entrevistadorDos' => $data['entrevistadorDos'],
            'otroEntrevistador' => $data['otroEntrevistador'],
            'calleNumero' => $data['calleNumero'],
            'cp' => $data['cp'],
            'col' => $data['col'],
            'delmpo' => $data['delmpo'],
            'edo' => $data['edo'],
            'pais' => $data['pais'],
            'reemplazo' => $data['reemplazo'],
            'nuevaCreacion' => $data['nuevaCreacion'],
            'confidencial' => $data['confidencial'],
            'cantidadDeVacantes' => $data['cantidadDeVacantes'],
            'edadDesde' => $data['edadDesde'],
            'edadHasta' => $data['edadHasta'],
            'sueldoMensual' => $data['sueldoMensual'],
            'sueldoMensualHasta' => $data['sueldoMensualHasta'],
            'sueldoTipo' => $data['sueldoTipo'],
            'divisa' => $data['divisa'],
            'sexo' => $data['sexo'],
            'horarioLaboral' => $data['horarioLaboral'],
            'escolaridad' => $data['escolaridad'],
            'compensacionVariable' => $data['compensacionVariable'],
            'prestaciones' => $data['prestaciones'],
            'experiencia' => $data['experiencia'],
            'viajesAlAnio' => $data['viajesAlAnio'],
            'puestosQueLaReportan' => $data['puestosQueLaReportan'],
            'habilidadesNecesarias' => $data['habilidadesNecesarias'],
            'sistemas' => $data['sistemas'],
            'mision' => $data['mision'],
            'funciones' => $data['funciones'],
            'indicadoresDesempenio' => $data['indicadoresDesempenio'],
            'entregables' => $data['entregables'],
            'problemasActuales' => $data['problemasActuales'],
            'resultadosCorto' => $data['resultadosCorto'],
            'resultadosMediano' => $data['resultadosMediano'],
            'motivosRechazo' => $data['motivosRechazo'],
            'empresasSimilares' => $data['empresasSimilares'],
            'planInternoReferidos' => $data['planInternoReferidos'],
            'descripcionColaboradorIdeal' => $data['descripcionColaboradorIdeal'],
            'viaComunicacionLevu' => $data['viaComunicacionLevu'],
            'preguntasClave' => $data['preguntasClave'],
            'culturaDeLaEmpresa' => $data['culturaDeLaEmpresa'],

            'logosYsimbolos' => $data['logosYsimbolos'],
            'normasYpatronesConducta' => $data['normasYpatronesConducta'],
            'fundamentosYvalores' => $data['fundamentosYvalores'],
            'politicaInterna' => $data['politicaInterna'],

			'folder' => $folder,
			
			'created_at' => $data['created_at'],
			'fechaPrimerosCandidatos' => $data['fechaPrimerosCandidatos'],
			'garantia' => $data['garantia'],
			'entrevista' => $data['ubicacion'],
			'statusVacante' => $data['statusVacante']
        ]);
		
		if($request->user()->hasRole('Reclutador')){
			if(Auth::user()->id != $data['idOwnerReclutador']){
				return 'Acción no permitida.';
			}else{
				$vacanteRelation= DB::table('vacantesRelation')->insert([
					'idVacante' => $lastId,
					'idOwnerReclutador' => Auth::user()->id,
					'idVendedor' => $data['idVendedor'],
				]);
			}
		}else{
			$vacanteRelation= DB::table('vacantesRelation')->insert([
				'idVacante' => $lastId,
				'idOwnerReclutador' => $data['idOwnerReclutador'],
				'idVendedor' => $data['idVendedor'],
			]);
		}
		
		// SEND EMAIL EMPRESA
		$mailData = array(
			'mailto' => $data['empresaEmail'],
			'subject' => 'Levu Talent - Alta de vacante',
			'from' => 'no-reply@levutalent.com', 
			'from_name' => 'Levu Webmaster',
			'copy' => $data['contactoEmail'],
		);

		Mail::send('emails.empresas.vacanteCreated', [
				'empresaEmail' => $data['empresaEmail'],
				'empresaName' => $data['empresaName'],
				'contactoName' => $data['contactoName'],
				'contactoEmail' => $data['contactoEmail'],
				'vacanteName' => $data['titulo']
			], function ($message) use ($mailData){
				$message->from( 'no-reply@levutalent.com', 'Levu Webmaster' );
				$message->to($mailData['mailto']);
				$message->subject($mailData['subject']);
				$message->cc($mailData['copy']);
				$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
			}
		);
		
		// SEND EMAIL RECLUTADOR
		$reclutador = User::where('id','=',$data['idOwnerReclutador'])
			->select('name','lastName','email')
			->first();
		
		$mailData2 = array(
			'mailto' => $reclutador['email'],
			'subject' => 'Levu Talent - Alta de vacante',
			'from' => 'no-reply@levutalent.com', 
			'from_name' => 'Levu Webmaster',
		);

		Mail::send('emails.reclutadores.vacanteCreated', [
				'empresaName' => $data['empresaName'],
				'recluName' => $reclutador['name'] . ' ' . $reclutador['lastName'],
				'recluEmail' => $reclutador['email'],
				'vacanteName' => $data['titulo']
			], function ($message) use ($mailData2){
				$message->from( 'no-reply@levutalent.com', 'Levu Webmaster' );
				$message->to($mailData2['mailto']);
				$message->subject($mailData2['subject']);
				$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
			}
		);

        return redirect('dashboard');
    }

    public function showEditForm(Request $request, $id)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		
		$role_empresas = 'Empresa';
        $empresas = User::whereHas(
	            'roles', function($q) use ($role_empresas){
	                $q->where('name', $role_empresas);
	            }
	        )
            ->select('id','name')
            ->orderBy('name', 'ASC')
            ->get();
		
        $vacante = DB::table('vacantes')
            ->select("vacantes.*",
                DB::raw("(SELECT vacantesRelation.idOwnerReclutador FROM vacantesRelation WHERE vacantesRelation.idVacante = $id) as ownerId"),
				DB::raw("(SELECT vacantesRelation.idVendedor FROM vacantesRelation WHERE vacantesRelation.idVacante = $id) as vendedorId"),
				DB::raw("(SELECT users.name FROM users WHERE users.id = vacantes.idUserEmpresa) as empresa"),
                DB::raw("(SELECT users.id FROM users WHERE users.id = vacantes.idUserEmpresa) as empresaId"),
                DB::raw("(SELECT empresasData.contactoName FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoName"),
                DB::raw("(SELECT empresasData.contactoEmail FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoEmail"),
                DB::raw("(SELECT empresasData.contactoPhone FROM empresasData WHERE empresasData.idUserEmpresa = vacantes.idUserEmpresa) as contactoPhone"),
                DB::raw("(SELECT sucursales.id FROM sucursales WHERE vacantes.idSucursal = sucursales.id) as sucursalId")
            )
            ->where('id','=', $id)
            ->first();
		
		$giroEmpresa = DB::table('sectores')
			->select('nombre')
			->whereRaw('id IN (SELECT idSector FROM empresasData WHERE idUserEmpresa = '.$vacante->idUserEmpresa.')')
			->first();
			
		$vacante->giroEmpresa = $giroEmpresa->nombre;
		
		if($request->user()->hasRole('Reclutador')){
			if(Auth::user()->id != $vacante->ownerId){
				return 'Acción no permitida.';
			}
		}
		
		$role_reclutador = 'Reclutador';
		$reclutadores = User::whereHas(
            'roles', function($q) use ($role_reclutador){
                $q->where('name','=','Reclutador')
					->orWhere('name','=','Vendedor')
					->orWhere('name','=','Admin')
					->orWhere('name','=','SuperAdmin');
            }
        )
            ->select('id','name', 'lastName')
            ->orderBy('name', 'ASC')
            ->get();

        $role_vendedor = 'Vendedor';
        $vendedores = User::whereHas(
            'roles', function($q) use ($role_vendedor){
                $q->where('name','=','Reclutador')
					->orWhere('name','=','Vendedor')
					->orWhere('name','=','Admin')
					->orWhere('name','=','SuperAdmin');
            }
        )
            ->select('id','name', 'lastName')
            ->orderBy('name', 'ASC')
            ->get();

        $sucursales = DB::table('sucursales')
        	->where('active','=',1)
            ->orderBy('name', 'ASC')
            ->get();

        return view('admins.vacantes.edit')
            ->with('vacante', $vacante)
			->with('empresas', $empresas)
			->with('reclutadores', $reclutadores)
			->with('vendedores', $vendedores)
			->with('sucursales',$sucursales);
    }
    public function update(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();
		
		/*if($request->user()->hasRole('Reclutador')){
			if(Auth::user()->id != $data['idOwnerReclutador']){
				return 'Acción no permitida.';
			}
		}*/
		
        DB::table('vacantes')
            ->where('id', $data['id'])
            ->update(
            [
				'idUserEmpresa' => $data['idUserEmpresa'],
				'idSucursal' => $data['idSucursal'],

                'estatus' => $data['estatus'],
				'titulo' => $data['titulo'],
				
				'tarifa' => $data['tarifa'],
				'valorAnticipo' => $data['valorAnticipo'],
				'valorCierre' => $data['valorCierre'],
				'bonoReclutador' => $data['bonoReclutador'],
				
				'costoInterno' => $data['costoInterno'],
				'bonoVendedor' => $data['bonoVendedor'],
				'bonoAdministrador' => $data['bonoAdministrador'],
				
				'bonoReclutadorPayed' => $data['bonoReclutadorPayed'],
				'bonoVendedorPayed' => $data['bonoVendedorPayed'],
				'bonoAdministradorPayed' => $data['bonoAdministradorPayed'],

				'convenioTarifa' => $data['convenioTarifa'],
				'convenioAnticipo' => $data['convenioAnticipo'],
			
                'jefeVacantenombre' => $data['jefeVacantenombre'],
                'jefeVacantePuesto' => $data['jefeVacantePuesto'],
                'entrevistadorUno' => $data['entrevistadorUno'],
                'entrevistadorDos' => $data['entrevistadorDos'],
                'calleNumero' => $data['calleNumero'],
                'cp' => $data['cp'],
                'col' => $data['col'],
                'delmpo' => $data['delmpo'],
                'edo' => $data['edo'],
                'pais' => $data['pais'],
                'reemplazo' => $data['reemplazo'],
                'nuevaCreacion' => $data['nuevaCreacion'],
                'confidencial' => $data['confidencial'],
                'cantidadDeVacantes' => $data['cantidadDeVacantes'],
                'edadDesde' => $data['edadDesde'],
                'edadHasta' => $data['edadHasta'],
                'sueldoMensual' => $data['sueldoMensual'],
                'sueldoMensualHasta' => $data['sueldoMensualHasta'],
                'sueldoTipo' => $data['sueldoTipo'],
                'divisa' => $data['divisa'],
                'sexo' => $data['sexo'],
                'horarioLaboral' => $data['horarioLaboral'],
                'escolaridad' => $data['escolaridad'],
                'compensacionVariable' => $data['compensacionVariable'],
                'prestaciones' => $data['prestaciones'],
                'experiencia' => $data['experiencia'],
                'viajesAlAnio' => $data['viajesAlAnio'],
                'puestosQueLaReportan' => $data['puestosQueLaReportan'],
                'habilidadesNecesarias' => $data['habilidadesNecesarias'],
                'sistemas' => $data['sistemas'],
                'mision' => $data['mision'],
                'funciones' => $data['funciones'],
                'indicadoresDesempenio' => $data['indicadoresDesempenio'],
                'entregables' => $data['entregables'],
                'problemasActuales' => $data['problemasActuales'],
                'resultadosCorto' => $data['resultadosCorto'],
                'resultadosMediano' => $data['resultadosMediano'],
                'motivosRechazo' => $data['motivosRechazo'],
                'empresasSimilares' => $data['empresasSimilares'],
                'planInternoReferidos' => $data['planInternoReferidos'],
                'descripcionColaboradorIdeal' => $data['descripcionColaboradorIdeal'],
                'viaComunicacionLevu' => $data['viaComunicacionLevu'],
                'preguntasClave' => $data['preguntasClave'],
                'elementosEstrategia' => $data['elementosEstrategia'],
                'culturaDeLaEmpresa' => $data['culturaDeLaEmpresa'],

                'logosYsimbolos' => $data['logosYsimbolos'],
	            'normasYpatronesConducta' => $data['normasYpatronesConducta'],
	            'fundamentosYvalores' => $data['fundamentosYvalores'],
	            'politicaInterna' => $data['politicaInterna'],
				
				'created_at' => $data['created_at'],
				'fechaPrimerosCandidatos' => $data['fechaPrimerosCandidatos'],
				'garantia' => $data['garantia'],
				'entrevista' => $data['ubicacion'],
				'statusVacante' => $data['statusVacante']
            ]
        );
		
		DB::table('vacantesRelation')
            ->where('idVacante', $data['id'])
            ->update(
            [
                'idOwnerReclutador' => $data['idOwnerReclutador'],
				'idVendedor' => $data['idVendedor'],
            ]
        );
		if($data['estatus'] == 'Cerrada'){
			DB::table('vacantesCandidatos')
				->where('idVacante','=', $data['id'])
				->update(
				[
					'bloqueado' => '0',
				]
			);
		}
		
		if($data['idOwnerReclutador'] != $data['idOwnerReclutadorOld']){
			// SEND EMAIL
			$oldReclu = DB::table('users')
				->select('name','lastName','email')
				->where('id','=',$data['idOwnerReclutadorOld'])
				->first();
			
			$newReclu = DB::table('users')
				->select('name','lastName','email')
				->where('id','=',$data['idOwnerReclutador'])
				->first();
				
			$mailData = array(
				'mailto' => $newReclu->email,
				'subject' => 'Levu Talent - Intercambio de vacante',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster',
				'cc' => $oldReclu->email,
			);
	
			Mail::send('emails.reclutadores.vacanteSwitch', [
					'oldRecluName' => $oldReclu->name,
					'oldRecluLastName' =>$oldReclu->lastName,
					'newRecluName' => $newReclu->name,
					'newRecluLastName' => $newReclu->lastName,
					'vacanteName' => $data['titulo'],
					'empresaName' => $data['empresaName']
				], function ($message) use ($mailData){
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->to($mailData['mailto']);
					$message->subject($mailData['subject']);
					$message->cc($mailData['cc']);
					$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
				}
			);
		}
		
		// ENVIO DE CORREOS DE NOTIFCACION A LAS EMPRESAS
		/*
		if($data['idUserEmpresa'] != $data['idUserEmpresa_old']){
			
			// ENVIAR CORREO A EMPRESA VIEJA
			$mailData = array(
				'mailto' => $data['empresaEmail_old'],
				'subject' => 'Levu Talent - Baja de vacante',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster',
				'cc' => $data['contactoEmail_old'],
			);
			
			Mail::send('emails.vacantes.vacanteSwitch', [
					'vacanteName' => $data['titulo'],
				], function ($message) use ($mailData){
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->to($mailData['mailto']);
					$message->subject($mailData['subject']);
					$message->cc($mailData['cc']);
					$message->bcc('icarrillo@levutalent.com');
				}
			);
			
			// SEND EMAIL EMPRESA NUEVA
			$mailData = array(
				'mailto' => $data['empresaEmail'],
				'subject' => 'Levu Talent - Alta de vacante',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster',
				'copy' => $data['contactoEmail'],
			);
	
			Mail::send('emails.empresas.vacanteCreated', [
					'empresaEmail' => $data['empresaEmail'],
					'empresaName' => $data['empresaName'],
					'contactoName' => $data['contactoName'],
					'contactoEmail' => $data['contactoEmail'],
					'vacanteName' => $data['titulo']
				], function ($message) use ($mailData){
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->to($mailData['mailto']);
					$message->subject($mailData['subject']);
					$message->cc($mailData['copy']);
					$message->bcc('icarrillo@levutalent.com');
				}
			);
		}
		*/
		
		if($request->user()->hasRole('Reclutador')){
        	return redirect('dashboard/vacantes');
		}else{
			return redirect('dashboard/vacantes/view/'.$data['id']);
		}
    }
	public function updateComments(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();
		
        DB::table('vacantes')
            ->where('id', $data['id'])
            ->update(
            [
                'comentarios' => $data['comentarios'],
            ]
        );
		
		// SEND NOTIFICATION EMAIL
		$copies = array();
		// Get all superadmins and admins emails
		$admins = User::select('email')
				->whereHas(
					'roles', function($q){
						$q->where('name', 'SuperAdmin');
					}
				)
				->get();
				
		foreach($admins as $admin){
			$copies[] = $admin->email;
		}

		$mailData = array(
			'mailto' => $data['recluEmail'],
			'subject' => 'Levu Talent - Nuevo comentario '.$data['vacanteTitle'],
			'from' => 'no-reply@levutalent.com', 
			'from_name' => 'Levu Webmaster',
			'cc' => $copies,
		);
		Mail::send('emails.admins.commentUpdated', [
				'vacanteID' => $data['id'],
				'vacanteName' => $data['vacanteTitle'],
				'empresaName' => $data['empresaName'],
				'comments' => $data['comentarios']
			], function ($message) use ($mailData){
				$message->from( 'no-reply@levutalent.com', 'Levu Webmaster' );
				$message->to($mailData['mailto']);
				$message->subject($mailData['subject']);
				$message->cc($mailData['cc']);
				$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
			}
		);
		
		return redirect($data['_returnPath'].'#tabCandidatos');
    }
    protected function delete(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();
        
		DB::table('vacantesCandidatos')
            ->where('idVacante', '=', $data['id'])
            ->delete();
		
		DB::table('vacantesRelation')
            ->where('idVacante', '=', $data['id'])
            ->delete();
			
		DB::table('vacantes')
            ->where('id', '=', $data['id'])
            ->delete();

        return redirect('dashboard');
    }
	
	protected function addCandidate(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();
        $idVacante = $data['idVacante'];

        ini_set('memory_limit','256M');
       	ini_set('post_max_size', '128M');
    	ini_set('upload_max_filesize', '50M');

		foreach($data['idUserCandidato'] as $userId => $added){
			$userExist = DB::table('vacantesCandidatos')
				->select('idUserCandidato')
				->where([
					['idUserCandidato','=', $userId],
					['idVacante','=',$idVacante]
				])
				->count();
			
			if($userExist == '1'){
				DB::table('vacantesCandidatos')
					->where([
						['idVacante', $idVacante],
						['idUserCandidato', $userId]
					])
					->update(
					[
						'added' => $added,
					]
				);
			}else{
				if($added == '1'){
					DB::table('vacantesCandidatos')->insert([
						'idVacante' => $idVacante,
						'idUserCandidato' => $userId,
						'assigned_at' => date('Y-m-d H:i:s')
					]);
					
					// SEND EMAIL
					$userData = DB::table('users')
						->select('email','name','lastName')
						->where('id','=', $userId)
						->first();
					$vacanteData = DB::table('vacantes')
						->select('titulo')
						->where('id','=', $data['idVacante'])
						->first();
					
					$mailData = array(
						'mailto' => $userData->email,
						'subject' => 'Levu Talent - Asignación a vacante',
						'from' => 'no-reply@levutalent.com', 
						'from_name' => 'Levu Webmaster'
					);
			
					Mail::send('emails.candidatos.assigned', [
							'candidatoName' => $userData->name.' '.$userData->lastName,
							'vacanteTitulo' => $vacanteData->titulo,
						], function ($message) use ($mailData){
							$message->to($mailData['mailto']);
							$message->from($mailData['from'], $mailData['from_name']);
							$message->subject($mailData['subject']);
							$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
						}
					);
					Mail::send('emails.empresas.assigned', [
							'candidatoName' => $userData->name.' '.$userData->lastName,
							'vacanteTitulo' => $vacanteData->titulo,
						], function ($message) use ($mailData){
							$message->to($mailData['mailto']);
							$message->from($mailData['from'], $mailData['from_name']);
							$message->subject($mailData['subject']);
							$message->bcc(['icarrillo@levutalent.com','igarza@levutalent.com']);
						}
					);
					
				}
			}
		}
        return redirect('dashboard/vacantes/view/'.$data['idVacante'].'#tabCandidatos');
    }
	
	protected function fileUpload(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Candidato', 'Vendedor']);
		$data = request()->all();
		//return $data;
		if($request->user()->hasRole('Candidato')){
			if(Auth::user()->id != $data['id']){
				return 'Acción no permitida.';
			}
		}
		
       	ini_set('memory_limit','256M');
		$fileName = NULL;
		
		$folder = $data['folder'];
		if($folder == NULL) {
			$folder = \App\Helpers\AppHelper::instance()->randomString();
		}

		$ramdomStr = \App\Helpers\AppHelper::instance()->randomString();

		$fileName = $data['fileType'] . '-' . time() . '-' . $ramdomStr . '.' . $data['_file']->getClientOriginalExtension();
		
		$data['_file']->move(storage_path('app/public/'.$folder), $fileName);

		if($data['fileType'] == 'cv' || $data['fileType'] == 'ltr' || $data['fileType'] == 'video'){
			DB::table('candidatosData')
				->where('idUserCandidato', $data['id'])
				->update(
				[
					$data['fileType'] => $fileName,
					'folder' => $folder,
				]
			);
		}
		if($data['fileType'] == 'resumenComp'){
			DB::table('vacantes')
				->where('id', $data['id'])
				->update(
				[
					'resumenComparativo' => $fileName,
					'folder' => $folder,
				]
			);
		}
        return redirect($data['_returnPath']);
    }
	
	// SEC FITS
	public function showSecFits(Request $request, $uid, $vid)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Empresa', 'Reclutador', 'Vendedor']);
		
		$secFits = DB::table('candidatosSecFits')
			->select("candidatosSecFits.*",
				DB::raw("(SELECT users.name FROM users WHERE users.id = $uid) AS candidatoName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id = $uid) AS candidatoLastName"),
				DB::raw("(SELECT candidatosData.folder FROM candidatosData WHERE candidatosData.idUserCandidato = $uid) AS folder"),
				DB::raw("(SELECT candidatosData.profilePic FROM candidatosData WHERE candidatosData.idUserCandidato = $uid) AS profilePic"),
				DB::raw("(SELECT vacantes.titulo FROM vacantes WHERE vacantes.id = $vid) AS vacante"),
				DB::raw("(SELECT vacantes.idUserempresa FROM vacantes WHERE vacantes.id = $vid) AS idUserEmpresa"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid)) AS cliente")
			)
            ->where([['idUserCandidato','=', $uid],['idVacante','=', $vid]])
			->get();
		
		$vacante = DB::table('vacantes')
			->where('id',$vid)
			->first();

		if($request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $secFits[0]->idUserEmpresa){
				return 'Acción no permitida.';
			}
		}

        return view('admins.vacantes.secFits')
            ->with('secFits', $secFits)
			->with('idUserCandidato', $uid)
			->with('vacante',$vacante)
			->with('idVacante', $vid);
    }
    public function editSecFits(Request $request, $uid, $vid)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $secFits = DB::table('candidatosSecFits')
			->select("candidatosSecFits.*",
				DB::raw("(SELECT users.name FROM users WHERE users.id = $uid) AS candidatoName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id = $uid) AS candidatoLastName"),
				DB::raw("(SELECT candidatosData.folder FROM candidatosData WHERE candidatosData.idUserCandidato = $uid) AS folder"),
				DB::raw("(SELECT candidatosData.profilePic FROM candidatosData WHERE candidatosData.idUserCandidato = $uid) AS profilePic"),
				DB::raw("(SELECT vacantes.titulo FROM vacantes WHERE vacantes.id = $vid) AS vacante"),
				DB::raw("(SELECT vacantes.idUserempresa FROM vacantes WHERE vacantes.id = $vid) AS idUserEmpresa"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid)) AS cliente")
			)
            ->where([['idUserCandidato','=', $uid],['idVacante','=', $vid]])
			->get();

		if(sizeof($secFits) == 0){
			$lastId = DB::table('candidatosSecFits')->insertGetId([
				'idUserCandidato' => $uid,
				'idVacante' => $vid,
			]);

			$secFits = DB::table('candidatosSecFits')
				->select("candidatosSecFits.*",
					DB::raw("(SELECT users.name FROM users WHERE users.id = $uid) AS candidatoName"),
					DB::raw("(SELECT users.lastName FROM users WHERE users.id = $uid) AS candidatoLastName"),
					DB::raw("(SELECT candidatosData.folder FROM candidatosData WHERE candidatosData.idUserCandidato = $uid) AS folder"),
					DB::raw("(SELECT candidatosData.profilePic FROM candidatosData WHERE candidatosData.idUserCandidato = $uid) AS profilePic"),
					DB::raw("(SELECT vacantes.titulo FROM vacantes WHERE vacantes.id = $vid) AS vacante"),
					DB::raw("(SELECT vacantes.idUserempresa FROM vacantes WHERE vacantes.id = $vid) AS idUserEmpresa"),
					DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid)) AS cliente")
				)
				->where('id','=', $lastId)
				->get();
		}

		$vacante = DB::table('vacantes')
			->where('id',$vid)
			->first();
		
        return view('admins.vacantes.secFitsEdit')
            ->with('secFits', $secFits)
			->with('idUserCandidato', $uid)
			->with('idVacante', $vid)
			->with('vacante',$vacante);
    }
	public function updateSecFits(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
        $data = request()->all();

        $divisor = 0;
        $suma = 0;

        if($data['puestosQueLaReportan']['calif'] > 0){
        	$suma+=$data['puestosQueLaReportan']['calif'];
        	$divisor++;
        }
        if($data['sistemas']['calif'] > 0){
        	$suma+=$data['sistemas']['calif'];
        	$divisor++;
        }
        if($data['habilidadesNecesarias']['calif'] > 0){
        	$suma+=$data['habilidadesNecesarias']['calif'];
        	$divisor++;
        }
        if($data['funciones']['calif'] > 0){
        	$suma+=$data['funciones']['calif'];
        	$divisor++;
        }
        if($data['entregables']['calif'] > 0){
        	$suma+=$data['entregables']['calif'];
        	$divisor++;
        }
        if($data['pruebas']['calif'] > 0){
        	$suma+=$data['pruebas']['calif'];
        	$divisor++;
        }
        if($data['descripcionColaboradorIdeal']['calif'] > 0){
        	$suma+=$data['descripcionColaboradorIdeal']['calif'];
        	$divisor++;
        }
        if($data['indicadoresDesempenio']['calif'] > 0){
        	$suma+=$data['indicadoresDesempenio']['calif'];
        	$divisor++;
        }
        if($data['mision']['calif'] > 0){
        	$suma+=$data['mision']['calif'];
        	$divisor++;
        }
        if($data['resultadosCorto']['calif'] > 0){
        	$suma+=$data['resultadosCorto']['calif'];
        	$divisor++;
        }
        if($data['resultadosMediano']['calif'] > 0){
        	$suma+=$data['resultadosMediano']['calif'];
        	$divisor++;
        }
        if($data['motivosRechazo']['calif'] > 0){
        	$suma+=$data['motivosRechazo']['calif'];
        	$divisor++;
        }
        if($data['empresasSimilares']['calif'] > 0){
        	$suma+=$data['empresasSimilares']['calif'];
        	$divisor++;
        }
        if($data['culturaDeLaEmpresa']['calif'] > 0){
        	$suma+=$data['culturaDeLaEmpresa']['calif'];
        	$divisor++;
        }
        if($data['logosYsimbolos']['calif'] > 0){
        	$suma+=$data['logosYsimbolos']['calif'];
        	$divisor++;
        }
        if($data['normasYpatronesConducta']['calif'] > 0){
        	$suma+=$data['normasYpatronesConducta']['calif'];
        	$divisor++;
        }
        if($data['fundamentosYvalores']['calif'] > 0){
        	$suma+=$data['fundamentosYvalores']['calif'];
        	$divisor++;
        }
        if($data['politicaInterna']['calif'] > 0){
        	$suma+=$data['politicaInterna']['calif'];
        	$divisor++;
        }
        
        if($suma != 0 && $divisor != 0){
        	$promedio = $suma/$divisor;
        }else{
        	$promedio = 0;
        }
        //return json_encode($data['puestosQueLaReportan']);

        DB::table('candidatosSecFits')
            ->where([
				['id', $data['id']],
				['idUserCandidato',$data['idUserCandidato']],
				['idVacante',$data['idVacante']]
			])
            ->update(
            [
                'experiencia' => $data['experiencia'],
                'filtro' => $data['filtro'],

                'puestosQueLaReportan' => json_encode($data['puestosQueLaReportan']),
                'sistemas' => json_encode($data['sistemas']),
                'habilidadesNecesarias' => json_encode($data['habilidadesNecesarias']),
                'funciones' => json_encode($data['funciones']),
                'entregables' => json_encode($data['entregables']),
                'pruebas' => json_encode($data['pruebas']),
                'descripcionColaboradorIdeal' => json_encode($data['descripcionColaboradorIdeal']),
                'indicadoresDesempenio' => json_encode($data['indicadoresDesempenio']),
                'mision' => json_encode($data['mision']),
                'resultadosCorto' => json_encode($data['resultadosCorto']),
                'resultadosMediano' => json_encode($data['resultadosMediano']),
                'motivosRechazo' => json_encode($data['motivosRechazo']),
                'empresasSimilares' => json_encode($data['empresasSimilares']),
                'culturaDeLaEmpresa' => json_encode($data['culturaDeLaEmpresa']),
                'logosYsimbolos' => json_encode($data['logosYsimbolos']),
                'normasYpatronesConducta' => json_encode($data['normasYpatronesConducta']),
                'fundamentosYvalores' => json_encode($data['fundamentosYvalores']),
                'politicaInterna' => json_encode($data['politicaInterna']),

                'calificacion' => $promedio,
                'recomendacion' => $data['recomendacion'],
				'completed' => $data['completed'],
            ]
        );

        return redirect($data['_returnPath']);
    }
	public function printSecFits(Request $request, $uid, $vid)
    {
        $request->user()->authorizeRoles(['Empresa']);
		
		$secFits = DB::table('candidatosSecFits')
			->select("candidatosSecFits.*",
				DB::raw("(SELECT users.name FROM users WHERE users.id = $uid) AS candidatoName"),
				DB::raw("(SELECT users.lastName FROM users WHERE users.id = $uid) AS candidatoLastName"),
				DB::raw("(SELECT vacantes.titulo FROM vacantes WHERE vacantes.id = $vid) AS vacante"),
				DB::raw("(SELECT vacantes.idUserempresa FROM vacantes WHERE vacantes.id = $vid) AS idUserEmpresa"),
				DB::raw("(SELECT users.name FROM users WHERE users.id IN(SELECT vacantes.idUserEmpresa FROM vacantes WHERE vacantes.id = $vid)) AS cliente")
			)
            ->where([['idUserCandidato','=', $uid],['idVacante','=', $vid]])
			->get();
		
		if($request->user()->hasRole('Empresa')){
			if(Auth::user()->id != $secFits[0]->idUserEmpresa){
				return 'Acción no permitida.';
			}
		}

        return view('admins.vacantes.secFitsPrint')
            ->with('secFits', $secFits)
			->with('idUserCandidato', $uid)
			->with('idVacante', $vid);
    }
	// ESTATUS CANDIDATO
	protected function estatusUser(Request $request)
    {
        $request->user()->authorizeRoles(['Admin','SuperAdmin','Reclutador','Empresa', 'Vendedor']);
		$data = request()->all();

		DB::table('vacantesCandidatos')
			->where([
				['idUserCandidato','=', $data['uid']],
				['idVacante','=', $data['idVacante']]
			])
			->update(
			[
				'estatus' => $data['estatus'],
			]
		);
		// Enviar correo de notificación al reclutador a cargo del candidato seleccionado
		// SEND NOTIFICATION EMAIL
		$bcc = array();
		// Get all superadmins and admins emails
		$admins = User::select('email')
				->whereHas(
					'roles', function($q){
						$q->where('name', 'Admin')->orWhere('name', 'SuperAdmin');
					}
				)
				->get();
				
		foreach($admins as $admin){
			$bcc[] = $admin->email;
		}

		$bcc[] = 'icarrillo@levutalent.com';
		//$bcc[] = 'igarza@levutalent.com';
		// Get reclutador email
		$vid = $data['idVacante'];
		
		$reclutador = DB::table('users')
			->select('email')
			->join('vacantesRelation', function ($join) use ($vid) {
				$join->on('users.id', '=', 'vacantesRelation.idOwnerReclutador')
					 ->where([
						['vacantesRelation.idVacante', '=', $vid]
					]);
			})
			->first();
		
		if($request->user()->hasRole('Empresa')){
			// Get empresa email
	
				
			$mailData = array(
				'subject' => 'Levu Talent - Actualización de estatus',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster',
				'cc' => $reclutador->email,
				'bcc' => $bcc,
			);
			
			Mail::send('emails.vacantes.actualizarEstatus', [
					'actualizadorName' => $data['actualizadorName'],
					'vacanteName' => $data['vacanteTitulo'],
					'vacanteID' => $data['idVacante'],
					'candidatoName' => $data['candidatoName'] . ' ' . $data['candidatoLastName'],
				], function ($message) use ($mailData){
					$message->to('igarza@levutalent.com');
					$message->from( $mailData['from'], $mailData['from_name'] );
					$message->subject($mailData['subject']);
					$message->cc($mailData['cc']);
					$message->bcc($mailData['bcc']);
				}
			);
		}else{
			$mailData = array(
				'mailto' => $reclutador->email,
				'subject' => 'Levu Talent - Actualización de estatus',
				'from' => 'no-reply@levutalent.com', 
				'from_name' => 'Levu Webmaster',
				'bcc' => $bcc,
			);
			
			Mail::send('emails.vacantes.actualizarEstatus', [
				'actualizadorName' => $data['actualizadorName'],
				'vacanteName' => $data['vacanteTitulo'],
				'candidatoName' => $data['candidatoName'] . ' ' . $data['candidatoLastName'],
				'vacanteID' => $data['idVacante'],
			], function ($message) use ($mailData){
				$message->from( $mailData['from'], $mailData['from_name'] );
				$message->to($mailData['mailto']);
				$message->subject($mailData['subject']);
				$message->bcc($mailData['bcc']);
			}
		);
		}
		
        return redirect($data['_returnPath']);
    }
	
	// BLOQUEAR CANDIDATO
	protected function blockUser(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		$data = request()->all();
		DB::table('vacantesCandidatos')
			->where('idUserCandidato','=', $data['id'])
			->update(
			[
				'added' => '0',
			]
		);
		
		DB::table('vacantesCandidatos')
			->where([['idUserCandidato', $data['id']],['idVacante','=',$data['idVacante']]])
			->update(
			[
				'added' => '1',
				'bloqueado' => $data['bloqueado'],
			]
		);
		
        return redirect($data['_returnPath']);
    }
	
	// COLOCAR USUARIO
	protected function hireUser(Request $request)
    {
        $request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		$data = request()->all();
		
		// Eliminar al candidato de las demás vacantes
		DB::table('vacantesCandidatos')
			->where('idUserCandidato','=', $data['id'])
			->update(
			[
				'added' => '0',
			]
		);
		
		/*DB::table('vacantesCandidatos')
			->where('idVacante','=',$data['idVacante'])
			->update(
			[
				'bloqueado' => '0',
				'colocado' => '0',
			]
		);*/
		
		// Coloca al candidato en la vacante
		DB::table('vacantesCandidatos')
			->where([['idUserCandidato','=', $data['id']],['idVacante','=',$data['idVacante']]])
			->update(
			[
				'added' => '1',
				'bloqueado' => '1',
				'colocado' => $data['colocado'],
				'idReclutadorHired' => Auth::user()->id,
				'hired_at' => date('Y-m-d H:i:s'),
			]
		);
		// REVISAR SI TODAS LAS PLAZAS HAN SIDO CUBIERTAS
		$plazas = DB::table('vacantes')
			->select('cantidadDeVacantes')
			->where('id','=',$data['idVacante'])
			->first();

		$cubiertas = DB::table('vacantesCandidatos')
			->where([
				['idVacante','=',$data['idVacante']],
				['colocado','=','1']
			])
			->count();

		// CERRAR VACANTE
		if($plazas->cantidadDeVacantes == $cubiertas){
			DB::table('vacantes')
				->where('id','=',$data['idVacante'])
				->update(
				[
					'estatus' => 'Cerrada con contratación',
					'fecha_cierre' => date('Y-m-d H:i:s')
				]
			);
		}
		
		// SEND EMAIL
		$vid = $data['idVacante'];
		$users = DB::table('users')
			->select('id','email','name','lastName',
				DB::Raw("(SELECT titulo FROM vacantes WHERE id = $vid) AS vacante")
			)
			->join('vacantesCandidatos', function ($join) use ($vid) {
				$join->on('users.id', '=', 'vacantesCandidatos.idUserCandidato')
					 ->where([
						['vacantesCandidatos.idVacante', '=', $vid],
						['vacantesCandidatos.added', '=', '1'],
					]);
			})
			->get();

		$bcc[] = 'icarrillo@levutalent.com';
		$bcc[] = 'igarza@levutalent.com';

		$reclutador = DB::table('users')
			->select('id','email','name','lastName')
			->join('vacantesRelation', function ($join) use ($vid) {
				$join->on('users.id', '=', 'vacantesRelation.idOwnerReclutador')
					 ->where([
						['vacantesRelation.idVacante', '=', $vid],
					]);
			})
			->first();

		$bcc[] = $reclutador->email;

		foreach($users as $user){
			// Correo al seleccionado
			if($data['id'] == $user->id){
				$mailData = array(
					'mailto' => $user->email,
					'subject' => 'Levu Talent - Has sido seleccionado/a',
					'from' => 'no-reply@levutalent.com', 
					'from_name' => 'Levu Webmaster',
					'bcc' => $bcc,
				);
		
				Mail::send('emails.candidatos.hired', [
						'candidatoName' => $user->name.' '.$user->lastName,
						'vacanteTitulo' => $user->vacante,
					], function ($message) use ($mailData){
						$message->to($mailData['mailto']);
						$message->from($mailData['from'], $mailData['from_name']);
						$message->subject($mailData['subject']);
						$message->bcc($mailData['bcc']);
					}
				);
				Mail::send('emails.empresas.hired', [
						'candidatoName' => $user->name.' '.$user->lastName,
						'vacanteTitulo' => $user->vacante,
					], function ($message) use ($mailData){
						$message->to($mailData['mailto']);
						$message->from($mailData['from'], $mailData['from_name']);
						$message->subject($mailData['subject']);
						$message->bcc($mailData['bcc']);
					}
				);
			}else{
				// Correo a los no seleccionados
				$mailData = array(
					'mailto' => $user->email,
					'subject' => 'Levu Talent - Vacante ocupada',
					'from' => 'no-reply@levutalent.com', 
					'from_name' => 'Levu Webmaster',
					'bcc' => $bcc,
				);
		
				Mail::send('emails.candidatos.notHired', [
						'candidatoName' => $user->name.' '.$user->lastName,
						'vacanteTitulo' => $user->vacante,
					], function ($message) use ($mailData){
						$message->to($mailData['mailto']);
						$message->from($mailData['from'], $mailData['from_name']);
						$message->subject($mailData['subject']);
						$message->bcc($mailData['bcc']);
					}
				);
			}
		}
		
        return redirect($data['_returnPath'].'#tabCandidatos');
    }

	public function hideCandidate(Request $request)
	{
		$request->user()->authorizeRoles(['SuperAdmin', 'Admin', 'Reclutador', 'Vendedor']);
		$data = request()->all();

		DB::table('candidatosData')
			->where('idUserCandidato','=', $data['id'])
			->update(
			[
				'is_hidden' => '1',
			]
		);

		return redirect($data['_returnPath']);
	}

	public function showCandidate(Request $request)
	{
		$request->user()->authorizeRoles(['SuperAdmin','Vendedor']);
		$data = request()->all();

		DB::table('candidatosData')
			->where('idUserCandidato','=', $data['id'])
			->update(
			[
				'is_hidden' => '0',
			]
		);

		return redirect($data['_returnPath']);
	}
}
