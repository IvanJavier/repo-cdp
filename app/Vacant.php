<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Enterprise;

class Vacant extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'vacantes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idUserEmpresa', 'idSucursal', 'tarifa', 'valorAnticipo', 'valorCierre', 'costoInterno', 'bonoReclutador', 'bonoReclutadorPayed', 'convenioTarifa', 'convenioAnticipo', 'titulo',
        'jefeVacantenombre', 'jefeVacantePuesto', 'entrevistadorUno', 'entrevistadorDos', 'calleNumero', 'cp', 'col', 'delmpo', 'edo', 'pais', 'remplazo', 'nuevaCreacion', 'confidencial',
        'cantidadDeVacantes', 'edadDesde', 'edadHasta', 'sueldoMensual', 'divisa', 'sexo', 'horarioLaboral', 'escolaridad', 'compensacionVariable', 'prestaciones', 'experiencia', 'viajesAlAnio',
        'puestosQueLaReportan', 'habilidadesNecesarias', 'sistemas', 'mision', 'vision', 'funciones', 'indicadoresDesempenio', 'entregables', 'problemasActuales', 'resultadosCorto', 'resultadosMediano',
        'motivosRechazo', 'empresasSimilares', 'planInternoReferidos', 'descripcionColaboradorIdeal', 'viaComunicacionLevu', 'preguntasClave', 'elementosEstrategia', 'culturaDeLaEmpresa',
        'logosYsimbolos', 'normasYpatronesConducta', 'fundamentosYvalores', 'politicaInterna', 'estatus', 'resumenComparativo', 'comentarios', 'folder', 'fechaPrimeroCandidatos'
    ];

    public static function getVacantsByEnterpriseId($id){

        $vacants = User::from('users as a')
                       ->select('a.id as empresa_id','a.name as empresa','c.id as folio','c.titulo as vacante','c.estatus as status','c.created_at as fechaCreacion','f.hired_at as fechaCierre','e.name as reclutadorNombre','e.lastName as reclutadorApellido')
                       ->leftJoin('empresasData as b', 'a.id', '=', 'b.idUserEmpresa')
                       ->leftJoin('vacantes as c', 'a.id', '=', 'c.idUserEmpresa')
                       ->leftJoin('vacantesRelation as d', 'c.id', '=', 'd.idVacante')
                       ->leftJoin('users as e', 'e.id', '=', 'd.idOwnerReclutador')
                       ->leftJoin('vacantesCandidatos as f', 'f.idVacante', '=', 'c.id')
                       ->where('a.id','=',$id)
                       ->where('c.idUserEmpresa','=',$id)
                       ->groupBy('c.id')
                       ->orderBy('a.id', 'desc')
                       ->get();
        return $vacants;
    }
}
