<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('lastName')->nullable();
            $table->string('email')->unique();
            $table->string('phone')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('usersRelations', function (Blueprint $table) {
            $table->integer('idUserCandidato');
            $table->integer('idOwnerReclutador');
        });

        Schema::create('candidatosData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserCandidato');
            $table->string('profilePic')->nullable();
            $table->string('puesto');
            $table->string('sector');
            $table->string('escolaridad');
            $table->string('rangoSueldo');
            $table->string('calleNumero')->nullable();
            $table->string('cp')->nullable();
            $table->string('col')->nullable();
            $table->string('delmpo')->nullable();
            $table->string('edo')->nullable();
            $table->string('pais')->nullable();
            $table->string('cv')->nullable(); // PDF
            $table->string('video')->nullable(); // MP4
            $table->string('ltr')->nullable(); // PDF
            $table->boolean('compensaciones');
        });

        Schema::create('candidatosCompensaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserCandidato');
            $table->string('empresaActual');
            $table->integer('antiguedad');
            // DATOS JSON
            $table->text('salarioBase')->nullable();
            $table->text('comisiones')->nullable();
            $table->text('bonoObjetivo')->nullable();
            $table->text('aguinaldo')->nullable();
            $table->text('primaVacacional')->nullable();
            $table->text('fondoAhorro')->nullable();
            $table->text('fondoRetiro')->nullable();
            $table->text('valesDespensa')->nullable();
            $table->text('valesRestaurante')->nullable();
            $table->text('valesGasolina')->nullable();
            $table->text('ptu')->nullable();
            $table->text('otro')->nullable();
            // PARTE 2
            $table->integer('diasVacaiones')->nullable();
            $table->text('automovil')->nullable();
            $table->text('gastosAutomovil')->nullable();
            $table->text('seguroVida')->nullable();
            $table->text('gastosMedMenores')->nullable();
            $table->text('gastoMedMayores')->nullable();
            $table->text('vivienda')->nullable();
            $table->text('apoyosEducativos')->nullable();
            $table->text('stockNumeroAcciones')->nullable();
            $table->text('stockValorPresente')->nullable();
            $table->text('otrasPrestciones')->nullable();
        });


        Schema::create('empresasData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserEmpresa');
            $table->string('profilePic')->nullable();
            $table->string('contactoName');
            $table->string('contactoEmail');
            $table->string('contactoPuesto');
            $table->string('contactoPhone');
            $table->string('giroEmpresa');
            $table->string('calleNumero')->nullable();
            $table->string('cp')->nullable();
            $table->string('col')->nullable();
            $table->string('delmpo')->nullable();
            $table->string('edo')->nullable();
            $table->string('pais')->nullable();

        });

        Schema::create('reclutadoresData', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserReclutador');
            $table->string('profilePic')->nullable();
            $table->string('calleNumero')->nullable();
            $table->string('cp')->nullable();
            $table->string('col')->nullable();
            $table->string('delmpo')->nullable();
            $table->string('edo')->nullable();
            $table->string('pais')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('usersRelations');
        Schema::dropIfExists('candidatosData');
        Schema::dropIfExists('candidatosCompensaciones');
        Schema::dropIfExists('empresasData');
        Schema::dropIfExists('reclutadoresData');
    }
}
