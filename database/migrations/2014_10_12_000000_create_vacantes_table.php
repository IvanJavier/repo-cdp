<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVacantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacantes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserEmpresa');
            // DATOS GENERALES DE LA VACANTE
            $table->string('titulo');
            $table->string('jefeVacantenombre');
            $table->string('jefeVacantePuesto');
            $table->string('entrevistadorUno');
            $table->string('entrevistadorDos');
            // DOMICILIO DE ENTREVISTA
            $table->string('calleNumero')->nullable();
            $table->string('cp')->nullable();
            $table->string('col')->nullable();
            $table->string('delmpo')->nullable();
            $table->string('edo')->nullable();
            $table->string('pais')->nullable();
            // DATOS DE LA VACANTE
            $table->boolean('reemplazo');
            $table->boolean('nuevaCreacion');
            $table->boolean('confidencial');
            $table->integer('cantidadDeVacantes');
            $table->integer('edadDesde');
            $table->integer('edadHasta');
            $table->double('sueldoMensual', 15, 2);
            $table->string('sexo');
            $table->string('horarioLaboral');
            $table->string('escolaridad');
            $table->string('compensacionVariable');
            $table->string('prestaciones');
            $table->string('experiencia');
            $table->string('viajesAlAnio');
            // ELEMENTOS INDISPENSABLES
            $table->text('puestosQueLaReportan');
            $table->text('habilidadesNecesarias');
            $table->text('sistemas');
            $table->text('mision');
            $table->text('funciones');
            $table->text('indicadoresDesempenio');
            $table->text('entregables');
            $table->text('problemasActuales');
            $table->text('resultadosCorto');
            $table->text('resultadosMediano');
            $table->text('motivosRechazo');
            $table->text('empresasSimilares');

            $table->text('planInternoReferidos');
            $table->text('descripcionColaboradorIdeal');
            $table->text('viaComunicacionLevu');
            $table->text('preguntasClave');
            $table->text('elementosEstrategia');

            $table->text('culturaDeLaEmpresa');
            // ELEMENTOS ADICIONALES
            $table->timestamps();
        });

        Schema::create('vacantesRelation', function (Blueprint $table) {
            $table->integer('idVacante');
            $table->integer('idOwnerReclutador');
        });

        Schema::create('vacantesCandidatos', function (Blueprint $table) {
            $table->integer('idVacante');
            $table->integer('idUserCandidato');
            $table->boolean('secFits');
            $table->boolean('referencias');
            $table->boolean('colocado');
        });

        Schema::create('secFits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserCandidato');
            $table->integer('idVacante');
            // ------ //
            $table->string('experiencia');
            $table->text('filtro');
            $table->text('adaptacionPorHabilidades');
            $table->text('adaptacionPorExperiencia');
            $table->text('adaptacionPorCultura');
            $table->text('aspectosPositivos');
            $table->text('aspectosNegativos');
            $table->integer('calificacion');
            $table->text('recomendacion');
        });

        Schema::create('referencias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idUserCandidato');
            $table->integer('idVacante');
            // ------ //
            $table->string('entrevistaA');
            $table->string('puesto');
            $table->string('empresa');
            $table->string('telefono');
            $table->string('correo')->nullable();
            $table->date('fechaEntrevista');
            $table->longText('respuestas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacantes');
        Schema::dropIfExists('vacantesRelation');
        Schema::dropIfExists('vacantesCandidatos');
        Schema::dropIfExists('secFits');
        Schema::dropIfExists('referencias');
    }
}
