<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFreelancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('freelances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('profilePic',100)->nullable();
            $table->string('direccion',250)->nullable();
            $table->string('cp',10)->nullable();
            $table->string('colonia',100)->nullable();
            $table->string('delmpo',100)->nullable();
            $table->string('edo',100)->nullable();
            $table->string('pais',100)->nullable();
            $table->string('folder',25)->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('freelances');
    }
}
