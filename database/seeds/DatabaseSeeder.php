<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // La creación de datos de roles debe ejecutarse primero
        $this->call(RoleTableSeeder::class);
        // Los usuarios necesitarán los roles previamente generados
        $this->call(UserTableSeeder::class);
        
        // $this->call(UsersTableSeeder::class);

        // ROLE DEFINITION
        /*$roles = [
            ['id' => 1,'role' => 'SuperAdmin'],
        	['id' => 2,'role' => 'Admin'],
        	['id' => 3,'role' => 'Reclutador'],
        	['id' => 4,'role' => 'Candidato'],
        	['id' => 5,'role' => 'Empresa'],
        ];
        DB::table('roles')->insert($roles);

        // USER SUPER ADMIN AND ADMIN
        $admin = [
            ['id' => 1, 'idRole' => 1 , 'name' => 'SuperAdmin', 'email' => 'superadmin@levu.mx', 'password' => Hash::make('LevuSuperAdmin*2018')],
            ['id' => 2, 'idRole' => 2 , 'name' => 'Admin', 'email' => 'admin@levu.mx', 'password' => Hash::make('LevuAdmin*2018')],

        ];
        DB::table('users')->insert($admin);*/

        // DEFAULT PARA REFERENCIAS
        // '{"¿En qué periodo y lugar han trabajado juntos?":"","¿Eras su superior, par o colaborador de esta persona?":"","¿¿Podrías comentarnos acerca de su desempeño técnico?":"","¿Con respecto a su estilo comportamental, se llevaba en general bien con todas las personas? ¿Y con su equipo en particular?":"","¿Qué podrías decirnos de su estilo de liderazgo? (responder solo si ha tenido gente bajo su responsabilidad directa)":"","¿Cuáles consideras que son sus fortalezas? (ténicas y actitudinales)":"","¿Cuáles consideras que son sus áreas de oportunidad? (ténicas y actitudinales)":"","¿Sabes porqué dejó la compañía?":"","¿Lo contratarís de nuevo si tuvieras la oportunidad?":""}';

        $sepomex = [
            ['id' => 1, 'cp' => '11200' , 'edo' => 'Ciudad de México', 'edoId' => '09', 'edoAbr' => 'cdmx', 'mpo' => 'Miguel Hidalgo', 'col' => 'Lomas Hermosa', 'pais' => 'México'],
            ['id' => 2, 'cp' => '11200' , 'edo' => 'Ciudad de México', 'edoId' => '09', 'edoAbr' => 'cdmx', 'mpo' => 'Miguel Hidalgo', 'col' => 'Lomas de Sotelo', 'pais' => 'México'],
            ['id' => 3, 'cp' => '06100' , 'edo' => 'Ciudad de México', 'edoId' => '09', 'edoAbr' => 'cdmx', 'mpo' => 'Cuauhtémoc', 'col' => 'Hipódromo', 'pais' => 'México'],

        ];
        DB::table('sepomex')->insert($sepomex);
    }
}
