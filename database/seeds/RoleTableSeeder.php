<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new Role();
        $role->name = 'SuperAdmin';
        $role->description = 'Super administrador nivel dios';
        $role->save();

        $role = new Role();
        $role->name = 'Admin';
        $role->description = 'Administrador general';
        $role->save();

        $role = new Role();
        $role->name = 'Reclutador';
        $role->description = 'Reclutadores';
        $role->save();

        $role = new Role();
        $role->name = 'Candidato';
        $role->description = 'Candidatos';
        $role->save();

        $role = new Role();
        $role->name = 'Empresa';
        $role->description = 'Empresa';
        $role->save();
    }
}
