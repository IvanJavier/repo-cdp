<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_superAdmin = Role::where('name', 'SuperAdmin')->first();
        $role_admin = Role::where('name', 'Admin')->first();
        $role_reclutador = Role::where('name', 'Reclutador')->first();
        $role_candidato = Role::where('name', 'Candidato')->first();
        $role_empresa = Role::where('name', 'Empresa')->first();

        /*'id' => 1,
        'idRole' => 1 ,
        'name' => 'SuperAdmin',
        'email' => 'superadmin@levu.mx',
        'password' => Hash::make('LevuSuperAdmin*2018')]*/

        $user = new User();
        $user->name = 'SuperAdmin';
        $user->email = 'superadmin@levu.mx';
        $user->password = Hash::make('LevuSuperAdmin*2018');
        $user->save();
        $user->roles()->attach($role_superAdmin);

        $user = new User();
        $user->name = 'Admin';
        $user->email = 'admin@levu.mx';
        $user->password = Hash::make('LevuAdmin*2018');
        $user->save();
        $user->roles()->attach($role_admin);

        $user = new User();
        $user->name = 'Reclutador';
        $user->email = 'reclutador@levu.mx';
        $user->password = Hash::make('LevuReclutador*2018');
        $user->save();
        $user->roles()->attach($role_reclutador);

        $user = new User();
        $user->name = 'Candidato';
        $user->email = 'candidato@levu.mx';
        $user->password = Hash::make('LevuCandidato*2018');
        $user->save();
        $user->roles()->attach($role_candidato);

        $user = new User();
        $user->name = 'Empresa';
        $user->email = 'empresa@levu.mx';
        $user->password = Hash::make('LevuEmpresa*2018');
        $user->save();
        $user->roles()->attach($role_empresa);
    }
}
