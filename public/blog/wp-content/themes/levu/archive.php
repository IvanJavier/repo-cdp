<?php get_header(); ?>
<section id="content" class="bg-wite">
	<div class="container">
		<!-- section -->
		<div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<?php $archive_date = get_the_time('F, Y'); ?>
                <h1 class="mt-50 mb-40 text-uppercase text-bold">Artículos de <?php echo $archive_date; ?></h1>
                <hr class="border-grey">
            </div>            
            
            <div class="col-md-8 col-sm-12 col-xs-12">
            
					<?php if ( have_posts() ) : ?>
    
                        <!-- the loop -->
                        <?php while ( have_posts() ) : the_post(); ?>
                        	<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 p0 mb-50">
                        	<div class="col-md-12 col-sm-12 col-xs-12 home-latest-single p0" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;"></div>
                            <?php 
							$category = get_the_category(); 
							foreach($category as $cat){
								$category_parent_id = $cat->category_parent;
								$category_id = $cat->cat_ID;
								if($cat->slug == 'empresas'){$category_bg = 'bg-ctqRed';}else{$category_bg = 'bg-ctqOrange';}
								$category_link = get_category_link( $category_id );
								
								if ( $category_parent_id == 0 ) {
									$category_id = $cat->cat_ID;
									$category_link = get_category_link( $category_id );
									$category_name = $cat->name;
									break;
								}
								
							}
							?>
                            <div class="col-md-12 cols-sm-12 col-xs-12 p15 color-white <?php echo $category_bg; ?>">
                            	<p class="text-bold m0">
                                    <a href="<?php echo esc_url( $category_link ); ?>" title="<?php echo ucfirst(strtolower($category_name)); ?>"><?php echo ucfirst(strtolower($category_name)); ?></a>
                                </p>
                            	<h2 class="m0"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
                            </div>
                        </div>
                        <?php endwhile; ?>
                        <!-- end of the loop -->
                    
                      <?php wp_reset_postdata(); ?>
                
                  <?php else:  ?>
                  	<div class="col-md-12 p0 mb-25 bg-white">
                    	<h3>No se encontraron resultados</h3>
                    </div>
                  <?php endif; ?>
              
            
            </div>
            
            
            <!-- SIDEBAR -->
            <?php get_sidebar(); ?>
            <!-- .SIDEBAR -->

		</div>
		<!-- /section -->
    </div>
</section>
   
<?php get_footer(); ?>
