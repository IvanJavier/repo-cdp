</main>
<footer class="pb-3">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <p class="m-0"><a href="mailto:jobs@levutalent.com" class="color-levuAzul font-20"><strong>jobs@levutalent.com</strong></a></p>
                            <p class="m-0"><small>Levu Talent Hunters 2018 • Todos los derechos reservados</small></p>
                        </div>
                        <div class="col-md-6 text-right">
                            <img src="http://levu.invasor.mx/img/logoHeader.png" height="50">
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div> 
        </div>
        <div class="clearfix"></div> 
    </footer>
<?php wp_footer(); ?>
</body>
</html>