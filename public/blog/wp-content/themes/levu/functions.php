<?php
ini_set( 'mysql.trace_mode', 0 );

function SearchFilter($query) {
	if ($query->is_search) {
		$query->set('post_type', 'post');
	}
	return $query;
}
 
add_filter('pre_get_posts','SearchFilter');

function wpb_custom_new_menu() {
	register_nav_menu('top-menu',__( 'Top Menu' ));
	register_nav_menu('bottom-menu',__( 'Bottom Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );

add_theme_support( 'post-thumbnails' );


// Changing excerpt length
function new_excerpt_length($length) {
	return 25;
}

add_filter('excerpt_length', 'new_excerpt_length',999);

// Changing excerpt more
function new_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


// Custom pagination
function custom_pagination($numpages = '', $pagerange = '', $paged='') {

  if (empty($pagerange)) {
    $pagerange = 2;
  }

  /**
   * This first part of our function is a fallback
   * for custom pagination inside a regular loop that
   * uses the global $paged and global $wp_query variables.
   * 
   * It's good because we can now override default pagination
   * in our theme, and use this function in default quries
   * and custom queries.
   */
  global $paged;
  if (empty($paged)) {
    $paged = 1;
  }
  if ($numpages == '') {
    global $wp_query;
    $numpages = $wp_query->max_num_pages;
    if(!$numpages) {
        $numpages = 1;
    }
  }

  /** 
   * We construct the pagination arguments to enter into our paginate_links
   * function. 
   */
  $pagination_args = array(
    //'base'            => get_pagenum_link(1) . '%_%',
    'format'          => '?page=%#%',
    'total'           => $numpages,
    'current'         => $paged,
    'show_all'        => False,
    'end_size'        => 1,
    'mid_size'        => $pagerange,
    'prev_next'       => True,
    'prev_text'       => __('&laquo;'),
    'next_text'       => __('&raquo;'),
    'type'            => 'plain',
    'add_args'        => false,
    'add_fragment'    => ''
  );

  $paginate_links = paginate_links($pagination_args);

  if ($paginate_links) {
    echo "<nav class='custom-pagination text-center'>";
      echo $paginate_links;
    echo "</nav>";
  }

}


/**
 * Register our sidebars and widgetized areas.
 *
 */
function wpb_widgets_init() {
	
	register_sidebar( array(
        'name'          => 'Sidebar-top',
        'id'            => 'side-bar-top',
        'before_widget' => '<div id="%1$s" class="mb-25 widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="text-uppercase mt-0 mb-10 color-levuAzul"><strong>',
        'after_title'   => '</strong></h4>',
    ) );
	register_sidebar( array(
        'name'          => 'Sidebar-middle',
        'id'            => 'side-bar-middle',
        'before_widget' => '<div id="%1$s" class="mb-25 widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="text-uppercase mt-0 mb-10 color-levuAzul"><strong>',
        'after_title'   => '</strong></h4>',
    ) );
	register_sidebar( array(
        'name'          => 'Sidebar-bottom',
        'id'            => 'side-bar-bottom',
        'before_widget' => '<div id="%1$s" class="mb-25 widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="text-uppercase mt-0 mb-10 color-levuAzul"><strong>',
        'after_title'   => '</strong></h4>',
    ) );
}
add_action( 'widgets_init', 'wpb_widgets_init' );

function title($limit) {
  $title = explode(' ', get_the_title(), $limit);
  if (count($title)>=$limit) {
    array_pop($title);
    $title = implode(" ",$title).'...';
  } else {
    $title = implode(" ",$title);
  }	
  $title = preg_replace('`[[^]]*]`','',$title);
  return $title;
}

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return $excerpt;
}

?>