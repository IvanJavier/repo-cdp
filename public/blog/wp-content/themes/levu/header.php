<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="">

    <title>Levu Talent Blog</title>

    <!-- Scripts -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script type="text/javascript" src="http://levutalent.com/blog/wp-content/themes/levu/js/functions.js"></script> 

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="http://levutalent.com/blog/wp-content/themes/levu/app.css" rel="stylesheet">
    <link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet">
    
    <?php wp_head(); ?>
</head>
<body class="bg-whiteGrey">
    <header>
        <nav class="bg-whiteGrey p-2">
            <a class="float-left" href="http://levutalent.com/">
                <img src="http://levutalent.com/img/logoHeader.png" height="75" id="logo">
            </a>
            <button class="btn bg-transparent float-right font-16 relative" id="menu-bars" onClick="$('#main-menu').slideToggle();"><i class="fas fa-bars"></i></button>   
            <ul class="nav navbar navbar-right pt-4 mb-0 pr-0 mr-0" id="main-menu">
                <li class="pr-5 pl-5"><a class="go-to" name="home" href="http://levutalent.com/#home">Home</a></li>
                <li class="pr-5 pl-5"><a class="go-to" name="about-us" href="http://levutalent.com/#about-us">About us</a></li>
                <li class="pr-5 pl-5"><a class="go-to" name="services" href="http://levutalent.com/#services">Services</a></li>
                <!-- <li class="pr-5 pl-5"><a href="http://levutalent.com/blog">Blog & news</a></strong></li> -->
                <li class="pr-5 pl-5"><a class="go-to" name="contact-us" href="http://levutalent.com/#contact-us">Contact us</a></li>
                <!--<li class="pr-5 pl-5"><a href="#" id="aLogin">Login</a></li>-->
            </ul>
            <div class="clearfix"></div>        
        </nav>
        <!-- <div id="siteLogin" style="width:400px; display:none; top:85px;" class="box-shadow absolute right0 bg-levuAzul">
            <form id="siteLogin-form" method="POST" action="http://levutalent.com/login">
                        
                <div class="form-group color-white">
                    <h1 class="mt-0"><strong>LOGIN</strong></h1>
                </div>
                
                <div class="form-group">
                    <input id="email" type="email" class="form-control bg-transparent color-white" name="email" required autofocus placeholder="E-mail">
                </div>
        
                <div class="form-group">
                    <input id="password" type="password" class="form-control bg-transparent color-white" name="password" required placeholder="Password">
                </div>
                <div class="form-group mb-0">
                    <button type="submit" class="btn bg-black color-white btn-block">
                        Login
                    </button>
                </div>
            </form>
            <div class="clearfix"></div>
        </div> -->
    </header>
    <main>