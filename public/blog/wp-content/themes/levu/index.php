<?php get_header(); ?>
<?php
  // set up or arguments for our custom query
  $query_args_featured = array(
	'post_type' => 'post',
	'category_name' => 'featured',
	'posts_per_page' => 1,
  );
  // create a new instance of WP_Query
  $the_query_featured = new WP_Query( $query_args_featured );
?>
<?php if ( $the_query_featured->have_posts() ) : ?>
	<?php while ( $the_query_featured->have_posts() ) : $the_query_featured->the_post(); ?>
		<?php $url_feat = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
        <div class="home-image relative" style="background:url(<?php echo $url_feat; ?>) no-repeat center; background-size:cover;">
    		<?php //echo do_shortcode('[metaslider id="7"]'); ?>
            <div class="container middle h-100">
                <div class="col-md-6 text-left p0 color-white text-bold">
                	<?php
					$category_feat = get_the_category();
					$category_names_feat = '';
					foreach($category_feat as $cat_feat){
						$category_id_feat = $cat_feat->cat_ID;
						$category_link_feat = get_category_link( $category_id_feat );
						$category_name_feat = $cat_feat->name;
						$category_names_feat .= '<a href="'.$category_link_feat.'" class="color-white">' . $category_name_feat . '</a>, ';
					}
					?>
                    <h4 class="bg-levuAzul color-white p-2 float-left m-0"><?php echo substr($category_names_feat, 0, -2); ?></h4>
                    <div class="clearfix"></div>
                    <h1 class="text-uppercase color-white"><strong><?php echo title(10); ?></strong></h1>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-white">Read more <i class="fas fa-long-arrow-alt-right"></i></a>
                </div>
            </div>
		</div>
	<?php endwhile; ?>
<?php endif; ?>
<div class="clear25"></div>

<section id="content">
	<div class="container">

		<?php
		  $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
          // set up or arguments for our custom query
          $query_args_latest = array(
            'post_type' => 'post',
            //'category_name' => 'candidatos,empresas',
            'posts_per_page' => 5,
			'paged' => $paged,
          );
          // create a new instance of WP_Query
          $the_query_latest = new WP_Query( $query_args_latest );
        ?>
        
        <div class="clear25"></div>
        
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<h3 class="color-levuAzul text-uppercase"><strong>Latest news</strong></h3>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
            	<?php if ( $the_query_latest->have_posts() ) : ?>
					<?php while ( $the_query_latest->have_posts() ) : $the_query_latest->the_post(); ?>
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 p0 mb-50 home-latest">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-grey">
                                <div class="col-md-6 col-sm-12 col-xs-12 home-latest-single p0 relative" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;">
                                    <small class="bg-levuAzul color-white p-2 m-0 absolute bottom0 left0 text-uppercase">
                                        <?php echo get_the_date() ?>
                                    </small>
                                </div>
                            </a>
                            <div class="col-md-6 cols-sm-12 col-xs-12 color-levuGris h-100 relative latest-text">
                                <div class="absolute bottom0">
                                    <p class="text-bold m-0 mb-2">
                                        <i class="far fa-comment"></i> <?php echo sizeof(get_comments(array('post_id' => $post->ID, 'status' => 'approve'))); ?>
                                    </p>
                                    <h3 class="mt-0 text-uppercase color-black"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-grey"><strong><?php echo title(7); ?></strong></a></h3>
                                    <small>
                                        <?php
                                        $category = get_the_category();
										$category_names = '';
                                        foreach($category as $cat){
                                            $category_id = $cat->cat_ID;
                                            $category_link = get_category_link( $category_id );
                                            $category_name = $cat->name;
											$category_names .= '<a href="'.$category_link.'">' . $category_name . '</a>, ';
                                        }
										echo substr($category_names, 0, -2);
                                        ?>
                                    </small>
                                    <p class="mb-0"><?php echo excerpt(20); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
				<?php endif; ?>
                <!-- pagination here -->
                <div id="pagination">
                    <?php
					$big = 999999999; // need an unlikely integer
					
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $the_query_latest->max_num_pages
					) );
					?>
                </div>
            </div>
            
            
            <!-- SIDEBAR -->
            <?php get_sidebar(); ?>
            <!-- .SIDEBAR -->
            
        </div>
        
        <div class="clear50"></div>
            
  </div> <!-- /.container -->
</section>

<?php get_footer(); ?>