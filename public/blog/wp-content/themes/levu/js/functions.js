$(document).ready(function(){
    $('#pagination').children('.page-numbers').addClass('btn btn-default');
	
	$('#aLogin').click(function(e){
		e.preventDefault();
		$('#siteLogin').toggle();
	});
	
	$.get( "/ajax/csrf/", function( _token ) {
		console.log(_token);
		$('head').append('<meta name="csrf-token" content="'+_token+'">');
		$('#siteLogin-form').append('<input type="hidden" name="_token" id="csrf-token" value="'+_token+'" />');
	});
});
$(window).scroll(function() {    
    var _scroll = $(window).scrollTop();
     //>=, not <=
    if (_scroll >= 100) {
        //clearHeader, not clearheader - caps H
        $("header").addClass('sticky');
    }else{
    	$("header").removeClass("sticky");
    }
	$('#siteLogin').hide();
	var _header_h = $('header').outerHeight();
	$('#siteLogin').css({'top':_header_h});
	
});
function toggleOficinas(_img, _address){
	$('#mainOfis #ofiImg').html( _img );
	$('#mainOfis #ofiAddress').html( _address );
	$('#mainOfis').fadeIn('fast');
}