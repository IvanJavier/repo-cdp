<?php
get_header(); ?>
<section id="content" class="bg-white">
	<div class="container">

        <main id="main" class="site-main" role="main">
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <?php the_title( '<h1 class="mt-50 mb-40 text-uppercase text-bold">', '</h1>' ); ?>
                        <hr class="border-grey">
                    </div><!-- .entry-header -->
                	
                    <div class="col-md-12 col-sm-12 col-xs-12 pb-50">
                    	<?php
							the_content();
						?>
                    </div>
                    
                </article><!-- #post-## -->
    
            <?php // End of the loop.
            endwhile;
            ?>
    
        </main><!-- .site-main -->
        
    </div>
</section>

<?php get_footer(); ?>
