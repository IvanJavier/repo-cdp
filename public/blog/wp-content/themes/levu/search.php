<?php get_header(); ?>

<section id="content" class="pt-85">
	<div class="container">
        
        <div class="clear25"></div>
        
        <div class="row">
        	<div class="col-md-12 col-sm-12 col-xs-12">
            	<h3 class="color-levuAzul text-uppercase"><strong><?php echo sprintf( __( '%s results for '), $wp_query->found_posts ); echo '&laquo;'.get_search_query().'&raquo;'; ?></strong></h3>
            </div>
            <div class="col-md-8 col-sm-12 col-xs-12">
            	<?php if ( have_posts() ) : ?>
					<?php while ( have_posts() ) : the_post(); ?>
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <div class="col-md-12 col-sm-12 col-xs-12 p0 mb-50 home-latest">
                            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-grey">
                                <div class="col-md-6 col-sm-12 col-xs-12 home-latest-single p0 relative" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;">
                                    <small class="bg-levuAzul color-white p-2 m-0 absolute bottom0 left0 text-uppercase">
                                        <?php echo get_the_date() ?>
                                    </small>
                                </div>
                            </a>
                            <div class="col-md-6 cols-sm-12 col-xs-12 color-levuGris h-100 relative">
                                <div class="absolute bottom0">
                                    <p class="text-bold m-0 mb-2">
                                        <i class="far fa-comment"></i> <?php echo sizeof(get_comments(array('post_id' => $post->ID, 'status' => 'approve'))); ?>
                                    </p>
                                    <h3 class="mt-0 text-uppercase color-black"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-grey"><strong><?php echo title(7); ?></strong></a></h3>
                                    <small>
                                        <?php
                                        $category = get_the_category();
										$category_names = '';
                                        foreach($category as $cat){
                                            $category_id = $cat->cat_ID;
                                            $category_link = get_category_link( $category_id );
                                            $category_name = $cat->name;
											$category_names .= '<a href="'.$category_link.'">' . $category_name . '</a>, ';
                                        }
										echo substr($category_names, 0, -2);
                                        ?>
                                    </small>
                                    <p class="mb-0"><?php echo excerpt(20); ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
				<?php endif; ?>
                <!-- pagination here -->
                <div id="pagination">
                    <?php
					$big = 999999999; // need an unlikely integer
					
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $the_query_latest->max_num_pages
					) );
					?>
                </div>
            </div>
            
            
            <!-- SIDEBAR -->
            <?php get_sidebar(); ?>
            <!-- .SIDEBAR -->
            
        </div>
        
        <div class="clear50"></div>
            
  </div> <!-- /.container -->
</section>
   
<?php get_footer(); ?>
