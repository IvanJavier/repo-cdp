<div class="search-form">
    <form method="get" id="searchform" action="<?php bloginfo('home'); ?>/">
    	<div class="col-md-12 p0">
        	<div class="">
            	<div class="col-md-10 p0">
                	<input type="text" class="form-control search-input" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" placeholder="Search..." />
                </div>
                <div class="col-md-2 pr-0">
                	<button type="button" id="searchsubmit" class="btn btn-default bg-levuAzul btn-search color-white border0 borderNone" onClick="$('#searchform').submit();"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
