<aside class="col-md-4">
	<?php if ( is_active_sidebar( 'side-bar-top' ) ) : ?>
        <div id="side-bar-top" class="widget-area col-md-12 col-sm-12 col-xs-12 mb-25" role="complementary">
            <?php dynamic_sidebar( 'side-bar-top' ); ?>
        </div>
    <?php endif; ?>    
    <?php if ( is_active_sidebar( 'side-bar-middle' ) ) : ?>
        <div id="side-bar-middle" class="widget-area col-md-12 col-sm-12 col-xs-12" role="complementary">
            <?php //dynamic_sidebar( 'side-bar-middle' ); ?>
            <div class="widget-odd widget-last widget-first widget-1 popular-posts-header widget popular-posts">
				<h4 class="text-uppercase mt-0 mb-10 color-levuAzul"><strong>Most read articles</strong></h4>
                <?php
					$args = array(
						'post_type' => 'post',
						'stats_views' => 0,
						'stats_date' => 1,
						'order_by' => 'views',
						'range' => 'all',
						'limit' => 5,
						'wpp_start' => '<ul class="wpp-list">',
						'wpp_end' => '</ul>',
						'thumbnail_width' => 75,
						'thumbnail_height' => 75,
						'post_html' => '<li>{thumb} {title} <span class="wpp-meta post-stats">{stats}</span></li>',
						'title_length' => 30
					);
					wpp_get_mostpopular($args);
				?>
            </div>
        </div>
    <?php endif; ?>
    
    <div id="side-bar-ourpick" class="widget-area col-md-12 col-sm-12 col-xs-12 mb-25" role="complementary">
        <div class="widget-odd widget-last widget-second widget-2 widget">
            <h4 class="text-uppercase mt-0 mb-10 color-levuAzul"><strong>Our pick</strong></h4>
            <?php
			  // set up or arguments for our custom query
			  $query_args_ourpick = array(
				'post_type' => 'post',
				'category_name' => 'our-pick',
				'posts_per_page' => 1
			  );
			  // create a new instance of WP_Query
			  $the_query_ourpick = new WP_Query( $query_args_ourpick );
			?>
            <?php if ( $the_query_ourpick->have_posts() ) : ?>
				<?php while ( $the_query_ourpick->have_posts() ) : $the_query_ourpick->the_post(); ?>
                    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <div class="col-md-12 col-sm-12 col-xs-12 p0 mb-50 home-latest">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-grey">
                            <div class="col-md-12 col-sm-12 col-xs-12 img-our-pick p0" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;">
                            </div>
                        </a>
                        <div class="clearfix"></div>
                        <p class="mt-3"><?php the_date(); ?> <span class="float-right"><i class="far fa-comment"></i> <?php echo sizeof(get_comments()); ?></span></p>
                        <h4 class="text-uppercase color-black"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="color-grey"><strong><?php echo title(5); ?></strong></a></h4>
                        <p><?php echo excerpt(10); ?></p>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
    
    <?php if ( is_active_sidebar( 'side-bar-bottom' ) ) : ?>
        <div id="side-bar-bottom" class="widget-area col-md-12 col-sm-12 col-xs-12 bg-white mb-25" role="complementary">
            <?php dynamic_sidebar( 'side-bar-bottom' ); ?>
        </div>
    <?php endif; ?>
</aside>