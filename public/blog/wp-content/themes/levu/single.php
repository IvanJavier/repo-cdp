<?php
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<?php $cat = get_the_category(); ?>

	
            <?php
            // Start the loop.
            while ( have_posts() ) : the_post(); ?>
            <?php
				// get post categories
				$cat_args = array(
					'order' => 'ASC',
					'orderby' => 'parent'
				  );
				$cats = wp_get_post_categories($post->ID, $cat_args);
				$cat_links = '';
				$date_css = '';
				$title_css = '';
				foreach ( $cats as $category ) {
					$cat_name = get_category( $category );
					//print_r($cat_name);
					if($cat_name->parent == 0){
						switch($cat_name->slug){
							case 'empresas':
								$date_css = 'single-date-red';
								$title_css = '--color-ctqRed';
							break;
							case 'candidatos':
								$date_css = 'single-date-orange';
								$title_css = '--color-ctqOrange';
							break;
						}
					}
					$current_cat = get_cat_name($category);
					$cat_link = get_category_link($category);
					$cat_links .= "<a href='$cat_link'>";
					$cat_links .= $current_cat;
					$cat_links .= "</a>, ";
				}
				$cat_links = substr($cat_links, 0, -2);
			?>
            
			<?php $commentsQty = get_comments_number( $post->ID ); ?>
            
            <div class="container">
                            
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                
                	<!-- Featured image -->
					<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                    <div class="col-md-12 relative post-feat-img p0 text-center" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;">
                    </div>
                	<div class="clearfix"></div>
                    <div class="entry-content relative">
                        <!-- entry header -->
                        <div class="col-md-10 col-sm-10 col-xs-10 pull-center bg-whiteGrey pt-5 pb-5">
							<?php the_title( '<h1 class="m0 text-uppercase color-black"><strong>', '</strong></h1>' ); ?>
                            <h5 class="cat-header text-uppercase">
                            <?php echo $cat_links; ?>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                        <!-- .entry-header -->
    
                        <div class="entry-text col-md-10 pull-center text-justify pb-25">
                            <div class="pull-left mb-50">
                                <?php the_date() ?>
                            </div>
                            <div class="clearfix"></div>
                            <!--<p>Por <?php the_author(); ?></p>-->
                            <?php
                                the_content();
                            ?>
                        </div>
                        
                        <div class="entry-text col-md-10 pull-center pt-25 pb-25">
                            <?php
                            if ( is_singular( 'post' ) ) {
                                // Previous/next post navigation.
                                the_post_navigation( array(
                                    'in_same_term'               => true,
                                    'screen_reader_text' => __( '&nbsp;' ),
                                    'next_text' => '<div class="bg-black color-white text-right text-uppercase" aria-hidden="true"><strong>Next post <i class="fas fa-long-arrow-alt-right"></i></strong></div>',
                                    'prev_text' => '<div class="bg-black color-white text-left text-uppercase" aria-hidden="true"><strong><i class="fas fa-long-arrow-alt-left"></i> Previous post</strong></div>',
                                ) );
                            }
                            ?>
                            <div class="clearfix"></div>
                        </div>
                        
                    </div>
                    <!-- .entry-content -->
                    
                </article><!-- #post-## -->
            </div> <!-- .CONTAINER -->
                
            <!-- COMENTARIOS -->
            <div class="bg-white pt-25 pb-25 mb-25">
            	<div class="container">
                    <div class="col-md-10 pull-center">
                    	<?php if ( $commentsQty > 0 ) : ?>
                        <h3 class="mb-25">Comments</h3>
                        <?php else : ?>
                        <h3 class="mb-25">No comments</h3>
                        <?php endif; ?>
                        <?php 
                            $args = array(
                                    'post_id' => $post->ID,
                                    'orderby' => array('comment_date'),
                                    'order' => 'DESC',
									'status' => 'approve'
                            );
                            $comments = get_comments($args);
                            foreach($comments as $comment) :
                                echo '<h6 class="color-grey-light">Por '.$comment->comment_author . '<br>' . $comment->comment_date . '</h6><p class="pb-25" style="border-bottom:1px solid #f1f1f1;">' . $comment->comment_content.'</p>';
                            endforeach;
                        ?>
                    
                    </div>
                    
                    <div class="col-md-10 pull-center">
                    
                    <?php // If comments are open or we have at least one comment, load up the comment template.
                        comment_form(
                            array(
                                'title_reply'=>'Leave a comment',
                                'comment_field' => '<textarea class="form-control mb-3 bg-whiteGrey" id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="Comment"></textarea>',
                                'comment_notes_before' => '',
                                'label_submit' => 'Send comment',
								'class_submit' => 'bg-levuAzul btn color-white pl-15 pr-15 submit text-uppercase',
                                'fields' => array(
									'author' => '<div class="col-md-6 pl0 mb-25"><input class="form-control bg-whiteGrey" placeholder="Name*" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
									'email'  => '<div class="col-md-6 pr0 mb-25"><input class="form-control bg-whiteGrey" placeholder="Email*" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>')
							)
                        );
                    ?>
                    </div>
                </div>
            </div>
            <!-- .COMENTARIOS -->
                
    		<!-- RELATED POSTS -->
            <!--<div class="bg-white">
            	<div class="container">
                    <div class="col-md-10 pull-center">
                    	<?php //related_posts(); ?>
                    </div>
                </div>
            </div>-->
            <!-- .RELATED POSTS -->
                
            <?php // End of the loop.
            endwhile;
            ?>
    
    </div><!-- .container -->


<?php get_footer(); ?>
