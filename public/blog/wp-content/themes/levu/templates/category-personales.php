
<section id="content" class="bg-white">
	<div class="container">

    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
        	<h1 class="mt-50 mb-40 text-uppercase text-bold color-lightGreen">Artículos para personal físicas y<br>interesadas en el rubro</h1>
            <hr class="border-grey">
        </div>
        
        <div class="col-md-9 col-sm-12 col-xs-12">
        
        	<?php if ( $the_query->have_posts() ) : ?>

            <!-- the loop -->
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
            	<?php
				// get post categories
				$cat_args = array(
					'order' => 'ASC',
					'orderby' => 'parent'
				  );
				$cats = wp_get_post_categories($post->ID, $cat_args);
				$cat_links = '';
				foreach ( $cats as $category ) {
					$current_cat = get_cat_name($category);
					$cat_link = get_category_link($category);
					$cat_links .= "<a href='$cat_link'>";
					$cat_links .= $current_cat;
					$cat_links .= "</a>, ";
				}
				$cat_links = substr($cat_links, 0, -2);
				?>
                <div class="col-md-12 p0 mb-25 bg-whiteGrey">
                	
                    <?php if ( has_post_thumbnail() ) : ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        	<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                            <div class="col-md-5 relative post-img p0 text-center">						
                            	<?php echo get_the_post_thumbnail( $post->ID, 'large', array( 'class' => 'img-h100 pull-center' ) ); ?>
                        		<div class="post-list-date p5 bg-lightGreen color-white">
									<?php the_date() ?>
                                </div>
                            </div>
                        </a>
                    <?php else : ?>
                    	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                            <div class="col-md-5 post-img bg-grey">
                        	</div>
                        </a>
					<?php endif; ?>
                    <div class="col-md-7 post-info pt-25 pb-25">
                    	<h5 class="cat-header text-uppercase">
                        	<?php echo $cat_links; ?>
							<?php //the_category(', '); ?>
                        </h5>
                        
                        <h4 class="blog-post-title text-uppercase text-bold"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                        
                        <?php
							$thetitle = $post->post_title; /* or you can use get_the_title() */
							echo wp_trim_words( $thetitle, 6, '...' );
							?>
                        </a></h4>
                        <p><?php the_excerpt(); ?></p>
                        <div class="col-md-12 p0">
                            <div class="col-md-9 pr-0 pt-10" style="border-bottom: 1px solid #333;"></div>
                            <div class="col-md-3 text-right">
                                <a href="<?php the_permalink(); ?>" class="btn btn-xs btn-success pr-15 pl-15 color-white bg-lightGreen text-uppercase bt0 br0 bb0 bl0">Leer más</a>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endwhile; ?>
            <!-- end of the loop -->
            <div class="clearfix"></div>
            <!-- pagination here -->
            <div id="pagination" class="col-md-12 text-center mt-30 mb-50">
                <?php
                  if (function_exists(custom_pagination)) {
                    custom_pagination($the_query->max_num_pages,"5",$paged);
                  }
                ?>
            </div>
        
          <?php wp_reset_postdata(); ?>
        
          <?php else:  ?>
            <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
          <?php endif; ?>
        	
        </div>
      
      	<!-- SIDEBAR -->
		<?php get_sidebar(); ?>
        <!-- .SIDEBAR -->
        	
        </div>
        
    </div> <!-- /.row -->
    
	
    
  </div><!-- .CONTAINER -->
  </section><!-- .CONTENT -->