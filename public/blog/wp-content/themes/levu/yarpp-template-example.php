<?php 
/*
YARPP Template: Simple
Author: mitcho (Michael Yoshitaka Erlewine)
Description: A simple example YARPP template.
*/
?>
<h3 class="mt-50">You may also like</h3>

<?php if (have_posts()):?>
<div class="row mt-15">
<?php while (have_posts()) : the_post(); ?>

	<?php if (has_post_thumbnail()):?>
	
		<div class="col-md-4 cols-sm-12 col-xs-12">                    
			<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
			<div class="col-md-12 relative p0 single-related" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;"></div>
			<div class="col-md-12 col-sm-12 col-xs-12 pt-20 pb-20 bg-white">
				<h4 style="width:100%;" class="text-uppercase text-bold">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
						<?php
						$thetitle = $post->post_title; /* or you can use get_the_title() */
						$getlength = strlen($thetitle);
						$thelength = 28;
						echo substr($thetitle, 0, $thelength);
						if ($getlength > $thelength) echo "...";
						?>
					</a>
				</h4>
				<h5 class="cat-header text-uppercase">
					<?php $cat = get_the_category();  ?>
					<?php echo '<a href="' . get_category_link($cat[0]->cat_ID) . '" title="' . $cat[0]->name . '">' . $cat[0]->name . '</a>'; ?>  / <?php the_date() ?>
				</h5>
			</div>
			
		</div>
   
	<?php endif; ?>
		
	<?php endwhile; ?>
	
	</div>

<?php else: ?>
<p>No related articles</p>
<?php endif; ?>