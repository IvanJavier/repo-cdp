<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: mitcho (Michael Yoshitaka Erlewine)
*/ ?>
<div class="row mt-50">
<div class="col-md-12 col-sm-12 col-xs-12">
    <h3 class="text-uppercase text-center">Artículos relacionados</h3>
    <?php if (have_posts()):?>
    	<div class="row mt-50">
        <?php while (have_posts()) : the_post(); ?>
        
            <?php if (has_post_thumbnail()):?>
            
                <div class="col-md-4 cols-sm-12 col-xs-12 p15"> 
                    <div class="col-md-12 cols-sm-12 col-xs-12 p15 single-related-container">                    
                        <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); ?>
                        <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                            <div class="col-md-12 relative p0 single-related" style="background:url(<?php echo $url; ?>) no-repeat center; background-size:cover;"></div>
                        </a>
                        <div class="col-md-12 col-sm-12 col-xs-12 p0 pt-20 bg-white">
                            <p class="text-uppercase text-bold text-left">
                                <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
                                    <?php
                                    $thetitle = $post->post_title; /* or you can use get_the_title() */
                                    $getlength = strlen($thetitle);
                                    $thelength = 30;
                                    echo substr($thetitle, 0, $thelength);
                                    if ($getlength > $thelength) echo "...";
                                    ?>
                                </a>
                            </p>
                            <h6 class="cat-header text-uppercase">
                                <?php $cat = get_the_category();  ?>
                                <?php echo '<a href="' . get_category_link($cat[0]->cat_ID) . '" title="' . $cat[0]->name . '">' . $cat[0]->name . '</a>'; ?>
                            </h6>
                        </div>
                    </div>
                </div>
           
            <?php endif; ?>
            
        <?php endwhile; ?>
        
        </div>
    
    <?php else: ?>
    <p>No se encontraron artículos relacionados.</p>
    <?php endif; ?>
</div>
</div>