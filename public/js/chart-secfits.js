$(window).load(function(){
    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(totalSectfits);
});
function viewSecFits(){
    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(totalSectfits);
}

function totalSectfits(){

    var id = $('#vacanteId').val();
    var chartDiv = document.getElementById('totalSecfits');
    $('#totalSecfits').empty();
    $('#totalSecfitsError').empty();
    $.ajax({
        url: '/dashboard/admin/getTotalSecfits',
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        data: {id:id},
        success: function (response) {
            //$('.loading').fadeIn(90000).html(response);
            if(Object.keys(response).length == 0){
                $('#totalSecfitsError').append("<h4>No existen resultados que se puedan gráficar<h4>");
            }else{
                //console.log(response);
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Candidatos');
                data.addColumn('number', 'Calificación');
                $.each(response, function (i, response) { 
                    var nombre = response.name;
                    var calificacion = response.calificacion;
                data.addRows([[nombre,calificacion]]);
                });
                
                var materialOptions = {
                width: 700,
                height: 300,
                colors: ['#5b78f1','#b8bdd2'],
                chart: {
                    title: 'Calificación asignada por el HeadHunter con base en la entrevista',
                    subtitle: 'Porcentaje SEC Fits (Empatía en habilidades, experiencia y cultura vs Primer Escaneo)'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                hAxis: {
                    maxTextLines: 20,
                    textStyle: {
                        fontSize: 10,
                    }
                },
                vAxis: {
                    viewWindow: { min: 80, max: 100 },
                    beginAtZero: false,
                    minValue: 85,
                    maxValue: 100,
                    gridlines: {count: 100},
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 9,
                    }
                },
                 bar: { groupWidth: '65%' },
                };

                function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                }
                drawMaterialChart(); 
            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}
