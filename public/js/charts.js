$(".datepicker").datepicker({
        dateFormat: "yy-mm-dd",
        changeMonth: true,
        changeYear: true,
        maxDate: "today",
});

function drawChartOpenVacants(){

      let from = $("#fromDateOpenVacants").val();
      let to = $("#toDateOpenVacants").val();
      if (from == '' && to == '' || from != '' && to == '' || from == '' && to != ''){
          swal({
            title: "Error",
            text: "Completa todos los campos",
            icon: "error",
        });
      }
      else{
            google.charts.load('current', {'packages':['corechart', 'bar']});
            google.charts.setOnLoadCallback(openVacants);
      }
};

function drawChartClosedVacants(){

      let from = $("#fromDateClosedVacants").val();
      let to = $("#toDateClosedVacants").val();
      if (from == '' && to == '' || from != '' && to == '' || from == '' && to != ''){
          swal({
            title: "Error",
            text: "Completa todos los campos",
            icon: "error",
            });
      }
      else{
            google.charts.load('current', {'packages':['corechart', 'bar']});
            google.charts.setOnLoadCallback(closedVacants);
      }
};

function drawChartVacantsClosedWithHiring(){

      let from = $("#fromDateVacantsClosedWithHiring").val();
      let to = $("#toDateVacantsClosedWithHiring").val();
      if (from == '' && to == '' || from != '' && to == '' || from == '' && to != ''){
          swal({
            title: "Error",
            text: "Completa todos los campos",
            icon: "error",
            });
      }
      else{
            google.charts.load('current', {'packages':['corechart', 'bar']});
            google.charts.setOnLoadCallback(vacantsClosedWithHiring);
      }
};

function drawChartVacantsClosedWithOutHiring(){

      let from = $("#fromDateVacantsClosedWithOutHiring").val();
      let to = $("#toDateVacantsClosedWithOutHiring").val();
      if (from == '' && to == '' || from != '' && to == '' || from == '' && to != ''){
          swal({
            title: "Error",
            text: "Completa todos los campos",
            icon: "error",
            });
      }
      else{
            google.charts.load('current', {'packages':['corechart', 'bar']});
            google.charts.setOnLoadCallback(vacantsClosedWithOutHiring);
      }
};

function drawChartSellers(){

      let from = $("#fromDateSellers").val();
      let to = $("#toDateSellers").val();
      if (from == '' && to == '' || from != '' && to == '' || from == '' && to != ''){
          swal({
            title: "Error",
            text: "Completa todos los campos",
            icon: "error",
            });
      }
      else{
            google.charts.load('current', {'packages':['corechart', 'bar']});
            google.charts.setOnLoadCallback(sellers);
      }
};

function drawChartVacantsWithAssurance(){

      let from = $("#fromDateVacantsAssurance").val();
      let to = $("#toDateVacantsAssurance").val();
      if (from == '' && to == '' || from != '' && to == '' || from == '' && to != ''){
          swal({
            title: "Error",
            text: "Completa todos los campos",
            icon: "error",
            });
      }
      else{
            google.charts.load('current', {'packages':['corechart', 'bar']});
            google.charts.setOnLoadCallback(vacantsWithAssurance);
      }
};

function openVacants(){

    var chartDiv = document.getElementById('openVacants');
    let from = $("#fromDateOpenVacants").val();
    let to = $("#toDateOpenVacants").val();
    $('#openVacants').empty();
    $.ajax({
        url: 'getOpenVacants',
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (xhr){
            $('#openVacants').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {
            //$('.loading').fadeIn(90000).html(response);
           $("#fromDateOpenVacants").val("");
           $("#toDateOpenVacants").val("");
           $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#openVacants').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                $('#savePDF').show();
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#5b78f1','#b8bdd2'],
                chart: {
                    title: 'Vacantes Abiertas',
                    subtitle: 'Vacantes que se encuentran abiertas'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                   // brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                 bar: { groupWidth: '75%' },
                };

                function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                google.visualization.events.addListener(materialChart, 'ready', function() {
                    var canvas,domURL,imageURI,svgParent;

                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputOpenVacants').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI;
                    });
                }
                drawMaterialChart(); 
            } 
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function closedVacants(){

    var chartDiv = document.getElementById('closedVacants');
    let from = $("#fromDateClosedVacants").val();
    let to = $("#toDateClosedVacants").val();
    $('#closedVacants').empty();
    $.ajax({
        url: 'getClosedVacants',
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (xhr){
            $('#closedVacants').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {

            $("#fromDateClosedVacants").val("");
            $("#toDateClosedVacants").val("");
            $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#closedVacants').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#f93434','#b8bdd2'],
                chart: {
                    title: 'Vacantes Cerradas',
                    subtitle: 'Vacantes que se encuentran cerradas'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                    //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                };

                function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                google.visualization.events.addListener(materialChart, 'ready', function() {
                    var canvas,domURL,imageURI,svgParent;

                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputClosedVacants').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI;
                    });                
                }
                drawMaterialChart();        
            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function vacantsClosedWithHiring(){

    var chartDiv = document.getElementById('vacantsClosedWithHiring');
    let from = $("#fromDateVacantsClosedWithHiring").val();
    let to = $("#toDateVacantsClosedWithHiring").val();
    $.ajax({
        url: 'getVacantsClosedWithHiring',
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (xhr){
            $('#vacantsClosedWithHiring').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {
            $("#fromDateVacantsClosedWithHiring").val("");
            $("#toDateVacantsClosedWithHiring").val("");
            $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#vacantsClosedWithHiring').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#28cb59','#b8bdd2'],
                chart: {
                    title: 'Vacantes Cerradas con contratación',
                    subtitle: 'Vacantes que se encuentran cerradas con contratación'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                    //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                };

                function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                google.visualization.events.addListener(materialChart, 'ready', function() {
                    var canvas,domURL,imageURI,svgParent;

                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputVacantsClosedWithHiring').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI;
                    });                
                }
                drawMaterialChart();
            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function vacantsClosedWithOutHiring(){

    var chartDiv = document.getElementById('vacantsClosedWithOutHiring');
    let from = $("#fromDateVacantsClosedWithOutHiring").val();
    let to = $("#toDateVacantsClosedWithOutHiring").val();
    $.ajax({
        url: 'getVacantsClosedWithOutHiring',
        dataType: "JSON",
        contentType: "application/json; charset=utf-8",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (xhr){
            $('#vacantsClosedWithOutHiring').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {
            $("#fromDateVacantsClosedWithOutHiring").val("");
            $("#toDateVacantsClosedWithOutHiring").val("");
            $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#vacantsClosedWithHiring').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#f66e33','#b8bdd2'],
                chart: {
                    title: 'Vacantes cerradas sin contratación',
                    subtitle: 'Vacantes que se encuentran cerradas sin contratación'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                    //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                };

                function drawMaterialChart() {
                var materialChart = new google.charts.Bar(chartDiv);
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                google.visualization.events.addListener(materialChart, 'ready', function() {
                    var canvas,domURL,imageURI,svgParent;

                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputVacantsClosedWithOutHiring').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI;
                    });                 
                }
                drawMaterialChart();

            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function sellers(){

    var chartDiv = document.getElementById('sellers');
    let from = $("#fromDateSellers").val();
    let to = $("#toDateSellers").val();
    let status = $("#status").val();
    if(status == 0){
        swal({
        title: "Ooppss!",
        text: "No seleccionaste alguna opción",
        icon: "warning",
        }); 
    }else{
        $.ajax({
            url: 'getSellers',
            dataType: "JSON",
            contentType: "application/json; charset=utf-8",
            type: "GET",
            data: {from:from, to:to, status:status},
            beforeSend: function (xhr){
                $('#sellers').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
            },
            success: function (response) {
                $("#fromDateSellers").val("");
                $("#toDateSellers").val("");
                $('select[name="status"]').find('option[value="0"]').attr("selected",true);
                $('.loading').hide();
                if(Object.keys(response).length == 0){
                    $('#sellers').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
                }else{
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Vendedor');
                    data.addColumn('number', 'N° Vacantes');
                    data.addColumn('number', 'Tarifa');
                    $.each(response, function (i, response) { 
                        var nombre = response.nombre;
                        var count = response.countVacantes;
                        var tarifa = response.tarifa;
                    data.addRows([[nombre,count,tarifa]]);
                    });
                    
                    var materialOptions = {
                    width: 500,
                    height: 350,
                    colors: ['#f9ef26','#b8bdd2'],
                    chart: {
                        title: 'Oportunidades',
                        subtitle: 'Montos totales de vendedores'
                    },
                    series: {
                        0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                        1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                    },
                    axes: {
                        y: {
                        distance: {label: 'N° Vacantes'}, // Left y-axis.
                        //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                        }
                    },
                    hAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 6,
                        }
                    },
                    vAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 8,
                        }
                    },
                    };

                    function drawMaterialChart() {
                    var materialChart = new google.charts.Bar(chartDiv);
                    materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                    google.visualization.events.addListener(materialChart, 'ready', function() {
                        var canvas,domURL,imageURI,svgParent;

                        // add svg namespace to chart
                        svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                        svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                        // create image URI
                        domURL = window.URL || window.webkitURL || window;
                        imageNode = materialChart.getContainer().cloneNode(true);
                        imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                        image = new Image();
                        image.onload = function() {
                            canvas = document.createElement('canvas');
                            canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                            canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                            canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                            $('#inputSellers').val(canvas.toDataURL('image/png'));
                        }
                        image.src = imageURI;
                        });                 
                    }
                    drawMaterialChart();

                }
            },
            error: function (response){
            console.log(JSON.stringify(response));
            }
        });
    }
}

function vacantsWithAssurance(){
    
    var chartDiv = document.getElementById('vacantsWithAssurance');
    let from = $("#fromDateVacantsAssurance").val();
    let to = $("#toDateVacantsAssurance").val();
    $('#vacantsWithAssurance').empty();
        $.ajax({
            url: 'getVacantsWithAssurance',
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            type: "GET",
            data: {from:from, to:to},
            beforeSend: function (xhr){
                $('#vacantsWithAssurance').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
            },
            success: function (response) {
                $("#fromDateVacantsAssurance").val("");
                $("#toDateVacantsAssurance").val("");
                $('select[name="status"]').find('option[value="0"]').attr("selected",true);
                $('.loading').hide();
                if(Object.keys(response).length == 0){
                    $('#vacantsWithAssurance').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
                }else{
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Vendedor');
                    data.addColumn('number', 'N° Vacantes');
                    data.addColumn('number', 'Tarifa');
                    $.each(response, function (i, response) { 
                        var nombre = response.nombre;
                        var count = response.countVacantes;
                        var tarifa = response.tarifa;
                    data.addRows([[nombre,count,tarifa]]);
                    });
                    
                    var materialOptions = {
                    width: 500,
                    height: 350,
                    colors: ['#7d52b0','#b8bdd2'],
                    chart: {
                        title: 'Vacantes con garantía',
                        subtitle: 'Vacantes que cuentan con una garantía'
                    },
                    series: {
                        0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                        1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                    },
                    axes: {
                        y: {
                        distance: {label: 'N° Vacantes'}, // Left y-axis.
                        //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                        }
                    },
                    hAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 6,
                        }
                    },
                    vAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 8,
                        }
                    },
                    };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputVacantsWithAssurance').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                }
            },
            error: function (response){
            console.log(JSON.stringify(response));
        }
        });
   
}

function savePDF(){
        var d = new Date();
        var year = d.getFullYear();
        var month = d.getMonth()+1;
        var day = d.getDate();
        var date = year+"/"+month+"/"+day;
        var pdfDoc = new jsPDF({
            orientation: 'landscape'
        });
        let openVacants = $('#inputOpenVacants').val(),
            closedVacants = $('#inputClosedVacants').val(),
            vacantsClosedWithHiring = $('#inputVacantsClosedWithHiring').val(),
            vacantsClosedWithOutHiring = $('#inputVacantsClosedWithOutHiring').val();            
            sellers = $('#inputSellers').val();

        if(openVacants != '' && closedVacants == '' && vacantsClosedWithHiring == '' && vacantsClosedWithHiring == '' && sellers == ''){
            swal({
            title: "Ooppss!",
            text: "No se encuentran todas las gráficas",
            icon: "warning",
            });            
        }else{
            pdfDoc.setFontSize(30);
            pdfDoc.text(10, 20, 'Reporte general');
            pdfDoc.addImage(openVacants, 'PNG', 10, 40, 150, 140, undefined,'FAST');
            pdfDoc.addImage(closedVacants, 'PNG', 153, 40,150,140,undefined,'FAST');
            pdfDoc.addPage();
            pdfDoc.addImage(vacantsClosedWithHiring, 'PNG', 10, 40, 150, 140, undefined,'FAST');
            pdfDoc.addImage(vacantsClosedWithOutHiring, 'PNG', 153, 40, 150, 140, undefined,'FAST');
            pdfDoc.addPage();
            pdfDoc.addImage(sellers, 'PNG', 10, 40, 150, 140, undefined,'FAST');
            pdfDoc.save('Reporte_'+date+'.pdf');
        }
}
