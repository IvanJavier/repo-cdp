    // this is the id of the form
    $("#formFreelance").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        var data = form.serialize();
 
        $.ajax({
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            dataType: "json",
            url: url,
            data: data, // serializes the form's elements.
            beforeSend: function(){
               swal({
                        title: "Espera un momento",
                        text: "Procesando petición...",
                        icon: "info",
                        timer: 6000,
                        button: false
                    }); 
            },
            success: function(response){
                if(response.success == true){
                    swal({
                        title: "Exito",
                        text: "Usuario registrado correctamente",
                        icon: "success",
                        timer: 3000,
                        button: false
                    });
                }else{
                        swal({
                        title: "Error",
                        text: "Usuario ya existe",
                        icon: "error",
                        timer: 3000,
                        button: false
                    });
                }
                $('#formFreelance')[0].reset();
            }
        });
    });

        $("#editFreelance").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = form.attr('action');
        var data = form.serialize();
 
        $.ajax({
            type: "POST",
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            dataType: "json",
            url: url,
            data: data, // serializes the form's elements.
            beforeSend: function(){
               swal({
                        title: "Espera un momento",
                        text: "Procesando petición...",
                        icon: "info",
                        timer: 6000,
                        button: false
                    }); 
            },
            success: function(response){
                if(response.success == true){
                    swal({
                        title: "Exito",
                        text: "Usuario actualizado correctamente",
                        icon: "success",
                        timer: 3000,
                        button: false
                    });
                }else{
                        swal({
                        title: "Error",
                        text: "Ocurrio un problema",
                        icon: "error",
                        timer: 3000,
                        button: false
                    });
                }
                $('#editFreelance')[0].reset();
            }
        });
    });

        