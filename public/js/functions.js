
$(document).ready(function(){
	
	$('[data-toggle="tooltip"]').tooltip();
	
	$('form').submit(function(e){
		$('#mainOverlay').fadeIn('fast').css({'display':'flex'});
		var _formId = $(this).attr('id');		
		if(_formId == 'editProfileForm'){
			var _file = $('#profilePic').val();
			var _fileOld = $('#profilePicOld').val();
			if(_file == '' && _fileOld == ''){
				alert('Selecciona una foto de perfil.');
				$('#mainOverlay').fadeOut('fast');
				return false;
			}
			
			var _val = $('input[name=updatePwdRadio]:checked').val();
			if(_val == '1'){
				var _val1 = $('input#password').val();
				var _val2 = $('input#password_repeat').val();
				if(_val1 == _val2){
					return true;
				}else{
					alert('Las contraseñas no coinciden.');
					$('#mainOverlay').fadeOut('fast');
					return false;
				}
			}else{
				return true;
			}
		}else{
			return true;
		}
	});
	
	if($('#side-line').length > 0){
		setTimeout(sideLine,1000);
	}
	
	setTimeout(function(){
		$("#home #welcome-text").addClass('active');
	},1500);
    
	xhr = new XMLHttpRequest();
	xhr.open("GET","img/site/map.svg?v=2.0",false);
	// Following line is just to be on the safe side;
	// not needed if your server delivers SVG with correct MIME type
	xhr.overrideMimeType("image/svg+xml");
	xhr.send("");
	document.getElementById("contact_us_heading")
	  .appendChild(xhr.responseXML.documentElement);

    $('g#cdmx').click(function(){
    	var _img = '<img src="img/site/cdmx.jpg" class="img-fluid">';
    	var _address = '<p><strong>Mexico City, Mexico</strong></p><p>Aristóteles 81 int 202, Col, Polanco IV Sección, Del Miguel Hidalgo C.P. 11550, CDMX.</p><p>+52 (55) 72616531, 72617095, 72619481, 72617171</p>';
    	toggleOficinas(_img, _address);
    });
    $('g#mty').click(function(){
    	var _img = '<img src="img/site/mty.jpg" class="img-fluid">';
    	var _address = '<p><strong>Monterrey, Mexico</strong></p><p>José Vasconcelos 142 Ote, 3er Piso, San Pedro Garza García, Nuevo León, México</p><p>+52 (81) 8356 6700​</p>';
    	toggleOficinas(_img, _address);
    });
	$('g#gdl').click(function(){
    	var _img = '<img src="img/site/gdl.jpg" class="img-fluid">';
    	var _address = '<p><strong>Guadalajara, Mexico</strong></p><p>Torre Mil 500, Av. de las Américas 1254, Piso 10, BEST Corporate offices, Country Club, 44610 Guadalajara, Jal.</p><p>+52 (33) 2101 0798</p>';
    	toggleOficinas(_img, _address);
    });
    $('g#torreon').click(function(){
    	var _img = '<img src="img/site/torreon.jpg" class="img-fluid">';
    	var _address = '<p><strong>Torreón, Mexico</strong></p><p>Boulevard Independencia 3890, Local 6, Col. El Fresno, CP 27018</p>';
    	toggleOficinas(_img, _address);
    });
    $('g#madrid').click(function(){
    	var _img = '<img src="img/site/madrid.jpg" class="img-fluid">';
    	var _address = '<p><strong>Madrid, Spain</strong></p><p>Torpedero Tucuman 22, 1º Derecha, 28001, Madrid</p>';
    	toggleOficinas(_img, _address);
    });
	
	if(window.location.hash) {
		var _hash = window.location.hash.substring(1);
		$('a[name="'+_hash+'"]').addClass('active').parent('li').addClass('active');
	}
	
	$('.go-to').click(function(e) {
		e.preventDefault();
		var hash = $(this).attr("name")
        var aLink = ('#' + $(this).attr('name'));
        if (aLink == 'home') {
            $('html, body').animate({
                scrollTop: $(aLink).offset().top
            }, 0);
        } else {
            $('html, body').animate({
                scrollTop: $(aLink).offset().top
            }, 500, function(){
				window.location.hash = hash;
			});
			//$('nav #home_menu').slideToggle();
        }
		//$('.go-to').removeClass('active');
		//$(this).addClass('active').parent('li').addClass('active');
		
        return false;
    });
	$('#aLogin').click(function(e){
		e.preventDefault();
		$('#siteLogin').toggle();
	});	
	
});
window.onresize = function(event) {
    sideLine();
};
$(window).scroll(function() {    
    var _scroll = $(window).scrollTop();
	// ANIM MENU
    if (_scroll >= 100) {
        $("header").addClass('sticky');
    }else{
    	$("header").removeClass("sticky");
    }
	// HIDE LOGIN FORM
	$('#siteLogin').hide();
	var _header_h = $('header').outerHeight();
	$('#siteLogin').css({'top':_header_h});
	
	// ANIMATE STUFF
	var _videoOffset = $('#videoPresetation').offset();
	if (_scroll >= (_videoOffset.top-400)) {
        $('#home #videoPresetation').addClass('active');
    }
	
	var _videoTextsOffset = $('#videoTexts').offset();
	if (_scroll >= (_videoTextsOffset.top-400)) {
        $('#home #videoTexts .anim').addClass('active');
    }
	
	var _slogan1offset = $('#slogan1').offset();
	if (_scroll >= (_slogan1offset.top-400)) {
        $('#slogan1').addClass('active');
    }
	
	var _slogan2offset = $('#slogan2').offset();
	if (_scroll >= (_slogan2offset.top-400)) {
        $('#slogan2').addClass('active');
    }
	
	var _ourMethosT1offset = $('#our-method-t1').offset();
	if (_scroll >= (_ourMethosT1offset.top-500)) {
        $('#our-method-t1').addClass('active');
    }
	var _ourMethosT2offset = $('#our-method-t2').offset();
	if (_scroll >= (_ourMethosT2offset.top-500)) {
        $('#our-method-t2').addClass('active');
    }
	var _ourMethosT3offset = $('#our-method-t3').offset();
	if (_scroll >= (_ourMethosT3offset.top-500)) {
        $('#our-method-t3').addClass('active');
    }
	
	
	var _servicesT1offset = $('#services-t1 h3').offset();
	if (_scroll >= (_servicesT1offset.top-500)) {
        $('#services-t1 h3').addClass('active');
		setTimeout(function(){
		  $('#services-t2').addClass('active');
		}, 250);
		setTimeout(function(){
		  $('#services-t3').addClass('active');
		}, 500);
		setTimeout(function(){
		  $('#services-t4').addClass('active');
		}, 750);
    }
	
	var _contact_us_headingOffset = $('#contact_us_heading').offset();
	if (_scroll >= (_contact_us_headingOffset.top-500)) {
        $('#contact_us_heading').addClass('active');
    }
});
function toggleOficinas(_img, _address){
	$('#mainOfis #ofiImg').html( _img );
	$('#mainOfis #ofiAddress').html( _address );
	$('#mainOfis').fadeIn();
}
function sideLine(){
    var _h = $('html').outerHeight();
    $('#side-line').css({'height': (_h-85)});
    $('#side-line-inner').css({'height': (_h-85)});

    var welcome_heading = $('#welcome_heading').offset();
    $('#welcome-side-bullet').css({'top': welcome_heading.top - 75});

    var our_mision_heading = $('#our_mision_heading').offset();
    $('#our-mision-side-bullet').css({'top': our_mision_heading.top - 75});

    var services_heading = $('#services_heading').offset();
    $('#services-side-bullet').css({'top': services_heading.top - 75});

    var contact_us_heading = $('#contact_us_heading').offset();
    $('#contact-us-side-bullet').css({'top': contact_us_heading.top - 75});
}
function toggleModal(_elmnt){
	$('#mainModal #modal-title').html( $(_elmnt).attr('data-title') );
	$('#mainModal #modal-submit-btn').html( $(_elmnt).attr('data-submit-btn') );
	$('#mainModal #modal-instructions').html( $(_elmnt).attr('data-instructions') );
	$('#mainModal').fadeIn();
}

function loadSepomex(){
	var ubicacion = $('input[name="ubicacion"]:checked').val();
	if( ubicacion == 'local' ){
		var cp_val = $('input#cp').val();
		var colonia;
		var delmpo;
		var edo;
		var pais;
		$.get( "/ajax/sepomex/"+cp_val, function( result ) {
			if(result.length == 0){
				alert('El CP proporcionado no existe.');
				$('input#cp').val('');
			}else if(result.length == 1){
				$.each(result, function(_key,_val){
					colonia = '<input id="col" type="text" class="form-control dirData" name="col" value="'+_val['col']+'" readonly>';
					delmpo = _val['mpo'];
					edo = _val['edo'];
					pais = _val['pais'];
				});
			}else{
				colonia = '<select id="col" class="form-control" name="col">';
				$.each(result, function(_key,_val){
					colonia += '<option value="'+_val['col']+'">'+_val['col']+'</option>';
					delmpo = _val['mpo'];
					edo = _val['edo'];
					pais = _val['pais'];
				});
				colonia += '</select>';
			}
			$('#sepomexCol').html(colonia);
			$('input#delmpo').val(delmpo);
			$('input#edo').val(edo);
			$('input#pais').val(pais);
			
		});
	}else{
		colonia = '<input id="col" type="text" class="form-control dirData" name="col" value="">';
		$('#sepomexCol').html(colonia);
		$('input#delmpo').val('');
		$('input#edo').val('');
		$('input#pais').val('');
	}
}
function empresasData(_id){
	$.get( "/ajax/empresasData/"+_id+"/", function( result ) {
		$.each(result, function(_key,_val){
			$('#contactoName').val(_val['contactoName']);
			$('#contactoEmail').val(_val['contactoEmail']);
			$('#contactoPhone').val(_val['contactoPhone']);
			$('#giroEmpresa').val(_val['giroEmpresa']);
			$('#empresaEmail').val(_val['empresaEmail']);
			$('#empresaName').val(_val['empresaName']);
			$('#calleNumero').val(_val['calleNumero']);
			$('#cp').val(_val['cp']);
			$('#col').val(_val['col']);
			$('#delmpo').val(_val['delmpo']);
			$('#edo').val(_val['edo']);
			$('#pais').val(_val['pais']);
		});
	});
}
function loadCandidatos(_userId, _roleId, _ownerId, _vacanteId, _formId, _csrf){
	$.get( "/ajax/candidatosList/"+_userId+"/"+_roleId+"/"+_ownerId+"/"+_vacanteId, function( result ) {
		
		var _tableAdded = '';
		var _tableNotAdded = '<table class="table table-stripped" id="modalNotAddedTable">';
		var _form = '';
		_form += '<form action="/dashboard/vacantes/addCandidate" method="post" id="'+_formId+'">';
		_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
		_form += '<input type="hidden" name="idVacante" value="'+_vacanteId+'">';
		_form += '<table class="table">';
		_form += '<tr>';
		_form += '<td width="50%"><input type="text" id="modalSearchCand" name="_q" class="form-control" placeholder="Nombre o correo (sensible a mayúsculas)"></td>';
		_form += '<td width="25%"><button type="button" class="btn btn-primary" onclick="_searchModalCand(1);">Buscar</button></td>';
		_form += '<td><button type="button" class="btn btn-dark" onclick="_searchModalCand(2);">Resetear</button></td>';
		_form += '</tr>';
		_form += '</table>';
		
		_tableAdded += '<table class="table table-stripped" id="modalAddedTable">';
		_tableAdded += '<tr>';
		_tableAdded += '<td width="150">Nombre</td>';
		_tableAdded += '<td width="150">Correo</td>';
		//_tableAdded += '<td>Estatus</td>';
		_tableAdded += '<td width="50"></td>';
		_tableAdded += '</tr>';
		$.each(result, function(_key,_val){
			var _added;
			var _addedClass = '';
			var _value = 0;
			var _estatus = '';
			if( _val['added'] == '1' ){
				_added = 'checked="checked"';
				_value = 1;
			}
			var _bloqueado;
			var _bloqueadoBG;
			if(_val['colocado'] == 1){
				_bloqueadoBG = '#00FF00';
				_bloqueado = 'disabled';
				_estatus = 'Colocado';
			}else if(_val['bloqueado'] == 1){
				_bloqueadoBG = '#FFFF00';
				_bloqueado = 'disabled';
				_estatus = 'Bloqueado';
			}
			
			if(_estatus != 'Colocado' && _estatus != 'Bloqueado'){
				if( _val['added'] == '1' ){
					_tableAdded += '<tr class="modalCandTr addedClass" style="background:'+_bloqueadoBG+'">';
					_tableAdded += '<td><label class="pointer"><input '+_bloqueado+' '+_added+' id="idUserCandidato'+_key+'" type="checkbox" name="idUserCandidatoCheck'+_val['id']+'" onclick="assignVal('+_val['id']+',this);"> '+_val['name']+' '+_val['lastName']+'</label><input type="hidden" class="idUserCandidato'+_val['id']+'" name="idUserCandidato['+_val['id']+']" value="'+_value+'"></td>';
					_tableAdded += '<td>'+_val['email']+'</td>';
					//_form += '<td>'+_estatus+'</td>';
					_tableAdded += '<td>';
					_tableAdded += '<a target="_blank" href="/dashboard/users/profile/'+_val['id']+'" class="btn btn-success btn-sm">';
					_tableAdded += '<i class="far fa-eye"></i>';
					_tableAdded += '</a>';
					_tableAdded += '</td>';
					_tableAdded += '</tr>';
				}else{
					_tableNotAdded += '<tr class="modalCandTr" style="background:'+_bloqueadoBG+'">';
					_tableNotAdded += '<td width="150"><label class="pointer"><input '+_bloqueado+' '+_added+' id="idUserCandidato'+_key+'" type="checkbox" name="idUserCandidatoCheck'+_val['id']+'" onclick="assignVal('+_val['id']+',this);"> '+_val['name']+' '+_val['lastName']+'</label><input type="hidden" class="idUserCandidato'+_val['id']+'" name="idUserCandidato['+_val['id']+']" value="'+_value+'"></td>';
					_tableNotAdded += '<td width="150">'+_val['email']+'</td>';
					//_tableNotAdded += '<td>'+_estatus+'</td>';
					_tableNotAdded += '<td width="50">';
					_tableNotAdded += '<a target="_blank" href="/dashboard/users/profile/'+_val['id']+'" class="btn btn-success btn-sm">';
					_tableNotAdded += '<i class="far fa-eye"></i>';
					_tableNotAdded += '</a>';
					_tableNotAdded += '</td>';
					_tableNotAdded += '</tr>';
				}
	        }
		});
		_tableAdded += '</table>';
		_tableNotAdded += '</table>';
		_form += _tableAdded + _tableNotAdded;
		_form += '</form>';
		_form += '<div class="clearfix"></div>';
		$('#modal-submit-btn').attr("onclick","removeUnselected('"+_formId+"')");
		$('#modal-body').html(_form);
	});
}

function removeUnselected(_form){
	$('#mainOverlay').fadeIn('fast').css({'display':'flex'});
	$('#modalNotAddedTable tr.modalCandTr td label input').each(function(){
		if( $(this).is(':checked') ){
			//console.log('true');
		}else{
			//console.log('false');
			$(this).parent('label').parent('td').parent('.modalCandTr').remove();
		}
	});
	$('#'+_form).submit();
}

function _searchModalCand(action){
	// form-addCandidate
	if(action == 1){
		var _q = $('#modalSearchCand').val();
		if(_q != ''){
			$('#modalNotAddedTable tr.modalCandTr').show();
			$('#modalNotAddedTable tr.modalCandTr:not(:contains("'+_q+'"))').each(function(){
				$(this).children('td').children('input').attr('disabled','disabled');
				$(this).children('td').children('label').children('input').attr('disabled','disabled');
				$(this).hide();
			});
		}else{
			$('#modalNotAddedTable tr.modalCandTr').children('td').children('input').removeAttr('disabled','disabled');
			$('#modalNotAddedTable tr.modalCandTr').children('td').children('label').children('input').removeAttr('disabled','disabled');
			$('#modalNotAddedTable tr.modalCandTr').show();
		}
	}else{
		$('#modalSearchCand').attr('value','');
		$('#modalNotAddedTable tr.modalCandTr').children('td').children('input').removeAttr('disabled','disabled');
		$('#modalNotAddedTable tr.modalCandTr').children('td').children('label').children('input').removeAttr('disabled','disabled');
		$('#modalNotAddedTable tr.modalCandTr').show();
	}
}

function assignVal(_inputId, _elmnt){
	console.log('#idUserCandidato'+_inputId);
	if( $(_elmnt).is(':checked') ){
		console.log('Checked '+_inputId);

		$('.idUserCandidato'+_inputId).attr('value','1');
	}else{
		console.log('Not checked '+_inputId);
		$('.idUserCandidato'+_inputId).attr('value','0');
	}
}

function deleteRecord(_userId, _recordType, _userEmail, _formId, _csrf, _folder, _returnPath){
	if(_recordType == 'Admin'){
		var _form = '';
		_form += '<p class="text-center">No podrá tener acceso al sistema.</p>';
		_form += '<form id="'+_formId+'" action="/dashboard/user/delete" method="POST" style="display: none;" onsubmit="$(\'#mainOverlay\').fadeIn(\'fast\').css({\'display\':\'flex\'});">';
		_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
		_form += '<input type="hidden" name="id" value="'+_userId+'">';
		_form += '<input type="hidden" name="email" value="'+_userEmail+'">';
		_form += '<input type="hidden" name="role" value="'+_recordType+'">';
		_form += '<input type="hidden" name="folder" value="'+_folder+'">';
		_form += '<input type="hidden" name="_returnPath" value="'+_returnPath+'">';
		_form += '</form>';
		$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
		$('#modal-body').html(_form);
	}else if(_recordType == 'Reclutador'){
		$.get( "/ajax/reclutadorRelations/"+_userId, function( _result ) {
			var _form = '';
			_form += '<p class="text-center">Este reclutador tiene <strong>'+_result[0]+'</strong> candidato(s) y/o <strong>'+_result[1]+'</strong> vacante(s) asignados a él/ella.</p>';
			if( _result[2].length == '0' ){
				_form += '<p class="text-center">Este es el único reclutador registrado, no es posible eliminarlo.</p>';
				$('#modal-submit-btn').text('Aceptar').attr('onclick','$("#mainModal").fadeOut(); $("#modal-body").html("");');
				$('#modal-body').html(_form);
			}else if(_result[0] > 0 || _result[1] > 0){
				_form += '<p class="text-center">Selecciona a qué otro reclutador quieres asignar estos registros:</p>';
				_form += '<form method="POST" action="/dashboard/user/delete" id="'+_formId+'" onsubmit="$(\'#mainOverlay\').fadeIn(\'fast\').css({\'display\':\'flex\'});">';
				_form += '<p class="text-center"><select name="id_newReclutador" class="form-control">';
				$.each(_result[2], function(_key,_val){
					_form += '<option value="'+_val['id']+'">'+_val['name']+' '+_val['lastName']+'</option>';
				});
				_form += '</select></p>';
				_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
				_form += '<input type="hidden" name="id" value="'+_userId+'">';
				_form += '<input type="hidden" name="email" value="'+_userEmail+'">';
				_form += '<input type="hidden" name="role" value="'+_recordType+'">';
				_form += '<input type="hidden" name="folder" value="'+_folder+'">';
				_form += '<input type="hidden" name="_returnPath" value="'+_returnPath+'">';
				_form += '</form>';
				$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
				$('#modal-body').html(_form);
			}else{
				_form += '<form method="POST" action="/dashboard/user/delete" id="'+_formId+'" onsubmit="$(\'#mainOverlay\').fadeIn(\'fast\').css({\'display\':\'flex\'});">';
				_form += '<input type="hidden" name="id_newReclutador" value="0">';
				_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
				_form += '<input type="hidden" name="id" value="'+_userId+'">';
				_form += '<input type="hidden" name="email" value="'+_userEmail+'">';
				_form += '<input type="hidden" name="role" value="'+_recordType+'">';
				_form += '<input type="hidden" name="folder" value="'+_folder+'">';
				_form += '<input type="hidden" name="_returnPath" value="'+_returnPath+'">';
				_form += '</form>';
				$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
				$('#modal-body').html(_form);
			}
		});
	}else if(_recordType == 'Empresa'){
		$.get( "/ajax/empresasRelations/"+_userId, function( _result ) {
			var _form = '';
			_form += '<p class="text-center">Esta empresa tiene <strong>'+_result+'</strong> vacante(s) asignados.</p>';
			_form += '<p class="text-center">La(s) vacantes asignadas, así como sus candidatos, se perderán de manera permanente.</p>';
			_form += '<form id="'+_formId+'" action="/dashboard/user/delete" method="POST" style="display: none;" onsubmit="$(\'#mainOverlay\').fadeIn(\'fast\').css({\'display\':\'flex\'});">';
			_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
			_form += '<input type="hidden" name="id" value="'+_userId+'">';
			_form += '<input type="hidden" name="email" value="'+_userEmail+'">';
			_form += '<input type="hidden" name="role" value="'+_recordType+'">';
			_form += '<input type="hidden" name="folder" value="'+_folder+'">';
			_form += '<input type="hidden" name="_returnPath" value="'+_returnPath+'">';
			_form += '</form>';
			$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
			$('#modal-body').html(_form);
		});
	}else if(_recordType == 'Candidato'){
		var _form = '';
		_form += '<p class="text-center">Será eliminado de todas las vacantes y reclutadores a los que esté asignado.</p>';
		_form += '<form id="'+_formId+'" action="/dashboard/user/delete" method="POST" style="display: none;" onsubmit="$(\'#mainOverlay\').fadeIn(\'fast\').css({\'display\':\'flex\'});">';
		_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
		_form += '<input type="hidden" name="id" value="'+_userId+'">';
		_form += '<input type="hidden" name="email" value="'+_userEmail+'">';
		_form += '<input type="hidden" name="role" value="'+_recordType+'">';
		_form += '<input type="hidden" name="folder" value="'+_folder+'">';
		_form += '<input type="hidden" name="_returnPath" value="'+_returnPath+'">';
		_form += '</form>';
		$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
		$('#modal-body').html(_form);
	}else if(_recordType == 'Vacante'){
		var _form = '';
		_form += '<p class="text-center">Se perderán los registros relacionados a esta vacante.</p>';
		_form += '<form id="'+_formId+'" action="/dashboard/vacantes/delete" method="POST" style="display: none;" onsubmit="$(\'#mainOverlay\').fadeIn(\'fast\').css({\'display\':\'flex\'});">';
		_form += '<input type="hidden" name="_token" value="'+_csrf+'">';
		_form += '<input type="hidden" name="id" value="'+_userId+'">';
		_form += '<input type="hidden" name="_returnPath" value="'+_returnPath+'">';
		_form += '</form>';
		$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
		$('#modal-body').html(_form);
	}
}

function showVideo(_videoUrl){
	var _video = '';
	_video += '<video width="100%" controls="true" autoplay><source src="'+_videoUrl+'" type="video/mp4"><source src="'+_videoUrl+'" type="video/webm">Your browser does not support HTML5 video.</video>';
	$('#modal-body').html(_video);
	$('#modal-submit-btn').attr('onclick','$(\'#mainModal\').fadeOut(); $(\'#modal-body\').html(\'\');');
}

function fillCompensaciones(){
	$('.times12').each(function(index, element) {
		if($(this).val() != ''){
			var _id = $(this).attr('id');
			var _val12 = parseFloat($(this).val())
			_val12 = _val12*12;
        	$('#'+_id+'Anual').val(_val12.toFixed(2));
		}
    });
	
	// formula: _aguinaldo * (salarioBruto / 30)
	var _aguinaldo = $('#aguinaldo').val();
	var _salarioBruto = $('#salarioBruto').val();
	var _aguinaldoAnual = _aguinaldo * (_salarioBruto/30);
	$('#aguinaldoAnual').val(_aguinaldoAnual.toFixed(2));
	// ------
	// formula: (( salarioBruto/30 ) * diasDeVacaciones / primaVacacional(%) )
	var _diasVacaciones = $('#diasVacaciones').val();
	var _diasVacaciones = $('#diasVacaciones').val();
	var _primaVacacional = $('#primaVacacional').val();
	console.log(1);
	var _primaVacAnual = ((_salarioBruto/30)*_diasVacaciones)*(_primaVacacional*0.01);
	console.log(2);
	$('#primaVacacionalAnual').val(_primaVacAnual.toFixed(2)) 
	
	
	var _totalMensual = parseFloat(0);
	$('.mensualVal').each(function(index, element) {
		if($(this).val() != ''){
			var _valM = parseFloat($(this).val())
        	_totalMensual+=_valM;
		}
    });
	$('#totalMensual').val(_totalMensual.toFixed(2));
	
	var _totalAnual = parseFloat(0);
	$('.anualVal').each(function(index, element) {
		if($(this).val() != ''){
			var _valA = parseFloat($(this).val())
        	_totalAnual+=_valA;
		}
    });
	$('#totalAnual').val(_totalAnual.toFixed(2));
}

function checkBoxCV(_form){
	if( $('#'+_form+' input.privacyPolicy').is(':checked')){
		if( document.getElementById("cv-file").files.length == 0 ){
			alert("Selecciona una archivo para enviar. Gracias.");
			return false;
		}
		return true;
	}else{
		return confirm('Do you accept the Privacy Policy?');
	}
}
function checkBoxContact(_form){
	if( $('#'+_form+' input.privacyPolicy').is(':checked')){
		return true;
	}else{
		return confirm('Do you accept the Privacy Policy?');
	}
}

function _toggleVal(_elmnt,_inputID,_formID){
	if($(_elmnt).prop('checked') == true){
		$('#'+_inputID).val(1);
	}else{
		$('#'+_inputID).val(0);
	}
	$('#'+_formID).submit();
}

function checkSalaryRange(){
	var _sueldoDesde = $('#sueldoDesde').val();
	var _sueldoHasta = $('#sueldoHasta').val();
	if(parseInt(_sueldoHasta) < parseInt(_sueldoDesde)){
		alert('El sueldo límite no puede ser menor al sueldo base.');
		$('#sueldoHasta').val(_sueldoDesde);
	}
}

/*function imgPreview(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
			$(input).parent('div').children('div.dog-pic').css({
				'background':'url('+e.target.result+')',
				'background-size':'cover'
			});
            //$('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}*/
function checkUpdatePwd(){
	var _val = $('input[name=updatePwdRadio]:checked').val();
	if(_val == '1'){
		$('.updatePwdDiv').show();
		$('.updatePwdInput').removeAttr('disabled').attr('required','required');
	}else{
		$('.updatePwdDiv').hide();
		$('.updatePwdInput').removeAttr('required').attr('disabled','disabled');
	}
}
function compareUpdatePwd(){
	
}

function levuTabs(_elmnt){
	var _id = $(_elmnt).attr('title');
	$('.levuTabs').removeClass('active');
	$(_elmnt).addClass('active');
	$('.levuTabsContent').hide();
	$('#'+_id).show();
	location.replace('#'+_id)
}

// CHECK VACANTES-CANDIDATOS
function candidatosEmpModal(_arr){
	var _form = '';
	var _formId = '_forma';
	_form += '<table class="table table-striped">';
	$.each(_arr, function(_key,_val){
		_form += '<tr>';
		_form += '<td>'+_val[0]+'</td>';
		_form += '<td>'+_val[1]+'</td>';
		_form += '</tr>';
	});
	_form += '';
	_form += '</table>';
	$('#modal-submit-btn').attr('onclick','$("#mainModal").fadeOut(); $("#modal-body").html("");');
	$('#modal-body').html(_form);
}
// VIDEO CV UPLOAD
function videoCVmodal(_uid){
	var isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
	var _form = '';
	var _formId = '_forma';
	_form += '<p class="text-center">Puedes grabar el video directo de tu computadora o subir un video previamente grabado en formato MP4 (H.264), hasta 2 minutos, hasta 50 MB.</p>';
	_form += '<video id="myVideo" class="video-js vjs-default-skin"></video>';
	_form += '<div class="col-md-12 text-center">';
	if(!isSafari){
		_form += '<button id="recordVideo'+_uid+'" type="button" onclick="startRecording('+_uid+');" class="btn btn-primary">Grabar video</button>';
	}
	_form += '<button id="uploadVideoBtn'+_uid+'" type="button" class="btn btn-success" onClick="$(\'#video_file'+_uid+'\').click()">Subir video</button>';
	_form += '</div>';
	$('#modal-submit-btn').attr('onclick','$("#'+_formId+'").submit();');
	$('#modal-body').html(_form);
}

var player;
function startRecording(_uid){
	$('button#uploadVideoBtn'+_uid).remove();
	$('.video-js').show();
	$('#recordVideo'+_uid).hide();
	$('#uploadVideo'+_uid).hide();
	
	console.log(player);
	if(!player){
		player = videojs("myVideo", {
			controls: true,
			width: 470,
			height: 265,
			fluid: false,
			plugins: {
				record: {
					audio: true,
					video: true,
					maxLength: 120,
					debug: true
				}
			}
		}, function(){
			// print version information at startup
			var msg = 'Using video.js ' + videojs.VERSION +
				' with videojs-record ' + videojs.getPluginVersion('record') +
				' and recordrtc ' + RecordRTC.version;
			videojs.log(msg);
		});
	}else{
		player.record().destroy();
		player = videojs("myVideo", {
			controls: true,
			width: 470,
			height: 265,
			fluid: false,
			plugins: {
				record: {
					audio: true,
					video: true,
					maxLength: 120,
					debug: true
				}
			}
		}, function(){
			// print version information at startup
			var msg = 'Using video.js ' + videojs.VERSION +
				' with videojs-record ' + videojs.getPluginVersion('record') +
				' and recordrtc ' + RecordRTC.version;
			videojs.log(msg);
		});
	}
	
	// error handling
	player.on('deviceError', function() {
		console.log('device error:', player.deviceErrorCode);
	});
	
	player.on('error', function(error) {
		console.log('error:', error);
	});
	
	// user clicked the record button and started recording
	player.on('startRecord', function() {
		console.log('started recording!');
	});
	
	// user completed recording and stream is available
	player.on('finishRecord', function() {
		// the blob object contains the recorded data that
		// can be downloaded by the user, stored on server etc.
		console.log('finished recording: ', player.recordedData);
		
		var data = player.recordedData;
		if (player.recordedData.video) {
			// for chrome for audio+video
			data = player.recordedData.video;
		}
		//$('#modal-body').append('<button class="btn btn-danger">Cancelar</button> <button class="btn btn-warning" onclick="uploadVideoCV('+data+','+_uid+')">Aceptar</button>');
		$('#mainOverlay').fadeIn('fast').css({'display':'flex'});
		uploadVideoCV(data,_uid);
	});
}
function uploadVideoCV(blob,_uid) {
	var _returnPath = $('#returnPath').val();
  var oData = new FormData(document.forms.namedItem("videoRecord"+_uid));

  oData.append("_file", blob, blob.name);

  var oReq = new XMLHttpRequest();
  oReq.open("POST", "/dashboard/vacantes/fileUpload", true);
  oReq.onload = function(oEvent) {
	if (oReq.status == 200) {
	  console.log("Uploaded!");
	  window.location.href = _returnPath;
	} else {
	  console.log("Error " + oReq.status + " occurred uploading your file.");
	  alert('Hubo un error al subir tu video.');
	}
  };

  oReq.send(oData);
}