const { toNumber } = require("lodash");

function initialCharts(){
 
    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(getOpenVacants);
    google.charts.setOnLoadCallback(getClosedVacants);
    google.charts.setOnLoadCallback(getVacantsClosedWithHiring);
    google.charts.setOnLoadCallback(getVacantsClosedWithOutHiring);
    google.charts.setOnLoadCallback(getSellers);
    google.charts.setOnLoadCallback(getVacantsWithAssurance);
    getVacantsByEnterprise();
}

function sellersInitialCharts(){
 
    google.charts.load('current', {'packages':['corechart', 'bar']});
    google.charts.setOnLoadCallback(getSellers);

}

function getOpenVacants(){

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var firstDay = (d.getFullYear(), d.getMonth(), 1);
    var nowDay = d.getDate();
    let from = "2015"+"-"+"01"+"-"+firstDay;
    let to = year+"-"+month+"-"+nowDay;
    var chartDiv = document.getElementById('openVacants');
    $('#openVacants').empty();
    $.ajax({
        url: 'getOpenVacants',
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (){
            $('#openVacants').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {
           $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#openVacants').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#5b78f1','#b8bdd2'],
                chart: {
                    title: 'Vacantes Abiertas',
                    subtitle: 'Vacantes que se encuentran abiertas'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                   // brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                 bar: { groupWidth: '75%' },
                };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputOpenVacants').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
            } 
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function getClosedVacants(){

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var firstDay = (d.getFullYear(), d.getMonth(), 1);
    var nowDay = d.getDate();
    let from = year+"-"+'01'+"-"+firstDay;
    let to = year+"-"+month+"-"+nowDay;
    var chartDiv = document.getElementById('closedVacants');
    $('#closedVacants').empty();
    $.ajax({
        url: 'getClosedVacants',
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (){
            $('#closedVacants').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {

            $("#fromDateClosedVacants").val("");
            $("#toDateClosedVacants").val("");
            $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#closedVacants').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#f93434','#b8bdd2'],
                chart: {
                    title: 'Vacantes Cerradas',
                    subtitle: 'Vacantes que se encuentran cerradas'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                    //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputClosedVacants').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));                    
            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function getVacantsClosedWithHiring(){

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var firstDay = (d.getFullYear(), d.getMonth(), 1);
    var nowDay = d.getDate();
    let from = year+"-"+'01'+"-"+firstDay;
    let to = year+"-"+month+"-"+nowDay;
    var chartDiv = document.getElementById('vacantsClosedWithHiring');
    $('#vacantsClosedWithHiring').empty();
    $.ajax({
        url: 'getVacantsClosedWithHiring',
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (){
            $('#vacantsClosedWithHiring').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {
            $("#fromDateVacantsClosedWithHiring").val("");
            $("#toDateVacantsClosedWithHiring").val("");
            $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#vacantsClosedWithHiring').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#28cb59','#b8bdd2'],
                chart: {
                    title: 'Vacantes Cerradas con contratación',
                    subtitle: 'Vacantes que se encuentran cerradas con contratación'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                    //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputVacantsClosedWithHiring').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));             
            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function getVacantsClosedWithOutHiring(){

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var firstDay = (d.getFullYear(), d.getMonth(), 1);
    var nowDay = d.getDate();
    let from = year+"-"+'01'+"-"+firstDay;
    let to = year+"-"+month+"-"+nowDay;
    var chartDiv = document.getElementById('vacantsClosedWithOutHiring');
    $('#vacantsClosedWithOutHiring').empty();
    $.ajax({
        url: 'getVacantsClosedWithOutHiring',
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        type: "GET",
        data: {from:from, to:to},
        beforeSend: function (){
            $('#vacantsClosedWithOutHiring').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
        },
        success: function (response) {
            $("#fromDateVacantsClosedWithOutHiring").val("");
            $("#toDateVacantsClosedWithOutHiring").val("");
            $('.loading').hide();
            if(Object.keys(response).length == 0){
                $('#vacantsClosedWithHiring').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }else{
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Reclutador');
                data.addColumn('number', 'N° Vacantes');
                data.addColumn('number', 'Tarifa');
                $.each(response, function (i, response) { 
                    var nombre = response.nombre;
                    var count = response.countVacantes;
                    var tarifa = response.tarifa;
                data.addRows([[nombre,count,tarifa]]);
                });
                
                var materialOptions = {
                width: 500,
                height: 350,
                colors: ['#f66e33','#b8bdd2'],
                chart: {
                    title: 'Vacantes cerradas sin contratación',
                    subtitle: 'Vacantes que se encuentran cerradas sin contratación'
                },
                series: {
                    0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                    1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                },
                axes: {
                    y: {
                    distance: {label: 'N° Vacantes'}, // Left y-axis.
                    //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                    }
                },
                hAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 6,
                    }
                },
                vAxis: {
                    maxTextLines: 10,
                    textStyle: {
                        fontSize: 8,
                    }
                },
                };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputVacantsClosedWithOutHiring').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));                
            }
        },
        error: function (response){
            console.log(JSON.stringify(response));
        }
    });
}

function getSellers(){

    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var firstDay = (d.getFullYear(), d.getMonth(), 1);
    var nowDay = d.getDate();
    let from = year+"-"+month+"-"+firstDay;
    let to = year+"-"+month+"-"+nowDay;
    var chartDiv = document.getElementById('sellers');
    let status = 5;
    $('#sellers').empty();
    if(status == 0){
        swal({
        title: "Ooppss!",
        text: "No seleccionaste alguna opción",
        icon: "warning",
        }); 
    }else{
        $.ajax({
            url: 'getSellers',
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            type: "GET",
            data: {from:from, to:to, status:status},
            beforeSend: function (xhr){
                $('#sellers').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
            },
            success: function (response) {
                $("#fromDateSellers").val("");
                $("#toDateSellers").val("");
                $('select[name="status"]').find('option[value="0"]').attr("selected",true);
                $('.loading').hide();
                if(Object.keys(response).length == 0){
                    $('#sellers').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
                }else{
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Vendedor');
                    data.addColumn('number', 'N° Vacantes');
                    data.addColumn('number', 'Tarifa');
                    $.each(response, function (i, response) { 
                        var nombre = response.nombre;
                        var count = response.countVacantes;
                        var tarifa = response.tarifa;
                    data.addRows([[nombre,count,tarifa]]);
                    });
                    
                    var materialOptions = {
                    width: 500,
                    height: 350,
                    colors: ['#f9ef26','#b8bdd2'],
                    chart: {
                        title: 'Oportunidades',
                        subtitle: 'Montos totales de vendedores'
                    },
                    series: {
                        0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                        1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                    },
                    axes: {
                        y: {
                        distance: {label: 'N° Vacantes'}, // Left y-axis.
                        //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                        }
                    },
                    hAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 6,
                        }
                    },
                    vAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 8,
                        }
                    },
                    };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputSellers').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                }
            },
            error: function (response){
            console.log(JSON.stringify(response));
        }
        });
    }
}

function getVacantsWithAssurance(){
    
    var d = new Date();
    var year = d.getFullYear();
    var month = d.getMonth()+1;
    var firstDay = (d.getFullYear(), d.getMonth(), 1);
    var nowDay = d.getDate();
    let from = year+"-"+month+"-"+firstDay;
    let to = year+"-"+month+"-"+nowDay;
    var chartDiv = document.getElementById('vacantsWithAssurance');
    let status = 5;
    $('#vacantsWithAssurance').empty();
    if(status == 0){
        swal({
        title: "Ooppss!",
        text: "No seleccionaste alguna opción",
        icon: "warning",
        }); 
    }else{
        $.ajax({
            url: 'getVacantsWithAssurance',
            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            type: "GET",
            data: {from:from, to:to, status:status},
            beforeSend: function (xhr){
                $('#vacantsWithAssurance').html('<div class="loading" style="text-align: center;"><img src="../../img/spinner.gif" /><br/>Un momento, por favor...</div>');
            },
            success: function (response) {
                $("#fromDateVacantsAssurance").val("");
                $("#toDateVacantsAssurance").val("");
                $('select[name="status"]').find('option[value="0"]').attr("selected",true);
                $('.loading').hide();
                if(Object.keys(response).length == 0){
                    $('#vacantsWithAssurance').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
                }else{
                    var data = new google.visualization.DataTable();
                    data.addColumn('string', 'Vendedor');
                    data.addColumn('number', 'N° Vacantes');
                    data.addColumn('number', 'Tarifa');
                    $.each(response, function (i, response) { 
                        var nombre = response.nombre;
                        var count = response.countVacantes;
                        var tarifa = response.tarifa;
                    data.addRows([[nombre,count,tarifa]]);
                    });
                    
                    var materialOptions = {
                    width: 500,
                    height: 350,
                    colors: ['#7d52b0','#b8bdd2'],
                    chart: {
                        title: 'Vacantes con garantía',
                        subtitle: 'Vacantes que cuentan con una garantía'
                    },
                    series: {
                        0: { axis: 'distance' }, // Bind series 0 to an axis named 'distance'.
                        1: { axis: 'brightness' } // Bind series 1 to an axis named 'brightness'.
                    },
                    axes: {
                        y: {
                        distance: {label: 'N° Vacantes'}, // Left y-axis.
                        //brightness: {side: 'right', label: 'Cantidades'} // Right y-axis.
                        }
                    },
                    hAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 6,
                        }
                    },
                    vAxis: {
                        maxTextLines: 10,
                        textStyle: {
                            fontSize: 8,
                        }
                    },
                    };

                var materialChart = new google.charts.Bar(chartDiv);
                google.visualization.events.addListener(materialChart, 'ready', function(){
                    var canvas,domURL,imageURI,svgParent;
                    // add svg namespace to chart
                    svgParent = materialChart.getContainer().getElementsByTagName('svg')[0];
                    svgParent.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

                    // create image URI
                    domURL = window.URL || window.webkitURL || window;
                    imageNode = materialChart.getContainer().cloneNode(true);
                    imageURI = domURL.createObjectURL(new Blob([svgParent.outerHTML], {type: 'image/svg+xml'}));
                    image = new Image();
                    image.onload = function() {
                        canvas = document.createElement('canvas');
                        canvas.setAttribute('width', parseFloat(svgParent.getAttribute('width')));
                        canvas.setAttribute('height', parseFloat(svgParent.getAttribute('height')));
                        canvas.getContext('2d').drawImage(image, 0, 0,600,350);
                        $('#inputVacantsWithAssurance').val(canvas.toDataURL('image/png'));
                    }
                    image.src = imageURI; 
                });
                materialChart.draw(data, google.charts.Bar.convertOptions(materialOptions));
                }
            },
            error: function (response){
            console.log(JSON.stringify(response));
        }
        });
    }
   
}

function getVacantsByEnterprise(){

    $.ajax({
        url: 'getVacantsByEnterprise',
        contentType: "application/json; charset=utf-8",
        dataType: "JSON",
        type: "GET",
        success: function (response) {
            if(Object.keys(response).length != 0){
                var link = '<table class="table table-striped">';
                link += '<thead>';
                link += '<tr>';
                link += '<th scope="col">Folio</th>';
                link += '<th scope="col">Empresa</th>';
                link += '<th scope="col">No. Vacantes</th>';
                link += '<th scope="col">Tarifa (Total)</th>';
                link += '</tr>';
                link += '</thead>';
                link += '<tbody>'
                $.each(response, function (i, response) {
                    link += '<tr><td>'+response.id+'</td><td>'+response.name+'</td><td>'+response.noVacantes+'</td><td>'+'$'+Intl.NumberFormat('es-MX').format(response.tarifa)+'</td></tr>';
                });
                link += '</tbody>';
                link += '</table>';
                $('#vacantsByEnterprise').append(link);
            }else{
                $('#vacantsByEnterprise').append("<h4>La búsqueda no obtuvo resultados, intente de nuevo<h4>");
            }
        }
    });
}
