@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12 bg-white">
        	<div class="col-md-12 mb-4">
                <h4>TUS VACANTES / YOUR VACANCIES - <strong>{{$vacants[0]['empresa']}}</strong></h4>
            </div>
            <div class="col-md-12 float-left">
                <table class="table table-striped table-bordered" style="width:100%" id="dataTable">
                    <thead style="text-align: center;">
                        <tr>
                        <th>Folio / Folio</th>
                        <th>Vacante / Vacancy</th>
                        <th>Estatus / Status</th>
                        <th>Fecha Creación / Creation date</th>
                        <th>Fecha Cierre / Close date</th>
                        <th>HeadHunter a Cargo / HeadHunter assigned </th>
                        <th>Detalle / Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($vacants->count())
                            @foreach ($vacants as $vacant)
                            <tr>
                                <td>{{ $vacant->folio }}</td>
                                <td>{{ $vacant->vacante }}</td>
                                @if ($vacant->status == "Cerrada con contratación" || $vacant->status == "Cerrada sin contratación" || $vacant->status == "Cerrada")
                                 <td style="color:rgb(226, 57, 57);">Cerrada</td>   
                                @else
                                <td style="color: rgb(56, 175, 56)">{{ $vacant->status }} </td>    
                                @endif
                                <td>{{ $vacant->fechaCreacion }}</td>
                                @if ($vacant->fechaCierre == "")
                                    <td>No disponible / Not available</td>
                                @else
                                    <td>{{ $vacant->fechaCierre}}</td>
                                @endif
                                <td>{{ $vacant->reclutadorNombre}} {{ $vacant->reclutadorApellido }}</td>
                                <td><a href="{{ url('/dashboard/vacantes/view/'.$vacant->folio)}}"  class="btn btn-success">Ir a la vacante / Go to vacancy</a></td>
                            </tr>
                            @endforeach
                        @else
                            <h4>Sin Resultados</h4>
                        @endif
                    </tbody>
                </table>
            </div>
            
            <div class="col-md-12 pt-3 pb-3 row">
                <p class="text-left">
                    <a href="{{ url('/dashboard/') }}" class="btn bg-levuAzul color-white btn-sm">
                        Volver al home
                    </a>
                </p>
            </div>
        </div>
    </div>

</div>
@include('partials.scripts.dataTables')
<script>
    var table = -$(document).ready(function() {
    $('#dataTable').DataTable({
        responsive: true
    } );
 
    new $.fn.dataTable.FixedHeader( table );
} );
</script>
@endsection
