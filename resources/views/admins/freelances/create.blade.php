@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

    <div class="main-content bg-whiteGrey">
        <div class="row justify-content-center">
            <div class="col-md-12 bg-white">
                <br><br>
                    <div class="col-md-12 float-left">
                        <form id="formFreelance" method="POST" action="{{ route('freelances.store')}}" enctype="multipart/form-data">
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            @csrf
                            <div class="form-group row text-center">
                                <div class="col-md-12 text-center">
                                    <h4>Datos principales</h4>
                                </div>                       
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" placeholder="type a first name" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-md-4 col-form-label text-md-right">Apellido</label>
                                <div class="col-md-6">
                                    <input id="lastName" type="text" class="form-control" name="lastName" placeholder="type a last name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" placeholder="type a valid email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">Teléfono</label>
                                <div class="col-md-6">
                                    <input id="phone" type="phone" class="form-control" name="phone" placeholder="type a phone number">    
                                </div>
                            </div>    
                            @include('partials/forms/adminRegisterFreelance')
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4 text-right">
                                    <a class="btn bg-black color-white" href="{{ URL::previous() }}">
                                        {{ __('Cancelar') }}
                                    </a>
                                    <button type="submit" class="btn bg-levuAzul color-white">
                                        Registrar
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="role" value="Freeelance">
                        </form>                    
                    </div>
            </div>
        </div>
    </div>
<script type="text/javascript" src="{{ asset('js/freelance-ajax.js') }}"></script>
@include('partials.scripts.jquery')
@endsection
