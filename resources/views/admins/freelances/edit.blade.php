@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

    <div class="main-content bg-whiteGrey">
        <div class="row justify-content-center">
            <div class="col-md-12 bg-white">
                <br><br>
                @if ($users->count())
                    @foreach ($users as $user)
                        <div class="col-md-12 float-left">
                        <form id="editFreelance" method="POST" action="{{ route('freelances.update', $user->id) }}" enctype="multipart/form-data">
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <input name="_method" type="hidden" value="PATCH">
                            @csrf
                            <div class="form-group row text-center">
                                <div class="col-md-12 text-center">
                                    <h4>Editar información</h4>
                                </div>                       
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="type a first name" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lastName" class="col-md-4 col-form-label text-md-right">Apellido</label>
                                <div class="col-md-6">
                                    <input id="lastName" type="text" class="form-control" name="lastName" value="{{ $user->lastName }}" placeholder="tipe a last name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="type a valid email" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">Teléfono</label>
                                <div class="col-md-6">
                                    <input id="phone" type="phone" class="form-control" name="phone" value="{{ $user->phone }}" placeholder="type a phone number">    
                                </div>
                            </div>    
                            <!--@include('partials/forms/adminRegisterFreelance')-->
                            <div class="form-group row text-center">
                                <div class="col-md-12 text-center">
                                    <h4>Datos adicionales</h4>         
                                </div>                       
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">Ubicación</label>

                                <div class="col-md-3">
                                    <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); loadSepomex();" checked> México</label>
                                </div>
                                <div class="col-md-3">
                                    <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); loadSepomex();"> Fuera de México</label>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="calleNumero" class="col-md-4 col-form-label text-md-right">Calle y número</label>

                                <div class="col-md-6">
                                    <input id="calleNumero" type="text" class="form-control" name="calleNumero" value="{{ $user->direccion }}@php if(isset($_GET['calleNumero'])){echo $_GET['calleNumero'];} @endphp">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="cp" class="col-md-4 col-form-label text-md-right">CP</label>

                                <div class="col-md-6">
                                    <input id="cp" type="text" class="form-control" name="cp" onchange="loadSepomex();" value="{{ $user->cp }} @php if(isset($_GET['cp'])){echo $_GET['cp'];} @endphp">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="col" class="col-md-4 col-form-label text-md-right">Colonia</label>

                                <div class="col-md-6" id="sepomexCol">
                                    <input id="col" type="text" class="form-control dirData" name="col" readonly value="{{ $user->colonia }}@php if(isset($_GET['col'])){echo $_GET['col'];} @endphp">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="delmpo" class="col-md-4 col-form-label text-md-right">Delegación / Municipio</label>

                                <div class="col-md-6">
                                    <input id="delmpo" type="text" class="form-control dirData" name="delmpo" readonly value="{{ $user->delmpo }} @php if(isset($_GET['delmpo'])){echo $_GET['delmpo'];} @endphp">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="edo" class="col-md-4 col-form-label text-md-right">Estado / Región</label>

                                <div class="col-md-6">
                                    <input id="edo" type="text" class="form-control dirData" name="edo" readonly value="{{ $user->edo }}@php if(isset($_GET['edo'])){echo $_GET['edo'];} @endphp">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="pais" class="col-md-4 col-form-label text-md-right">País</label>

                                <div class="col-md-6">
                                    <input id="pais" type="text" class="form-control dirData" name="pais" readonly value="{{ $user->pais }}@php if(isset($_GET['pais'])){echo $_GET['pais'];} @endphp">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4 text-right">
                                    <a class="btn bg-black color-white" href="{{ URL::previous() }}">
                                        {{ __('Regresar') }}
                                    </a>
                                    <button type="submit" class="btn bg-levuAzul color-white">
                                        Actualizar
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="role" value="Freeelance">
                        </form>                    
                    </div>
                @endforeach  
            @endif
            </div>
        </div>
    </div>
<script type="text/javascript" src="{{ asset('js/freelance-ajax.js') }}"></script>
@include('partials.scripts.jquery')
@endsection
