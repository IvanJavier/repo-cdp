@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')


<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<div class="col-md-12 float-left">
            	<h2>Usuarios - Freelance </h2>
                <a href="{{ route('freelances.create') }}" class="btn btn-info" >Añadir usuario</a>
            </div>
            <div class="form-group mb-5">
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 float-left">
                <table class="table table-bordred table-striped">
                    <thead>
                        <th><strong>NOMBRE</strong></th>
                        <th class="mobileHide"><strong>CORREO</strong></th>
                        <th class="mobileHide"><strong>TEL.</strong></th>
                        <th>Ver</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </thead>
                    <tbody>
                        @if ($users->count())
                        @foreach($users as $user)
                            <tr>
                                <td>{{$user->name}} {{$user->lastName}}</td>
                                <td class="mobileHide">{{ $user->email}}</td>
                                <td class="mobileHide">{{$user->phone}}</td>
                                <td><a href="{{ route('freelances.show', $user->id)}}" class="btn btn-primary" data-toggle="tooltip" data-placement="bottom" title="Detalle"><i class="far fa-eye"></i></a></td>
                                <td><a href="{{ action('FreelanceController@edit', $user->id)}}" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Editar"><i class="far fa-edit"></i></a></td>
                                <td>
                                    <form action="{{ action('FreelanceController@destroy', $user->id)}}" method="POST">
                                    {{csrf_field()}}
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button class="btn btn-danger" type="submit" id="btnDelete" onclick="confirm('¿Estas seguro?')"><i class="far fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach                             
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        {{$users->links()}}
    </div>
</div>
<script type="text/javascript" src="{{ asset('js/freelance-ajax.js') }}"></script>
@include('partials.scripts.jquery')
@endsection
