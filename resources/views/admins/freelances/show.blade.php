@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')


<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-12">
            @if ($users->count())
                @foreach ($users as $user)
                    <br>
                    <div class="container">
                    <div class="col-xs-12 col-sm-4">
                        <figure>
                            <img src="{{ asset('storage').'/'.$user->folder.'/'.$user->folder.$user->profilePic }}" width="200" height="200" class="img-circle img-responsive">

                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h3>{{ $user->name }} {{ $user->lastName }}</h3>
                        <p><strong>Email: </strong> {{ $user->email }}</p>
                        <p><strong>Phone: </strong> {{ $user->phone}}</p>
                        <p><strong>Direccion: </strong> {{ $user->direccion}}</p>
                        <p><strong>Colonia: </strong> {{ $user->colonia }}</p>
                        <p><strong>CP: </strong> {{ $user->cp}}</p>
                        <p><strong>Delegación/Municipio: </strong> {{ $user->delmpo }}</p>
                        <p><strong>Estado/ciudad: </strong> {{ $user->edo }}</p>
                        <p><strong>Pais: </strong> {{ $user->pais }}</p>
                    </div>             
                    </div>            
                    <div class="col-xs-12 divider text-center">
                        <div class="col-xs-12 col-sm-4 emphasis">  
                            <a class="btn btn-success btn-block" href="{{ route('freelance.index')}}"><span class="fa fa-plus-circle"></span> Regresar </a>
                        </div>
                        <div class="col-xs-12 col-sm-4 emphasis">
                            <a class="btn btn-info btn-block" href="{{ route('freelances.edit', $user->id)}}"><span class="fa fa-user"></span> Editar </a>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
</div>
@include('partials.scripts.jquery')
@endsection
