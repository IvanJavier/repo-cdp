@extends('layouts.console')

@section('title', "LEVU Talent - Referencias")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12 bg-white row pt-5">
            <div class="col-md-6">
                <h2>Sucursales</h2>
                <form class="w-100" method="post" action="{{URL::to('dashboard/sucursalesSectores/sucursalUpload')}}">
                    {{ csrf_field() }}
                    <table class="table border-bottom">
                        <tr>
                            <td>
                                <input type="text" name="name" class="form-control" placeholder="Nueva sucursal" required>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Añadir sucursal"><i class="fas fa-cloud-upload-alt"></i></button>
                            </td>
                        </tr>
                    </table>
                </form>
                <table class="table table-striped">
                    @foreach($sucursales as $sucursal)
                    <tr>
                        <td>{{$sucursal->name}}</td>
                        <td>
                            <form id="formActive" method="post" action="{{URL::to('dashboard/sucursalesSectores/sucursalUpdate')}}">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" value="{{$sucursal->id}}">
                                @if($sucursal->active == 1)
                                    <input type="hidden" name="active" value="0">
                                    <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Deshabilitar sucursal"><i class="fas fa-power-off"></i></button>
                                @else
                                    <input type="hidden" name="active" value="1">
                                    <button type="submit" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="bottom" title="Habilitar sucursal"><i class="fas fa-power-off"></i></button>
                                @endif
                            </form>
                            
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            <div class="col-md-6">
                <h2>Sectores</h2>
                <form class="w-100" method="post" action="{{URL::to('dashboard/sucursalesSectores/sectorUpload')}}">
                    {{ csrf_field() }}
                    <table class="table border-bottom">
                        <tr>
                            <td>
                                <input type="text" name="nombre" class="form-control" placeholder="Nuevo sector" required>
                            </td>
                            <td>
                                <button type="submit" class="btn btn-success" data-toggle="tooltip" data-placement="bottom" title="Añadir sector"><i class="fas fa-cloud-upload-alt"></i></button>
                            </td>
                        </tr>
                    </table>
                </form>
                <table class="table table-striped">
                    @foreach($sectores as $sector)
                        @if($sector->active == 1)
                            <div class="p-2 pr-3 pl-3 border float-left m-1">
                                <form id="sector{{$sector->id}}" method="post" action="{{URL::to('dashboard/sucursalesSectores/sectorUpdate')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="active" value="0">
                                    <input type="hidden" name="id" value="{{$sector->id}}">
                                    {{$sector->nombre}}
                                    <a href="#" class="text-danger ml-2 mt-2" onclick="$('#sector{{$sector->id}}').submit()" data-toggle="tooltip" data-placement="bottom" title="Deshabilitar sector '{{$sector->nombre}}'"><i class="fas fa-power-off"></i></a>
                                </form>
                            </div>
                        @else
                            <div class="p-2 pr-3 pl-3 border float-left m-1">
                                <form id="sector{{$sector->id}}" method="post" action="{{URL::to('dashboard/sucursalesSectores/sectorUpdate')}}">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="active" value="1">
                                    <input type="hidden" name="id" value="{{$sector->id}}">
                                    {{$sector->nombre}}
                                    <a href="#" class="text-warning ml-2 mt-2" onclick="$('#sector{{$sector->id}}').submit()" data-toggle="tooltip" data-placement="bottom" title="Habilitar sector '{{$sector->nombre}}"><i class="fas fa-power-off"></i></a>
                                </form>
                            </div>
                        @endif
                    @endforeach
                </table>
            </div>
        </div>
    	
    </div>
</div>
@endsection
