@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')


<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<form id="candiFilter">
            	<div class="col-md-12">
                    <h1>Lista de candidatos</h1>
                </div>
                <div class="form-group mb-5">
                    <div class="col-md-6 float-left">
                        <h3 class="m-0">Tipo de usuarios: {{$role_type}}</h3>
                    </div>
                    <div class="col-md-6 p-0 float-left text-right">
                        <a class="btn bg-black color-white float-right" href="{{ url('/dashboard/users/'.$role_type)}}">Resetar</a>
                        <button type="submit" class="btn bg-levuAzul color-white mr-2 float-right">Buscar</button>
                        <input type="text" class="form-control col-6 mr-2 float-right" name="q" placeholder="Nombre, correo o puesto" value="{{$_q}}">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <input type="hidden" id="sortBy" value="{{$sortBy}}" name="sortBy">
                <input type="hidden" id="sort" value="{{$sort}}" name="sort">
                <table class="table">
                    <tr>
                        <td>
                            <strong>NOMBRE</strong>
                            <a data-toggle="tooltip" data-placement="bottom" title="Nombre ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('name'); $('#sort').val('ASC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-up"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Nombre descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('name'); $('#sort').val('DESC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </a>
                        </td>
                        <td class="mobileHide">
                            <strong>CORREO</strong>
                            <a data-toggle="tooltip" data-placement="bottom" title="Correo ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('email'); $('#sort').val('ASC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-up"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Correo descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('email'); $('#sort').val('DESC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </a>
                        </td>
                        <td>
                            <strong>PUESTO</strong>
                            <a data-toggle="tooltip" data-placement="bottom" title="Puesto ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('puesto'); $('#sort').val('ASC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-up"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Puesto descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('puesto'); $('#sort').val('DESC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </a>
                        </td>
                        <td class="mobileHide" width="200">
                            <strong>SUELDO</strong>
                            <a data-toggle="tooltip" data-placement="bottom" title="Sueldo ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('sueldoDesde'); $('#sort').val('ASC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-up"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Sueldo descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('sueldoDesde'); $('#sort').val('DESC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </a>
                        </td>
                        <td class="mobileHide" width="150">
                            <strong>SECTOR</strong>
                            <a data-toggle="tooltip" data-placement="bottom" title="Sector ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('sector'); $('#sort').val('ASC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-up"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Sector descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('sector'); $('#sort').val('DESC'); $('#candiFilter').submit();">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </a>
                        </td>
                        <td class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Bloqueado"><strong>B</strong></span></td>
                        <td class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Colocado"><strong>C</strong></span></td>
                        <td width="130"></td>
                    </tr>
                    <tr class="mobileHide">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>
                            <span class="float-left small">Desde $</span><input style="width:50px; border:0; padding:0;" class="float-left small" type="text" name="sueldoDesde" id="sueldoDesde" value="{{$sueldoDesde}}" readonly> <span class="float-left small">hasta $</span><input style="width:50px; border:0; padding:0;" class="float-left small" type="text" name="sueldoHasta" id="sueldoHasta" value="{{$sueldoHasta}}" readonly>
                            <div class="clearfix"></div>
                            <div id="slider-range"></div>
                        </td>
                        <td width="150">
                            <select class="form-control" name="idSector">
                                <option value="0" @if($idSector == NULL || $idSector == '0') selected @endif>Todos</option>
                                @foreach($sectores as $sector)
                                    <option value="{{$sector->id}}" @if($idSector == $sector->id) selected @endif>{{$sector->nombre}}</option>
                                @endforeach
                                <option value="newSector">Otro</option>
                            </select>
                        </td>
                        <td class="text-center"></td>
                        <td class="text-center"></td>
                        <td width="130">
                            <a href="{{ url('/dashboard/users/'.$role_type)}}" class="btn btn-sm bg-black color-white mr-2 float-right" data-toggle="tooltip" data-placement="bottom" title="Resetear"><i class="fas fa-retweet"></i></a>
                            <button type="submit" class="btn btn-sm bg-levuAzul color-white mr-2 float-right" data-toggle="tooltip" data-placement="bottom" title="Filtrar"><i class="fas fa-filter"></i></button>
                        </td>
                    </tr>
                    @foreach($users as $key=>$user)
                        
                        <tr>
                            <td>
                                {{$user->name}} {{$user->lastName}} @if(count($user->vacantes) > 0) <button type="button" class="mobileHide btn btn-xs btn-default font-12" onclick="$('.tr-user-{{$user->id}}').fadeToggle();" data-toggle="tooltip" data-placement="bottom" title="Vacantes"><i class="fas fa-clipboard-list mr-1 font-16"></i> <i class="fas fa-caret-down"></i></button> @endif
                            </td>
                            <td class="mobileHide">
                                {{ str_limit($user->email, $limit = 15, $end = '...') }}
                            </td>
                            <td>
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$user->puesto}}">{{ str_limit($user->puesto, $limit = 10, $end = '...') }}</span>
                            </td>
                            <td class="mobileHide">
                                ${{number_format($user->sueldoDesde, 0, '.', ',')}} a ${{number_format($user->sueldoHasta, 0, '.', ',')}} {{$user->divisa}}
                            </td>
                            <td class="mobileHide">
                                <span data-toggle="tooltip" data-placement="bottom" title="{{$user->sector}}">{{ str_limit($user->sector, $limit = 10, $end = '...') }}</span>
                            </td>
                            <td class="text-center">
                                @if($user->colocado == '1') <i class="fas fa-check font-18 text-success"></i> @elseif($user->bloqueado == '1') <i class="fas fa-exclamation font-22 text-warning"></i> @endif
                            </td>
                            <td class="text-center">
                                @if($user->colocado == '1') <i class="fas fa-check font-18 text-success"></i> @endif
                            </td>
                            <td>
                                <a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle">
                                    <i class="far fa-eye"></i>
                                </a>
                                @if( Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') )
                                    <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <button type="button" class="btn bg-black btn-sm color-white" data-title="Eliminar {{$role_type}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$role_type}}?" data-submit-btn="Eliminar {{$role_type}}" onclick="deleteRecord('{{$user->id}}', '{{$role_type}}', '{{$user->email}}','form-delete{{$role_type}}','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @elseif(Auth::user()->hasRole('Reclutador'))
                                    @if($role_type != 'Empresa')
                                        <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <button type="button" class="btn bg-black btn-sm color-white" data-title="Eliminar {{$role_type}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$role_type}}?" data-submit-btn="Eliminar {{$role_type}}" onclick="deleteRecord('{{$user->id}}', '{{$role_type}}', '{{$user->email}}','form-delete{{$role_type}}','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @if(count($user->vacantes) > 0)
                        	@foreach($user->vacantes as $vacante)
                            	@if( Auth::user()->hasRole('Reclutador') )
                                    @if( Auth::user()->id == $vacante->ownerId )
                                        <tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                            <td></td>
                                            <td><a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}">{{$vacante->titulo}}</a></td>
                                            <td>{{$vacante->empresa}}</td>
                                            <td>
                                                <a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm">
                                                    <i class="far fa-eye"></i>
                                                </a>
                                                <a target="_blank" href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @else
                                    	<tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                            <td></td>
                                            <td>{{$vacante->titulo}}</td>
                                            <td>{{$vacante->empresa}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    @endif
                                @else
                                	<tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                        <td></td>
                                        <td><a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}">{{$vacante->titulo}}</a></td>
                                        <td>{{$vacante->empresa}}</td>
                                        <td>
                                            <a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm">
                                                <i class="far fa-eye"></i>
                                            </a>
                                            <a target="_blank" href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                        
                    @endforeach
                </table>
            </form>
            <div class="col-md-12 pt-3 pb-3 row">
                <p class="text-left">
                	<a href="{{ url('/dashboard/') }}" class="btn bg-levuAzul color-white btn-sm">
                        Volver al home
                    </a>
                    @if( Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') )
                        <a href="{{ url('/dashboard/users/registration/') }}/{{$role_type}}" class="btn bg-levuAzul color-white btn-sm">
                            Nuevo {{$role_type}}
                        </a>
                    @elseif( Auth::user()->hasRole('Reclutador') && $role_type == 'Candidato')
                    	<a href="{{ url('/dashboard/users/registration/') }}/{{$role_type}}" class="btn bg-levuAzul color-white btn-sm">
                            Nuevo {{$role_type}}
                        </a>
                    @endif
                </p>
            </div>
        </div>
    </div>

    {{ $users->appends($_GET)->links() }}
    
    
</div>
<script>
	@if($sueldoDesde != NULL)
		$( "#slider-range" ).slider({
		  range: true,
		  min: 0,
		  max: 500000,
		  values: [ {{$sueldoDesde}}, {{$sueldoHasta}} ],
		  slide: function( event, ui ) {
			$( "#sueldoDesde" ).val( ui.values[ 0 ] );
			$( "#sueldoHasta" ).val( ui.values[ 1 ] );
		  }
		});
		$( "#sueldoDesde" ).val( '{{$sueldoDesde}}' );
		$( "#sueldoHasta" ).val( '{{$sueldoHasta}}' );
	@else
		$( "#slider-range" ).slider({
		  range: true,
		  min: 0,
		  max: 500000,
		  values: [ 0, 500000 ],
		  slide: function( event, ui ) {
			$( "#sueldoDesde" ).val( ui.values[ 0 ] );
			$( "#sueldoHasta" ).val( ui.values[ 1 ] );
		  }
		});
		$( "#sueldoDesde" ).val( '0' );
		$( "#sueldoHasta" ).val( '500000' );
	@endif
    
</script>
@endsection
