@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')


<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<div class="col-md-12">
            	<h1>Lista de empresas</h1>
            </div>
            <div class="form-group mb-5">
                <div class="col-md-6 float-left">
                    <h3 class="m-0">Tipo de usuarios: {{$role_type}}</h3>
                </div>
                <div class="col-md-6 float-left text-right mobileHide">
                    <form id="empFilter">
                        <a class="btn bg-black color-white float-right" href="{{ url('/dashboard/users/'.$role_type)}}">Resetar</a>
                        <button type="submit" class="btn bg-levuAzul color-white mr-2 float-right">Buscar</button>
                        <input type="text" class="form-control col-6 mr-2 float-right" name="q" placeholder="Nombre o correo">
                        <input type="hidden" id="sortBy" value="{{$sortBy}}" name="sortBy">
                        <input type="hidden" id="sort" value="{{$sort}}" name="sort">
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12">
            	<table class="table">
            	<tr>
                	<td>
                        <strong>NOMBRE</strong>
                        <a data-toggle="tooltip" data-placement="bottom" title="Nombre ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('name'); $('#sort').val('ASC'); $('#empFilter').submit();">
                                <i class="fas fa-long-arrow-alt-up"></i>
                            </a>
                            <a data-toggle="tooltip" data-placement="bottom" title="Nombre descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('name'); $('#sort').val('DESC'); $('#empFilter').submit();">
                                <i class="fas fa-long-arrow-alt-down"></i>
                            </a>
                    </td>
                    <td class="mobileHide">
                        <strong>CORREO</strong>
                        <a data-toggle="tooltip" data-placement="bottom" title="Correo ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('email'); $('#sort').val('ASC'); $('#empFilter').submit();">
                            <i class="fas fa-long-arrow-alt-up"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Correo descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('email'); $('#sort').val('DESC'); $('#empFilter').submit();">
                            <i class="fas fa-long-arrow-alt-down"></i>
                        </a>
                    </td>
                    <td class="mobileHide"><strong>TEL.</strong></td>
                    <td>
                        <strong>SECTOR</strong>
                        <a data-toggle="tooltip" data-placement="bottom" title="Sector ascendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('sector'); $('#sort').val('ASC'); $('#empFilter').submit();">
                            <i class="fas fa-long-arrow-alt-up"></i>
                        </a>
                        <a data-toggle="tooltip" data-placement="bottom" title="Sector descendente" href="#" class="btn btn-sm p-1" onclick="$('#sortBy').val('sector'); $('#sort').val('DESC'); $('#empFilter').submit();">
                            <i class="fas fa-long-arrow-alt-down"></i>
                        </a>
                    </td>
                    <td></td>
                </tr>
                @foreach($users as $key=>$user)
                	<tr>
                        <td>
                        	{{$user->name}} @if(count($user->vacantes) > 0) <button type="button" class="mobileHide btn btn-xs btn-default font-12" onclick="$('.tr-user-{{$user->id}}').slideToggle();" data-toggle="tooltip" data-placement="bottom" title="Vacantes"><i class="fas fa-clipboard-list mr-1 font-16"></i> <i class="fas fa-caret-down"></i></button> @endif
                        </td>
                        <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                        <td class="mobileHide">{{$user->phone}}</td>
                        <td><span data-toggle="tooltip" data-placement="bottom" title="{{$user->sector}}">{{ str_limit($user->sector, $limit = 20, $end = '...') }}</span></td>
                        <td>
                        	<a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle">
                                <i class="far fa-eye"></i>
                            </a>
                            @if( Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Vendedor') )
                                <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                    <i class="far fa-edit"></i>
                                </a>
                                <button type="button" class="btn bg-black btn-sm color-white" data-title="Eliminar {{$role_type}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$role_type}}?" data-submit-btn="Eliminar {{$role_type}}" onclick="deleteRecord('{{$user->id}}', '{{$role_type}}', '{{$user->email}}','form-delete{{$role_type}}','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                    <i class="far fa-trash-alt"></i>
                                </button>
                            @elseif(Auth::user()->hasRole('Reclutador'))
                                @if($role_type != 'Empresa')
                                    <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <button type="button" class="btn bg-black btn-sm color-white" data-title="Eliminar {{$role_type}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$role_type}}?" data-submit-btn="Eliminar {{$role_type}}" onclick="deleteRecord('{{$user->id}}', '{{$role_type}}', '{{$user->email}}','form-delete{{$role_type}}','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @endif
                            @endif
                        </td>
                    </tr>
                    @if(count($user->vacantes) > 0)
                        @foreach($user->vacantes as $vacante)
                            @if( Auth::user()->hasRole('Reclutador') )
                                @if( Auth::user()->id == $vacante->ownerId )
                                	<tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                        <td></td>
                                        <td><a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}">{{$vacante->titulo}}</a></td>
                                        <td>
                                        	<a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle">
                                                <i class="far fa-eye"></i>
                                            </a>
                                            <a target="_blank" href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @else
                                	<tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                        <td></td>
                                        <td>{{$vacante->titulo}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                @endif
                            @else
                            	<tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                    <td></td>
                                    <td><a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}">{{$vacante->titulo}}</a></td>
                                    <td>
                                    	<a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle">
                                            <i class="far fa-eye"></i>
                                        </a>
                                        <a target="_blank" href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                @endforeach
            </table>
            </div>
            <div class="col-md-12 pt-3 pb-3 row">
                <p class="text-left">
                	<a href="{{ url('/dashboard/') }}" class="btn bg-levuAzul color-white btn-sm">
                        Volver al home
                    </a>
                    @if( Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') )
                        <a href="{{ url('/dashboard/users/registration/') }}/{{$role_type}}" class="btn bg-levuAzul color-white btn-sm">
                            Nuevo {{$role_type}}
                        </a>
                    @elseif( Auth::user()->hasRole('Reclutador') && $role_type == 'Candidato')
                    	<a href="{{ url('/dashboard/users/registration/') }}/{{$role_type}}" class="btn bg-levuAzul color-white btn-sm">
                            Nuevo {{$role_type}}
                        </a>
                    @endif
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
