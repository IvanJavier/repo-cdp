@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')


<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-12">
        	<div class="col-md-12 float-left">
            	<h1>Lista de HeadHunters</h1>
            </div>
            <div class="form-group mb-5">
                <div class="col-md-12 float-left">
                    <h3 class="m-0">Tipo de usuarios: {{$role_type}}</h3>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="form-group mb-5">
                <form>
                    <div class="col-md-3 float-left">
                        <select class="form-control" name="estatus">
                            <option value="all" @if(app('request')->input('estatus') == 'all' || !app('request')->input('estatus')) selected @endif>Todos los estatus</option>
                            <option value="Abierta" @if(app('request')->input('estatus') == 'Abierta') selected @endif>Abiertas</option>
                            <option value="Cerrada" @if(app('request')->input('estatus') == 'Cerrada') selected @endif>Cerrada</option>
                            <option value="Cerrada sin contratación" @if(app('request')->input('estatus') == 'Cerrada sin contratación') selected @endif>Cerrada sin contratación</option>
                            <option value="Cerrada con contratación" @if(app('request')->input('estatus') == 'Cerrada con contratación') selected @endif>Cerrada con contratación</option>
                        </select>
                    </div>
                    <div class="col-md-3 float-left">
                        <input type="text" name="fromDate" placeholder="Desde" class="form-control bg-white datepicker" @if(app('request')->input('fromDate')) value="{{app('request')->input('fromDate')}}" @endif readonly>
                    </div>
                    <div class="col-md-3 float-left">
                        <input type="text" name="toDate" placeholder="Hasta" class="form-control bg-white datepicker" @if(app('request')->input('toDate')) value="{{app('request')->input('toDate')}}" @endif readonly>
                    </div>
                    <div class="col-md-3 float-left">
                        <button type="submit" class="btn btn-success"><i class="fas fa-search"></i></button>
                        <a href="{{ URL::to('/dashboard/users/Reclutador') }}" class="btn btn-danger"><i class="fas fa-sync-alt"></i></a>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 float-left">
                <table class="table">
                    <tr>
                        <td><strong>NOMBRE</strong></td>
                        <td class="mobileHide"><strong>CORREO</strong></td>
                        <td class="mobileHide"><strong>TEL.</strong></td>
                        <td></td>
                    </tr>
                    @foreach($users as $key=>$user)
                        <tr>
                            <td>
                                {{$user->name}} {{$user->lastName}} @if(count($user->vacantes) > 0) @if( Auth::user()->hasRole('SuperAdmin') ) ({{count($user->vacantes)}} - ${{ number_format($user->vacantes->sum('tarifa'),2) }}) @endif <button type="button" class="mobileHide btn btn-xs btn-default font-12" onclick="$('.tr-user-{{$user->id}}').slideToggle();" data-toggle="tooltip" data-placement="bottom" title="Vacantes"><i class="fas fa-clipboard-list mr-1 font-16"></i> <i class="fas fa-caret-down"></i></button> @endif
                            </td>
                            <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                            <td class="mobileHide">{{$user->phone}}</td>
                            <td>
                                <a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle">
                                    <i class="far fa-eye"></i>
                                </a>
                                @if( Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') )
                                    <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <button type="button" class="btn bg-black btn-sm color-white" data-title="Eliminar {{$role_type}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$role_type}}?" data-submit-btn="Eliminar {{$role_type}}" onclick="deleteRecord('{{$user->id}}', '{{$role_type}}', '{{$user->email}}','form-delete{{$role_type}}','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                @elseif(Auth::user()->hasRole('Reclutador'))
                                    @if($role_type != 'Empresa')
                                        <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <button type="button" class="btn bg-black btn-sm color-white" data-title="Eliminar {{$role_type}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$role_type}}?" data-submit-btn="Eliminar {{$role_type}}" onclick="deleteRecord('{{$user->id}}', '{{$role_type}}', '{{$user->email}}','form-delete{{$role_type}}','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                        @if(count($user->vacantes) > 0)
                            @foreach($user->vacantes as $vacante)
                                <tr class="tr-user-child tr-user-{{$user->id}}" style="display:none;">
                                    <td>&bull; <a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}">{{$vacante->titulo}}</a> - <span class="@if($vacante->estatus == 'Abierta') text-success @else text-danger @endif">{{$vacante->estatus}}</span></td>
                                    <td>
                                        Fecha de creación: {{$vacante->created_at}}
                                    </td>
                                    <td>
                                        <a target="_blank" href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle de vacante">
                                            <i class="far fa-eye"></i>
                                        </a>
                                        <a target="_blank" href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Editar vacante">
                                            <i class="far fa-edit"></i>
                                        </a>
                                    </td>
                                    <td></td>
                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                </table>
            </div>
            
            <div class="col-md-12 pt-3 pb-3">
                <p class="text-left">
                	<a href="{{ url('/dashboard/') }}" class="btn bg-levuAzul color-white btn-sm">
                        Volver al home
                    </a>
                    @if( Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') )
                        <a href="{{ url('/dashboard/users/registration/') }}/{{$role_type}}" class="btn bg-levuAzul color-white btn-sm">
                            Nuevo {{$role_type}}
                        </a>
                    @elseif( Auth::user()->hasRole('Reclutador') && $role_type == 'Candidato')
                    	<a href="{{ url('/dashboard/users/registration/') }}/{{$role_type}}" class="btn bg-levuAzul color-white btn-sm">
                            Nuevo {{$role_type}}
                        </a>
                    @endif
                </p>
            </div>
        </div>
    </div>
</div>
@endsection
