@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="bg-white">
                <div class="">
                	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                		<h2 class="p-3 text-center">Detalle de {{$user->roles[0]->name}}</h2>
                        
                        <div class="clearfix">
                            <div class="col-md-6 float-left text-right"><strong>Tipo de usuario</strong></div>
    
                            <div class="col-md-6 float-left">
                                {{$user->roles[0]->name}}
                            </div>
                        </div>
                        @if($user->roles[0]->name == 'Candidato')
                            <div class="clearfix">
                                <div class="col-md-6 float-left text-right"><strong>HeadHunter a cargo</strong></div>
    
                                <div class="col-md-6 float-left">
                                    {{$reclutador->name}} {{$reclutador->lastName}}
                                </div>
                            </div>
                        @endif
                        
                    @else
                    	<h2 class="p-3 text-center">Mi perfil</h2>
                    @endif
                    
                    <div class="form-group text-center">
                        <div class="col-md-12 text-center">
                            <h4>Datos principales</h4>         
                        </div>                       
                    </div>
                        
                    <div class="clearfix">
                    	@if($user->roles[0]->name == 'Admin')
                            <div class="col-md-12 float-left text-center">
                                <div class="col-md-12 float-left text-center">
                                    <p><strong>Nombre</strong><br>
                                    {{ $user->name }} {{ $user->lastName }}</p>
                                </div>
                                <div class="col-md-12 float-left text-center">
                                    <p><strong>Correo</strong><br>
                                    {{ $user->email }}</p>
                                </div>
                                <div class="col-md-12 float-left text-center">
                                    <p><strong>Teléfono</strong><br>
                                    {{ $user->phone }}</p>
                                </div>
                            </div>
                        @else
                        	<div class="col-md-6 float-left text-right">
                                @if( $user_data[0]->profilePic != NULL)
                                    <img src="{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->profilePic }}" width="100" class="img-fluid">
                                @else
                                    <img src="{{ asset('img/no-img.jpg') }}" width="100" class="img-fluid">
                                @endif
                            </div>
                            <div class="col-md-6 float-left">
                                <div class="col-md-12 float-left">
                                    <p><strong>Nombre</strong><br>
                                    {{ $user->name }} @if($user->roles[0]->name != 'Empresa') {{ $user->lastName }} @endif</p>
                                </div>
                                <div class="col-md-12 float-left">
                                    <p><strong>Correo</strong><br>
                                    {{ $user->email }}</p>
                                </div>
                                <div class="col-md-12 float-left">
                                    <p><strong>Teléfono</strong><br>
                                    {{ $user->phone }}</p>
                                </div>
                            </div>
                        @endif
                    </div>

					
                    <div class="clearfix">
                    {{-- --}}
                    	<div class="form-group row text-center">
                            <div class="col-md-12 text-center">
                                <h4>Datos adicionales</h4>         
                            </div>                       
                        </div>
                        <div class="form-group row text-center">
                            <div class="col-md-12 text-center">
                                <h4>Ubicación</h4>         
                            </div>                       
                        </div>
                        <div class="">
                            <div class="col-md-6 pull-center">
                                <table class="table table-condensed">
                                    <tr>
                                        <td><strong>Calle y número</strong></td>
                                        <td>{{$user_data[0]->calleNumero}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>CP</strong></td>
                                        <td>{{$user_data[0]->cp}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Colonia</strong></td>
                                        <td>{{$user_data[0]->col}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Delegación / Municipio</strong></td>
                                        <td>{{$user_data[0]->delmpo}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>Estado / Región</strong></td>
                                        <td>{{$user_data[0]->edo}}</td>
                                    </tr>
                                    <tr>
                                        <td><strong>País</strong></td>
                                        <td>{{$user_data[0]->pais}}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    {{-- --}}
                    <div class="col-md-12 mt-5 mb-5">
                        <div class="col-md-12 text-center">
                            <a class="btn bg-black color-white" href="{{ URL::previous() }}">
                                Volver
                            </a>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                            	<button class="btn bg-black color-white" data-title="Eliminar {{$user->roles[0]->name}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$user->roles[0]->name}}?" data-submit-btn="Eliminar {{$user->roles[0]->name}}" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-delete{{$user->roles[0]->name}}','{{ csrf_token() }}', '', '{{ url('/dashboard/users/'.$user->roles[0]->name) }}'); toggleModal(this);">
                                    Eliminar {{$user->roles[0]->name}}
                                </button>
                                <a class="btn bg-levuAzul color-white" href="{{ url('/dashboard/users/edit/') . '/' . $user->id}}">
                                    Editar {{$user->roles[0]->name}}
                                </a>
                                @if($user->roles[0]->name == 'Empresa')
                                	<a class="btn bg-levuAzul color-white" href="{{ url('/dashboard/vacantes/create')}}">
                                        Crear vacante
                                    </a>
                                @endif
                            @elseif( Auth::user()->hasRole('Reclutador'))
                            	@if($user->roles[0]->name == 'Candidato')
                                    <button class="btn bg-black color-white" data-title="Eliminar {{$user->roles[0]->name}}" data-instructions="¿Estás seguro/a de querer eliminar este {{$user->roles[0]->name}}?" data-submit-btn="Eliminar {{$user->roles[0]->name}}" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-delete{{$user->roles[0]->name}}','{{ csrf_token() }}', '', '{{ url('/dashboard/users/'.$user->roles[0]->name) }}'); toggleModal(this);">
                                        Eliminar {{$user->roles[0]->name}}
                                    </button>
                                    <a class="btn bg-levuAzul color-white" href="{{ url('/dashboard/users/edit/') . '/' . $user->id}}">
                                        Editar {{$user->roles[0]->name}}
                                    </a>
                                @endif
                            @elseif(Auth::user()->hasRole('Candidato') || Auth::user()->hasRole('Empresa'))
                            	<a class="btn bg-levuAzul color-white" href="{{ url('/dashboard/users/edit/') . '/' . $user->id}}">
                                    Editar datos
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection