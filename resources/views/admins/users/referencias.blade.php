@extends('layouts.console')

@section('title', "LEVU Talent - Referencias")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
    	<div class="col-md-12 row bg-white">
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-6 float-left mb-4">
                    <h1 class="m-0">REFERENCIAS</h1>
                </div>
                <div class="col-md-6 float-left text-right mb-4 refsBtn">
                	@if(Auth::user()->hasRole('Candidato'))
                        <a class="btn bg-black color-white" href="{{ url('/dashboard/') }}">
                            Volver
                        </a>
                    @else
                        <a class="btn bg-black color-white" href="{{ URL::previous() }}">
                            Volver
                        </a>
                    @endif
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Candidato'))
                        <a href="{{ url('/dashboard/users/referencias/new/'.$idUserCandidato) }}" class="btn bg-levuAzul color-white">
                            Nueva referencia
                        </a>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 row bg-white  p-5">
                <div class="col-md-3 float-left">
                    <p><strong>Candidato: </strong></p>
                    <p>{{$userData->name}} {{$userData->lastName}}</p>
                </div>
            </div>
        </div>
        
        <div class="col-md-12 pl-0 pr-0 mt-5">
        	<div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0">
            	<div class="col-md-12 bg-white p-4 mobileHide">
                    <div class="col-md-2 float-left">
                        <strong>Nombre de la referencia</strong>
                    </div>
                    <div class="col-md-2 float-left">
                        <strong>Empresa</strong>
                    </div>
                    <div class="col-md-2 float-left">
                        <strong>Puesto</strong>
                    </div>
                    <div class="col-md-3 float-left">
                        <strong>Correo</strong>
                    </div>
                    <div class="col-md-3 float-left">
                    </div>
                    <div class="clearfix"></div>
                </div>
                @foreach($referencias as $referencia)
                    	<div class="col-md-12 border-top p-3 bg-white">
                        	<div class="col-md-2 float-left">
                            	<span class="mobileShow"><strong>Nombre de la referencia:</strong> </span>
                            	{{$referencia->entrevistaA}}
                            </div>
                            <div class="col-md-2 float-left">
                            	<span class="mobileShow"><strong>Empresa:</strong> </span>
                            	{{$referencia->empresa}}
                            </div>
                            <div class="col-md-2 float-left">
                            	<span class="mobileShow"><strong>Puesto:</strong> </span>
                            	{{$referencia->puesto}}
                            </div>
                            <div class="col-md-3 float-left">
                            	<span class="mobileShow"><strong>Correo:</strong> </span>
                            	{{$referencia->correo}}
                            </div>
                            <div class="col-md-3 float-left">
                            	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Candidato'))
                                	<a href="{{ url('/dashboard/users/referencias/view/'.$referencia->id.'/'.$idUserCandidato) }}" class="btn bg-black color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle de referencia">
                                        <i class="far fa-eye"></i>
                                    </a>
                            	@elseif(Auth::user()->hasRole('Empresa'))
                                	@if($referencia->completed == '0')
                                        <span class="btn bg-black btn-disabled color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="No disponible">
                                            <i class="far fa-eye"></i>
                                        </span>
                                    @else
                                    	<a href="{{ url('/dashboard/users/referencias/view/'.$referencia->id.'/'.$idUserCandidato) }}" class="btn bg-black color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle de referencia">
                                            <i class="far fa-eye"></i>
                                        </a>
                                    @endif
                                @endif
                                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Candidato'))
                                    <a href="{{ url('/dashboard/users/referencias/edit/'.$referencia->id.'/'.$idUserCandidato) }}" class="btn bg-black color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Editar referencia">
                                        <i class="far fa-edit"></i>
                                    </a>
                                    <button type="button" class="btn bg-black color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Eliminar referencia" onClick="$('#ref{{$referencia->id}}{{$idUserCandidato}}-delete').submit();">
                                            <i class="far fa-trash-alt"></i>
                                        </button>
                                	<form action="{{ url('/dashboard/users/referencias/delete') }}" method="post" id="ref{{$referencia->id}}{{$idUserCandidato}}-delete">
                                        {{ csrf_field() }}
                        				<input type="hidden" name="_returnPath" value="/dashboard/users/referencias/{{$idUserCandidato}}">
                                    	<input type="hidden" name="rid" value="{{$referencia->id}}">
                                        <input type="hidden" name="uid" value="{{$idUserCandidato}}">
                                    </form>
                                @endif
                        	</div>
                            <div class="clearfix"></div>
                        </div>
                    @endforeach
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
</div>
@endsection
