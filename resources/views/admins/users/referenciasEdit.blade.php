@extends('layouts.console')

@section('title', "LEVU Talent - Editar referencia")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="bg-white">
                <div class="">
                	<h3 class="p-3">Editar referencia</h3>
                    <form id="secFitsForm" method="POST" action="{{ url('/dashboard/users/referencias/update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="_returnPath" value="/dashboard/users/referencias/{{$idUserCandidato}}">
                        <input type="hidden" name="id" value="{{$referencias->id}}">
                        <input type="hidden" name="idUserCandidato" value="{{$idUserCandidato}}">
                        <div class="form-group col-md-12 row">
                            <div class="col-md-6 float-left">
                                <label for="candidato" class="col-form-label text-md-left">Candidato</label>
                                <input id="candidato" type="text" class="form-control" name="candidato" value="{{$referencias->candidatoName}} {{$referencias->candidatoLastName}}" disabled>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="form-group col-md-12 row">
                        	<div class="col-md-4 float-left">
                                <label for="entrevistaA" class="col-form-label text-md-left">Nombre de la referencia:</label>
                                <input id="entrevistaA" type="text" class="form-control" name="entrevistaA" value="{{$referencias->entrevistaA}}" required>
                            </div>
                            <div class="col-md-4 float-left">
                                <label for="puesto" class="col-form-label text-md-left">Puesto:</label>
                                <input id="puesto" type="text" class="form-control" name="puesto" value="{{$referencias->puesto}}" required>
                            </div>
                            <div class="col-md-4 float-left">
                                <label for="empresa" class="col-form-label text-md-left">Empresa:</label>
                                <input id="empresa" type="text" class="form-control" name="empresa" value="{{$referencias->empresa}}" required>
                            </div>
                            
                            <div class="col-md-4 float-left">
                                <label for="telefono" class="col-form-label text-md-left">Teléfono:</label>
                                <input id="telefono" type="text" class="form-control" name="telefono" value="{{$referencias->telefono}}" required>
                            </div>
                            <div class="col-md-4 float-left">
                                <label for="correo" class="col-form-label text-md-left">Correo:</label>
                                <input id="correo" type="text" class="form-control" name="correo" value="{{$referencias->correo}}" required>
                            </div>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <div class="col-md-4 float-left">
                                    <label for="fechaEntrevista" class="col-form-label text-md-left">Fecha de entrevista:</label>
                                    <input id="fechaEntrevista" type="text" class="form-control bg-white datepicker" readonly name="fechaEntrevista" value="{{$referencias->fechaEntrevista}}">
                                </div>
                            @else
                            	<input type="hidden" name="fechaEntrevista" value="{{$referencias->fechaEntrevista}}">
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <div class="form-group col-md-12 row">
                        	@foreach(json_decode($referencias->respuestas, true) as $key=>$resp)
                            	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="preg1">{{$resp[0]}}</label>
                                            <input type="hidden" name="respuestas[{{$key}}][0]" value="{{$resp[0]}}">
                                            <textarea class="form-control" rows="5" id="preg1" name="respuestas[{{$key}}][1]">{{$resp[1]}}</textarea>
                                        </div>
                                    </div>
                                @else
                                	<input type="hidden" name="respuestas[{{$key}}][0]" value="{{$resp[0]}}">
                                    <input type="hidden" name="respuestas[{{$key}}][1]" value="{{$resp[1]}}">
                            	@endif
                            @endforeach
                            <div class="clearfix"></div>
                        </div>
                        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                            <div class="form-group col-md-12 mt-5 row">
                                <div class="col-md-12 float-left">
                                    <p>¿REFERENCIA COMPLETA?: </p>
                                    <label class="mr-5">Si <input type="radio" name="completed" value="1" @if($referencias->completed == '1') checked="checked" @endif></label>
                                    <label>No <input type="radio" name="completed" value="0" @if($referencias->completed == '0') checked="checked" @endif></label>
                                </div>
                            </div>
                        @elseif(Auth::user()->hasRole('Candidato'))
                        	<input type="hidden" name="completed" value="0">
                        @endif
                        
                        
                        <div class="form-group col-md-12 mb-3">
                            <div class="col-md-12 text-right">
                                <a class="btn bg-black color-white" href="{{ url('/dashboard/users/referencias/'.$idUserCandidato.'/') }}">
                                    Cancelar
                                </a>
                                <button type="submit" class="btn bg-levuAzul color-white">
                                    Actualizar Referencia
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
