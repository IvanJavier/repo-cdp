@extends('layouts.console')

@section('title', "LEVU Talent - Referencias")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12 row bg-white">
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-6 float-left mb-4">
                    <h1 class="m-0">REFERENCIAS</h1>
                </div>
                <div class="col-md-6 float-left text-right mb-4 refsBtn">
                    <a class="btn bg-black color-white" href="{{ url('/dashboard/users/referencias/'.$idUserCandidato) }}">
                        Volver
                    </a>
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                        <a href="{{ url('/dashboard/users/referencias/edit/'.$referencias->id.'/'.$idUserCandidato) }}" class="btn bg-levuAzul color-white">
                            Editar referencia
                        </a>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-2 float-left">
                    <p><strong>Entrevista a:</strong></p>
                    <p>{{$referencias->entrevistaA}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Puesto:</strong></p>
                    <p>{{$referencias->puesto}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Empresa:</strong></p>
                    <p>{{$referencias->empresa}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Teléfono:</strong></p>
                    <p>{{$referencias->telefono}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Correo:</strong></p>
                    <p>{{$referencias->correo}}</p>
                </div>
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                    <div class="col-md-2 float-left">
                        <p><strong>Fecha de entrevista:</strong></p>
                        <p>{{$referencias->fechaEntrevista}}</p>
                    </div>
                @endif
            </div>
        </div>
        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Empresa'))
            @foreach(json_decode($referencias->respuestas, true) as $key=>$resp)
                <div class="col-md-12 pl-0 pr-0">
                    <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                        <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>{{$resp[0]}}</strong></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                        <div class="col-md-12 bg-white p-4">
                            <p>{{$resp[1]}}</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            @endforeach
        @endif
    </div>
</div>
@endsection
