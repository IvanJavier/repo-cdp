@extends('layouts.console')

@section('title', "LEVU Talent - Registro de usuarios")

@section('content')

<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <h3>Formato de compensaciones / Current total rewards package</h3>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <input type="hidden" name="id" value="{{$user->id}}">
                    <input type="hidden" name="_returnPath" value="{{Request::url()}}">
                    <div class="form-group row text-center">
                        <div class="col-md-12 text-center">
                            <h4>Datos principales</h4>         
                        </div>                       
                    </div>
                    <div class="form-group col-md-12 row">
                        <div class="col-md-6">
                            <p><strong>Candidato / Candidate </strong> {{$user->name}} {{$user->lastName}}</p>
                        </div>
                    </div>
                    <div class="form-group col-md-12 row">
                        <div class="col-md-6">
                            <p><strong>Empresa actual / Current employer</strong> {{$compensaciones->empresaActual}}</p>
                        </div>
                    </div>
                    <div class="form-group col-md-12 row">
                        <div class="col-md-6">
                            <p><strong>Antigüedad / Year in current role</strong> {{$compensaciones->antiguedad}}</p>
                        </div>
                    </div>
                    
                    <div class="col-md-12 mt-5 row">
                        <table class="table table-striped">
                            <tr>
                                <th width="40%"><strong>Compensación</strong></th>
                                <th width="20%" class="text-center"><strong>% o días</strong></th>
                                <th width="20%" class="text-center"><strong>Mensual ({{$compensaciones->divisa}})</strong></th>
                                <th width="20%" class="text-center"><strong>Anual ({{$compensaciones->divisa}})</strong></th>
                            </tr>
                            <tr>
                                @php $salarioBruto = json_decode($compensaciones->salarioBruto, true) @endphp
                                <td>Salario bruto mensual (antes de impuestos)</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($salarioBruto[0],2)}}</td>
                                <td class="text-center">$ {{ number_format($salarioBruto[1],2)}}</td>
                            </tr>                                
                            <tr>
                                @php $comisiones = json_decode($compensaciones->comisiones, true) @endphp
                                <td>Comisiones mensuales (promedio)</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($comisiones[0],2) }}</td>
                                <td class="text-center">$ {{ number_format($comisiones[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $bonoMensualPorObjetivos = json_decode($compensaciones->bonoMensualPorObjetivos, true) @endphp
                                <td>Bono mensual por objetivos</td>
                                <td class="text-center">{{$bonoMensualPorObjetivos[0]}}</td>
                                <td class="text-center">$ {{ number_format($bonoMensualPorObjetivos[1],2) }}</td>
                                <td class="text-center">$ {{ number_format($bonoMensualPorObjetivos[2],2) }}</td>
                            </tr>
                            <tr>
                                @php $bonoAnualPorObjetivos = json_decode($compensaciones->bonoAnualPorObjetivos, true) @endphp
                                <td>Bono anual por objetivos</td>
                                <td class="text-center">{{$bonoAnualPorObjetivos[0]}}</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($bonoAnualPorObjetivos[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $aguinaldo = json_decode($compensaciones->aguinaldo, true) @endphp
                                <td>Aguinaldo (dias)</td>
                                <td class="text-center">{{$aguinaldo[0]}} días</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($aguinaldo[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $primaVacacional = json_decode($compensaciones->primaVacacional, true) @endphp
                                <td>Prima vacacional (%)</td>
                                <td class="text-center">{{$primaVacacional[0]}}%</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($primaVacacional[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $fondoAhorro = json_decode($compensaciones->fondoAhorro, true) @endphp
                                <td>Fondo de ahorro</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($fondoAhorro[0],2) }}</td>
                                <td class="text-center">$ {{ number_format($fondoAhorro[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $fondoRetiro = json_decode($compensaciones->fondoRetiro, true) @endphp
                                <td>Fondo de retiro</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($fondoRetiro[0],2) }}</td>
                                <td class="text-center">$ {{ number_format($fondoRetiro[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $valesDespensa = json_decode($compensaciones->valesDespensa, true) @endphp
                                <td>Vales de despensa</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($valesDespensa[0],2) }}</td>
                                <td class="text-center">$ {{ number_format($valesDespensa[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $valesRestaurante = json_decode($compensaciones->valesRestaurante, true) @endphp
                                <td>Vales de restaurante</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($valesRestaurante[0],2) }}</td>
                                <td class="text-center">$ {{ number_format($valesRestaurante[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $valesGasolina = json_decode($compensaciones->valesGasolina, true) @endphp
                                <td>Vales de gasolina</td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($valesGasolina[0],2) }}</td>
                                <td class="text-center">$ {{ number_format($valesGasolina[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $ptu = json_decode($compensaciones->ptu, true) @endphp
                                <td>PTU</td>
                                <td></td>
                                <td></td>
                                <td class="text-center">$ {{ number_format($ptu[0],2) }}</td>
                            </tr>
                            <tr>
                                @php $otro = json_decode($compensaciones->otro, true) @endphp
                                <td>Otros (especificar)</td>
                                <td colspan="2" class="text-center">{{$otro[0]}}</td>
                                <td class="text-center">$ {{ number_format($otro[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $total = json_decode($compensaciones->total, true) @endphp
                                <td><strong>TOTAL</strong></td>
                                <td></td>
                                <td class="text-center"><strong>$ {{ number_format($total[0],2) }}</strong></td>
                                <td class="text-center"><strong>$ {{ number_format($total[1],2) }}</strong></td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Días de vacaciones</td>
                                <td class="text-center">{{$compensaciones->diasVacaciones}}</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                @php $automovil = json_decode($compensaciones->automovil, true) @endphp
                                <td>Automóvil de la empresa</td>
                                <td class="text-center">{{$automovil[0]}}</td>
                                <td class="text-right"><strong>Modelo</strong></td>
                                <td class="text-center">{{$automovil[1]}}</td>
                            </tr>
                            <tr>
                                @php $gastosAutomovil = json_decode($compensaciones->gastosAutomovil, true) @endphp
                                <td>Gastos del automóvil</td>
                                <td class="text-center">{{$gastosAutomovil[0]}}</td>
                                <td class="text-right"><strong>Monto</strong></td>
                                <td class="text-center">$ {{ number_format($gastosAutomovil[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $seguroVida = json_decode($compensaciones->seguroVida, true) @endphp
                                <td>Seguro de vida (meses)</td>
                                <td class="text-center">{{$seguroVida[0]}}</td>
                                <td class="text-right"><strong>Meses</strong></td>
                                <td class="text-center">{{$seguroVida[1]}}</td>
                            </tr>
                            <tr>
                                @php $gastosMedMenores = json_decode($compensaciones->gastosMedMenores, true) @endphp
                                <td>Seguro de gastos médicos menores</td>
                                <td class="text-center">{{$gastosMedMenores[0]}}</td>
                                <td class="text-right"><strong>Familiar</strong></td>
                                <td class="text-center">{{$gastosMedMenores[1]}}</td>
                            </tr>
                            <tr>
                                @php $gastoMedMayores = json_decode($compensaciones->gastoMedMayores, true) @endphp
                                <td>Seguro de gastos médicos mayores</td>
                                <td class="text-center">{{$gastoMedMayores[0]}} </td>
                                <td class="text-right"><strong>Familiar</strong></td>
                                <td class="text-center">{{$gastoMedMayores[1]}}</td>
                            </tr>
                            <tr>
                                @php $vivienda = json_decode($compensaciones->vivienda, true) @endphp
                                <td>Vivienda (renta/housing)</td>
                                <td class="text-center">{{$vivienda[0]}}</td>
                                <td class="text-right"><strong>Monto</strong></td>
                                <td class="text-center">$ {{$vivienda[1]}}</td>
                            </tr>
                            <tr>
                                @php $apoyosEducativos = json_decode($compensaciones->apoyosEducativos, true) @endphp
                                <td>Apoyos educativos / Becas</td>
                                <td class="text-center">{{$apoyosEducativos[0]}}</td>
                                <td class="text-right"><strong>Monto</strong></td>
                                <td class="text-center">$ {{ number_format($apoyosEducativos[1],2) }}</td>
                            </tr>
                            <tr>
                                @php $stockNumeroAcciones = json_decode($compensaciones->stockNumeroAcciones, true) @endphp
                                <td>Stock options (número de acciones)</td>
                                <td class="text-center">@if($stockNumeroAcciones[0] == '1') Si @else No @endif</td>
                                <td class="text-right"></td>
                                <td></td>
                            </tr>
                            <tr>
                                @php $stockValorPresente = json_decode($compensaciones->stockValorPresente, true) @endphp
                                <td>Stock options (valor presente)</td>
                                <td class="text-center">@if($stockValorPresente[0] == '1') Si @else No @endif</td>
                                <td class="text-right"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Otras prestaciones y comentarios:</td>
                                <td colspan="3" class="text-center">{{$compensaciones->otrasPrestaciones}}</td>
                            </tr>
                            <tr>
                                <td colspan="4">&nbsp;
                                    
                                </td>
                            </tr>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                                <tr>
                                    <td>Completado</td>
                                    <td colspan="3" class="text-center">
                                        Completado: @if($compensaciones->completed == '1') Si @else No @endif
                                    </td>
                                </tr>
                            @endif
                        </table>
                        <div class="clearfix"></div>
                    </div>
                     

                    <div class="col-md-12 form-group mb-5">
                        <div class="col-md-6 float-right text-right">
                        	@if(Auth::user()->hasRole('Empresa'))
                                <a class="btn bg-black color-white" href="{{ URL::previous() . '#tabCandidatos' }}">
                                    {{ __('Volver') }}
                                </a>
                            @elseif(Auth::user()->hasRole('Candidato'))
                                <a class="btn bg-black color-white" href="{{ '/dashboard' }}">
                                    {{ __('Volver') }}
                                </a>
                            @else
                            	<a class="btn bg-black color-white" href="{{ URL::previous() }}">
                                    {{ __('Volver') }}
                                </a>
                            @endif
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Candidato'))
                                <a href="{{ url('/dashboard/users/compensaciones/edit/'.$user->id) }}" class="btn btn-primary">
                                    Editar formato
                                </a>
                            @endif
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
