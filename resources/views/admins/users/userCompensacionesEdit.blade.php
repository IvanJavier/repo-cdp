@extends('layouts.console')

@section('title', "LEVU Talent - Registro de usuarios")

@section('content')

<div class="main-content">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <h3>Formato de compensaciones / Current total rewards package</h3>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form id="compensacionesForm" method="POST" action="{{ url('/dashboard/users/compensaciones/update') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <input type="hidden" name="_returnPath" value="{{ URL::previous() . '#tabCandidatos' }}">
                        <div class="form-group row text-center">
                            <div class="col-md-12 text-center">
                                <h4>Datos principales / General Data</h4>         
                            </div>                       
                        </div>
                        <div class="form-group col-md-12 row">
                        	<div class="col-md-6">
                                <label for="name" class="col-form-label text-md-left">Candidato / Candidate</label>
                                <input id="name" type="text" class="form-control" name="email" value="{{$user->name}} {{$user->lastName}}" disabled>
                            </div>
                        </div>
                        <div class="form-group col-md-12 row">
                        	<div class="col-md-6">
                                <label for="empresaActual" class="col-form-label text-md-left">Empresa actual / Current employer</label>
                                <input id="empresaActual" type="text" class="form-control" name="empresaActual" value="{{$compensaciones->empresaActual}}" required>
                            </div>
                        </div>
                        <div class="form-group col-md-12 row">
                        	<div class="col-md-6">
                                <label for="antiguedad" class="col-form-label text-md-left">Antigüedad / Years in current role</label>
                                <input id="antiguedad" type="number" class="form-control" name="antiguedad" value="{{$compensaciones->antiguedad}}" required>
                            </div>
                        </div>
                        <div class="form-group col-md-12 row">
                        	<div class="col-md-6">
                                <label for="divisa" class="col-form-label text-md-left">Moneda / Currency</label>
                                <select class="form-control" name="divisa" id="divisa">
                                    <option value="MXN" @if($compensaciones->divisa =="MXN") selected @endif>Pesos (MXN)</option>
                                    <option value="USD" @if($compensaciones->divisa =="USD") selected @endif> Dolar (USD)</option>
                                    <option value="EUR" @if($compensaciones->divisa =="EUR") selected @endif>Euros (EUR)</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-12 mt-5 row compensacionesForm">
                        	<p><strong>NOTA: Favor de ingresar la información en los recuadros marcados únicamente / Note: Please complete with your current information</strong></p>
                            <table class="table table-striped">
                            	<tr>
                                    <th width="40%"><strong>Compensación / Heading</strong></th>
                                    <th width="20%" class="text-center"><strong>% o días</strong></th>
                                    <th width="20%" class="text-center"><strong>Mensual / Monthly ( @if($compensaciones->divisa == "") MXN @else {{$compensaciones->divisa}} @endif )</strong></th>
                                    <th width="20%" class="text-center"><strong>Anual / Annual ( @if($compensaciones->divisa == "") MXN @else {{$compensaciones->divisa}} @endif )</strong></th>
                                </tr>
                                <tr>
                                	@php $salarioBruto = json_decode($compensaciones->salarioBruto, true) @endphp
                                	<td>Salario bruto mensual (antes de impuestos) / Base salary</td>
                                    <td></td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="salarioBruto[]" id="salarioBruto" step="0.01" value="{{$salarioBruto[0]}}" onChange="fillCompensaciones();"></td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="salarioBruto[]" id="salarioBrutoAnual" step="0.01" readonly value="{{$salarioBruto[1]}}"></td>
                                </tr>                                
                                <tr>
                                	@php $comisiones = json_decode($compensaciones->comisiones, true) @endphp
                                	<td>Comisiones mensuales (promedio) / Monthly sales commissions</td>
                                    <td></td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="comisiones[]" id="comisiones" step="0.01" value="{{$comisiones[0]}}" onChange="fillCompensaciones();"></td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="comisiones[]" id="comisionesAnual" step="0.01" readonly value="{{$comisiones[1]}}"></td>
                                </tr>
                                <tr>
                                	@php $bonoMensualPorObjetivos = json_decode($compensaciones->bonoMensualPorObjetivos, true) @endphp
                                	<td>Bono mensual por objetivos / Monthly performance bonus</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="bonoMensualPorObjetivos[]" value="Si" @if($bonoMensualPorObjetivos[0] == 'Si') checked="checked" @endif onClick="$('.bmo-input').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="bonoMensualPorObjetivos[]" value="No" @if($bonoMensualPorObjetivos[0] == 'No' || $bonoMensualPorObjetivos[0] == NULL) checked="checked" @endif onClick="$('.bmo-input').attr('readonly','readonly')"></label>
                                    </td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal bmo-input" name="bonoMensualPorObjetivos[]" id="bonoMensualPorObjetivos" step="0.01" value="{{$bonoMensualPorObjetivos[1]}}" onChange="fillCompensaciones();" @if($bonoMensualPorObjetivos[0] == 'No' || $bonoMensualPorObjetivos[0] == NULL) readonly @endif></td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="bonoMensualPorObjetivos[]" id="bonoMensualPorObjetivosAnual" step="0.01" readonly value="{{$bonoMensualPorObjetivos[2]}}"></td>
                                </tr>
                                <tr>
                                	@php $bonoAnualPorObjetivos = json_decode($compensaciones->bonoAnualPorObjetivos, true) @endphp
                                	<td>Bono anual por objetivos / Annual performance bonus</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="bonoAnualPorObjetivos[]" value="Si" @if($bonoAnualPorObjetivos[0] == 'Si') checked="checked" @endif onClick="$('.bao-input').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="bonoAnualPorObjetivos[]" value="No" @if($bonoAnualPorObjetivos[0] == 'No' || $bonoAnualPorObjetivos[0] == NULL) checked="checked" @endif onClick="$('.bao-input').attr('readonly','readonly')"></label>
                                    </td>
                                    <td></td>
                                    <td><span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success anualVal bao-input" name="bonoAnualPorObjetivos[]" id="bonoAnualPorObjetivosAnual" step="0.01" value="{{$bonoAnualPorObjetivos[1]}}" @if($bonoAnualPorObjetivos[0] == 'No' || $bonoAnualPorObjetivos[0] == NULL) readonly @endif></td>
                                </tr>
                                <tr>
                                	@php $aguinaldo = json_decode($compensaciones->aguinaldo, true) @endphp
                                	<td>Aguinaldo (dias)/ Christmas bonus</td>
                                    <td>
                                    	<input type="number" class="form-control float-left col-md-11 p-0 border-success" name="aguinaldo[]" id="aguinaldo" step="1" value="{{$aguinaldo[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="aguinaldo[]" id="aguinaldoAnual" step="0.01" readonly value="{{$aguinaldo[1]}}">
                                    </td>
                                </tr>
                                <tr>
                                	@php $primaVacacional = json_decode($compensaciones->primaVacacional, true) @endphp
                                	<td>Prima vacacional (%) / Vacations bonus</td>
                                    <td>
                                    	<input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success" name="primaVacacional[]" id="primaVacacional" step="1" value="{{$primaVacacional[0]}}" onChange="fillCompensaciones();"> <span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">%</span>
                                    </td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="primaVacacional[]" id="primaVacacionalAnual" step="0.01" readonly value="{{$primaVacacional[1]}}">
                                    </td>
                                </tr>
                                <tr>
                                	@php $fondoAhorro = json_decode($compensaciones->fondoAhorro, true) @endphp
                                	<td>Fondo de ahorro / Saving fund from company</td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="fondoAhorro[]" id="fondoAhorro" step="0.01" value="{{$fondoAhorro[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="fondoAhorro[]" id="fondoAhorroAnual" step="0.01" readonly value="{{$fondoAhorro[1]}}">
                                    </td>
                                </tr>
                                <tr>
                                	@php $fondoRetiro = json_decode($compensaciones->fondoRetiro, true) @endphp
                                	<td>Fondo de retiro / Retirement fund</td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="fondoRetiro[]" id="fondoRetiro" step="0.01" value="{{$fondoRetiro[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="fondoRetiro[]" id="fondoRetiroAnual" step="0.01" readonly value="{{$fondoRetiro[1]}}">
                                    </td>
                                </tr>
                                <tr>
                                	@php $valesDespensa = json_decode($compensaciones->valesDespensa, true) @endphp
                                	<td>Vales de despensa / Food allowance</td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="valesDespensa[]" id="valesDespensa" step="0.01" value="{{$valesDespensa[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="valesDespensa[]" id="valesDespensaAnual" step="0.01" readonly value="{{$valesDespensa[1]}}">
                                    </td>
                                </tr>
                                <tr>
                                	@php $valesRestaurante = json_decode($compensaciones->valesRestaurante, true) @endphp
                                	<td>Vales de restaurante / Restaurants allowance</td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="valesRestaurante[]" id="valesRestaurante" step="0.01" value="{{$valesRestaurante[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="valesRestaurante[]" id="valesRestauranteAnual" step="0.01" readonly value="{{$valesRestaurante[0]}}">
                                    </td>
                                </tr>
                                <tr>
                                	@php $valesGasolina = json_decode($compensaciones->valesGasolina, true) @endphp
                                	<td>Vales de gasolina / Gasoline allowance</td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success times12 mensualVal" name="valesGasolina[]" id="valesGasolina" step="0.01" value="{{$valesGasolina[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="valesGasolina[]" id="valesGasolinaAnual" step="0.01" readonly value="{{$valesGasolina[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                </tr>
                                <tr>
                                	@php $ptu = json_decode($compensaciones->ptu, true) @endphp
                                	<td>PTU</td>
                                    <td></td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 border-success anualVal" name="ptu[]" id="ptuAnual" step="0.01" value="{{$ptu[0]}}" onChange="fillCompensaciones();">
                                    </td>
                                </tr>
                                <tr>
                                	@php $otro = json_decode($compensaciones->otro, true) @endphp
                                	<td>Otros (especificar) / Others</td>
                                    <td colspan="2">
                                    	<textarea class="form-control" name="otro[]">{{$otro[0]}}</textarea>
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0 anualVal" name="otro[]" id="otroAnual" step="0.01" value="{{$otro[1]}}" onChange="fillCompensaciones();">
                                    </td>
                                </tr>
                                <tr>
                                	@php $total = json_decode($compensaciones->total, true) @endphp
                                	<td><strong>TOTAL</strong></td>
                                    <td></td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0" name="total[]" id="totalMensual" step="0.01" readonly value="{{$total[0]}}">
                                    </td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0" name="total[]" id="totalAnual" step="0.01" readonly value="{{$total[1]}}">
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4">&nbsp;</td>
                                </tr>
                                <tr>
                                	<td>Días de vacaciones / Paid holidays</td>
                                    <td>
                                    	<input type="number" class="form-control float-left col-md-11 p-0 border-success" name="diasVacaciones" id="diasVacaciones" step="0.01" value="{{$compensaciones->diasVacaciones}}" onChange="fillCompensaciones();">
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                	@php $automovil = json_decode($compensaciones->automovil, true) @endphp
                                	<td>Automóvil de la empresa / Car</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="automovil[]" value="Si" @if($automovil[0] == 'Si') checked="checked" @endif onClick="$('#automovilModelo').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="automovil[]" value="No" @if($automovil[0] == 'No' || $automovil[0] == NULL) checked="checked" @endif onClick="$('#automovilModelo').attr('readonly','readonly')"></label>
                                    </td>
                                    <td class="text-right">Modelo</td>
                                    <td>
                                    	<input type="text" class="form-control float-right col-md-11 p-0" name="automovil[]" id="automovilModelo" value="{{$automovil[1]}}" @if($automovil[0] == 'No' || $automovil[0] == NULL) readonly @endif>
                                    </td>
                                </tr>
                                <tr>
                                	@php $gastosAutomovil = json_decode($compensaciones->gastosAutomovil, true) @endphp
                                	<td>Gastos del automóvil / Car expenses</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="gastosAutomovil[]" value="Si" @if($gastosAutomovil[0] == 'Si') checked="checked" @endif onClick="$('#gastosAutomovilMonto').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="gastosAutomovil[]" value="No" @if($gastosAutomovil[0] == 'No' || $gastosAutomovil[0] == NULL) checked="checked" @endif onClick="$('#gastosAutomovilMonto').attr('readonly','readonly')"></label>
                                    </td>
                                    <td class="text-right">Monto</td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0" name="gastosAutomovil[]" id="gastosAutomovilMonto" step="0.01" value="{{$gastosAutomovil[1]}}" @if($gastosAutomovil[0] == 'No' || $gastosAutomovil[0] == NULL) readonly @endif>
                                    </td>
                                </tr>
                                <tr>
                                	@php $seguroVida = json_decode($compensaciones->seguroVida, true) @endphp
                                	<td>Seguro de vida (meses) / Life insurance</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="seguroVida[]" value="Si" @if($seguroVida[0] == 'Si') checked="checked" @endif onClick="$('#seguroVidaMeses').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="seguroVida[]" value="No" @if($seguroVida[0] == 'No' || $seguroVida[0] == NULL) checked="checked" @endif onClick="$('#seguroVidaMeses').attr('readonly','readonly')"></label>
                                    </td>
                                    <td class="text-right">Meses</td>
                                    <td>
                                    	<input type="number" class="form-control float-right col-md-11 p-0" name="seguroVida[]" id="seguroVidaMeses" step="1" value="{{$seguroVida[1]}}" @if($seguroVida[0] == 'No' || $seguroVida[0] == NULL) readonly @endif>
                                    </td>
                                </tr>
                                <tr>
                                	@php $gastosMedMenores = json_decode($compensaciones->gastosMedMenores, true) @endphp
                                	<td>Seguro de gastos médicos menores / Basic medical insurance</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="gastosMedMenores[0]" value="Si" @if($gastosMedMenores[0] == 'Si') checked="checked" @endif></label>
                                        <label>No <input type="radio" name="gastosMedMenores[0]" value="No" @if($gastosMedMenores[0] == 'No' || $gastosMedMenores[0] == NULL) checked="checked" @endif></label>
                                    </td>
                                    <td class="text-right">Familiar</td>
                                    <td class="text-center">
                                    	<label class="mr-5">Si <input type="radio" name="gastosMedMenores[1]" value="Si" @if($gastosMedMenores[1] == 'Si') checked="checked" @endif></label>
                                        <label>No <input type="radio" name="gastosMedMenores[1]" value="No" @if($gastosMedMenores[1] == 'No' || $gastosMedMenores[1] == NULL) checked="checked" @endif></label>
                                    </td>
                                </tr>
                                <tr>
                                	@php $gastoMedMayores = json_decode($compensaciones->gastoMedMayores, true) @endphp
                                	<td>Seguro de gastos médicos mayores / Full medical insurance</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="gastoMedMayores[0]" value="Si" @if($gastoMedMayores[0] == 'Si') checked="checked" @endif></label>
                                        <label>No <input type="radio" name="gastoMedMayores[0]" value="No" @if($gastoMedMayores[0] == 'No' || $gastoMedMayores[0] == NULL) checked="checked" @endif></label>
                                    </td>
                                    <td class="text-right">Familiar</td>
                                    <td class="text-center">
                                    	<label class="mr-5">Si <input type="radio" name="gastoMedMayores[1]" value="Si" @if($gastoMedMayores[1] == 'Si') checked="checked" @endif></label>
                                        <label>No <input type="radio" name="gastoMedMayores[1]" value="No" @if($gastoMedMayores[1] == 'No' || $gastoMedMayores[1] == NULL) checked="checked" @endif></label>
                                    </td>
                                </tr>
                                <tr>
                                	@php $vivienda = json_decode($compensaciones->vivienda, true) @endphp
                                	<td>Vivienda (renta/housing) / Housing allowance</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="vivienda[]" value="Si" @if($vivienda[0] == 'Si') checked="checked" @endif onClick="$('#viviendaMonto').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="vivienda[]" value="No" @if($vivienda[0] == 'No' || $vivienda[0] == NULL) checked="checked" @endif onClick="$('#viviendaMonto').attr('readonly','readonly')"></label>
                                    </td>
                                    <td class="text-right">Monto</td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="text" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0" name="vivienda[]" id="viviendaMonto" step=".01" value="{{$vivienda[1]}}" @if($vivienda[0] == 'No' || $vivienda[0] == NULL) readonly @endif>
                                    </td>
                                </tr>
                                <tr>
                                	@php $apoyosEducativos = json_decode($compensaciones->apoyosEducativos, true) @endphp
                                	<td>Apoyos educativos / Becas / Education allowance</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="apoyosEducativos[]" value="Si" @if($apoyosEducativos[0] == 'Si') checked="checked" @endif onClick="$('#apoyosEducativosMonto').removeAttr('readonly')"></label>
                                        <label>No <input type="radio" name="apoyosEducativos[]" value="No" @if($apoyosEducativos[0] == 'No' || $apoyosEducativos[0] == NULL) checked="checked" @endif onClick="$('#apoyosEducativosMonto').attr('readonly','readonly')"></label>
                                    </td>
                                    <td class="text-right">Monto</td>
                                    <td>
                                    	<span class="float-left col-md-1 col-sm-2 col-xs-2 p-0 pt-2">$</span> <input type="number" step="0.01" class="form-control float-left col-md-11 col-sm-10 col-xs-10 p-0" name="apoyosEducativos[]" id="apoyosEducativosMonto" step=".01" value="{{$apoyosEducativos[1]}}" @if($apoyosEducativos[0] == 'No' || $apoyosEducativos[0] == NULL) readonly @endif>
                                    </td>
                                </tr>
                                <tr>
                                	@php $stockNumeroAcciones = json_decode($compensaciones->stockNumeroAcciones, true) @endphp
                                	<td>Stock/Acciones</td>
                                    <td>
                                    	<label class="mr-5">Si / Yes <input type="radio" name="stockNumeroAcciones[]" value="Si" @if($stockNumeroAcciones[0] == 'Si') checked="checked" @endif></label>
                                        <label>No <input type="radio" name="stockNumeroAcciones[]" value="No" @if($stockNumeroAcciones[0] == 'No' || $stockNumeroAcciones[0] == NULL) checked="checked" @endif></label>
                                    </td>
                                    <td class="text-right"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                	<td>Otras prestaciones y comentarios / Others:</td>
                                    <td colspan="3">
                                    	<textarea class="form-control" name="otrasPrestaciones">{{$compensaciones->otrasPrestaciones}}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">&nbsp;
                                    	
                                    </td>
                                </tr>
                                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                    <tr>
                                        <td>Completado / Complete</td>
                                        <td colspan="3">
                                            <label class="mr-5">Si / Yes <input type="radio" name="completed" value="1" @if($compensaciones->completed == '1') checked="checked" @endif></label>
                                            <label>No <input type="radio" name="completed" value="0" @if($compensaciones->completed == '0') checked="checked" @endif></label>
                                        </td>
                                    </tr>
                                @elseif(Auth::user()->hasRole('Candidato'))
                                	<tr>
                                        <td colspan="4">
                                            <input type="hidden" name="completed" value="1">
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            <div class="clearfix"></div>
                        </div>
                         

                        <div class="col-md-12 form-group mb-5">
                            <div class="col-md-6 float-right text-right">
                                <a class="btn bg-black color-white" href="{{ URL::previous() . '#tabCandidatos' }}">
                                    {{ __('Cancelar') }}
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    Guardar / actualizar formato / Save
                                </button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
