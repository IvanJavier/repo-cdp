@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="bg-white col-md-12">
                <div class="">
                	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                		<h2 class="p-3 text-center">Editar usuario</h2>
                    @else
                    	<h2 class="p-3 text-center">Editar perfil</h2>
                    @endif
                    <form method="POST" id="editProfileForm" action="{{ url('/dashboard/users/edit') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        @if($reclutadores != NULL)
						<div class="form-group mb-5">
                                <div class="col-md-12 text-right">
                                	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                                        <label for="idOwnerReclutador" class="col-md-6 col-form-label">¿A qué HeadHunter pertenece este candidato?</label>									
                                        <div class="col-md-6">
                                            <select name="idOwnerReclutador" id="idOwnerReclutador" class="form-control">
                                                @foreach($reclutadores as $reclutador)
                                                    <option value="{{$reclutador->id}}" @if($reclutador->id == $idReclutador->idOwnerReclutador) selected="selected" @endif>{{$reclutador->name}} {{$reclutador->lastName}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                    	<input type="hidden" name="idOwnerReclutador" id="idOwnerReclutador" value="{{$idReclutador->idOwnerReclutador}}">
                                    @endif
                                </div>
                                <div class="clearfix"></div>               
                            </div>
                        @endif
                        
                        <div class="form-group row text-center">
                            <div class="col-md-12 text-center">
                                <h4>Datos principales</h4>         
                            </div>                       
                        </div>
                        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Tipo de usuario') }}</label>
    
                                <div class="col-md-6">
                                    {{$user->roles[0]->name}}
                                </div>
                            </div>
                        @endif
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if($user->roles[0]->name != 'Empresa')
                            <div class="form-group row">
                                <label for="lastName" class="col-md-4 col-form-label text-md-right">{{ __('Apellido') }}</label>

                                <div class="col-md-6">
                                    <input id="lastName" type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="{{ $user->lastName }}">

                                    @if ($errors->has('lastName'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('lastName') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @else
                            <input id="lastName" type="hidden" name="lastName" value="">
                        @endif

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Correo') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $user->phone }}">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right">¿Actualizar contraseña?</label>
                            <div class="col-md-6">
                                <label>Si <input type="radio" name="updatePwdRadio" value="1" onClick="checkUpdatePwd();"> | </label>
                                <label>No <input type="radio" name="updatePwdRadio" value="0" onClick="checkUpdatePwd();" checked></label>
                            </div>
                        </div>
                        <div class="form-group row updatePwdDiv">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Nueva contraseña</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control updatePwdInput" name="password" value="" disabled>
                            </div>
                        </div>
                        <div class="form-group row updatePwdDiv">
                            <label for="password_repeat" class="col-md-4 col-form-label text-md-right">Repetir contraseña</label>
                            <div class="col-md-6">
                                <input id="password_repeat" type="password" class="form-control updatePwdInput" name="password_repeat" value="" disabled>
                            </div>
                        </div>

                        @if($user->roles[0]->name == 'Reclutador')
                            @include('partials.forms.adminEditReclutador')
                        @elseif($user->roles[0]->name == 'Candidato')
                            @include('partials.forms.adminEditCandidato')
                        @elseif($user->roles[0]->name == 'Empresa')
                            @include('partials.forms.adminEditEmpresa')
                        @endif

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4 text-right">
                            	@if(Auth::user()->hasRole('Candidato') || Auth::user()->hasRole('Empresa'))
                                    <a class="btn bg-black color-white" href="{{ url('/dashboard') }}">
                                        {{ __('Cancelar') }}
                                    </a>
                                @else
                                	@if($user->roles[0]->name == 'Admin')
                                        <a class="btn bg-black color-white" href="{{ url('/dashboard') }}">
                                            {{ __('Cancelar') }}
                                        </a>
                                    @else
                                    	<a class="btn bg-black color-white" href="{{ url('/dashboard/users/'.$user->roles[0]->name) }}">
                                            {{ __('Cancelar') }}
                                        </a>
                                    @endif
                                @endif
                                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                                    <button type="submit" class="btn bg-levuAzul color-white">
                                        {{ __('Actualizar usuario') }}
                                    </button>
                                @else
                                	<button type="submit" class="btn bg-levuAzul color-white">
                                        {{ __('Actualizar datos') }}
                                    </button>
                                @endif
                            </div>
                        </div>
                        @if($user->roles[0]->name != 'Admin' && $user->roles[0]->name != 'Vendedor')
                        	<input id="folder" type="hidden" name="folder" value="{{$user_data->folder}}">
                        @endif
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <input type="hidden" name="role_type" value="{{$user->roles[0]->name}}">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
