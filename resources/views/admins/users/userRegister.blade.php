@extends('layouts.console')

@section('title', "LEVU Talent - Registro de usuarios")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="bg-white">

                <div class="col-md-12">
                	<h3 class="p-3 text-center">Nuevo {{$role_type}}</h3>
                	@if($role_type == 'Candidato' && (sizeof($reclutadores) == 0 || $reclutadores == NULL))
                        <p>Debes dar de alta por lo menos un HeadHunter.</p>
                        <p>
                            <a href="{{ url('/dashboard/users/registration/Reclutador') }}" class="btn btn-success btn-sm">
                                Nuevo HeadHunter
                            </a>
                        </p>
                   	@else
                    
                        <form id="regForm" method="POST" action="{{ url('/dashboard/users/registration') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            @if($role_type == 'Candidato')
                            	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                                    <div class="form-group mb-5">
                                        <div class="col-md-12 text-right">
                                            <label for="idOwnerReclutador" class="col-md-6 col-form-label">¿A qué HeadHunter pertenece este candidato?</label>									
                                            <div class="col-md-6">
                                                <select name="idOwnerReclutador" id="idOwnerReclutador" class="form-control">
                                                    @foreach($reclutadores as $reclutador)
                                                        <option value="{{$reclutador->id}}">{{$reclutador->name}} {{$reclutador->lastName}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>                
                                    </div>
                                @elseif(Auth::user()->hasRole('Reclutador'))
                                	<input name="idOwnerReclutador" id="idOwnerReclutador" type="hidden" value="{{Auth::user()->id}}">
                                @endif
                            @endif
                            <div class="form-group row text-center">
                                <div class="col-md-12 text-center">
                                    <h4>Datos principales</h4>
                                    @php if(isset($_GET['exist'])){echo '<span class="text-danger">El correo proporcionado ya existe.</span>';} @endphp        
                                </div>                       
                            </div>
                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('*Nombre') }}</label>
    
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="@php if(isset($_GET['name'])){echo $_GET['name'];} @endphp" required autofocus>
    
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            @if($role_type != 'Empresa')
                                <div class="form-group row">
                                    <label for="lastName" class="col-md-4 col-form-label text-md-right">{{ __('Apellido') }}</label>
    
                                    <div class="col-md-6">
                                        <input id="lastName" type="text" class="form-control{{ $errors->has('lastName') ? ' is-invalid' : '' }}" name="lastName" value="@php if(isset($_GET['lastName'])){echo $_GET['lastName'];} @endphp">
    
                                        @if ($errors->has('lastName'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('lastName') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            @else
                                <input id="lastName" type="hidden" name="lastName" value="">
                            @endif
    
                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('*Correo') }}</label>
    
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="@php if(isset($_GET['email'])){echo $_GET['email'];} @endphp" required>
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            <div class="form-group row">
                                <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Teléfono') }}</label>
    
                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="@php if(isset($_GET['phone'])){echo $_GET['phone'];} @endphp">
    
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
    
                            @if($role_type == 'Reclutador')
                                @include('partials/forms/adminRegisterReclutador')
                            @elseif($role_type == 'Candidato')
                                @include('partials/forms/adminRegisterCandidato')
                            @elseif($role_type == 'Empresa')
                                @include('partials/forms/adminRegisterEmpresa')
                            @endif
    
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4 text-right">
                                    <a class="btn bg-black color-white" href="{{ URL::previous() }}">
                                        {{ __('Cancelar') }}
                                    </a>
                                    <button type="submit" class="btn bg-levuAzul color-white">
                                        Registrar {{$role_type}}
                                    </button>
                                </div>
                            </div>
                            <input type="hidden" name="role" value="{{$role_type}}">
                        </form>
                        
                    @endif
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
@endsection
