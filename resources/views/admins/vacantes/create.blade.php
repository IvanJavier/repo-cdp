@extends('layouts.console')

@section('title', "LEVU Talent - Primer escaneo")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="bg-white">

                <div class="">
                	<h3 class="p-3">Primer escaneo</h3>
                    <p class="p-3 font-18 text-danger">(*) Datos que el cliente verá en su dashboard.</p>
                	@if(sizeof($reclutadores) == 0 || $reclutadores == NULL)
                        <p>Debes dar de alta por lo menos un HeadHunter.</p>
                        <p>
                            <a href="{{ url('/dashboard/users/registration/Reclutador') }}" class="btn btn-success btn-sm">
                                Nuevo HeadHunter
                            </a>
                        </p>
                    @elseif(sizeof($empresas) == 0 || $empresas == NULL)
                    	<p>Debes dar de alta por lo menos una empresa.</p>
                        <p>
                            <a href="{{ url('/dashboard/users/registration/Empresa') }}" class="btn btn-success btn-sm">
                                Nueva empresa
                            </a>
                        </p>
                    @else
                        <form id="regForm" method="POST" action="{{ url('/dashboard/vacantes/create') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
							<div class="form-group col-md-12 mb-5 row">
                                <div class="col-md-4">
                                    <label for="idSucursal" class="col-form-label">Ubicación de la vacante</label>
                                    <select name="idSucursal" id="idSucursal" class="form-control">
                                        @foreach($sucursales as $sucursal)
                                            <option value="{{$sucursal->id}}">{{$sucursal->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
								<div class="col-md-4 float-left">
									<label for="created_at" class="col-form-label">Fecha de creación*</label>
                                    <input id="created_at" name="created_at" type="text" class="form-control datepicker" value="@php echo date('Y-m-d'); @endphp" required autocomplete="off">
								</div>
								<div class="col-md-4 float-left">
									<label for="fechaPrimerosCandidatos" class="col-form-label">Fecha primeros candidatos</label>
                                    <input id="fechaPrimerosCandidatos" name="fechaPrimerosCandidatos" type="text" class="form-control datepicker" value="" autocomplete="off">
								</div>           
							</div>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Vendedor'))
                            <div class="form-group mb-5 col-md-12 row">
                                
                                <div class="col-md-4">
                                    <label for="idOwnerReclutador" class="col-form-label">¿A qué HeadHunter pertenece esta vacante?*</label>
                                    <select name="idOwnerReclutador" id="idOwnerReclutador" class="form-control">
                                        @foreach($reclutadores as $reclutador)
                                            <option value="{{$reclutador->id}}">{{$reclutador->name}} {{$reclutador->lastName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    @if(Auth::user()->hasRole('SuperAdmin'))
                                        <label for="idVendedor" class="col-form-label">Nombre del vendedor</label>
                                        <select name="idVendedor" id="idVendedor" class="form-control">
                                            @foreach($vendedores as $vendedor)
                                                <option value="{{$vendedor->id}}">{{$vendedor->name}} {{$vendedor->lastName}}</option>
                                            @endforeach
                                        </select>
                                    @elseif(Auth::user()->hasRole('Vendedor'))
                                        <input type="hidden" name="idVendedor" id="idVendedor" value="{{Auth::user()->id}}">
                                    @else
                                     	<input type="hidden" name="idVendedor" id="idVendedor" value="1">
                                    @endif
                                </div>
                                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Vendedor'))
                                <div class="col-md-4">
                                <label for="garantia" class="col-form-label">¿Es garantía?</label><br>
                                <strong>Si: </strong><input type="radio" name="garantia" id="garantia" value="1">
                                <strong>No: </strong><input type="radio" name="garantia" id="garantia" value="0" checked>                        
                                </div>  
                                @endif
                            </div>
                            @elseif(Auth::user()->hasRole('Reclutador'))
                                <input name="idOwnerReclutador" id="idOwnerReclutador" type="hidden" value="{{Auth::user()->id}}">
                                <input type="hidden" name="idVendedor" id="idVendedor" value="1">
                            @endif
                            @if(Auth::user()->hasRole('superAdmin') || Auth::user()->hasRole('Admin'))
                                <div class="form-group mb-5">
                                    <div class="col-md-3 float-left">
                                        <label for="costoInterno" class="col-form-label">Costo interno</label>
                                        <input id="costoInterno" name="costoInterno" type="number" class="form-control" value="">
                                    </div>
									<div class="col-md-3 float-left">
                                        <label for="tarifa" class="col-form-label">Tarifa</label>
                                        <input id="tarifa" name="tarifa" type="number" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3 float-left">
                                        <label for="valorAnticipo" class="col-form-label">Valor del anticipo</label>
                                        <input id="valorAnticipo" name="valorAnticipo" type="number" class="form-control" value="">
                                    </div>
                                    <div class="col-md-3 float-left">
                                        <label for="valorCierre" class="col-form-label">Valor del cierre</label>
                                        <input id="valorCierre" name="valorCierre" type="number" class="form-control" value="">
                                    </div>              
                                </div>
							
								<div class="form-group mb-5">
                                    <div class="col-md-4 float-left">
                                        <label for="bonoReclutador" class="col-form-label">Bono al HeadHunter</label>
                                        <input id="bonoReclutador" name="bonoReclutador" type="number" class="form-control" value="">
                                    </div> 
									<div class="col-md-4 float-left">
                                        <label for="bonoVendedor" class="col-form-label">Bono al vendedor</label>
                                        <input id="bonoVendedor" name="bonoVendedor" type="number" class="form-control" value="">
                                    </div> 
									<div class="col-md-4 float-left">
                                        <label for="bonoAdministrador" class="col-form-label">Bono al administrador</label>
                                        <input id="bonoAdministrador" name="bonoAdministrador" type="number" class="form-control" value="">
                                    </div> 
                                </div>

                                <div class="form-group mb-5">
                                    <div class="col-md-4 float-left">
                                        <label for="convenioTarifa" class="col-form-label">Convenio tarifa</label>
                                        <input id="convenioTarifa" name="convenioTarifa" type="text" class="form-control" value="" required>
                                    </div> 
                                    <div class="col-md-4 float-left">
                                        <label for="convenioAnticipo" class="col-form-label">Convenio anticipo</label>
                                        <input id="convenioAnticipo" name="convenioAnticipo" type="text" class="form-control" value="" required>
                                    </div> 
                                    <div class="col-md-4 float-left">
                                        <label for="statusVacante" class="col-form-label">Estatus</label>
                                        <select class="form-control" name="statusVacante" id="statusVacante">
                                            <option value="Estable" selected>Estable</option>
                                            <option value="Posible Cierre">Posible Cierre</option>
                                            <option value="Búsqueda Urgente">Búsqueda Urgente</option>
                                        </select>
                                    </div> 
                                </div>
                            @else
								<input id="costoInterno" name="costoInterno" type="hidden" value="">
                            	<input id="tarifa" name="tarifa" type="hidden" value="">
                                <input id="valorAnticipo" name="valorAnticipo" type="hidden" value="">
                                <input id="valorCierre" name="valorCierre" type="hidden" value="">
                                <input id="bonoReclutador" name="bonoReclutador" type="hidden" value="">
                                <input id="bonoVendedor" name="bonoVendedor" type="hidden" class="form-control" value="">
                                <input id="bonoAdministrador" name="bonoAdministrador" type="hidden" class="form-control" value="">
                                <input id="convenioTarifa" name="convenioTarifa" type="hidden" class="form-control" value="">
                                <input id="convenioAnticipo" name="convenioAnticipo" type="hidden" class="form-control" value="">
                            @endif
                            
                            <div class="form-group text-center">
                                <div class="col-md-12 text-center">
                                    <h3>Datos del proyecto</h3>
                                </div>                       
                            </div>
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="" class="col-form-label">Fecha*</label>
                                    <input id="" type="text" class="form-control" value="{{date('Y-m-d H:i:s')}}" required readonly disabled>
                                </div>
                                <div class="col-md-6">
                                    <label for="titulo" class="col-form-label">Vacante*</label>
                                    <input id="titulo" type="text" class="form-control" name="titulo" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="" class="col-form-label">Empresa*</label>
                                    <select name="idUserEmpresa" class="form-control" onchange="empresasData(this.value)">
                                        <option value="0" disabled selected>Selecciona una empresa*</option>
                                        @foreach($empresas as $empresa)
                                            <option value="{{$empresa->id}}">{{$empresa->name}}</option>
                                        @endforeach
                                    </select>
                                    <input id="empresaEmail" type="hidden" name="empresaEmail">
                                    <input id="empresaName" type="hidden" name="empresaName">
                                </div>
                                <div class="col-md-6">
                                    <label for="jefeVacantenombre" class="col-form-label">Jefe de la vacante (nombre)*</label>
                                    <input id="jefeVacantenombre" type="text" class="form-control" name="jefeVacantenombre" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="contactoName" class="col-form-label">Nombre del contacto*</label>
                                    <input id="contactoName" type="text" class="form-control" name="contactoName" required readonly>
                                </div>
                                <div class="col-md-6">
                                    <label for="jefeVacantePuesto" class="col-form-label">Jefe de la vacante (puesto)*</label>
                                    <input id="jefeVacantePuesto" type="text" class="form-control" name="jefeVacantePuesto" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="contactoEmail" class="col-form-label">Email del contacto*</label>
                                    <input id="contactoEmail" type="text" class="form-control" name="contactoEmail" required readonly>
                                </div>
                                <div class="col-md-6">
                                    <label for="entrevistadorUno" class="col-form-label">Entrevistador 1*</label>
                                    <input id="entrevistadorUno" type="text" class="form-control" name="entrevistadorUno" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
    
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="contactoPhone" class="col-form-label">Teléfono de contacto*</label>
                                    <input id="contactoPhone" type="text" class="form-control" name="contactoPhone" required readonly disabled>
                                </div>
                                <div class="col-md-6">
                                    <label for="entrevistadorDos" class="col-form-label">Entrevistador 2*</label>
                                    <input id="entrevistadorDos" type="text" class="form-control" name="entrevistadorDos" required>
                                </div>
                                
                                <div class="clearfix"></div>
                            </div>
    
                            <div class="form-group mb-5">
                                <div class="col-md-6">
                                    <label for="giroEmpresa" class="col-form-label">Sector de la empresa*</label>
                                    <input id="giroEmpresa" type="text" class="form-control" name="giroEmpresa" required readonly disabled>
                                </div>
                                <div class="col-md-6">
                                    <label for="otroEntrevistador" class="col-form-label">Otro Entrevistador</label>
                                    <input id="otroEntrevistador" type="text" class="form-control" name="otroEntrevistador">
                                </div>
                                <div class="clearfix"></div>
                            </div>
    
                            <div class="form-group text-center mb-3">
                                <div class="col-md-12 text-center">
                                    <h3>Domicilio de la entrevista</h3>
                                </div>                       
                            </div>
                            <div class="form-group mb-3">
                                <div class="col-md-5 text-right">
                                    <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); $('.dirEntrevista').removeAttr('readonly'); loadSepomex();" checked> México</label>
                                </div>
                                <div class="col-md-2 text-center">
                                    <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); $('.dirEntrevista').removeAttr('readonly','readonly');  loadSepomex();"> Fuera de México</label>
                                </div>
                                <div class="col-md-2 text-left">
                                    <label class="pointer"><input id="ubicacion2" type="radio" name="ubicacion" value="video" onclick="$('.dirData').attr('readonly','readonly'); $('.dirEntrevista').attr('readonly','readonly');">Entrevista por video</label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="calleNumero" class="col-form-label">Calle y número*</label>
                                    <input id="calleNumero" type="text" class="form-control dirEntrevista" name="calleNumero" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="cp" class="col-form-label">CP*</label>
                                    <input id="cp" type="text" class="form-control dirEntrevista" name="cp" onchange="loadSepomex();" required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="col" class="col-form-label">Colonia*</label>
                                    <div class="col-md-12 pl-0 pr-0" id="sepomexCol">
                                        <input id="col" type="text" class="form-control dirData" name="col" readonly>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="delmpo" class="col-form-label">Delegación / Municipio*</label>
                                    <input id="delmpo" type="text" class="form-control dirData" name="delmpo" readonly>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-5">
                                <div class="col-md-6">
                                    <label for="edo" class="col-form-label">Estado / Región*</label>
                                    <input id="edo" type="text" class="form-control dirData" name="edo" readonly>
                                </div>
                                <div class="col-md-6">
                                    <label for="pais" class="col-form-label">País*</label>
                                    <input id="pais" type="text" class="form-control dirData" name="pais" readonly>
                                </div>
                                <div class="clearfix"></div>
                            </div>
    
    
                            <div class="form-group text-center mb-3">
                                <div class="col-md-12 text-center">
                                    <h3>Datos de la vacante</h3>
                                </div>                       
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="" class="col-md-12 col-form-label p0">¿Es reemplazo?*</label>
                                    <div class="col-md-6 p0">
                                        <label class="pointer"><input id="reemplazo0" type="radio" name="reemplazo" value="1" checked> Si</label>
                                    </div>
                                    <div class="col-md-6 p0">
                                        <label class="pointer"><input id="reemplazo1" type="radio" name="reemplazo" value="0">No</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="" class="col-md-12 col-form-label p0">Vacante confidencial*</label>
                                    <div class="col-md-6 p0">
                                        <label class="pointer"><input id="confidencial0" type="radio" name="confidencial" value="1" checked> Si</label>
                                    </div>
                                    <div class="col-md-6 p0">
                                        <label class="pointer"><input id="confidencial1" type="radio" name="confidencial" value="0">No</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="" class="col-md-12 col-form-label p0">¿Nueva creación?*</label>
                                    <div class="col-md-6 p0">
                                        <label class="pointer"><input id="nuevaCreacion0" type="radio" name="nuevaCreacion" value="1" checked> Si</label>
                                    </div>
                                    <div class="col-md-6 p0">
                                        <label class="pointer"><input id="nuevaCreacion1" type="radio" name="nuevaCreacion" value="0">No</label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="cantidadDeVacantes" class="col-form-label">Cantidad de vacantes*</label>
                                    <input id="cantidadDeVacantes" type="number" class="form-control" name="cantidadDeVacantes" placeholder="Indicar la cantidad de personas a contratar." required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="experiencia" class="col-form-label">Experiencia*</label>
                                    <input id="experiencia" type="text" class="form-control" name="experiencia" placeholder="Años de experiencia, sector o industrias de experiencia, procesos de experiencia del candidato" required>
                                </div>
                                <div class="col-md-6">
                                    <label for="escolaridad" class="col-form-label">Escolaridad*</label>
                                    <input id="escolaridad" type="text" class="form-control" name="escolaridad" placeholder="Indicar si se requiere licenciatura, maestría o doctorado en algún campo en específico." required>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="" class="col-form-label col-md-12 pl-0">Edad*</label>
                                    
                                    <label for="edadDesde" class="col-md-3 pl-0">Desde</label>
                                    <input id="edadDesde" type="number" class="form-control col-md-3" name="edadDesde" required placeholder="Desde">
                                    <label for="edadHasta" class="col-md-3 text-center">hasta</label>
                                    <input id="edadHasta" type="number" class="form-control col-md-3" name="edadHasta" required placeholder="Hasta">
                                </div>
                                <div class="col-md-6">
                                    <label for="" class="col-md-12 col-form-label pl-0">Sexo*</label>
                                    <div class="col-md-4 p0">
                                        <label class="pointer"><input id="sexo0" type="radio" name="sexo" value="Hombre" checked> Hombre</label>
                                    </div>
                                    <div class="col-md-4 p0">
                                        <label class="pointer"><input id="sexo1" type="radio" name="sexo" value="Mujer"> Mujer</label>
                                    </div>
                                    <div class="col-md-4 p0">
                                        <label class="pointer"><input id="sexo2" type="radio" name="sexo" value="Indistinto"> Indistinto</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="sueldoMensual" class="col-form-label">Salario mensual*</label>
                                    <div class="clearfix"></div>
                                    <input id="sueldoMensual" type="number" step="0.01" class="form-control float-left mr-2 sueldoMensual" name="sueldoMensual" required placeholder="Asegurar determinar un salario para tener una búsqueda eficiente.">
                                    <input id="sueldoMensualHasta" type="number" step="0.01" class="form-control float-left mr-2 sueldoMensual" name="sueldoMensualHasta" required placeholder="Asegurar determinar un salario para tener una búsqueda eficiente.">
                                    <select id="divisa" name="divisa" class="form-control float-left sueldoMensual">
                                        <option value="MXN">Pesos (MXN)</option>
                                        <option value="USD">Dolares (USD)</option>
                                        <option value="EUR">Euros (EUR)</option>
                                    </select>
                                    <label class="sueldoMensual float-left border p-2 mt-3 mr-2"><input type="radio" name="sueldoTipo" id="sueldoTipo0" value="Netos" checked="checked"> Netos</label>
                                    <label class="sueldoMensual float-left border p-2 mt-3"><input type="radio" name="sueldoTipo" id="sueldoTipo1" value="Brutos"> Brutos</label>
                                </div>
                                <div class="col-md-6">
                                    <label for="prestaciones" class="col-form-label">Prestaciones*</label>
                                    <input id="prestaciones" type="text" class="form-control" name="prestaciones" required placeholder="Enlistar todas las prestaciones y el tipo de contrato.">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="compensacionVariable" class="col-form-label">Compensación variable*</label>
                                    <input id="compensacionVariable" type="text" class="form-control" name="compensacionVariable" required placeholder="Indicar el valor del bono en moneda, meses o %, y la periodicidad de pago.">
                                </div>
                                <div class="col-md-6">
                                    <label for="viajesAlAnio" class="col-form-label">Cantidad de viajes de trabajo al año*</label>
                                    <input id="viajesAlAnio" type="text" class="form-control" name="viajesAlAnio" required placeholder="Indicar un porcentaje aproximado del tiempo del año que pasa de viaje.">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-5">
                                <div class="col-md-6">
                                    <label for="horarioLaboral" class="col-form-label">Días y horario de trabajo*</label>
                                    <input id="horarioLaboral" type="text" class="form-control" name="horarioLaboral" required placeholder="Indicar los días de trabajo, horas de entrada y de salida. Agregar si existe remoto, flexible, etc">
                                </div>
                                <div class="clearfix"></div>
                            </div>
    
                            <div class="form-group row text-center mb-3">
                                <div class="col-md-12 text-center">
                                    <h3>Elementos indispensables</h3>
                                </div>                       
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="puestosQueLaReportan" class="col-form-label">Puestos que la reportan*</label>
                                    <textarea id="puestosQueLaReportan" type="text" class="form-control" name="puestosQueLaReportan" required placeholder="Tamaño del equipo, puestos de sus reportes directos e indirectos."></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="habilidadesNecesarias" class="col-form-label">Habilidades y conocimientos técnicos necesarios*</label>
                                    <textarea id="habilidadesNecesarias" type="text" class="form-control" name="habilidadesNecesarias" required placeholder="¿Qué conocimientos técnicos debe tener el candidato? ¿Qué tipo de procesos debe conocer? ¿Existe alguna disciplina, aparato, herramienta, producto, servicio que el candidato deba dominar?."></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="sistemas" class="col-form-label">Software e idiomas requeridos*</label>
                                    <textarea id="sistemas" type="text" class="form-control" name="sistemas" required placeholder="¿Existe algún software, tecnología o programa en especial que el candidato deba dominar? ¿Cuáles softwares son similares a los que el candidato debe saber?"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="mision" class="col-form-label">Misión del puesto*</label>
                                    <textarea id="mision" type="text" class="form-control" name="mision" required placeholder="En una frase, ¿A que viene el candidato a la empresa y al departamento?"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="funciones" class="col-form-label">Funciones (principales responsabilidades)*</label>
                                    <textarea id="funciones" type="text" class="form-control" name="funciones" required placeholder="Si tuvieras que resumir las funciones del puesto en 5, ¿Cuáles serían esas 5 funciones principales que desempeñará? ¿Existe algo que quieras aclarar que el puesto NO hace?"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="indicadoresDesempenio" class="col-form-label">Indicadores de desempeño del puesto*</label>
                                    <textarea id="indicadoresDesempenio" type="text" class="form-control" name="indicadoresDesempenio" required placeholder="¿Cómo lo vas a medir? ¿Con cuales indicadores? ¿Ya existen o los van a definir junto con el candidato?"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="entregables" class="col-form-label">Entregables del puesto*</label>
                                    <textarea id="entregables" type="text" class="form-control" name="entregables" required placeholder="¿Cuáles serán los entregables del puesto, cada cuanto y que tipo de formato son? ¿Son análisis mensuales, semanales, planes de trabajo, reportes sacados de un sistema? ¿Existen hoy?"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="problemasActuales" class="col-form-label">Problemas actuales del puesto*</label>
                                    <textarea id="problemasActuales" type="text" class="form-control" name="problemasActuales" required placeholder="¿Qué problema existe en el área que quieres resolver contratando a esta persona? ¿A qué problemas se va a enfrentar esta persona al ingresar?"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="resultadosCorto" class="col-form-label">Resultados esperados del puesto a corto plazo*</label>
                                    <textarea id="resultadosCorto" type="text" class="form-control" name="resultadosCorto" required placeholder="Después hablaremos de resultados a mediano plazo pero, ¿Qué esperarías ver en esta persona en los primeros 30 días en la oficina? ¿Alguna actitud, alguna participación, alguna destreza, etc?"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="resultadosMediano" class="col-form-label">Resultados esperados del puesto a mediano plazo*</label>
                                    <textarea id="resultadosMediano" type="text" class="form-control" name="resultadosMediano" required placeholder="¿Qué esperarías ver de resultados de esta persona en 6 o 9 meses? ¿Qué te gustaría que esté funcionando de forma diferente y mejor?"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="motivosRechazo" class="col-form-label">Motivos o características de rechazo inmediato de un candidato*</label>
                                    <textarea id="motivosRechazo" type="text" class="form-control" name="motivosRechazo" required placeholder="¿Existe algún motivo por el que puedas descartar a un candidato inmediatamente? Cualquier motivo."></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="empresasSimilares" class="col-form-label">Empresas o sectores similares*</label>
                                    <textarea id="empresasSimilares" type="text" class="form-control" name="empresasSimilares" required placeholder="¿Existe alguna empresa de la que no debamos contactar a sus colaboradores? ¿Existe alguna empresa en particular de la que quieras que evaluemos personas? ¿Existe algún sector o industria que sea similar a la tuya?"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="planInternoReferidos" class="col-form-label">Pruebas o exámenes técnicos que aplicará el cliente*</label>
                                    <textarea id="planInternoReferidos" type="text" class="form-control" name="planInternoReferidos" required placeholder="Además de nuestras pruebas, ¿Ustedes aplicarán alguna prueba, exámen, assessment, challenge, etc en particular?"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="descripcionColaboradorIdeal" class="col-form-label">Desc. de un colaborador ideal hoy en este dpto.*</label>
                                    <textarea id="descripcionColaboradorIdeal" type="text" class="form-control" name="descripcionColaboradorIdeal" required placeholder="Con fines de presentar perfiles similares, ¿Me podrías describir la personalidad de un colaborador de la empresa o el departamento que actualmente es visto con excelente desempeño?"></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-6">
                                    <label for="viaComunicacionLevu" class="col-form-label">Vía preferida de comunicación con el HeadHunter*</label>
                                    <textarea id="viaComunicacionLevu" type="text" class="form-control" name="viaComunicacionLevu" required placeholder="¿Cómo prefieres que sea nuestra comunicación durante el proceso? ¿Solo por mail, solo por chat, solo por teléfono?"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="preguntasClave" class="col-form-label">Preguntas claves para primer filtro telefónico*</label>
                                    <textarea id="preguntasClave" type="text" class="form-control" name="preguntasClave" required placeholder="ESTA SECCIÓN NUNCA LA VEN EL CLIENTE NI EL CANDIDATO: Escribir aquí preguntas que serán determinantes en un primer filtro telefónico con los candidatos para saber si se les hace el proceso o no conviene."></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
    
                            <div class="form-group text-center mb-3">
                                <div class="col-md-12 text-center">
                                    <h3>Cultura de la empresa</h3>
                                </div>  
                                <div class="clearfix"></div>                     
                            </div>
                            
                            <div class="form-group mb-3">
                                <div class="col-md-12">
                                    <label>Cultura general*</label>
                                    <textarea id="culturaDeLaEmpresa" type="text" class="form-control" name="culturaDeLaEmpresa" required placeholder="NUNCA PONER EL NOMBRE DEL CLIENTE, PUEDE SER CONFIDENCIAL. Esta sección la ven el cliente y el candidato. Indicar datos sobre la empresa, años de antigüedad, cantidad de empleados, en donde tienen oficinas o presencia. Tipo de productos o servicios que venden, crecimiento que han tenido en los años, si situación actual en temas de retos, cambios, obstáculos, etc."></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group mb-5">
                                <div class="col-md-6 float-left">
                                    <label>Logos y símbolos*</label>
                                    <textarea id="logosYsimbolos" type="text" class="form-control" name="logosYsimbolos" required placeholder="¿Cómo son sus oficinas? ¿Cómo visten? ¿cómo es el lenguaje dentro de la empresa entre compañeros? ¿Son flexibles? ¿Es un estilo conservador, típico, millennial?"></textarea>
                                </div>
                                <div class="col-md-6 float-left">
                                    <label>Normas y patrones de conducta*</label>
                                    <textarea id="normasYpatronesConducta" type="text" class="form-control" name="normasYpatronesConducta" required placeholder="¿Cómo es el estilo de liderazgo de quién será su jefe directo? ¿Existen reglas especiales dentro de la empresa y las oficinas? En general, ¿cómo opinas que es la relación entre los colaboradores?"></textarea>
                                </div>
                                <div class="col-md-6 float-left">
                                    <label>Fundamentos y valores*</label>
                                    <textarea id="fundamentosYvalores" type="text" class="form-control" name="fundamentosYvalores" required placeholder="Si preguntara a los colaboradores sobre el ambiente laboral en la empresa, ¿Qué crees que me responderían? ¿Cuáles son los valores o moral que tienen en común las personas dentro de la empresa?"></textarea>
                                </div>
                                <div class="col-md-6 float-left">
                                    <label>Política interna*</label>
                                    <textarea id="politicaInterna" type="text" class="form-control" name="politicaInterna" required placeholder="¿Existen procesos documentados? ¿Las cabezas de área tienen poder de decisión o no? ¿Qué tanto se involucra la Dirección General en el día a día? Cuéntame una propuesta que se haya implementado en los últimos 12 meses y que fue idea de un colaborador."></textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="form-group mt-3 mb-3">
                                <div class="col-md-12 text-right">
                                    <a class="btn bg-black color-white" href="{{ URL::previous() }}">
                                        Cancelar
                                    </a>
                                    <button type="submit" class="btn bg-levuAzul color-white">
                                        Registrar vacante
                                    </button>
                                </div>
                                <div class="clearfix"></div>
                            </div>
    
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
