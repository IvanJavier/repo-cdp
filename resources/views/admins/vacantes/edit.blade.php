@extends('layouts.console')

@section('title', "LEVU Talent - Editar vacante")

@section('content')

<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        
        <div class="col-md-12">
            <div class="bg-white">

                <div class="">
                	<h3 class="p-3">Editar vacante</h3>
                    <p class="p-3 font-18 text-danger">(*) Datos que el cliente verá en su dashboard.</p>
                    <form id="regForm" method="POST" action="{{ url('/dashboard/vacantes/edit') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
						<div class="form-group col-md-12 mb-5 row">
                             <div class="col-md-4">
                                    <label for="idSucursal" class="col-form-label">Sucursal</label>
                                    <select name="idSucursal" id="idSucursal" class="form-control">
                                        @foreach($sucursales as $sucursal)
                                            <option value="{{$sucursal->id}}" @if($sucursal->id == $vacante->sucursalId) selected @endif>{{$sucursal->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
							<div class="col-md-4 float-left">
								<label for="created_at" class="col-form-label">Fecha de creación*</label>
								<input id="created_at" name="created_at" type="text" class="form-control datepicker" value="{{$vacante->created_at}}" required autocomplete="off">
							</div>
							<div class="col-md-4 float-left">
								<label for="fechaPrimerosCandidatos" class="col-form-label">Fecha primeros candidatos</label>
								<input id="fechaPrimerosCandidatos" name="fechaPrimerosCandidatos" type="text" class="form-control datepicker" value="{{$vacante->fechaPrimerosCandidatos}}" autocomplete="off">
							</div>           
						</div>
                        <div class="form-group col-md-12 mb-5 row">
                            <div class="col-md-6 float-left">
                                <label for="estatus" class="col-form-label">Estatus de la vacante*</label>
                                <select name="estatus" class="form-control">
                                    <option value="Abierta" class="text-success" @if($vacante->estatus == 'Abierta') selected="selected" @endif>Abierta</option>
                                    <option value="Cerrada" class="text-success" @if($vacante->estatus == 'Cerrada') selected="selected" @endif>Cerrada</option>
                                    <option value="Cerrada con contratación" class="text-success" @if($vacante->estatus == 'Cerrada con contratación') selected="selected" @endif>Cerrada con contratación</option>
                                    <option value="Cerrada sin contratación" class="text-success" @if($vacante->estatus == 'Cerrada sin contratación') selected="selected" @endif>Cerrada sin contratación</option>
                                </select>
                            </div>
                            <div class="col-md-6 float-left">
                                <label for="garantia" class="col-form-label">¿Es garantía?</label><br>
                                @if ($vacante->garantia == 0)
                                <strong>No: </strong><input type="radio" name="garantia" id="garantia" value="0" checked>                                                                  
                                <strong>Si: </strong><input type="radio" name="garantia" id="garantia" value="1">                                                 
                                @else
                                <strong>No: </strong><input type="radio" name="garantia" id="garantia" value="0">                                                                  
                                <strong>Si: </strong><input type="radio" name="garantia" id="garantia" value="1" checked>                 
                                @endif
                            </div>        
                        </div>
                        <div class="form-group col-md-12 mb-5 row">
                        	<div class="col-md-6 float-left">
                            	@if(Auth::user()->hasRole('Reclutador'))
                                    <label for="idOwnerReclutador" class="col-form-label">¿Deseas transferir esta vacante? Selecciona al nuevo HeadHunter.</label>
                                @else
                                	<label for="idOwnerReclutador" class="col-form-label">¿A qué HeadHunter pertenece esta vacante?*</label>
                                @endif
                                <select name="idOwnerReclutador" id="idOwnerReclutador" class="form-control">
                                    @foreach($reclutadores as $reclutador)
                                        <option value="{{$reclutador->id}}" @if($vacante->ownerId == $reclutador->id) selected="selected" @endif>{{$reclutador->name}} {{$reclutador->lastName}}</option>
                                    @endforeach
                                </select>
                                @if(Auth::user()->hasRole('Reclutador'))
                                	<small>NOTA: Al transferir la vacante, perderás tu acceso a ella.</small>
                                @endif
                                <input id="idOwnerReclutadorOld" name="idOwnerReclutadorOld" type="hidden" value="{{$vacante->ownerId}}">
                            </div>
                            {{-- VENDEDOR --}}
                            <div class="col-md-6 float-left">
                            	@if(Auth::user()->hasRole('SuperAdmin'))
                                    <label for="idVendedor" class="col-form-label">Nombre del vendedor</label>
                                    <select name="idVendedor" id="idVendedor" class="form-control">
                                        @foreach($vendedores as $vendedor)
                                            <option value="{{$vendedor->id}}" @if($vacante->vendedorId == $vendedor->id) selected="selected" @endif>{{$vendedor->name}} {{$vendedor->lastName}}</option>
                                        @endforeach
                                    </select>
                                @else
                                	<input type="hidden" name="idVendedor" id="idVendedor" value="{{$vacante->vendedorId}}">
                                @endif
                            </div>
                        </div>
                        @if(Auth::user()->hasRole('superAdmin') || Auth::user()->hasRole('Admin'))
                            <div class="form-group col-md-12 row mb-5">
								<div class="col-md-3 float-left">
									<label for="costoInterno" class="col-form-label">Costo interno</label>
									<input id="costoInterno" name="costoInterno" type="number" class="form-control" value="{{$vacante->costoInterno}}">
								</div>
                                <div class="col-md-3 float-left">
                                    <label for="tarifa" class="col-form-label">Tarifa</label>
                                    <input id="tarifa" name="tarifa" type="number" class="form-control" value="{{$vacante->tarifa}}">
                                </div>
                                <div class="col-md-3 float-left">
                                    <label for="valorAnticipo" class="col-form-label">Valor del anticipo</label>
                                    <input id="valorAnticipo" name="valorAnticipo" type="number" class="form-control" value="{{$vacante->valorAnticipo}}">
                                </div>
                                <div class="col-md-3 float-left">
                                    <label for="valorCierre" class="col-form-label">Valor del cierre</label>
                                    <input id="valorCierre" name="valorCierre" type="number" class="form-control" value="{{$vacante->valorCierre}}">
                                </div>     
                            </div>
							
							<div class="form-group col-md-12 row mb-5">
								<div class="col-md-4 float-left">
									<label for="bonoReclutador" class="col-form-label">Bono al HeadHunter</label>
									<input id="bonoReclutador" name="bonoReclutador" type="number" class="form-control" value="{{$vacante->bonoReclutador}}">

									<label for="">Pagado:</label> <input class="ml-3 mr-3" type="radio" name="bonoReclutadorPayed" value="1" @if($vacante->bonoReclutadorPayed == 1) checked="checked" @endif>Si | <input class="ml-3 mr-3" type="radio" name="bonoReclutadorPayed" value="0" @if($vacante->bonoReclutadorPayed == 0) checked="checked" @endif>No
								</div> 
								<div class="col-md-4 float-left">
									<label for="bonoVendedor" class="col-form-label">Bono al vendedor</label>
									<input id="bonoVendedor" name="bonoVendedor" type="number" class="form-control" value="{{$vacante->bonoVendedor}}">

									<label for="">Pagado:</label> <input class="ml-3 mr-3" type="radio" name="bonoVendedorPayed" value="1" @if($vacante->bonoVendedorPayed == 1) checked="checked" @endif>Si | <input class="ml-3 mr-3" type="radio" name="bonoVendedorPayed" value="0" @if($vacante->bonoVendedorPayed == 0) checked="checked" @endif>No
								</div> 
								<div class="col-md-4 float-left">
									<label for="bonoAdministrador" class="col-form-label">Bono al administrador</label>
									<input id="bonoAdministrador" name="bonoAdministrador" type="number" class="form-control" value="{{$vacante->bonoAdministrador}}">

									<label for="">Pagado:</label> <input class="ml-3 mr-3" type="radio" name="bonoAdministradorPayed" value="1" @if($vacante->bonoAdministradorPayed == 1) checked="checked" @endif>Si | <input class="ml-3 mr-3" type="radio" name="bonoAdministradorPayed" value="0" @if($vacante->bonoAdministradorPayed == 0) checked="checked" @endif>No
								</div> 
							</div>

                            <div class="form-group col-md-12 row mb-5">
                                <div class="col-md-4 float-left">
                                    <label for="convenioTarifa" class="col-form-label">Convenio tarifa</label>
                                    <input id="convenioTarifa" name="convenioTarifa" type="text" class="form-control" value="{{$vacante->convenioTarifa}}">
                                </div> 
                                <div class="col-md-4 float-left">
                                    <label for="convenioAnticipo" class="col-form-label">Convenio anticipo</label>
                                    <input id="convenioAnticipo" name="convenioAnticipo" type="text" class="form-control" value="{{$vacante->convenioAnticipo}}">
                                </div>
                                <div class="col-md-4 float-left">
                                    <label for="statusVacante" class="col-form-label">Estatus</label>
                                    <select class="form-control" name="statusVacante" id="statusVacante">
                                        <option value="{{$vacante->statusVacante}}">{{$vacante->statusVacante}}</option>
                                        <option>Selecciona...</option>
                                        <option value="Estable">Estable</option>
                                        <option value="Posible Cierre">Posible Cierre</option>
                                        <option value="Búsqueda Urgente">Búsqueda Urgente</option>
                                    </select>
                                </div> 
                            </div>


                        @else
                           	<input id="costoInterno" name="costoInterno" type="hidden" value="{{$vacante->costoInterno}}">
							<input id="tarifa" name="tarifa" type="hidden" value="{{$vacante->tarifa}}">
                            <input id="valorAnticipo" name="valorAnticipo" type="hidden" value="{{$vacante->valorAnticipo}}">
                            <input id="valorCierre" name="valorCierre" type="hidden" value="{{$vacante->valorCierre}}">
                            <input id="bonoReclutador" name="bonoReclutador" type="hidden" value="{{$vacante->bonoReclutador}}">
						
							<input id="bonoReclutadorPayed" name="bonoReclutadorPayed" type="hidden" value="{{$vacante->bonoReclutadorPayed}}">
                            <input id="bonoVendedor" name="bonoVendedor" type="hidden" class="form-control" value="{{$vacante->bonoVendedor}}">
							<input id="bonoVendedorPayed" name="bonoVendedorPayed" type="hidden" value="{{$vacante->bonoVendedorPayed}}">
                            <input id="bonoAdministrador" name="bonoAdministrador" type="hidden" class="form-control" value="{{$vacante->bonoAdministrador}}">
							<input id="bonoAdministradorPayed" name="bonoAdministradorPayed" type="hidden" value="{{$vacante->bonoAdministradorPayed}}">

                            <input id="convenioTarifa" name="convenioTarifa" type="hidden" value="{{$vacante->convenioTarifa}}">
                            <input id="convenioAnticipo" name="convenioAnticipo" type="hidden" value="{{$vacante->convenioAnticipo}}">
                        @endif
                        
                        <div class="form-group text-center mb-3">
                            <div class="col-md-12 text-center">
                                <h3>Datos del proyecto</h3>
                            </div>    
                            <div class="clearfix"></div>                   
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Fecha de creación*</label>
                                <input id="" type="text" class="form-control" value="{{$vacante->created_at}}" required readonly disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="titulo" class="col-form-label">Vacante*</label>
                                <input id="titulo" value="{{$vacante->titulo}}" type="text" class="form-control" name="titulo" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="" class="col-form-label">Empresa*</label>
                                {{--
                                <input id="" type="text" class="form-control" name="empresaName" readonly value="{{$vacante->empresa}}">
                                <input type="hidden" name="idUserEmpresa" value="{{$vacante->empresaId}}">
                                --}}
                                
                                <select name="idUserEmpresa" class="form-control" onchange="empresasData(this.value)">
                                    <option value="0" disabled selected>Selecciona una empresa</option>
                                    @foreach($empresas as $empresa)
                                        <option value="{{$empresa->id}}" @if($vacante->empresaId == $empresa->id) selected="selected" @endif>{{$empresa->name}}</option>
                                    @endforeach
                                </select>
                                <input id="empresaEmail" type="hidden" name="empresaEmail">
                                <input id="empresaName" type="hidden" name="empresaName"> 
                                
                                <input id="idUserEmpresa_old" type="hidden" name="idUserEmpresa_old" value="{{$vacante->empresaId}}">
                            </div>
                            <div class="col-md-6">
                                <label for="jefeVacantenombre" class="col-form-label">Jefe de la vacante (nombre)*</label>
                                <input id="jefeVacantenombre" value="{{$vacante->jefeVacantenombre}}" type="text" class="form-control" name="jefeVacantenombre" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="contactoName" class="col-form-label">Nombre del contacto*</label>
                                <input id="contactoName" value="{{$vacante->contactoName}}" type="text" class="form-control" name="contactoName" required readonly disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="jefeVacantePuesto" class="col-form-label">Jefe de la vacante (puesto)*</label>
                                <input id="jefeVacantePuesto" value="{{$vacante->jefeVacantePuesto}}" type="text" class="form-control" name="jefeVacantePuesto" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="contactoEmail" class="col-form-label">Email del contacto*</label>
                                <input id="contactoEmail" value="{{$vacante->contactoEmail}}" type="text" class="form-control" name="contactoEmail" required readonly disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="entrevistadorUno" class="col-form-label">Entrevistador 1*</label>
                                <input id="entrevistadorUno" value="{{$vacante->entrevistadorUno}}" type="text" class="form-control" name="entrevistadorUno" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group mb-5">
                            <div class="col-md-6">
                                <label for="contactoPhone" class="col-form-label">Teléfono de contacto*</label>
                                <input id="contactoPhone" value="{{$vacante->contactoPhone}}" type="text" class="form-control" name="contactoPhone" required readonly disabled>
                            </div>
                            <div class="col-md-6">
                                <label for="entrevistadorDos" class="col-form-label">Entrevistador 2*</label>
                                <input id="entrevistadorDos" value="{{$vacante->entrevistadorDos}}" type="text" class="form-control" name="entrevistadorDos" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="giroEmpresa" class="col-form-label">Sector de la empresa*</label>
                                <input id="giroEmpresa" value="{{$vacante->giroEmpresa}}" type="text" class="form-control" name="giroEmpresa" required readonly disabled>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group mb-3 text-center">
                            <div class="col-md-12 text-center">
                                <h3>Domicilio de la entrevista</h3>
                            </div> 
                            <div class="clearfix"></div>                      
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-5 text-right">
                                <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); $('.dirEntrevista').removeAttr('readonly'); loadSepomex();" @if($vacante->entrevista == 'local') checked @endif> México</label>
                            </div>
                            <div class="col-md-2 text-center">
                                <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); $('.dirEntrevista').removeAttr('readonly','readonly');"  @if($vacante->entrevista === 'extranjero') checked @endif> Fuera de México</label>
                            </div>
                            <div class="col-md-2 text-left">
                                <label class="pointer"><input id="ubicacion2" type="radio" name="ubicacion" value="video" onclick="$('.dirData').attr('readonly','readonly'); $('.dirEntrevista').attr('readonly','readonly');" @if($vacante->entrevista === 'video') checked @endif>Entrevista por video</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="calleNumero" class="col-form-label">Calle y número*</label>
                                <input id="calleNumero" value="{{$vacante->calleNumero}}" type="text" class="form-control dirEntrevista" name="calleNumero" required>
                            </div>
                            <div class="col-md-6">
                                <label for="cp" class="col-form-label">CP*</label>
                                <input id="cp" value="{{$vacante->cp}}" type="text" class="form-control dirEntrevista" name="cp" onchange="loadSepomex();" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="col" class="col-form-label">Colonia*</label>
                                <div class="col-md-12 pl-0 pr-0" id="sepomexCol">
                                    <input id="col" value="{{$vacante->col}}" type="text" class="form-control dirData" name="col" readonly required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="delmpo" class="col-form-label">Delegación / Municipio*</label>
                                <input id="delmpo" value="{{$vacante->delmpo}}" type="text" class="form-control dirData" name="delmpo" readonly required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-5">
                            <div class="col-md-6">
                                <label for="edo" class="col-form-label">Estado / Región*</label>
                                <input id="edo" value="{{$vacante->edo}}" type="text" class="form-control dirData" name="edo" readonly required>
                            </div>
                            <div class="col-md-6">
                                <label for="pais" class="col-form-label">País*</label>
                                <input id="pais" value="{{$vacante->pais}}" type="text" class="form-control dirData" name="pais" readonly required>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group mb-3 text-center">
                            <div class="col-md-12 text-center">
                                <h3>Datos de la vacante</h3>
                            </div>  
                            <div class="clearfix"></div>                     
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="" class="col-md-12 col-form-label p0">¿Es reemplazo?*</label>
                                <div class="col-md-6 p0">
                                    <label class="pointer"><input id="reemplazo0" type="radio" name="reemplazo" value="1" @if($vacante->reemplazo == '1') checked @endif> Si</label>
                                </div>
                                <div class="col-md-6 p0">
                                    <label class="pointer"><input id="reemplazo1" type="radio" name="reemplazo" value="0" @if($vacante->reemplazo == '0') checked @endif>No</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-md-12 col-form-label p0">Vacante confidencial*</label>
                                <div class="col-md-6 p0">
                                    <label class="pointer"><input id="confidencial0" type="radio" name="confidencial" value="1" @if($vacante->confidencial == '1') checked @endif> Si</label>
                                </div>
                                <div class="col-md-6 p0">
                                    <label class="pointer"><input id="confidencial1" type="radio" name="confidencial" value="0" @if($vacante->confidencial == '0') checked @endif>No</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="" class="col-md-12 col-form-label p0">¿Nueva creación?*</label>
                                <div class="col-md-6 p0">
                                    <label class="pointer"><input id="nuevaCreacion0" type="radio" name="nuevaCreacion" value="1" @if($vacante->nuevaCreacion == '1') checked @endif> Si</label>
                                </div>
                                <div class="col-md-6 p0">
                                    <label class="pointer"><input id="nuevaCreacion1" type="radio" name="nuevaCreacion" value="0" @if($vacante->nuevaCreacion == '0') checked @endif>No</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="cantidadDeVacantes" class="col-form-label">Cantidad de vacantes*</label>
                                <input id="cantidadDeVacantes" value="{{$vacante->cantidadDeVacantes}}" type="number" class="form-control" name="cantidadDeVacantes" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="experiencia" class="col-form-label">Experiencia*</label>
                                <input id="experiencia" value="{{$vacante->experiencia}}" type="text" class="form-control" name="experiencia" required>
                            </div>
                            <div class="col-md-6">
                                <label for="escolaridad" class="col-form-label">Escolaridad*</label>
                                <input id="escolaridad" value="{{$vacante->escolaridad}}" type="text" class="form-control" name="escolaridad" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="" class="col-form-label col-md-12 pl-0">Edad*</label>
                                <label for="edadDesde" class="col-md-2 pl-0">Desde</label>
                                <input id="edadDesde" value="{{$vacante->edadDesde}}" type="number" class="form-control col-md-3" name="edadDesde" required placeholder="Desde">
                                <label for="edadHasta" class="col-md-2 text-center">hasta</label>
                                <input id="edadHasta" value="{{$vacante->edadHasta}}" type="number" class="form-control col-md-3" name="edadHasta" required placeholder="Hasta">
                            </div>
                            <div class="col-md-6">
                                <label for="" class="col-md-12 col-form-label pl-0">Sexo*</label>
                                <div class="col-md-4 p0">
                                    <label class="pointer"><input id="sexo0" type="radio" name="sexo" value="Hombre" @if($vacante->sexo == 'Hombre') checked @endif> Hombre</label>
                                </div>
                                <div class="col-md-4 p0">
                                    <label class="pointer"><input id="sexo1" type="radio" name="sexo" value="Mujer" @if($vacante->sexo == 'Mujer') checked @endif> Mujer</label>
                                </div>
                                <div class="col-md-4 p0">
                                    <label class="pointer"><input id="sexo2" type="radio" name="sexo" value="Indistinto" @if($vacante->sexo == 'Indistinto') checked @endif> Indistinto</label>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="sueldoMensual" class="col-form-label">Salario mensual*</label>
                                <div class="clearfix"></div>
                                <input id="sueldoMensual" value="{{$vacante->sueldoMensual}}" type="number" step="0.01" class="form-control float-left mr-2 sueldoMensual" name="sueldoMensual" required>
                                <input id="sueldoMensualHasta" value="{{$vacante->sueldoMensualHasta}}" type="number" step="0.01" class="form-control float-left mr-2 sueldoMensual" name="sueldoMensualHasta" required>
                                <select id="divisa" name="divisa" clss="form-control float-left sueldoMensual">
                                    <option @if($vacante->divisa == 'MXN')selected="selected"@endif value="MXN">Pesos (MXN)</option>
                                    <option @if($vacante->divisa == 'USD')selected="selected"@endif value="USD">Dolares (USD)</option>
                                    <option @if($vacante->divisa == 'EUR')selected="selected"@endif value="EUR">Euros (EUR)</option>
                                </select>
                                <label class="sueldoMensual float-left border p-2 mt-3 mr-2"><input type="radio" name="sueldoTipo" id="sueldoTipo0" value="Netos" @if($vacante->sueldoTipo == 'Netos') checked="checked" @endif> Netos</label>
                                <label class="sueldoMensual float-left border p-2 mt-3"><input type="radio" name="sueldoTipo" id="sueldoTipo1" value="Brutos" @if($vacante->sueldoTipo == 'Brutos') checked="checked" @endif> Brutos</label>
                            </div>
                            <div class="col-md-6">
                                <label for="prestaciones" class="col-form-label">Prestaciones*</label>
                                <input id="prestaciones" value="{{$vacante->prestaciones}}" type="text" class="form-control" name="prestaciones" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="compensacionVariable" class="col-form-label">Compensación variable*</label>
                                <input id="compensacionVariable" value="{{$vacante->compensacionVariable}}" type="text" class="form-control" name="compensacionVariable" required>
                            </div>
                            <div class="col-md-6">
                                <label for="viajesAlAnio" class="col-form-label">Cantidad de viajes de trabajo al año*</label>
                                <input id="viajesAlAnio" value="{{$vacante->viajesAlAnio}}" type="text" class="form-control" name="viajesAlAnio" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-5">
                            <div class="col-md-6">
                                <label for="horarioLaboral" class="col-form-label">Días y horario de trabajo*</label>
                                <input id="horarioLaboral" value="{{$vacante->horarioLaboral}}" type="text" class="form-control" name="horarioLaboral" required>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group mb-3 text-center">
                            <div class="col-md-12 text-center">
                                <h3>Elementos indispensables</h3>
                            </div>  
                            <div class="clearfix"></div>                     
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="puestosQueLaReportan" class="col-form-label">Puestos que la reportan*</label>
                                <textarea id="puestosQueLaReportan" type="text" class="form-control" name="puestosQueLaReportan" required>{{$vacante->puestosQueLaReportan}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="habilidadesNecesarias" class="col-form-label">Habilidades y conocimientos técnicos necesarios*</label>
                                <textarea id="habilidadesNecesarias" type="text" class="form-control" name="habilidadesNecesarias" required>{{$vacante->habilidadesNecesarias}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="sistemas" class="col-form-label">Software e idiomas requeridos*</label>
                                <textarea id="sistemas" type="text" class="form-control" name="sistemas" required>{{$vacante->sistemas}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="mision" class="col-form-label">Misión del puesto*</label>
                                <textarea id="mision" type="text" class="form-control" name="mision" required>{{$vacante->mision}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="funciones" class="col-form-label">Funciones (principales responsabilidades)*</label>
                                <textarea id="funciones" type="text" class="form-control" name="funciones" required>{{$vacante->funciones}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="indicadoresDesempenio" class="col-form-label">Indicadores de desempeño del puesto*</label>
                                <textarea id="indicadoresDesempenio" type="text" class="form-control" name="indicadoresDesempenio" required>{{$vacante->indicadoresDesempenio}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="entregables" class="col-form-label">Entregables del puesto*</label>
                                <textarea id="entregables" type="text" class="form-control" name="entregables" required>{{$vacante->entregables}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="problemasActuales" class="col-form-label">Problemas actuales del puesto*</label>
                                <textarea id="problemasActuales" type="text" class="form-control" name="problemasActuales" required>{{$vacante->problemasActuales}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="resultadosCorto" class="col-form-label">Resultados esperados del puesto a corto plazo*</label>
                                <textarea id="resultadosCorto" type="text" class="form-control" name="resultadosCorto" required>{{$vacante->resultadosCorto}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="resultadosMediano" class="col-form-label">Resultados esperados del puesto a mediano plazo*</label>
                                <textarea id="resultadosMediano" type="text" class="form-control" name="resultadosMediano" required>{{$vacante->resultadosMediano}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="motivosRechazo" class="col-form-label">Motivos o características de rechazo inmediato de un candidato*</label>
                                <textarea id="motivosRechazo" type="text" class="form-control" name="motivosRechazo" required>{{$vacante->motivosRechazo}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="empresasSimilares" class="col-form-label">Empresas o sectores similares*</label>
                                <textarea id="empresasSimilares" type="text" class="form-control" name="empresasSimilares" required>{{$vacante->empresasSimilares}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="planInternoReferidos" class="col-form-label">Pruebas o exámenes técnicos que aplicará el cliente*</label>
                                <textarea id="planInternoReferidos" type="text" class="form-control" name="planInternoReferidos" required>{{$vacante->planInternoReferidos}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="descripcionColaboradorIdeal" class="col-form-label">Descripción de un colaborador ideal hoy en este departamento*</label>
                                <textarea id="descripcionColaboradorIdeal" type="text" class="form-control" name="descripcionColaboradorIdeal" required>{{$vacante->descripcionColaboradorIdeal}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-3">
                            <div class="col-md-6">
                                <label for="viaComunicacionLevu" class="col-form-label">Vía preferida de comunicación con Levu*</label>
                                <textarea id="viaComunicacionLevu" type="text" class="form-control" name="viaComunicacionLevu" required>{{$vacante->viaComunicacionLevu}}</textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="preguntasClave" class="col-form-label">Preguntas claves para primer filtro telefónico*</label>
                                <textarea id="preguntasClave" type="text" class="form-control" name="preguntasClave" required>{{$vacante->preguntasClave}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-5">
                            <div class="col-md-12">
                                <label for="elementosEstrategia" class="col-form-label">Elementos de la estrategia a utilizar en el resumen comparativo*</label>
                                <textarea id="elementosEstrategia" type="text" class="form-control" name="elementosEstrategia" required>{{$vacante->elementosEstrategia}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group mb-3 text-center">
                            <div class="col-md-12 text-center">
                                <h3>Cultura de la empresa</h3>
                            </div> 
                            <div class="clearfix"></div>                      
                        </div>
                        <div class="form-group mb-5">
                            <div class="col-md-12">
                                <label>Cultura general*</label>
                                <textarea id="culturaDeLaEmpresa" type="text" class="form-control" name="culturaDeLaEmpresa" required>{{$vacante->culturaDeLaEmpresa}}</textarea>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group mb-5">
                                <div class="col-md-6 float-left">
                                    <label>Logos y símbolos*</label>
                                    <textarea id="logosYsimbolos" type="text" class="form-control" name="logosYsimbolos" required>{{$vacante->logosYsimbolos}}</textarea>
                                </div>
                                <div class="col-md-6 float-left">
                                    <label>Normas y patrones de conducta*</label>
                                    <textarea id="normasYpatronesConducta" type="text" class="form-control" name="normasYpatronesConducta" required>{{$vacante->normasYpatronesConducta}}</textarea>
                                </div>
                                <div class="col-md-6 float-left">
                                    <label>Fundamentos y valores*</label>
                                    <textarea id="fundamentosYvalores" type="text" class="form-control" name="fundamentosYvalores" required>{{$vacante->fundamentosYvalores}}</textarea>
                                </div>
                                <div class="col-md-6 float-left">
                                    <label>Política interna*</label>
                                    <textarea id="politicaInterna" type="text" class="form-control" name="politicaInterna" required>{{$vacante->politicaInterna}}</textarea>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        
                        <div class="form-group mb-3">
                            <div class="col-md-12 text-right">
                                <a class="btn bg-black color-white" href="{{ url('/dashboard/vacantes') }}">
                                    Cancelar
                                </a>
                                <button type="submit" class="btn bg-levuAzul color-white">
                                    Actualizar vacante
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <input type="hidden" name="id" value="{{$vacante->id}}"> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
