@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
        <div class="col-md-12 bg-white">
        	<div class="col-md-12 mb-4">
                <h1>Vacantes</h1>
            </div>
            <div class="col-md-12 row pt-3 pb-3 border-bottom mobileHide">
            	<form action="" method="get" id="vacantesFilter" class="">
					<div class="col-md-3 float-left">
						<p><strong>Ordernar por:</strong></p>
                        <p>
                            <select id="order" name="order" class="form-control" onChange="$('#vacantesFilter').submit();">
                                <option value="folioAsc" @if($order == 'folioAsc') selected="selected" @endif>Folio ascendente</option>
                                <option value="folioDesc" @if($order == 'folioDesc') selected="selected" @endif>Folio descendente</option>
                                <option value="estatusAsc" @if($order == 'estatusAsc') selected="selected" @endif>Estatus ascendente</option>
                                <option value="estatusDesc" @if($order == 'estatusDesc') selected="selected" @endif>Estatus descendente</option>
								<option value="tituloAsc" @if($order == 'tituloAsc') selected="selected" @endif>Título ascendente</option>
                                <option value="tituloDesc" @if($order == 'tituloDesc') selected="selected" @endif>Título descendente</option>
                                <option value="fechaAsc" @if($order == 'fechaAsc') selected="selected" @endif>Fecha ascendente</option>
                                <option value="fechaDesc" @if($order == 'fechaDesc') selected="selected" @endif>Fecha descendente</option>
                                <option value="reclutadorAsc" @if($order == 'reclutadorAsc') selected="selected" @endif>HeadHunter ascendente</option>
                                <option value="reclutadorDesc" @if($order == 'reclutadorDesc') selected="selected" @endif>HeadHunter descendente</option>
                                <option value="empresaAsc" @if($order == 'empresaAsc') selected="selected" @endif>Empresa ascendente</option>
                                <option value="empresaDesc" @if($order == 'empresaDesc') selected="selected" @endif>Empresa descendente</option>
                            </select>
                        </p>
					</div>
					<div class="clearfix"></div>
                    <div class="col-md-1 float-left">
                    	<p><strong>Folio</strong></p>
                        {{--<p>
                            <select id="order" name="order" class="form-control" onChange="$('#vacantesFilter').submit();">
                                <option value="asc" @if($orderID == 'asc') selected="selected" @endif>Ascendente</option>
                                <option value="desc" @if($orderID == 'desc') selected="selected" @endif>Descendente</option>
                            </select>
                        </p>--}}
                    </div>
                    <div class="col-md-1 float-left">
                        <p>
                            <strong>Estatus</strong>
                        </p>
                        <p>
                            <select id="estatus" name="estatus" class="form-control" onChange="$('#vacantesFilter').submit();">
                                <option value="0">Todos</option>
                                <option value="Abierta" @if($estatus == 'Abierta') selected="selected" @endif>Abiertas</option>
                                <option value="Cerrada" @if($estatus == 'Cerrada') selected="selected" @endif>Cerradas</option>
                                <option value="Cerrada con contratación" @if($estatus == 'Cerrada con contratación') selected="selected" @endif>Cerradas con contratación</option>
                                <option value="Cerrada sin contratación" @if($estatus == 'Cerrada sin contratación') selected="selected" @endif>Cerradas sin contratación</option>
                            </select>
                        </p>
                    </div>
                    <div class="col-md-2 float-left">
                        <p><strong>Fecha</strong></p>
                        <div class="col-md-6 float-left p-0">
                            <input type="text" name="from" class="form-control bg-white datepicker" placeholder="Desde" onChange="$('#vacantesFilter').submit();" value="{{$from}}" readonly>
                        </div>
                        <div class="col-md-6 float-left p-0">
                            <input type="text" name="to" class="form-control bg-white datepicker" placeholder="Hasta" onChange="$('#vacantesFilter').submit();" value="{{$to}}" readonly>
                        </div>
                    </div>
                    <div class="col-md-2 float-left">
						<p><strong>Vacante</strong></p>
                        {{--<p>
                            <select id="titulo" name="titulo" class="form-control" onChange="$('#vacantesFilter').submit();">
                                <option value="asc" @if($orderTitle == 'asc') selected="selected" @endif>Ascendente</option>
                                <option value="desc" @if($orderTitle == 'desc') selected="selected" @endif>Descendente</option>
                            </select>
                        </p>--}}
					</div>
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                        <div class="col-md-2 float-left">
                            <p><strong>HeadHunter a cargo</strong></p>
                            <p>
                                <select id="reclutador" name="reclutador" class="form-control" onChange="$('#vacantesFilter').submit();">
                                    <option value="0">Todos</option>
                                    @foreach($reclutadores as $reclu)
                                        <option value="{{$reclu->id}}" @if($reclu->id == $reclutador) selected="selected" @endif>{{$reclu->name}} {{$reclu->lastName}}</option>
                                    @endforeach
                                </select>
                            </p>
                        </div>
                    @endif
                    <div class="col-md-1 float-left">
                        <p>
                            <strong>Empresa</strong>
                        </p>
                        <p>
                            <select id="empresa" name="empresa" class="form-control" onChange="$('#vacantesFilter').submit();">
                                <option value="0">Todas</option>
                                @foreach($empresas as $emp)
                                	<option value="{{$emp->id}}" @if($emp->id == $empresa) selected="selected" @endif>{{$emp->name}}</option>
                                @endforeach
                            </select>
                        </p>
                    </div>
                    <div class="col-md-3 float-left text-right">
                    	<p>&nbsp;</p>
                    	<p>
                        	<a href="{{ url('/dashboard/vacantes?estatus=0&from=&to=&reclutador=0&empresa=0') }}" class="btn bg-black color-white">Resetear filtros</a>
                            <a href="{{ url('/dashboard/vacantes/export?estatus='.$estatus.'&from='.$from.'&to='.$to.'&reclutador='.$reclutador.'&empresa='.$empresa.'') }}" target="_blank" class="btn bg-levuAzul color-white">Exportar</a>
                        </p>
                    </div>
                </form>
            </div>
            @foreach($vacantes as $key=>$vacante)
            	<div class="vacanteList col-md-12 row pt-3 pb-3 border-bottom @if($key%2 == 0) bg-whiteGrey @endif">
                    <div class="col-md-1 float-left">{{$vacante->id}} @if(count($vacante->candidatos) > 0) <button class="mobileHide btn btn-xs font-12 btn-default" onclick="$('.tr-user-{{$vacante->id}}').slideToggle();" data-toggle="tooltip" data-placement="bottom" title="Candidatos"><i class="fas fa-users"></i> <i class="fas fa-caret-down"></i></button> @endif</div>
                    <div class="col-md-1 float-left @if($vacante->estatus == 'Abierta') text-success @else text-danger @endif">{{$vacante->estatus}}</div>
                    <div class="col-md-2 float-left">{{$vacante->created_at}}</div>
                    <div class="col-md-2 float-left">{{$vacante->titulo}}</div>
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                    	<div class="col-md-2 float-left">{{$vacante->recluName}} {{$vacante->recluLastName}}</div>
                    @endif
                    <div class="col-md-1 float-left">{{$vacante->empresa}}</div>
                    <div class="col-md-3 float-left text-right">
                    	<a href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle de vacante">
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar vacante">
                            <i class="far fa-edit"></i>
                        </a>
                        <button class="btn bg-black btn-sm color-white" data-title="Eliminar vacante" data-instructions="¿Estás seguro/a de querer eliminar esta vacante?" data-submit-btn="Eliminar vacante" onclick="deleteRecord('{{$vacante->id}}', 'Vacante', '','form-deleteVacante','{{ csrf_token() }}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar vacante">
                            <i class="far fa-trash-alt"></i>
                        </button>
                    </div>
                    @if(count($vacante->candidatos) > 0)
                    	@foreach($vacante->candidatos as $candidato)
                        	<div class="col-md-12 pt-3 pb-3 tr-user-{{$vacante->id}}" style="display:none;">
                            	<div class="col-md-2"></div>
                                <div class="col-md-3">{{$candidato->name}} {{$candidato->lastName}}</div>
                                <div class="col-md-3">{{$candidato->email}}</div>
                                <div class="col-md-2">
                                    <a target="_blank" href="{{ url('/dashboard/users/profile/') }}/{{$candidato->id}}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle de candidato">
                                        <i class="far fa-eye"></i>
                                    </a>
                                    <a target="_blank" href="{{ url('/dashboard/users/edit/') }}/{{$candidato->id}}" class="btn bg-transparent color-black btn-sm" data-toggle="tooltip" data-placement="bottom" title="Editar candidato">
                                        <i class="far fa-edit"></i>
                                    </a>
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        @endforeach
                    @endif
                </div>
           	@endforeach
            
            <div class="col-md-12 pt-3 pb-3 row">
                <p class="text-left">
                    <a href="{{ url('/dashboard/') }}" class="btn bg-levuAzul color-white btn-sm">
                        Volver al home
                    </a>
                    <a href="{{ url('/dashboard/vacantes/create') }}" class="btn bg-levuAzul color-white btn-sm">
                        Nueva vacante
                    </a>
                </p>
            </div>
        </div>
    </div>

    {{ $vacantes->appends($_GET)->links() }}

</div>
@endsection
