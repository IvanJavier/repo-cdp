@extends('layouts.print')

@section('title', "LEVU Talent - Vacante")

@section('content')
<h1>Vacante / primer escaneo: {{$vacante->titulo}}</h1>


<table width="100%" style="magin-bottom:25px;">
	<tr>
    	<td colspan="6" valign="top">
        	<p><strong>STATUS: </strong><span @if($vacante->estatus == 'Abierta') class="text-success" @else class="text-danger" @endif>{{$vacante->estatus}}</span></p>
        </td>
    </tr>
    <tr>
    	<td colspan="6" valign="top">&nbsp;
        	
        </td>
    </tr>
	<tr>
    	<td width="16.6%" valign="top">
        	<p><strong>FECHA DE CREACIÓN</strong></p>
            <p>{{$vacante->created_at}}</p>
        </td>
        <td width="16.6%" valign="top">
        	<p><strong>EMPRESA</strong></p>
            <p>{{$vacante->empresa}}</p>
        </td>
        <td width="16.6%" valign="top">
        	<p><strong>SECTOR</strong></p>
            <p>{{$vacante->giroEmpresa}}</p>
        </td>
        <td width="16.6%" valign="top">
        	<p><strong>CONTACTO</strong></p>
            <p>{{$vacante->contactoName}}</p>
        </td>
        <td width="16.6%" valign="top">
        	<p><strong>EMAIL</strong></p>
            <p>{{$vacante->contactoEmail}}</p>
        </td>
        <td width="16.6%" valign="top">
        	<p><strong>TELÉFONO</strong></p>
            <p>{{$vacante->contactoPhone}}</p>
        </td>
    </tr>
    <tr>
    	<td colspan="6" valign="top">&nbsp;
        	
        </td>
    </tr>
    <tr>
    	<td valign="top">
        	<p><strong>NOMBRE DEL HEADHUNTER</strong></p>
            <p>{{$vacante->recluName}} {{$vacante->recluLastName}}</p>
        </td>
        <td valign="top">
        	<p><strong>JEFE DE LA VACANTE (NOMBRE)</strong></p>
            <p>{{$vacante->jefeVacantenombre}}</p>
        </td>
        <td valign="top">
        	<p><strong>JEFE DE LA VACANTE (PUESTO)</strong></p>
            <p>{{$vacante->jefeVacantePuesto}}</p>
        </td>
        <td valign="top">
        	<p><strong>ENTREVISTADOR 1</strong></p>
            <p>{{$vacante->entrevistadorUno}}</p>
        </td>
        <td valign="top">
        	<p><strong>ENTREVISTADOR 2</strong></p>
            <p>{{$vacante->entrevistadorDos}}</p>
        </td>
        <td valign="top"></td>
    </tr>
    <tr>
    	<td colspan="6" valign="top">&nbsp;
        	
        </td>
    </tr>
    <tr>
    	<td colspan="6" valign="top">
        	<p><strong style="border-bottom:5px solid var(--color-levuAzul);">DOMICILIO DE LA ENTREVISTA</strong></p>
        </td>
    </tr>
    <tr>
    	<td valign="top">
        	<p><strong>Calle y número</strong></p>
            <p>{{$vacante->calleNumero}}</p>
        </td>
        <td valign="top">
        	<p><strong>Colonia</strong></p>
            <p>{{$vacante->col}}</p>
        </td>
        <td valign="top">
        	<p><strong>CP</strong></p>
            <p>{{$vacante->cp}}</p>
        </td>
        <td valign="top">
        	<p><strong>Del. / Mpo.</strong></p>
            <p>{{$vacante->delmpo}}</p>
        </td>
        <td valign="top">
        	<p><strong>Estado</strong></p>
            <p>{{$vacante->edo}}</p>
        </td>
        <td valign="top"></td>
    </tr>
</table>
{{-- DATOS DE LA VACANTE --}}
<table width="100%" style="magin-bottom:25px; margin-top:25px;">
	<tr>
    	<td colspan="4" valign="top">
        	<p><strong style="border-bottom:5px solid var(--color-levuAzul);">DATOS DE LA VACANTE</strong></p>
        </td>
    </tr>
    <tr>
    	<td width="25%" valign="top">
        	<p><strong>¿ES REEMPLAZO?</strong></p>
            <p>@if($vacante->reemplazo == '1') Si @else No @endif</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>VACANTE CONFIDENCIAL</strong></p>
            <p>@if($vacante->confidencial == '1') Si @else No @endif</p>
        </td>
        <td width="25%" valign="top">
        	<P><strong>¿NUEVA CREACIÓN?</strong></P>
            <p>@if($vacante->nuevaCreacion == '1') Si @else No @endif</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>CANTIDAD DE VACANTES</strong></p>
            <p>{{$vacante->cantidadDeVacantes}}</p>
        </td>
    </tr>
    <tr>
    	<td width="25%" valign="top">
        	<p><strong>EXPERIENCIA</strong></p>
            <p>{{$vacante->experiencia}}</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>ESCOLARIDAD</strong></p>
            <p>{{$vacante->escolaridad}}</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>EDAD</strong></p>
            <p>Desde {{$vacante->edadDesde}} hasta {{$vacante->edadHasta}}</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>SEXO</strong></p>
            <p>@if($vacante->sexo == 'Hombre') Hombre @elseif($vacante->sexo == 'Mujer') Mujer @elseif($vacante->sexo == 'Indistinto') Indistinto @endif</p>
        </td>
    </tr>
    <tr>
    	<td width="25%" valign="top">
        	<p><strong>SALARIO MENSUAL</strong></p>
            <p>$ {{ number_format($vacante->sueldoMensual,2) }} MXN</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>PRESTACIONES</strong></p>
            <p>{{$vacante->prestaciones}}</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>COMPENSACIÓN VARIABLE</strong></p>
            <p>{{$vacante->compensacionVariable}}</p>
        </td>
        <td width="25%" valign="top">
        	<p><strong>CANTIDAD DE VIAJES AL AÑO</strong></p>
            <p>{{$vacante->viajesAlAnio}}</p>
        </td>
    </tr>
    <tr>
    	<td colspan="4" valign="top">
        	<p><strong>DÍAS Y HORARIOS DE TRABAJO</strong></p>
            <p>{{$vacante->horarioLaboral}}</p>
        </td>
    </tr>
</table>
{{-- ELEMENTOS INDISPENSABLES --}}
<table width="100%" style="magin-bottom:25px; margin-top:25px;">
	<tr>
    	<td colspan="4" valign="top">
        	<p><strong style="border-bottom:5px solid var(--color-levuAzul);">ELEMENTOS INDISPENSABLES</strong></p>
        </td>
    </tr>
    <tr>
    	<td valign="top">
        	<p><strong>HABILIDADES Y CONOCIMIENTOS TÉCNICOS NECESARIOS</strong></p>
            <p>{{$vacante->habilidadesNecesarias}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>PUESTOS QUE LA REPORTAN</strong></p>
            <p>{{$vacante->puestosQueLaReportan}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<P><strong>SISTEMAS (SOFTWARE)</strong></P>
            <p>{{$vacante->sistemas}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>MISIÓN DEL PUESTO</strong></p>
            <p>{{$vacante->mision}}</p>
        </td>
    </tr>
    
    <tr>
        <td valign="top">
        	<p><strong>FUNCIONES (PRINCIPALES RESPONSABILIDADES)</strong></p>
            <p>{{$vacante->funciones}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>INDICADIRES DE DESEMPEÑO DEL PUESTO</strong></p>
            <p>{{$vacante->indicadoresDesempenio}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>ENTREGABLES DEL PUESTO</strong></p>
            <p>{{$vacante->entregables}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>PROBLEMAS ACTUALES DEL PUESTO</strong></p>
            <p>{{$vacante->problemasActuales}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>RESULTADOS ESPRADOS DEL PUESTOS A CORTO PLAZO</strong></p>
            <p>{{$vacante->resultadosCorto}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>RESULTADOS ESPRADOS DEL PUESTOS A MEDIANO PLAZO</strong></p>
            <p>{{$vacante->resultadosMediano}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>MOTIVOS O CARACTERÍSTICAS DE RECHAZO INMEDIATO DE UNA CANDIDATO</strong></p>
            <p>{{$vacante->motivosRechazo}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>EMPRESAS O SECTORES SIMILARES</strong></p>
            <p>{{$vacante->empresasSimilares}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>PODEMOS HACER UN PLAN INTERNO DE REFERIDOS</strong></p>
            <p>{{$vacante->planInternoReferidos}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>DESCRIPCIÓN DE UN COLABORADOR IDEAL HOY EN ESTE DEPARTAMENTO</strong></p>
            <p>{{$vacante->descripcionColaboradorIdeal}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>VÍA PREFERIDA DE COMUNICACIÓN CON LEVU</strong></p>
            <p>{{$vacante->viaComunicacionLevu}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>PREGUNTAS CLAVE PARA PRIMER FILTRO TELEFÓNICO</strong></p>
            <p>{{$vacante->preguntasClave}}</p>
        </td>
    </tr>
    <tr>
        <td valign="top">
        	<p><strong>ELEMENTOS DE LA ESTRATEGIA A UTILIZAR EN EL RESÚMEN COMPARATIVO</strong></p>
            <p>{{$vacante->elementosEstrategia}}</p>
        </td>
    </tr>
</table>
{{-- CULTURA DE LA EMPRESA --}}
<table width="100%" style="magin-bottom:25px; margin-top:25px;">
	<tr>
    	<td colspan="4" valign="top">
        	<p><strong style="border-bottom:5px solid var(--color-levuAzul);">CULTURA DE LA EMPRESA</strong></p>
        </td>
    </tr>
    <tr>
    	<td valign="top">
            <p>{{$vacante->culturaDeLaEmpresa}}</p>
        </td>
    </tr>
</table>
@endsection
