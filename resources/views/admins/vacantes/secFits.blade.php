@extends('layouts.console')

@section('title', "LEVU Talent - Editar vacante")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
    
    	<div class="col-md-12 row bg-white pt-5">
            <div class="col-md-12 row bg-white">
                <div class="col-md-6 float-left">
                    <h1 class="m-0">SEC FITS</h1>
                </div>
                <div class="col-md-6 float-left text-right refsBtn">
                    <a class="btn bg-black color-white" href="{{ url('/dashboard/vacantes/view/'.$idVacante.'#tabCandidatos') }}">
                        Volver
                    </a>
                    @if(Auth::user()->hasRole('Empresa'))
                    	<a target="_blank" href="{{ url('/dashboard/vacantes/secFits/print/'.$idUserCandidato.'/'.$idVacante) }}" class="btn bg-levuAzul color-white">
                            Imprimir Sec Fits
                        </a>
                    @endif
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                        <a href="{{ url('/dashboard/vacantes/secFits/edit/'.$idUserCandidato.'/'.$idVacante) }}" class="btn bg-levuAzul color-white">
                            Editar Sec Fits
                        </a>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 row bg-white p-5">
            	<div class="col-md-2 float-left">
                	@if($secFits[0]->profilePic != NULL)
                    	<div class="enterprisePic" style="background:url({{ asset('storage').'/'.$secFits[0]->folder.'/'.$secFits[0]->profilePic }}) no-repeat center; background-size:cover;">
                        </div>
                    @else
                    	<div class="enterprisePic" style="background:url({{ asset('img/no-img.jpg') }}) no-repeat center; background-size:cover;">
                        </div>
                    @endif
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Vacante</strong><br>
                            {{$secFits[0]->vacante}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Cliente</strong><br>
                            {{$secFits[0]->cliente}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Candidato</strong><br>
                            {{$secFits[0]->candidatoName}} {{$secFits[0]->candidatoLastName}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Experiencia</strong><br>
                            {{$secFits[0]->experiencia}}</p>
                </div>
            </div>
        </div>
        
        <div class="col-md-12 pl-0 pr-0">
        	<div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
            	<span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>FILTRO</strong></span>
                <div class="clearfix"></div>
            </div>
        	<div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
            	<div class="col-md-12 bg-white p-4">
                    <p>{{$secFits[0]->filtro}}</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
            
        @if($secFits[0]->adaptacionPorHabilidades == NULL && $secFits[0]->adaptacionPorExperiencia == NULL && $secFits[0]->adaptacionPorCultura == NULL && $secFits[0]->aspectosPositivos == NULL && $secFits[0]->aspectosNegativos == NULL)
            <div class="col-md-12 pl-0 pr-0">
                <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title bg-white w-100 pb-2 pl-3 pr-3 float-left m-0"><strong>ADAPTACIÓN POR HABILIDADES TÉCNICAS / SKILLS FIT</strong></span>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Primer escaneo</strong></span>
                </div>
                <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Perfil candidato</strong></span>
                </div>
                
                <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">

                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Puestos que le reportan</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->puestosQueLaReportan}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $puestosQueLaReportan = json_decode($secFits[0]->puestosQueLaReportan,true) @endphp
                            <p>
                               {{$puestosQueLaReportan['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($puestosQueLaReportan['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Software e idiomas requeridos</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->sistemas}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $sistemas = json_decode($secFits[0]->sistemas,true) @endphp
                            <p>
                               {{$sistemas['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($sistemas['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Habilidades y conocimientos técnicos necesarios</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->habilidadesNecesarias}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $habilidadesNecesarias = json_decode($secFits[0]->habilidadesNecesarias,true) @endphp
                            <p>
                               {{$habilidadesNecesarias['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($habilidadesNecesarias['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Funciones (Principales responsabilidades)</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->funciones}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $funciones = json_decode($secFits[0]->funciones,true) @endphp
                            <p>
                               {{$funciones['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($funciones['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Entregables del puesto</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->entregables}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $entregables = json_decode($secFits[0]->entregables,true) @endphp
                            <p>
                               {{$entregables['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($entregables['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Pruebas o exámenes técnicos que aplicará el cliente</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->planInternoReferidos}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $pruebas = json_decode($secFits[0]->pruebas,true) @endphp
                            <p>
                               {{$pruebas['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($pruebas['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Descripción de un colaborador ideal hoy en ese departamento</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->descripcionColaboradorIdeal}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $descripcionColaboradorIdeal = json_decode($secFits[0]->descripcionColaboradorIdeal,true) @endphp
                            <p>
                               {{$descripcionColaboradorIdeal['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($descripcionColaboradorIdeal['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Indicadores de desempeño del puesto</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->indicadoresDesempenio}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $indicadoresDesempenio = json_decode($secFits[0]->indicadoresDesempenio,true) @endphp
                            <p>
                               {{$indicadoresDesempenio['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($indicadoresDesempenio['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
            {{-- --}}
            <div class="col-md-12 pl-0 pr-0">
                <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0  bg-white w-100"><strong>ADAPTACIÓN POR EXPERIENCIA</strong></span>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Primer escaneo</strong></span>
                </div>
                <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Perfil candidato</strong></span>
                </div>

                <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Misión del puesto</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->mision}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $mision = json_decode($secFits[0]->mision,true) @endphp
                            <p>
                               {{$mision['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($mision['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Resultados esperados en el corto plazo</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->resultadosCorto}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $resultadosCorto = json_decode($secFits[0]->resultadosCorto,true) @endphp
                            <p>
                               {{$resultadosCorto['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($resultadosCorto['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Resultados esperados en el mediano plazo</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->resultadosMediano}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $resultadosMediano = json_decode($secFits[0]->resultadosMediano,true) @endphp
                            <p>
                               {{$resultadosMediano['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($resultadosMediano['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Motivos o caracteristicas de rechazo inmediato</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->motivosRechazo}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $motivosRechazo = json_decode($secFits[0]->motivosRechazo,true) @endphp
                            <p>
                               {{$motivosRechazo['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($motivosRechazo['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Empresas o sectores similares</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->empresasSimilares}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $empresasSimilares = json_decode($secFits[0]->empresasSimilares,true) @endphp
                            <p>
                               {{$empresasSimilares['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($empresasSimilares['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            {{-- --}}
            <div class="col-md-12 pl-0 pr-0">
                <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0  bg-white w-100"><strong>ADAPTACIÓN POR CULTURA</strong></span>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Primer escaneo</strong></span>
                </div>
                <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Perfil candidato</strong></span>
                </div>

                <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Cultura general</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->culturaDeLaEmpresa}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $culturaDeLaEmpresa = json_decode($secFits[0]->culturaDeLaEmpresa,true) @endphp
                            <p>
                               {{$culturaDeLaEmpresa['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($culturaDeLaEmpresa['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Logos y simbolos</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->logosYsimbolos}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $logosYsimbolos = json_decode($secFits[0]->logosYsimbolos,true) @endphp
                            <p>
                               {{$logosYsimbolos['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($logosYsimbolos['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Normas y patrones</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->normasYpatronesConducta}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $normasYpatronesConducta = json_decode($secFits[0]->normasYpatronesConducta,true) @endphp
                            <p>
                               {{$normasYpatronesConducta['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($normasYpatronesConducta['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Fundamentos y valores</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->fundamentosYvalores}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $fundamentosYvalores = json_decode($secFits[0]->fundamentosYvalores,true) @endphp
                            <p>
                               {{$fundamentosYvalores['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($fundamentosYvalores['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <!-- ---- -->
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <p><strong>Política interna</strong></p>
                        </div>
                        <div class="col-md-6 float-left">
                            <p>{{$vacante->politicaInterna}}</p>
                        </div>
                        <div class="col-md-6 float-left">
                            @php $politicaInterna = json_decode($secFits[0]->politicaInterna,true) @endphp
                            <p>
                               {{$politicaInterna['text']}} 
                            </p>
                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                                <p>
                                    @switch($politicaInterna['calif'])
                                        @case(0)
                                            <span>N/A</span>
                                        @break
                                        @case(100)
                                            <span class="text-success">Total fit</span>
                                        @break
                                        @case(85)
                                            <span class="text-warning">Qualified</span>
                                        @break
                                        @default
                                            <span class="text-danger">Need training</span>
                                    @endswitch
                                </p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            {{-- --}}
            <div class="col-md-12 pl-0 pr-0">
                <div class="col-md-8 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 w-100 pl-3 pr-3 float-left m-0"><strong>Recomendaciones para posible contratación</strong></span>
                </div>
                <div class="col-md-4 pl-5 pt-5 pt-5 pb-0">
                    <span class="card-title pb-2 w-100 pl-3 pr-3 float-left m-0"><strong>Calificación</strong></span>
                </div>
                <div class="col-md-8 pt-0 pb-5 pl-5 pt-0">
                    <div class="col-md-12 bg-white p-4">
                        <p>{{$secFits[0]->recomendacion}}</p>
                    </div>
                </div>
                <div class="col-md-4 pt-0 pb-5 pl-5 pt-0">
                    <div class="col-md-12 bg-white p-4">
                        <p class="font-32">{{ number_format($secFits[0]->calificacion,1)}}</p>
                    </div>
                </div>
            </div>
        @else
            
            <h2>SEC FITS</h2>


            <div class="col-md-12 pl-0 pr-0">
            	<div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                	<span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>NIVEL DE ADAPTACIÓN POR HABILIDADES, EXPERIENCIA Y CULTURA</strong></span>
                    <div class="clearfix"></div>
                </div>
            	<div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                	<div class="col-md-12 bg-white p-4">
                        <div class="col-md-4 float-left">
                            <p><strong>Adaptación por habilidades</strong></p>
                                <p>{{$secFits[0]->adaptacionPorHabilidades}}</p>
                        </div>
                        <div class="col-md-4 float-left">
                            <p><strong>Adaptación por experiencias vividas</strong></p>
                                <p>{{$secFits[0]->adaptacionPorExperiencia}}</p>
                        </div>
                        <div class="col-md-4 float-left">
                            <p><strong>Adaptación por cultura</strong></p>
                                <p>{{$secFits[0]->adaptacionPorCultura}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            
            <div class="col-md-12 pl-0 pr-0">
            	<div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                	<span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>CALIFICACIÓN DE HEADHUNTER</strong></span>
                    <div class="clearfix"></div>
                </div>
            	<div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                	<div class="col-md-12 bg-white p-4">
                        <div class="col-md-4 float-left">
                            <p><strong>Aspectos Positivos</strong></p>
                                <p>{{$secFits[0]->aspectosPositivos}}</p>
                        </div>
                        <div class="col-md-4 float-left">
                            <p><strong>Aspectos Negativos</strong></p>
                                <p>{{$secFits[0]->aspectosNegativos}}</p>
                        </div>
                        <div class="col-md-4 float-left">
                            <p><strong>Calificación</strong></p>
                                <p class="font-48">{{ number_format($secFits[0]->calificacion,1)}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
