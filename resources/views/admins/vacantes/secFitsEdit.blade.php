@extends('layouts.console')

@section('title', "LEVU Talent - Editar vacante")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
     <form class="w-100" id="secFitsForm" method="POST" action="{{ url('/dashboard/vacantes/secFits/update') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="hidden" name="_returnPath" value="/dashboard/vacantes/view/{{$idVacante}}#tabCandidatos">
        <input type="hidden" name="id" value="{{$secFits[0]->id}}">
        <input type="hidden" name="idUserCandidato" value="{{$idUserCandidato}}">
        <input type="hidden" name="idVacante" value="{{$idVacante}}">

        <div class="col-md-12 row bg-white pt-5">
            <div class="col-md-12 row bg-white">
                <div class="col-md-6 float-left">
                    <h1 class="m-0">SEC FITS</h1>
                </div>
                <div class="col-md-6 float-left text-right refsBtn">
                    <a class="btn bg-black color-white" href="{{ URL::to('/dashboard/vacantes/view/'.$idVacante.'#tabCandidatos') }}">
                        Volver
                    </a>
                    @if(Auth::user()->hasRole('Empresa'))
                        <a target="_blank" href="{{ url('/dashboard/vacantes/secFits/print/'.$idUserCandidato.'/'.$idVacante) }}" class="btn bg-levuAzul color-white">
                            Imprimir Sec Fits
                        </a>
                    @endif
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                        <a href="{{ url('/dashboard/vacantes/secFits/edit/'.$idUserCandidato.'/'.$idVacante) }}" class="btn bg-levuAzul color-white">
                            Editar Sec Fits
                        </a>
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-2 float-left">
                    @if(isset($secFits[0]->profilePic))
                        <div class="enterprisePic" style="background:url({{ asset('storage').'/'.$secFits[0]->folder.'/'.$secFits[0]->profilePic }}) no-repeat center; background-size:cover;">
                        </div>
                    @else
                        <div class="enterprisePic" style="background:url({{ asset('img/no-img.jpg') }}) no-repeat center; background-size:cover;">
                        </div>
                    @endif
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Vacante</strong><br>
                            {{$secFits[0]->vacante}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Cliente</strong><br>
                            {{$secFits[0]->cliente}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Candidato</strong><br>
                            {{$secFits[0]->candidatoName}} {{$secFits[0]->candidatoLastName}}</p>
                </div>
                <div class="col-md-2 float-left">
                    <p><strong>Experiencia</strong><br>
                    <input type="" name="experiencia" class="form-control" value="{{$secFits[0]->experiencia}}"></p>
                </div>
            </div>
        </div>
   
        <div class="col-md-12 pl-0 pr-0">
            <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>FILTRO</strong></span>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                <div class="col-md-12 bg-white p-4">
                    <textarea class="form-control" placeholder="En esta sección debe haber datos personales del candidato que no estén en el CV. Agregar notas que son relevantes en la vacante. Describir sus motivadores, su estado de salud actual, si vive cerca del lugar de trabajo, sus pasatiempos, datos sobre su familia, datos que sabemos que al cliente le va a interesar conocer sobre la persona que le estamos presentando." name="filtro">{{$secFits[0]->filtro}}</textarea>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
        <div class="col-md-12 pl-0 pr-0">
            <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title bg-white w-100 pb-2 pl-3 pr-3 float-left m-0"><strong>ADAPTACIÓN POR HABILIDADES TÉCNICAS / SKILLS FIT</strong></span>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Primer escaneo</strong></span>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Perfil candidato</strong></span>
            </div>

            <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Puestos que le reportan</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->puestosQueLaReportan}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $puestosQueLaReportan = json_decode($secFits[0]->puestosQueLaReportan,true) @endphp
                        <p>
                            <textarea name="puestosQueLaReportan[text]" class="form-control" placeholder="¿Has tenido personas a tu cargo? ¿Qué puestos han sido? ¿Hace cuanto tiempo te asignaron el primer equipo? Si preguntara a tu equipo sobre tu liderazgo, ¿Qué crees que me responderían? ¿Cómo aseguras que cumplan sus metas?">{{$puestosQueLaReportan['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="puestosQueLaReportan[calif]">
                                <option value="0" @if($puestosQueLaReportan['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($puestosQueLaReportan['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($puestosQueLaReportan['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($puestosQueLaReportan['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Software e idiomas requeridos</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->sistemas}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $sistemas = json_decode($secFits[0]->sistemas,true) @endphp
                        <p>
                            <textarea name="sistemas[text]" class="form-control" placeholder="Preguntar al candidato(a) sobre su grado de dominio en los softwares que el cliente solicitó. Solicitar al candidato(a) ejemplos de cuando utilizó estos softwares de manera exitosa. Indicar su nivel en los idiomas solicitados, donde los estudio y por cuanto tiempo.">{{$sistemas['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="sistemas[calif]">
                                <option value="0" @if($sistemas['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($sistemas['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($sistemas['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($sistemas['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Habilidades y conocimientos técnicos necesarios</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->habilidadesNecesarias}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $habilidadesNecesarias = json_decode($secFits[0]->habilidadesNecesarias,true) @endphp
                        <p>
                            <textarea name="habilidadesNecesarias[text]" class="form-control" placeholder="Preguntar al candidato(a) ejemplos de proyectos, trabajos, asignaciones, etc en donde haya adquirido el conocimiento técnico que el cliente solicitó en el Primer Escaneo, identificar si hoy los utiliza y desde cuando. Mencionarle cada habilidad técnica y que mencione eventos, fechas, nombres, etc; justificar con datos la respuesta.">{{$habilidadesNecesarias['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="habilidadesNecesarias[calif]">
                                <option value="0" @if($habilidadesNecesarias['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($habilidadesNecesarias['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($habilidadesNecesarias['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($habilidadesNecesarias['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Funciones (Principales responsabilidades)</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->funciones}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $funciones = json_decode($secFits[0]->funciones,true) @endphp
                        <p>
                            <textarea name="funciones[text]" class="form-control" placeholder="El cliente desea saber que puede o ya ha realizado las mismas funciones, seleccionar 2 funciones claves en la vacante y pedir al candidato(a) que explique un caso de éxito y un fracaso realizando estas funciones, así como el impacto en la empresa y sus propias lecciones aprendidas.">{{$funciones['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="funciones[calif]">
                                <option value="0" @if($funciones['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($funciones['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($funciones['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($funciones['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Entregables del puesto</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->entregables}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $entregables = json_decode($secFits[0]->entregables,true) @endphp
                        <p>
                            <textarea name="entregables[text]" class="form-control" placeholder="Preguntar al candidato(a) cuales son los entregables que debe cumplir, que periodicidad tienen, a quien se los debe entregar y lo más importante, como obtiene la información y datos de sus entregables (¿de un sistema? ¿Él solo o se apoya en alguien?">{{$entregables['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name=" entregables[calif]">
                                <option value="0" @if($entregables['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($entregables['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($entregables['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($entregables['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Pruebas o exámenes técnicos que aplicará el cliente</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->planInternoReferidos}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $pruebas = json_decode($secFits[0]->pruebas,true) @endphp
                        <p>
                            <textarea name="pruebas[text]" class="form-control" placeholder="Preguntar al candidato(a) si ha pasado por una prueba similar en el pasado. En función de a prueba; solicitar al candidato(a) un ejemplo con datos y números de una experiencia previa haciendo lo que le pediran que demuestre con la prueba.">{{$pruebas['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="pruebas[calif]">
                                <option value="0" @if($pruebas['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($pruebas['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($pruebas['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($pruebas['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Descripción de un colaborador ideal hoy en ese departamento</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->descripcionColaboradorIdeal}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $descripcionColaboradorIdeal = json_decode($secFits[0]->descripcionColaboradorIdeal,true) @endphp
                        <p>
                            <textarea name="descripcionColaboradorIdeal[text]" class="form-control" placeholder="Esta sección es para indagar si la personalidad del candidato(a) empata con la de un buen colaborador actual del cliente. Preguntar al candidato(a) cuál es el rasgo de su personalidad que considera que menos agrada a los demás. Preguntar al candidato(a) lo que sus compañeros opinan sobre él / ella.">{{$descripcionColaboradorIdeal['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name=" descripcionColaboradorIdeal[calif]">
                                <option value="0" @if($descripcionColaboradorIdeal['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($descripcionColaboradorIdeal['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($descripcionColaboradorIdeal['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($descripcionColaboradorIdeal['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Indicadores de desempeño del puesto</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->indicadoresDesempenio}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $indicadoresDesempenio = json_decode($secFits[0]->indicadoresDesempenio,true) @endphp
                        <p>
                            <textarea name="indicadoresDesempenio[text]" class="form-control" placeholder="¿Cómo te miden hoy? ¿Cuáles metas tienes hoy ya cumplidas y en cuáles consideras que existe riesgo de no cumplirlas? ¿Y porqué?">{{$indicadoresDesempenio['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="indicadoresDesempenio[calif]">
                                <option value="0" @if($indicadoresDesempenio['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($indicadoresDesempenio['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($indicadoresDesempenio['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($indicadoresDesempenio['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>
        {{-- --}}
        <div class="col-md-12 pl-0 pr-0">
            <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0  bg-white w-100"><strong>ADAPTACIÓN POR EXPERIENCIA</strong></span>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Primer escaneo</strong></span>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Perfil candidato</strong></span>
            </div>

            <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Misión del puesto</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->mision}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $mision = json_decode($secFits[0]->mision,true) @endphp
                        <p>
                            <textarea name="mision[text]" class="form-control" placeholder="Preguntar y justificar con datos (fechas, números, pesos, decimales, porcentajes, etc) un evento o experiencia previa del candidato en donde ya hizo lo mismo que implica la misión con el cliente.">{{$mision['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="mision[calif]">
                                <option value="0" @if($mision['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($mision['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($mision['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($mision['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Resultados esperados en el corto plazo</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->resultadosCorto}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $resultadosCorto = json_decode($secFits[0]->resultadosCorto,true) @endphp
                        <p>
                            <textarea name="resultadosCorto[text]" class="form-control" placeholder="Preguntar al candidato(a) qué haría los primeros 30 días de su ingreso para cumplir lo que el cliente espera en ese periodo. Preguntar lo que NO haría. Esta sección evalúa su habilidad para administrar transiciones.">{{$resultadosCorto['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="resultadosCorto[calif]">
                                <option value="0" @if($resultadosCorto['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($resultadosCorto['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($resultadosCorto['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($resultadosCorto['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Resultados esperados en el mediano plazo</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->resultadosMediano}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $resultadosMediano = json_decode($secFits[0]->resultadosMediano,true) @endphp
                        <p>
                            <textarea name="resultadosMediano[text]" class="form-control" placeholder="Preguntar al candidato(a) qué haría los primeros 6 – 9 meses de su ingreso para cumplir lo que el cliente espera en ese periodo. Preguntar lo que NO haría. Esta sección evalúa su habilidad para administrar transiciones.">{{$resultadosMediano['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="resultadosMediano[calif]">
                                <option value="0" @if($resultadosMediano['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($resultadosMediano['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($resultadosMediano['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($resultadosMediano['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Motivos o caracteristicas de rechazo inmediato</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->motivosRechazo}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $motivosRechazo = json_decode($secFits[0]->motivosRechazo,true) @endphp
                        <p>
                            <textarea name="motivosRechazo[text]" class="form-control" placeholder="Confirmar con ejemplos y datos que el candidato(a) no incurre en ninguno de los motivos mencionados por el cliente.">{{$motivosRechazo['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="motivosRechazo[calif]">
                                <option value="0" @if($motivosRechazo['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($motivosRechazo['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($motivosRechazo['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($motivosRechazo['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Empresas o sectores similares</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->empresasSimilares}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $empresasSimilares = json_decode($secFits[0]->empresasSimilares,true) @endphp
                        <p>
                            <textarea name="empresasSimilares[text]" class="form-control" placeholder="Preguntar y describir los sectores industriales y otras empresas en las que ha trabajado el candidato.">{{$empresasSimilares['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="empresasSimilares[calif]">
                                <option value="0" @if($empresasSimilares['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($empresasSimilares['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($empresasSimilares['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($empresasSimilares['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        {{-- --}}
        <div class="col-md-12 pl-0 pr-0">
            <div class="col-md-12 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0  bg-white w-100"><strong>ADAPTACIÓN POR CULTURA</strong></span>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Primer escaneo</strong></span>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>Perfil candidato</strong></span>
            </div>

            <div class="col-md-12 pt-0 pb-5 pl-5 pt-0">
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Cultura general</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->culturaDeLaEmpresa}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $culturaDeLaEmpresa = json_decode($secFits[0]->culturaDeLaEmpresa,true) @endphp
                        <p>
                            <textarea name="culturaDeLaEmpresa[text]" class="form-control" placeholder="Esta sección es crítica, se debe justificar que el candidato(a) puede empatar sus expectativas de carrera profesional con la cultura del cliente. Describir de qué tamaño, alcance y estilo o cultura general de la empresa y las personas dentro de la misma.">{{$culturaDeLaEmpresa['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="culturaDeLaEmpresa[calif]">
                                <option value="0" @if($culturaDeLaEmpresa['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($culturaDeLaEmpresa['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($culturaDeLaEmpresa['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($culturaDeLaEmpresa['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Logos y simbolos</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->logosYsimbolos}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $logosYsimbolos = json_decode($secFits[0]->logosYsimbolos,true) @endphp
                        <p>
                            <textarea name="logosYsimbolos[text]" class="form-control" placeholder="¿Cómo debe ser el ambiente de trabajo para que te sientas feliz trabajando allí? ¿Cuáles son tus preferencias o necesidades en temas de horarios, flexibilidad de permisos, código de vestimenta?">{{$logosYsimbolos['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="logosYsimbolos[calif]">
                                <option value="0" @if($logosYsimbolos['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($logosYsimbolos['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($logosYsimbolos['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($logosYsimbolos['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Normas y patrones</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->normasYpatronesConducta}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $normasYpatronesConducta = json_decode($secFits[0]->normasYpatronesConducta,true) @endphp
                        <p>
                            <textarea name="normasYpatronesConducta[text]" class="form-control" placeholder="¿Cuándo fue la última vez que te reconocieron? ¿Cómo consideras que se debe reconocer a un colaborador de alto desempeño? ¿Cuál es tu estilo de participación en una reunión de trabajo? ¿Qué regla de tu actual trabajo no te parece correcta?">{{$normasYpatronesConducta['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="normasYpatronesConducta[calif]">
                                <option value="0" @if($normasYpatronesConducta['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($normasYpatronesConducta['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($normasYpatronesConducta['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($normasYpatronesConducta['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 bg-whiteGrey p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Fundamentos y valores</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->fundamentosYvalores}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $fundamentosYvalores = json_decode($secFits[0]->fundamentosYvalores,true) @endphp
                        <p>
                            <textarea name="fundamentosYvalores[text]" class="form-control" placeholder="Pedir al candidato(a) detalles de la última meta que consiguió en conjunto con un equipo de trabajo y cual fue su aportación en ese logro. Preguntar al candidato(a) cual es su reacción cuando es testigo de algún tipo de injusticia, en cualquier lugar.">{{$fundamentosYvalores['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="fundamentosYvalores[calif]">
                                <option value="0" @if($fundamentosYvalores['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($fundamentosYvalores['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($fundamentosYvalores['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($fundamentosYvalores['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <!-- ---- -->
                <div class="col-md-12 bg-white p-4">
                    <div class="col-md-12 float-left">
                        <p><strong>Política interna</strong></p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p>{{$vacante->politicaInterna}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        @php $politicaInterna = json_decode($secFits[0]->politicaInterna,true) @endphp
                        <p>
                            <textarea name="politicaInterna[text]" class="form-control" placeholder="Preguntar al candidato(a) la última vez que participo en una decisión importante, cual fue su involucramiento y describir ese caso. Preguntar al candidato(a) que le admira al mejor de los jefes directos que ha tenido.">{{$politicaInterna['text']}}</textarea>
                        </p>
                        <p>
                            <select class="form-control" name="politicaInterna[calif]">
                                <option value="0" @if($politicaInterna['calif'] == 0)selected="selected"@endif>N/A</option>
                                <option value="100" @if($politicaInterna['calif'] == 100)selected="selected"@endif>Total fit</option>
                                <option value="85" @if($politicaInterna['calif'] == 85)selected="selected"@endif>Qualified</option>
                                <option value="70" @if($politicaInterna['calif'] == 70)selected="selected"@endif>Need training</option>
                            </select>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        {{-- --}}
        <div class="col-md-12 pl-0 pr-0">
            <div class="col-md-8 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 w-100 pl-3 pr-3 float-left m-0"><strong>Recomendaciones para posible contratación</strong></span>
            </div>
            <div class="col-md-4 pl-5 pt-5 pt-5 pb-0">
                <span class="card-title pb-2 w-100 pl-3 pr-3 float-left m-0"><strong>Calificación</strong></span>
            </div>
            <div class="col-md-8 pt-0 pb-5 pl-5 pt-0">
                <div class="col-md-12 bg-white p-4">
                    <textarea class="form-control" name="recomendacion" placeholder="En esta sección no es necesario escribir sobre temas de salario. Detallar situaciones a considerar en caso de seleccionar a este candidato(a), por ejemplo: tiempo de entrega en su actual trabajo, viajes ya planeado, temas relacionados a seguros médicos, si es extranjero su situación de permisos, horarios de entrevistas.">{{$secFits[0]->recomendacion}}</textarea>
                </div>
            </div>
            <div class="col-md-4 pt-0 pb-5 pl-5 pt-0">
                <div class="col-md-12 bg-white p-4">
                    <p>{{ number_format($secFits[0]->calificacion,1)}}</p>
                </div>
            </div>
        </div>

        <div class="col-md-12 pl-0 pr-0 mb-5">
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0 float-left">
                <p>¿SEC FITS COMPLETO?: </p>
                <label class="mr-5">Si <input type="radio" name="completed" value="1" @if($secFits[0]->completed == '1') checked="checked" @endif></label>
                <label>No <input type="radio" name="completed" value="0" @if($secFits[0]->completed == '0') checked="checked" @endif></label>
            </div>
            <div class="col-md-6 pl-5 pt-5 pt-5 pb-0 text-right float-left">
                <a class="btn bg-black color-white" href="{{ URL::previous() . '#tabCandidatos' }}">
                    Cancelar
                </a>
                <button type="submit" class="btn bg-levuAzul color-white">
                    Actualizar Sec Fits
                </button>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
        
    </div>
</div>
@endsection
