@extends('layouts.print')

@section('title', "LEVU Talent - SEC FITS")

@section('content')
<h1>SEC FITS</h1>
<table width="100%" style="margin-bottom:25px;">
    <tr>
        <td>
            <p><strong>Vacante</strong></p>
            <p>{{$secFits[0]->vacante}}</p>
        </td>
        <td>
            <p><strong>Candidato</strong></p>
            <p>{{$secFits[0]->candidatoName}} {{$secFits[0]->candidatoLastName}}</p>
        </td>
        <td>
            <p><strong>Cliente</strong></p>
            <p>{{$secFits[0]->cliente}}</p>
        </td>
        <td>
            <p><strong>Emperiencia</strong></p>
            <p>{{$secFits[0]->experiencia}}</p>
        </td>
    </tr>
</table>
<table width="100%" style="margin-bottom:25px;">
    <tr>
        <td colspan="4">
            <p><strong style="border-bottom:5px solid var(--color-levuAzul);">FILTRO</strong></p>
            <p>{{$secFits[0]->filtro}}</p>
        </td>
    </tr>
</table>
<table width="100%" style="margin-bottom:25px;">
    <tr>
        <td colspan="3">
            <p><strong style="border-bottom:5px solid var(--color-levuAzul);">NIVEL DE ADAPTACIÓN POR HABILIDADES, EXPERIENCIA Y CULTURA</strong></p>
        </td>
    </tr>
    <tr>
        <td width="33%" style="padding:0px 10px 0px 0px;" valign="top">
            <p><strong>Adaptacion por habilidades</strong></p>
            <p>{{$secFits[0]->adaptacionPorHabilidades}}</p>
        </td>
        <td width="34%" style="padding:0px 10px;" valign="top">
            <p><strong>Adaptación por experiencias vividas</strong></p>
            <p>{{$secFits[0]->adaptacionPorExperiencia}}</p>
        </td>
        <td width="33%" style="padding:0px 0px 0px 10px;" valign="top">
            <p><strong>Adaptación por cultura</strong></p>
             <p>{{$secFits[0]->adaptacionPorCultura}}</p>
        </td>
    </tr>
</table>
<table width="100%" style="margin-bottom:25px;">
    <tr>
        <td colspan="3">
            <p><strong style="border-bottom:5px solid var(--color-levuAzul);">CALIFICACIÓN DE HEADHUNTER</strong></p>
        </td>
    </tr>
    <tr>
        <td width="33%" style="padding:0px 10px 0px 0px;" valign="top">
            <p><strong>Aspectos Positivos</strong></p>
            <p>{{$secFits[0]->aspectosPositivos}}</p>
        </td>
        <td width="34%" style="padding:0px 10px;" valign="top">
            <p><strong>Aspectos Negativos</strong></p>
            <p>{{$secFits[0]->aspectosNegativos}}</p>
        </td>
        <td width="33%" style="padding:0px 0px 0px 10px;" valign="top">
            <p><strong>Calificación</strong></p>
            <p class="font-48">{{$secFits[0]->calificacion}}</p>
        </td>
    </tr>
</table>
<table width="100%" style="margin-bottom:25px;">
    <tr>
        <td colspan="4">
            <p><strong style="border-bottom:5px solid var(--color-levuAzul);">RECOMENDACIONES PARA POSIBLE SELECCIÓN Y SALARIO ACTUAL</strong></p>
            <p>{{$secFits[0]->recomendacion}}</p>
        </td>
    </tr>
</table>
@endsection
