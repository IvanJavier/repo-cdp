@extends('layouts.console')

@section('title', "LEVU Talent - Detalle de vacante")

@section('content')
<div class="main-content bg-whiteGrey">
    <div class="row justify-content-center">
    	<div class="col-md-12 row bg-white pt-5">
            <div class="col-md-12 bg-white">
                <div class="col-md-6 float-left">
                    <h1 class="m-0">Vacante / primer escaneo: {{$vacante->titulo}}</h1>
                    <p><strong>STATUS: </strong><span @if($vacante->estatus == 'Abierta') class="text-success" @else class="text-danger" @endif>{{$vacante->estatus}}</span> @if($vacante->fecha_cierre != "")<strong>FECHA DE CIERRE: </strong>{{ $vacante->fecha_cierre}}@endif </p>
                </div>
                @if(Auth::user()->hasRole('Candidato'))
                	<div class="col-md-6 float-left text-right refsBtn">
                        <a class="btn bg-black color-white" href="{{ url('/dashboard') }}">
                            Volver / Back
                        </a>
                    </div>
                @endif
                @if(  Auth::user()->hasRole('Empresa'))
                	<div class="col-md-6 float-left text-right refsBtn">
                    	<a class="btn bg-black color-white" href="{{ URL::to('/dashboard/vacantes') }}">
                            Volver / Back
                        </a>
                        <a class="btn bg-levuAzul color-white" href="{{ url('/dashboard/vacantes/print/'.$vacante->id) }}" target="_blank">Imprimir vacante / Print vacancy</a>
                    </div>
                @endif
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                    <div class="col-md-6 float-left text-right refsBtn">
                        <a class="btn bg-black color-white" href="{{ URL::to('/dashboard/vacantes') }}">
                            Volver / Back
                        </a>
                        <a href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-levuAzul color-white">
                            Editar vacante / Edit job
                        </a>
                    </div>
                @endif
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                    <div class="col-md-12 float-left mt-3">
                        <div class="col-md-3">
                            <p><strong>SUCURSAL</strong><br>
                                {{$vacante->sucursalName}}</p>
                        </div>
                        <div class="col-md-3">
                            <p><strong>STATUS VACANTE</strong><br>
                                {{$vacante->statusVacante}}</p>
                        </div>
                    </div>
                @endif
                <div class="clearfix"></div>
            </div>
            @if(Auth::user()->hasRole('superAdmin') || Auth::user()->hasRole('Admin'))
                <div class="col-md-12 row bg-white p-5">
					<div class="col-md-3 float-left">
                        <p><strong>COSTO INTERNO</strong><br>
                        $ {{number_format($vacante->costoInterno,2)}} MXN</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>TARIFA</strong><br>
                        $ {{ number_format($vacante->tarifa,2) }} MXN</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>VALOR DEL ANTICIPO</strong><br>
                        $ {{number_format($vacante->valorAnticipo,2)}} MXN</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>VALOR DEL CIERRE</strong><br>
                        $ {{number_format($vacante->valorCierre,2)}} MXN</p>
                    </div>
                </div>
			
				<div class="col-md-12 row bg-white p-5">
					<div class="col-md-4 float-left">
                        <p><strong>BONO PARA EL HEADHUNTER</strong><br>
                        $ {{number_format($vacante->bonoReclutador,2)}} MXN</p>
						<p><strong>Pagado: </strong>@if($vacante->bonoReclutadorPayed == 1) Si @else No @endif</p>
                    </div>
                    <div class="col-md-4 float-left">
                        <p><strong>BONO AL VENDEDOR</strong><br>
                        $ {{number_format($vacante->bonoVendedor,2)}} MXN</p>
						<p><strong>Pagado: </strong>@if($vacante->bonoVendedorPayed == 1) Si @else No @endif</p>
                    </div>
                    <div class="col-md-4 float-left">
                        <p><strong>BONO AL ADMINISTRADOR</strong><br>
                        $ {{number_format($vacante->bonoAdministrador,2)}} MXN</p>
						<p><strong>Pagado: </strong>@if($vacante->bonoAdministradorPayed == 1) Si @else No @endif</p>
                    </div>
                </div>

                <div class="col-md-12 row bg-white p-5">
                    <div class="col-md-6 float-left">
                        <p><strong>CONVENIO TARIFA</strong><br>
                        {{$vacante->convenioTarifa}}</p>
                    </div>
                    <div class="col-md-6 float-left">
                        <p><strong>CONVENIO ANTICIPO</strong><br>
                        {{$vacante->convenioAnticipo}}</p>
                    </div>
                </div>
            @endif
            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Empresa') || Auth::user()->hasRole('Vendedor'))
                <div class="col-md-12 row bg-white  p-5">
                    <div class="col-md-3 float-left">
                        <p><strong>FECHA DE CREACIÓN / CREATION DATE</strong><br>
                        {{$vacante->created_at}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>NOMBRE DEL HEADHUNTER / HEADHUNTER NAME</strong><br>
                        {{$vacante->recluName}} {{$vacante->recluLastName}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>EMAIL DEL HEADHUNTER / HEADHUNTER EMAIL</strong><br>
                        {{$vacante->recluEmail}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>TELEFONO DEL HEADHUNTER / HEADHUNTER PHONE</strong><br>
                        {{$vacante->recluPhone}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>JEFE DE LA VACANTE (NOMBRE) / JOB SUPERVISOR (NAME)</strong><br>
                        {{$vacante->jefeVacantenombre}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>JEFE DE LA VACANTE (PUESTO) / JOB SUPERVISOR (POSITION)</strong><br>
                        {{$vacante->jefeVacantePuesto}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>ENTREVISTADOR 1 / INTERVIEWER 1</strong><br>
                        {{$vacante->entrevistadorUno}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>ENTREVISTADOR 2 / INTERVIEWER 2</strong><br>
                        {{$vacante->entrevistadorDos}}</p>
                    </div>
                    @if(Auth::user()->hasRole('SuperAdmin'))
                        <div class="col-md-3 float-left">
                            <p><strong>VENDEDOR</strong><br>
                            {{$vacante->vendedorName}} {{$vacante->vendedorLastName}}</p>
                        </div>
                    @endif
                </div>
            @endif
            @if(Auth::user()->hasRole('Candidato'))
                <div class="col-md-12 row bg-white  p-5">
                    <div class="col-md-3 float-left">
                        <p><strong>GIRO / COMPANY SECTOR</strong><br>
                        {{$vacante->giroEmpresa}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>CANT. DE VIAJES DE TRABAJO AL AÑO / TRIPS PER YEAR</strong><br>
                        {{$vacante->viajesAlAnio}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>DÍAS Y HORARIO DE TRABAJO / WORK SCHEDULE</strong><br>
                        {{$vacante->horarioLaboral}}</p>
                    </div>
                    <div class="col-md-3 float-left">
                        <p><strong>EXPERIENCIA REQUERIDA / EXPERIENCE REQUIRED</strong><br>
                        {{ $vacante->experiencia}}
                        </p>
                    </div>
                </div>
            @endif
        </div>
        {{-- DOMICILIO DE LA ENTREVISTA --}}
        <div class="col-md-12 pl-0 pr-0 pt-5">
        	<div class="col-md-12 pl-5 pb-0">
            	<span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>DOMICILIO DE LA ENTREVISTA / INTERVIEW ADDRESS</strong></span>
            </div>
        	<div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0">
            	@if ($vacante->entrevista === 'video')
                <div class="col-md-12">
                <h5><strong>Entrevista por video</strong></h5>
                </div>
                @else
                <div class="col-md-12 bg-white">
                    <div class="col-md-2 float-left pt-3">
                        <p><strong>Calle y número / St. & number</strong><br>
                        {{$vacante->calleNumero}}</p>
                    </div>
                    <div class="col-md-2 float-left pt-3">
                        <p><strong>Colonia / Neighborhood</strong><br>
                        {{$vacante->col}}</p>
                    </div>
                    <div class="col-md-2 float-left pt-3">
                        <p><strong>CP / Zipcode</strong><br>
                        {{$vacante->cp}}</p>
                    </div>
                    <div class="col-md-2 float-left pt-3">
                        <p><strong>Del. / Mpo. / County</strong><br>
                        {{$vacante->delmpo}}</p>
                    </div>
                    <div class="col-md-2 float-left pt-3">
                        <p><strong>Estado / State</strong><br>
                        {{$vacante->edo}}</p>
                    </div>
                    <div class="col-md-2 float-left pt-3">
                        <p><strong>País / Country</strong><br>
                        {{$vacante->pais}}</p>
                    </div>
                </div>            
                @endif
            </div>
        </div>
        
        @if(Auth::user()->hasRole('Candidato'))
            
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-12 float-left">
                    <p><strong>PUESTOS QUE LA REPORTAN / DIRECT REPORTS</strong><br>
                    {{$vacante->puestosQueLaReportan}}</p>
                </div>
            </div>
            <div class="col-md-12 row bg-whiteGrey  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>HABILIDADES Y CONOCIMIENTOS TÉCNICOS NECESARIOS / TECHNICAL SKILLS</strong><br>
                    {{$vacante->habilidadesNecesarias}}</p>
                </div>
            </div>
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-12 float-left">
                    <p><strong>SOFTWARE E IDIOMAS REQUERIDOS / SOFTWARE AND LANGUAGES</strong><br>
                    {{$vacante->sistemas}}</p>
                </div>
            </div>
            <div class="col-md-12 row bg-white p-5">
                <div class="col-md-12 float-left">
                    <p><strong>MISIÓN DEL PUESTO / MISSION</strong><br>
                    {{$vacante->mision}}</p>
                </div>
            </div>
            
            <div class="col-md-12 row bg-white  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>FUNCIONES (PRINCIPALES RESPONSABILIDADES) / MAIN RESPONSABILITIES</strong><br>
                    {{$vacante->funciones}}</p>
                </div>
            </div>
            
            <div class="col-md-12 row bg-whiteGrey  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>INDICADORES DE DESEMPEÑO DEL PUESTO / KPIs</strong><br>
                    {{$vacante->indicadoresDesempenio}}</p>
                </div>
            </div>
            
            <div class="col-md-12 row bg-white  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>ENTREGABLES DEL PUESTO / DELIVERABLES</strong><br>
                    {{$vacante->entregables}}</p>
                </div>
            </div>
            
            <div class="col-md-12 row bg-whiteGrey  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>RESULTADOS ESPERADOS DEL PUESTO A CORTO PLAZO / SHORT TERM GOALS</strong><br>
                    {{$vacante->resultadosCorto}}</p>
                </div>
            </div>
            
            <div class="col-md-12 row bg-white  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>RESULTADOS ESPERADOS DEL PUESTO A MEDIANO PLAZO / MEDIUM TERM GOALS</strong><br>
                    {{$vacante->resultadosMediano}}</p>
                </div>
            </div>
            
            <div class="col-md-12 row bg-whiteGrey  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>CULTURA DE LA EMPRESA / COMPANY'S CULTURE</strong><br>
                    {{$vacante->culturaDeLaEmpresa}}</p>
                </div>
            </div>

            <div class="col-md-12 row bg-white  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>LOGOS Y SIMBOLOS / LOGOS AND SYMBOLS</strong><br>
                    {{$vacante->logosYsimbolos}}</p>
                </div>
            </div>

            <div class="col-md-12 row bg-whiteGrey  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>NORMAS Y PATRONES DE CONDUCTA / NORMS AND CODES OF CONDUCT</strong><br>
                    {{$vacante->normasYpatronesConducta}}</p>
                </div>
            </div>

            <div class="col-md-12 row bg-white  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>FUNDAMENTOS Y VALORES / FUNDAMENTAL VALUES</strong><br>
                    {{$vacante->fundamentosYvalores}}</p>
                </div>
            </div> 

            <div class="col-md-12 row bg-whiteGrey  p-5">
                <div class="col-md-12 float-left">
                    <p><strong>POLÍTICA INTERNA / INTERNAL POLICIES</strong><br>
                    {{$vacante->politicaInterna}}</p>
                </div>
            </div>           
        @endif
		
        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Empresa') || Auth::user()->hasRole('Vendedor'))
            {{-- TABS --}}
            <div class="col-md-12 pl-0 pr-0 pt-0 pb-5">
                <div class="col-md-12 pl-5 pr-5 pt-5 pb-0 mobileHide">
                    <div class="levuTabs active col-md-3 pointer pt-2 pb-2 text-center float-left" id="tabDatosVacanteBtn" title="tabDatosVacante" onClick="levuTabs(this);">DATOS DE LA VACANTE / VACANCY DATA</div>
                    <div class="levuTabs col-md-3 pointer pt-2 pb-2 text-center float-left" id="tabElmntIndispensablesBtn" title="tabElmntIndispensables" onClick="levuTabs(this);">ELEMENTOS INDISPENSABLES / REQUIRED ELEMENTS</div>
                    <div class="levuTabs col-md-3 pointer pt-2 pb-2 text-center float-left" id="tabCulturaEmpresaBtn" title="tabCulturaEmpresa" onClick="levuTabs(this);">CULTURA DE LA EMPRESA / COMPANY'S CULTURE</div>
                    <div class="levuTabs col-md-3 pointer pt-2 pb-2 text-center float-left" id="tabCandidatosBtn" title="tabCandidatos" onClick="levuTabs(this); viewSecFits();">CANDIDATOS / CANDIDATES</div>
                </div>
                {{-- DATOS DE LA VACANTE --}}
                <div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0 levuTabsContent" id="tabDatosVacante">
                	<div class="col-md-12 bg-whiteGrey p-4 mobileShow">
                    	<h2>Datos de la vacante / Job data</h2>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-3 float-left">
                            <label for="" class="col-md-12 col-form-label p0">¿Es reemplazo? / Replacement post?</label>
                            <p>@if($vacante->reemplazo == '1') Si / Yes @else No @endif</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="" class="col-md-12 col-form-label p0">Vacante confidencial / Classified</label>
                            <p>@if($vacante->confidencial == '1') Si / Yes @else No @endif
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="" class="col-md-12 col-form-label p0">¿Nueva creación? / New role?</label>
                            <p>@if($vacante->nuevaCreacion == '1') Si / Yes @else No @endif</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="" class="col-form-label">Cantidad de vacantes / Total vacancies</label>
                            <p>{{$vacante->cantidadDeVacantes}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-3 float-left">
                            <label for="experiencia" class="col-form-label">Experiencia / Experience</label>
                            <p>{{$vacante->experiencia}}</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="escolaridad" class="col-form-label">Escolaridad / Highest degree achieved</label>
                            <p>{{$vacante->escolaridad}}</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="" class="col-form-label col-md-12 pl-0">Edad / Age</label>
                            <p>Desde {{$vacante->edadDesde}} hasta {{$vacante->edadHasta}}</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="" class="col-md-12 col-form-label pl-0">Sexo / Gender</label>
                            <p>@if($vacante->sexo == 'Hombre') Hombre / Male @elseif($vacante->sexo == 'Mujer') Mujer / Female @elseif($vacante->sexo == 'Indistinto') Indistinto @endif</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-3 float-left">
                            <label for="sueldoMensual" class="col-form-label">Salario mensual / Base salary</label>
                            @if($vacante->sueldoMensualHasta)
                                <p>Desde $ {{ number_format($vacante->sueldoMensual,2) }} hasta {{ number_format($vacante->sueldoMensualHasta,2) }} @if($vacante->divisa == 'MXN' || $vacante->divisa == NULL) MXN @else {{$vacante->divisa}} @endif {{$vacante->sueldoTipo}}</p>
                            @else
                                <p>$ {{ number_format($vacante->sueldoMensual,2) }} @if($vacante->divisa == 'MXN' || $vacante->divisa == NULL) MXN @else {{$vacante->divisa}} @endif {{$vacante->sueldoTipo}}</p>
                            @endif
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="prestaciones" class="col-form-label">Prestaciones / Benefits</label>
                                <p>{{$vacante->prestaciones}}</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="compensacionVariable" class="col-form-label">Compensación variable / Variable compensations</label>
                                <p>{{$vacante->compensacionVariable}}</p>
                        </div>
                        <div class="col-md-3 float-left">
                            <label for="viajesAlAnio" class="col-form-label">Cantidad de viajes de trabajo al año / Work travel (% per year)</label>
                                <p>{{$vacante->viajesAlAnio}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-3 float-left">
                            <label for="horarioLaboral" class="col-form-label">Días y horario de trabajo / Work schedule</label>
                                <p>{{$vacante->horarioLaboral}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                {{-- ELEMENTOS INDISPENSABLES --}}
                <div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0 levuTabsContent" id="tabElmntIndispensables">
                	<div class="col-md-12 bg-whiteGrey p-4 mobileShow">
                    	<h2>Elementos indispensables / Required elements</h2>
                    </div>
					<div class="col-md-12 bg-white p-4">
						<div class="col-md-12 float-left">
                            <label for="habilidadesNecesarias" class="col-form-label">Habilidades y conocimientos técnicos necesarios / Technical skills</label>
                                <p>{{$vacante->habilidadesNecesarias}}</p>
                        </div>
                        <div class="clearfix"></div>
					</div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="puestosQueLaReportan" class="col-form-label">Puestos que la reportan / Direct reports</label>
                                <p>{{$vacante->puestosQueLaReportan}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="sistemas" class="col-form-label">Software e idiomas requeridos / Software and languages</label>
                                <p>{{$vacante->sistemas}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="mision" class="col-form-label">Misión del puesto / Mission</label>
                                <p>{{$vacante->mision}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
					<div class="col-md-12 bg-white p-4">
						<div class="col-md-12 float-left">
                            <label for="funciones" class="col-form-label">Funciones (principales responsabilidades) / Main responsibilities</label>
                                <p>{{$vacante->funciones}}</p>
                        </div>
                        <div class="clearfix"></div>
					</div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-4 float-left">
                            <label for="indicadoresDesempenio" class="col-form-label">Indicadores de desempeño del puesto / KPIs</label>
                                <p>{{$vacante->indicadoresDesempenio}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="entregables" class="col-form-label">Entregables del puesto / Deliverables</label>
                                <p>{{$vacante->entregables}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="problemasActuales" class="col-form-label">Problemas actuales del puesto / Challenges and problems to solve</label>
                                <p>{{$vacante->problemasActuales}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="resultadosCorto" class="col-form-label">Resultados esperados del puesto a corto plazo / Short term goals</label>
                            <p>{{$vacante->resultadosCorto}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="resultadosMediano" class="col-form-label">Resultados esperados del puesto a mediano plazo / Medium term goals</label>
                            <p>{{$vacante->resultadosMediano}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="motivosRechazo" class="col-form-label">Motivos o características de rechazo inmediato de un candidato / Motives for immediate rejection</label>
                                <p>{{$vacante->motivosRechazo}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="empresasSimilares" class="col-form-label">Empresas o sectores similares / Similar companies and industries</label>
                                <p>{{$vacante->empresasSimilares}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="planInternoReferidos" class="col-form-label">Pruebas o exámenes técnicos que aplicará el cliente / Technical tests the client will apply</label>
                                <p>{{$vacante->planInternoReferidos}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="descripcionColaboradorIdeal" class="col-form-label">Descripción de un colaborador ideal hoy en este departamento / Profile of a current employee with excellent results</label>
                                <p>{{$vacante->descripcionColaboradorIdeal}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="viaComunicacionLevu" class="col-form-label">Vía preferida de comunicación con Levu / Preferred communication channel with Levu</label>
                                <p>{{$vacante->viaComunicacionLevu}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @if (!Auth::user()->hasRole('Empresa'))
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="preguntasClave" class="col-form-label">Preguntas claves para primer filtro telefónico / Key questions for first filter via phone</label>
                                <p>{{$vacante->preguntasClave}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endif
                    <!--<div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="elementosEstrategia" class="col-form-label">Elementos de la estrategia a utilizar en el resumen comparativo </label>
                                <p>{{$vacante->elementosEstrategia}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>-->
                </div>
                {{-- CULTURA DE LA EMPRESA --}}
                <div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0 levuTabsContent" id="tabCulturaEmpresa">
                	<div class="col-md-12 bg-whiteGrey p-4 mobileShow">
                    	<h2>Cultura de la empresa / Company culture</h2>
                    </div>

                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="culturaDeLaEmpresa" class="col-form-label">Cultura general / Cultural overview</label>
                            <p>{{ $vacante->culturaDeLaEmpresa }}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @if($vacante->logosYsimbolos != NULL)
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="logosYsimbolos" class="col-form-label">Logos y simbolos / Logos and symbols</label>
                            <p>{{$vacante->logosYsimbolos}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endif
                     @if($vacante->normasYpatronesConducta != NULL)
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="normasYpatronesConducta" class="col-form-label">Normas y patrones de conducta / Norms and codes of conduct</label>
                            <p>{{$vacante->normasYpatronesConducta}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endif
                    @if($vacante->fundamentosYvalores != NULL)
                    <div class="col-md-12 bg-whiteGrey p-4">
                        <div class="col-md-12 float-left">
                            <label for="fundamentosYvalores" class="col-form-label">Fundamentos y valores / Fundamental values</label>
                            <p>{{$vacante->fundamentosYvalores}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endif
                    @if($vacante->politicaInterna != NULL)
                    <div class="col-md-12 bg-white p-4">
                        <div class="col-md-12 float-left">
                            <label for="politicaInterna" class="col-form-label">Política interna / Internal policies</label>
                            <p>{{$vacante->politicaInterna}}</p>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    @endif
                </div>
                {{-- CANDIDATOS --}}
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('Vendedor'))
                    <div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0 levuTabsContent" id="tabCandidatos">
                    	{{-- DESKTOP --}}
                        <div class="col-md-12 bg-white p-4 mobileHide">
                            <div class="col-md-12">
                                <p>Simbología:</p>

                                <p><span style="color:rgba(255,255,0,1);"><i class="fas fa-square"></i></span> 'No revisado' / 'To be reviewed'</p>
                               
                                <p><span style="color:rgba(0,0,255,.25);"><i class="fas fa-square"></i></span> 'Revisado, quiero entrevistarlo' o 'Entrevistado, continúa' / 'Schedule interview' or 'Interviewed and continues'</p>

                                <p><span style="color:rgba(255,0,0,.25);"><i class="fas fa-square"></i></span> 'NO quiero entrevistarlo' o 'Entrevistado, rechazado' / 'Do NOT schedule interview' or 'Rejected'</p>
                            </div>
                            <table class="table table-stripped font-12">
                                <tr>
                                    <th>Nombre / Name</th>
                                    <th>Fecha de asignación / Assigned at</th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Aquí puedes ver y/o descargar el CV">CV</span></th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Skills, experience y Cuture Fits">SEC&nbsp;FITS</span></th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Levu Talent Report (Psicometría)">LTR</span></th>
                                    <th class="text-center" width="130"><span data-toggle="tooltip" data-placement="bottom" title="Detalle de percepciones">Salario Actual / Current Salary</span></th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Referencias laborales">Refs.</span></th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Conoce en 2 min. al candidato">Video</span></th>
                                    <th class="text-center" width="100">Estatus / Status</th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Bloqueado">Bloq.</span></th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Colocado">Col.</span></th>
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Colocado">Ocultar</span></th>
                                    @if (Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Vendedor'))
                                    <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Colocado">Mostrar</span></th>
                                    @endif
                                </tr>
                                @foreach($candidatos as $candidato)
                                    @php $bgColor = 'rgba(255,255,255,1)'; @endphp
                                    @if($candidato->colocado == '1')
                                        @php $bgColor = 'rgba(0,255,0,.25)'; @endphp
                                    @elseif($candidato->estatus == 'No revisado')
                                        @php $bgColor = 'rgba(255,255,0,.25)'; @endphp
                                    @elseif($candidato->estatus == 'Revisado, quiero entrevistarlo' || $candidato->estatus == 'Entrevistado, continúa')
                                        @php $bgColor = 'rgba(0,0,255,.25)'; @endphp
                                    @elseif($candidato->estatus == 'Revisado, NO quiero entrevistarlo' || $candidato->estatus == 'Entrevistado, rechazado')
                                        @php $bgColor = 'rgba(255,0,0,.25)'; @endphp
                                    @endif
                                    <tr  style="background:{{$bgColor}}" >
                                        <td><a href="{{ url('/dashboard/users/profile/'.$candidato->id) }}">{{$candidato->name}} {{$candidato->lastName}}</a></td>
                                        <!--<td><span data-toggle="tooltip" data-placement="bottom" title="{{$candidato->email}}">{{ str_limit($candidato->email, $limit = 10, $end = '...') }}</span></td>-->
                                        @if($candidato->assigned_at)
                                            <td><span data-toggle="tooltip" data-placement="bottom" title="{{$candidato->assigned_at}}">{{date('d-m-Y', strtotime($candidato->assigned_at))}}</span></td>
                                        @else
                                            <td><span data-toggle="tooltip" data-placement="bottom" title=""></span></td>
                                        @endif
                                        {{-- CV --}}
                                        <td class="text-center">
                                            @if($candidato->cv != NULL)
                                                <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->cv }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                            @endif
                                            <button class="btn btn-xs bg-black color-white font-14" onClick="$('#cv_file{{$candidato->id}}').click()"><i class="fas fa-cloud-upload-alt"></i></button>
                                            <form id="cvUpload{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                {{ csrf_field() }}
                                                <input type="file" id="cv_file{{$candidato->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#cvUpload{{$candidato->id}}desktop').submit();">
                                                <input type="hidden" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" name="folder" value="{{$candidato->folder}}">
                                                <input type="hidden" name="fileType" value="cv">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                        {{-- SEC FITS --}}
                                        <td class="text-center">
                                            @if($candidato->secFits != 0)
                                                <a href="{{ url('/dashboard/vacantes/secFits/'.$candidato->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                            @endif
                                            <a href="{{ url('/dashboard/vacantes/secFits/edit/'.$candidato->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white font-14"><i class="fas fa-edit"></i></a>
                                        </td>
                                        {{-- LTR --}}
                                        <td class="text-center">
                                            @if($candidato->ltr != NULL)
                                                <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->ltr }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                            @endif
                                            <button class="btn btn-xs bg-black color-white font-14" onClick="$('#ltr_file{{$candidato->id}}').click()"><i class="fas fa-cloud-upload-alt"></i></button>
                                            <form id="ltrUpload{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                {{ csrf_field() }}
                                                <input type="file" id="ltr_file{{$candidato->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#ltrUpload{{$candidato->id}}desktop').submit();">
                                                <input type="hidden" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" name="folder" value="{{$candidato->folder}}">
                                                <input type="hidden" name="fileType" value="ltr">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                        {{-- COMPENSACIONES --}}
                                        <td class="text-center">
                                            @if($candidato->compensaciones != 0)
                                            <a href="{{ url('/dashboard/users/compensaciones/'.$candidato->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                            @endif
                                            <a href="{{ url('/dashboard/users/compensaciones/edit/'.$candidato->id) }}" class="btn btn-xs bg-black color-white font-14"><i class="fas fa-edit"></i></a>
                                        </td>
                                        {{-- REFERENCIAS --}}
                                        <td class="text-center">
                                            <a href="{{ url('/dashboard/users/referencias/'.$candidato->id ) }}" target="_blank" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                        </td>
                                        {{-- VIDEO --}}
                                        <td class="text-center">
                                            @if($candidato->video != NULL)
                                                <button type="button" class="btn btn-xs bg-black color-white mr-1 font-14" data-title="Video {{$candidato->name}} {{$candidato->lastName}}" data-instructions="" data-submit-btn="Aceptar" onclick="showVideo('{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->video }}'); toggleModal(this);">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                            @endif
                                            <button class="btn btn-xs bg-black color-white font-14" data-title="Video CV" data-instructions="Selecciona una opción." data-submit-btn="Aceptar" onclick="videoCVmodal('{{$candidato->id}}'); toggleModal(this);"><i class="fas fa-cloud-upload-alt"></i></button>
                                            <form id="videoRecord{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" id="uid" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" id="folder" name="folder" value="{{$candidato->folder}}">
                                                <input type="hidden" id="fileType" name="fileType" value="video">
                                                <input type="hidden" id="returnPath" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                            <form id="videoUpload{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                {{ csrf_field() }}
                                                <input type="file" id="video_file{{$candidato->id}}" name="_file" style="visibility:hidden; position:absolute;" class="_video_file" title="{{$candidato->id}}">
                                                <input type="hidden" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" name="folder" value="{{$candidato->folder}}">
                                                <input type="hidden" name="fileType" value="video">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                        {{-- ESTATUS --}}
                                        <td class="text-center">
                                            <form id="estatusUser{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/estatusUser') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="actualizadorName" value="{{Auth::user()->name}} {{Auth::user()->lastName}}">
                                                <select name="estatus" class="form-control" onChange="$('#estatusUser{{$candidato->id}}desktop').submit();">
                                                    <option value="No revisado" @if($candidato->estatus == 'No revisado') selected="selected" @endif>No revisado / To be reviewed</option>
                                                    <option value="Revisado, quiero entrevistarlo" @if($candidato->estatus == 'Revisado, quiero entrevistarlo') selected="selected" @endif>Revisado, quiero entrevistarlo / Schedule interview</option>
                                                    <option value="Revisado, NO quiero entrevistarlo" @if($candidato->estatus == 'Revisado, NO quiero entrevistarlo') selected="selected" @endif>Revisado, NO quiero entrevistarlo / Do NOT schedule interview</option>
                                                    <option value="Entrevistado, continúa" @if($candidato->estatus == 'Entrevistado, continúa') selected="selected" @endif>Entrevistado, continúa / Interviewed and continues</option>
                                                    <option value="Entrevistado, rechazado" @if($candidato->estatus == 'Entrevistado, rechazado') selected="selected" @endif>Entrevistado, rechazado / Rejected</option>
                                                </select>
                                                
                                                <input type="hidden" name="uid" value="{{$candidato->id}}">
                                                <input type="hidden" name="candidatoName" value="{{$candidato->name}}">
                                                <input type="hidden" name="candidatoLastName" value="{{$candidato->lastName}}">
                                                <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                <input type="hidden" name="vacanteTitulo" value="{{$vacante->titulo}}">
                                                <input type="hidden" name="idUserEmpresa" value="{{$vacante->idUserEmpresa}}">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                        {{-- BLOQUEADO --}}
                                        <td class="text-center">
                                            <form id="blockUser{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/blockUser') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input @if($colocadoCount == $vacante->cantidadDeVacantes || $vacante->estatus == 'Cerrada') disabled @endif onChange="_toggleVal(this,'bloqueado{{$candidato->id}}','blockUser{{$candidato->id}}desktop');" type="checkbox" name="bloqueadoTemp" value="1" @if($candidato->bloqueado == '1') checked="checked" @endif>
                                                <input type="hidden" id="bloqueado{{$candidato->id}}" name="bloqueado" value="{{$candidato->bloqueado}}">
                                                <input type="hidden" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                        {{-- COLOCADO --}}
                                        <td class="text-center">
                                            <form id="hireUser{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/hireUser') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input @if($colocadoCount == $vacante->cantidadDeVacantes || $vacante->estatus == 'Cerrada') disabled @endif onChange="_toggleVal(this,'colocado{{$candidato->id}}','hireUser{{$candidato->id}}desktop');" type="checkbox" name="colocadoTemp" value="1" @if($candidato->colocado == '1') checked="checked" @endif>
                                                <input type="hidden" id="colocado{{$candidato->id}}" name="colocado" value="{{$candidato->colocado}}">
                                                <input type="hidden" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                        {{-- OCULTAR --}}
                                        <td class="text-center">
                                            <form id="hideCandidate{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/hideCandidate') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input @if($candidato->is_hidden == '1') disabled checked @endif onChange="_toggleVal(this,'colocado{{$candidato->id}}','hideCandidate{{$candidato->id}}desktop');" type="checkbox" name="hideCandidate" value="1">
                                                <input type="hidden" id="hideCandidate{{$candidato->id}}" name="hideCandidate" value="{{$candidato->colocado}}">
                                                <input type="hidden" name="id" value="{{$candidato->id}}">
                                                <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>                                            
                                        </td>
                                        @if (Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Vendedor'))
                                            {{-- Mostrar If SuperAdmin --}}
                                            <td class="text-center">
                                                <form id="showCandidate{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/showCandidate') }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input @if($candidato->is_hidden == '0') disabled @endif onChange="_toggleVal(this,'colocado{{$candidato->id}}','showCandidate{{$candidato->id}}desktop');" type="checkbox" name="showCandidate" value="0">
                                                    <input type="hidden" id="showCandidate{{$candidato->id}}" name="showCandidate" value="{{$candidato->colocado}}">
                                                    <input type="hidden" name="id" value="{{$candidato->id}}">
                                                    <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                    <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                </form>                                            
                                            </td>
                                            
                                        @endif
                                    </tr>
                                @endforeach
                            </table>
                            <div class="form-group mb-3 row">
                                <div class="col-md-12">
                                    @if (Auth::user()->hasRole('Reclutador') || Auth::user()->hasRole('SuperAdmin'))
                                    <input id="vacanteId" type="hidden" value="{{$vacante->id}}"  readonly>
                                    <div class="col-md-12 bg-white p-4">
                                        <div id="totalSecfitsError" style="text-align: center;"></div>
                                        <div id="totalSecfits"></div>
                                    </div>  
                                    @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-6 text-left float-left">
                                        <form id="resumenCompUpload{{$vacante->id}}desktop" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                            {{ csrf_field() }}
                                            <input type="file" id="resumenComp_file{{$vacante->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#resumenCompUpload{{$vacante->id}}desktop').submit();">
                                            <input type="hidden" name="id" value="{{$vacante->id}}">
                                            <input type="hidden" name="folder" value="{{$vacante->folder}}">
                                            <input type="hidden" name="fileType" value="resumenComp">
                                            <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                        </form>
                                        @if($vacante->resumenComparativo == NULL)
                                            <button class="btn bg-black color-white" onClick="$('#resumenComp_file{{$vacante->id}}').click()">
                                                Subir resumen comparativo / Upload comparative summary
                                            </button>
                                        @else
                                            <button class="btn bg-black color-white" onClick="$('#resumenComp_file{{$vacante->id}}').click()">
                                                Actualizar resumen comparativo / Update comparative summary
                                            </button>
                                            <a class="btn btn-light" href="{{ asset('storage').'/'.$vacante->folder.'/'.$vacante->resumenComparativo }}" target="_blank">Descargar resumen comparativo</a>
                                        @endif
                                    </div>
                                    <div class="col-md-6 text-right float-left">
                                    	@if($vacante->estatus == 'Abierta')
                                            <button type="button" class="btn bg-levuAzul color-white" data-title="Asignar candidatos" data-instructions="Selecciona los candidatos que deseas asignar a la vacante." data-submit-btn="Asignar / actualizar candidatos" onclick="loadCandidatos('{{Auth::user()->id}}','{{Auth::user()->roles[0]->pivot->role_id}}','{{$vacante->ownerId}}',{{$vacante->id}},'form-addCandidate','{{ csrf_token() }}'); toggleModal(this);">
                                                Asignar - eliminar candidatos / Assign - delete candidates
                                            </button>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 mt-5">
									<div class="col-md-12">
										<form id="vacanteComment{{$vacante->id}}" action="{{ url('/dashboard/vacantes/updateComments') }}" enctype="multipart/form-data" method="POST">
											{{ csrf_field() }}
											<p><strong>Dejar comentarios / Leave comments (<span id="charcount">0</span> de 350 caracteres)</strong></p>
											<textarea onKeyDown="limitText(this,350);" onKeyUp="limitText(this,350);" name="comentarios" id="comentarios{{$vacante->id}}" class="form-control" placeholder="Comentario">{{$vacante->comentarios}}</textarea>
											<input type="submit" class="btn bg-black color-white float-right mt-3" value="Dejar comentario / Leave comment">
											<input type="hidden" name="id" value="{{$vacante->id}}">
                                            
                                            <input type="hidden" name="vacanteTitle" value="{{$vacante->titulo}}">
                                            <input type="hidden" name="empresaName" value="{{$vacante->empresa}}">
                                            <input type="hidden" name="recluEmail" value="{{$vacante->recluEmail}}">
                                            
											<input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
										</form>
									</div>
								</div>
								<div class="clearfix"></div>
                            </div>
                        </div>
                        {{-- END DESKTOP --}}

                        {{-- MOBILE --}}
                        <div class="mobileShow">
                            <div class="col-md-12 bg-whiteGrey p-4">
                                <h2>Candidatos Candidates</h2>
                            </div>
                            <div class="col-md-12">
                                <p>Simbología:</p>

                                <p><span style="color:rgba(255,255,0,1);"><i class="fas fa-square"></i></span> 'No revisado' / 'To be reviewed'</p>
                               
                                <p><span class="ml-4" style="color:rgba(0,0,255,.25);"><i class="fas fa-square"></i></span> 'Revisado, quiero entrevistarlo' o 'Entrevistado, continúa' / 'Schedule interview' or 'Interviewed and continues'</p>

                                <p><span class="ml-4" style="color:rgba(255,0,0,.25);"><i class="fas fa-square"></i></span> 'NO quiero entrevistarlo' o 'Entrevistado, rechazado' / 'Do NOT schedule interview' or 'Rejected'</p>
                            </div>
                            @foreach($candidatos as $candidato)
                                @php $bgColor = 'rgba(255,255,255,1)'; @endphp
                                @if($colocadoCount == $vacante->cantidadDeVacantes && $candidato->colocado == '1')
                                    @php $bgColor = 'rgba(0,255,0,.25)'; @endphp
                                @elseif($candidato->estatus == 'No revisado')
                                    @php $bgColor = 'rgba(255,255,0,.25)'; @endphp
                                @elseif($candidato->estatus == 'Revisado, quiero entrevistarlo' || $candidato->estatus == 'Entrevistado, continúa')
                                    @php $bgColor = 'rgba(0,0,255,.25)'; @endphp
                                @elseif($candidato->estatus == 'Revisado, NO quiero entrevistarlo' || $candidato->estatus == 'Entrevistado, rechazado')
                                    @php $bgColor = 'rgba(255,0,0,.25)'; @endphp
                                @endif
                                <div class="col-md-12 col-sm-12 col-xs-12 bg-white mb-3 pt-3 pb-3" style="background:{{$bgColor}}">
                                    <p><strong>{{$candidato->name}} {{$candidato->lastName}} ({{$candidato->email}})</strong></p>
                                    <div class="col-md-12 p-0">
                                        <table class="table">
                                            <tr>
                                                <td class="text-center">
                                                    <p><strong>CV:</strong></p>
                                                    <p>
                                                        @if($candidato->cv != NULL)
                                                            <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->cv }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                                        @endif
                                                        <button class="btn btn-xs bg-black color-white font-14" onClick="$('#cv_file{{$candidato->id}}').click()"><i class="fas fa-cloud-upload-alt"></i></button>
                                                        <form id="cvUpload{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="file" id="cv_file{{$candidato->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#cvUpload{{$candidato->id}}mobile').submit();">
                                                            <input type="hidden" name="id" value="{{$candidato->id}}">
                                                            <input type="hidden" name="folder" value="{{$candidato->folder}}">
                                                            <input type="hidden" name="fileType" value="cv">
                                                            <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                        </form>
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>SEC FITS:</strong></p>
                                                    <p>
                                                        @if($candidato->secFits != 0)
                                                            <a href="{{ url('/dashboard/vacantes/secFits/'.$candidato->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                                        @endif
                                                        <a href="{{ url('/dashboard/vacantes/secFits/edit/'.$candidato->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white font-14"><i class="fas fa-edit"></i></a>
                                                    </span>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>LTR:</strong></p>
                                                    <p>
                                                        @if($candidato->ltr != NULL)
                                                            <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->ltr }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                                        @endif
                                                        <button class="btn btn-xs bg-black color-white font-14" onClick="$('#ltr_file{{$candidato->id}}').click()"><i class="fas fa-cloud-upload-alt"></i></button>
                                                        <form id="ltrUpload{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="file" id="ltr_file{{$candidato->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#ltrUpload{{$candidato->id}}mobile').submit();">
                                                            <input type="hidden" name="id" value="{{$candidato->id}}">
                                                            <input type="hidden" name="folder" value="{{$candidato->folder}}">
                                                            <input type="hidden" name="fileType" value="ltr">
                                                            <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                        </form>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <p><strong>COMP:</strong></p>
                                                    <p>
                                                        @if($candidato->compensaciones != 0)
                                                            <a href="{{ url('/dashboard/users/compensaciones/'.$candidato->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                                        @endif
                                                        <a href="{{ url('/dashboard/users/compensaciones/edit/'.$candidato->id) }}" class="btn btn-xs bg-black color-white font-14"><i class="fas fa-edit"></i></a>
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>REFS:</strong></p>
                                                    <p>
                                                        <a href="{{ url('/dashboard/users/referencias/'.$candidato->id ) }}" target="_blank" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>VIDEO:</strong></p>
                                                    <p>
                                                        @if($candidato->video != NULL)
                                                            <button type="button" class="btn btn-xs bg-black color-white mr-1 font-14" data-title="Video {{$candidato->name}} {{$candidato->lastName}}" data-instructions="" data-submit-btn="Aceptar" onclick="showVideo('{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->video }}'); toggleModal(this);">
                                                                <i class="fas fa-eye"></i>
                                                            </button>
                                                        @endif
                                                        <button class="btn btn-xs bg-black color-white font-14" data-title="Video CV" data-instructions="Selecciona una opción." data-submit-btn="Aceptar" onclick="videoCVmodal('{{$candidato->id}}'); toggleModal(this);"><i class="fas fa-cloud-upload-alt"></i></button>
                                                        <form id="videoRecord{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" id="uid" name="id" value="{{$candidato->id}}">
                                                            <input type="hidden" id="folder" name="folder" value="{{$candidato->folder}}">
                                                            <input type="hidden" id="fileType" name="fileType" value="video">
                                                            <input type="hidden" id="returnPath" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                        </form>
                                                        <form id="videoUpload{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                                            {{ csrf_field() }}
                                                            <input type="file" id="video_file{{$candidato->id}}mobile" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#videoUpload{{$candidato->id}}mobile').submit();">
                                                            <input type="hidden" name="id" value="{{$candidato->id}}">
                                                            <input type="hidden" name="folder" value="{{$candidato->folder}}">
                                                            <input type="hidden" name="fileType" value="video">
                                                            <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                        </form>
                                                    </p>
                                                </td>
                                            </tr>
                                            {{-- ESTATUS --}}
                                            <tr>
                                                <td class="text-center">
                                                    <form id="estatusUser{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/estatusUser') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="actualizadorName" value="{{Auth::user()->name}} {{Auth::user()->lastName}}">
                                                        <select name="estatus" class="form-control" onChange="$('#estatusUser{{$candidato->id}}mobile').submit();">
                                                            <option value="No revisado" @if($candidato->estatus == '1') selected="selected" @endif>No revisado</option>
                                                            <option value="Revisado, quiero entrevistarlo" @if($candidato->estatus == '1') selected="selected" @endif>Revisado, quiero entrevistarlo</option>
                                                            <option value="Revisado, NO quiero entrevistarlo" @if($candidato->estatus == '1') selected="selected" @endif>Revisado, NO quiero entrevistarlo</option>
                                                            <option value="Entrevistado, continúa" @if($candidato->estatus == '1') selected="selected" @endif>Entrevistado, continúa</option>
                                                            <option value="Entrevistado, rechazado" @if($candidato->estatus == '1') selected="selected" @endif>Entrevistado, rechazado</option>
                                                        </select>
                                                        
                                                        <input type="hidden" name="uid" value="{{$candidato->id}}">
                                                        <input type="hidden" name="candidatoName" value="{{$candidato->name}}">
                                                        <input type="hidden" name="candidatoLastName" value="{{$candidato->lastName}}">
                                                        <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                        <input type="hidden" name="vacanteTitulo" value="{{$vacante->titulo}}">
                                                        <input type="hidden" name="idUserEmpresa" value="{{$vacante->idUserEmpresa}}">
                                                        <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                    </form>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <form id="blockUser{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/blockUser') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <label for="bloqueado{{$candidato->id}}">
                                                        <input id="bloqueado{{$candidato->id}}" @if($colocadoCount == $vacante->cantidadDeVacantes || $vacante->estatus == 'Cerrada') disabled @endif onChange="_toggleVal(this,'bloqueado{{$candidato->id}}','blockUser{{$candidato->id}}mobile');" type="checkbox" name="bloqueadoTemp" value="1" @if($candidato->bloqueado == '1') checked="checked" @endif> Bloqueado</label>
                                                        <input type="hidden" id="bloqueado{{$candidato->id}}" name="bloqueado" value="{{$candidato->bloqueado}}">
                                                        <input type="hidden" name="id" value="{{$candidato->id}}">
                                                        <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                        <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                    </form>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" class="text-center">
                                                    <form id="hireUser{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/hireUser') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <label for="colocado{{$candidato->id}}">
                                                        <input id="colocado{{$candidato->id}}" @if($colocadoCount == $vacante->cantidadDeVacantes || $vacante->estatus == 'Cerrada') disabled @endif onChange="_toggleVal(this,'colocado{{$candidato->id}}','hireUser{{$candidato->id}}mobile');" type="checkbox" name="colocadoTemp" value="1" @if($candidato->colocado == '1') checked="checked" @endif> Colocado</label>
                                                        <input type="hidden" id="colocado{{$candidato->id}}" name="colocado" value="{{$candidato->colocado}}">
                                                        <input type="hidden" name="id" value="{{$candidato->id}}">
                                                        <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                        <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                    </form>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                            <div class="form-group mb-3 row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-center float-left">
                                        <form id="resumenCompUpload{{$vacante->id}}mobile" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                            {{ csrf_field() }}
                                            <input type="file" id="resumenComp_file{{$vacante->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#resumenCompUpload{{$vacante->id}}mobile').submit();">
                                            <input type="hidden" name="id" value="{{$vacante->id}}">
                                            <input type="hidden" name="folder" value="{{$vacante->folder}}">
                                            <input type="hidden" name="fileType" value="resumenComp">
                                            <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                        </form>
                                        @if($vacante->resumenComparativo == NULL)
                                            <button class="btn bg-black color-white mb-3" onClick="$('#resumenComp_file{{$vacante->id}}').click()">
                                                Subir resumen comparativo
                                            </button>
                                        @else
                                            <button class="btn bg-black color-white mb-3" onClick="$('#resumenComp_file{{$vacante->id}}').click()">
                                                Actualizar resumen comparativo
                                            </button>
                                            <a class="btn btn-light mb-3" href="{{ asset('storage').'/'.$vacante->folder.'/'.$vacante->resumenComparativo }}" target="_blank">Descargar resumen comparativo</a>
                                        @endif
                                    	@if($vacante->estatus == 'Abierta')
                                            <button type="button" class="btn bg-levuAzul color-white mb-3" data-title="Asignar candidatos" data-instructions="Selecciona los candidatos que deseas asignar a la vacante." data-submit-btn="Asignar / actualizar candidatos" onclick="loadCandidatos('{{Auth::user()->id}}','{{Auth::user()->roles[0]->pivot->role_id}}','{{$vacante->ownerId}}',{{$vacante->id}},'form-addCandidate','{{ csrf_token() }}'); toggleModal(this);">
                                                Asignar / eliminar candidatos
                                            </button>
                                        @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        {{-- END MOBILE --}}
                    </div>
                @elseif(Auth::user()->hasRole('Empresa'))
                	<div class="col-md-12 pt-0 pr-5 pb-5 pl-5 pt-0 levuTabsContent" id="tabCandidatos">
                    	{{-- DESKTOP --}}
                        <div class="col-md-12 bg-white p-4 mobileHide">
                            <table class="table table-stripped font-12">
                            <tr>
                                <th>Nombre</th>
                                <th>Fecha de asignación / Assigned at</th>
                                <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Aquí puedes ver y/o descargar el CV">CV</span></th>
                                <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Skills, experience y Cuture Fits">SEC&nbsp;FITS</span></th>
                                <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Levu Talent Report (Psicometría)">LTR</span></th>
                                <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Detalle de percepciones">Comp.</span></th>
                                <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Referencias laborales">Refs.</span></th>
                                <th class="text-center"><span data-toggle="tooltip" data-placement="bottom" title="Conoce en 2 min. al candidato">Video</span></th>
                                <th class="text-center" width="100">Estatus</th>
                            </tr>
                            @foreach($candidatos as $candidato)
                                @php $bgColor = 'rgba(255,255,255,1)'; @endphp
                                @if($colocadoCount == $vacante->cantidadDeVacantes && $candidato->colocado == '1')
                                    @php $bgColor = 'rgba(0,255,0,.25)'; @endphp
                                @elseif($candidato->estatus == 'No revisado')
                                    @php $bgColor = 'rgba(255,255,0,.25)'; @endphp
                                @elseif($candidato->estatus == 'Revisado, quiero entrevistarlo' || $candidato->estatus == 'Entrevistado, continúa')
                                    @php $bgColor = 'rgba(0,0,255,.25)'; @endphp
                                @elseif($candidato->estatus == 'Revisado, NO quiero entrevistarlo' || $candidato->estatus == 'Entrevistado, rechazado')
                                    @php $bgColor = 'rgba(255,0,0,.25)'; @endphp
                                @endif
                                {{-- @if($candidato->cv != NULL && $candidato->secFits != 0 && $candidato->ltr != NULL) --}}
                                    <tr style="background: {{$bgColor}}">
                                        <td>{{$candidato->name}} {{$candidato->lastName}}</td>
                                        @if($candidato->assigned_at)
                                            <td><span data-toggle="tooltip" data-placement="bottom" title="{{$candidato->assigned_at}}">{{date('d-m-Y', strtotime($candidato->assigned_at))}}</span></td>
                                        @else
                                            <td><span data-toggle="tooltip" data-placement="bottom" title=""></span></td>
                                        @endif
                                        {{-- CV --}}
                                        <td class="text-center">
                                            @if($candidato->cv != NULL)
                                                <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->cv }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                            @else
                                                <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></span>
                                            @endif
                                        </td>
                                        {{-- SEC FITS --}}
                                        <td class="text-center">
                                            @if($candidato->secFits != 0)
                                                <a href="{{ url('/dashboard/vacantes/secFits/'.$candidato->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                            @else
                                                <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                            @endif
                                        </td>
                                        {{-- LTR --}}
                                        <td class="text-center">
                                            @if($candidato->ltr != NULL)
                                                <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->ltr }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                            @else
                                                <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                            @endif
                                        </td>
                                        {{-- COMPENSACIONES --}}
                                        <td class="text-center">
                                            @if($candidato->compensaciones != 0)
                                                <a href="{{ url('/dashboard/users/compensaciones/'.$candidato->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                            @else
                                                <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                            @endif
                                        </td>
                                        {{-- REFERENCIAS --}}
                                        <td class="text-center">
                                            @if($candidato->referencias != 0)
                                                <a href="{{ url('/dashboard/users/referencias/'.$candidato->id ) }}" target="_blank" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                            @else
                                                <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                            @endif
                                        </td>
                                        {{-- VIDEO --}}
                                        <td class="text-center">
                                            @if($candidato->video != NULL)
                                                <button type="button" class="btn btn-xs bg-black color-white mr-1 font-14" data-title="Video {{$candidato->name}} {{$candidato->lastName}}" data-instructions="" data-submit-btn="Aceptar" onclick="showVideo('{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->video }}'); toggleModal(this);">
                                                    <i class="fas fa-eye"></i>
                                                </button>
                                            @else
                                                <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                            @endif
                                        </td>
                                        {{-- ESTATUS --}}
                                        <td class="text-center">
                                            <form id="estatusUser{{$candidato->id}}desktop" action="{{ url('/dashboard/vacantes/estatusUser') }}" method="POST">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="actualizadorName" value="{{Auth::user()->name}} {{Auth::user()->lastName}}">
                                                <select name="estatus" class="form-control" onChange="$('#estatusUser{{$candidato->id}}desktop').submit();">
                                                	<option value="No revisado" @if($candidato->estatus == 'No revisado') selected="selected" @endif>No revisado</option>
                                                    <option value="Revisado, quiero entrevistarlo" @if($candidato->estatus == 'Revisado, quiero entrevistarlo') selected="selected" @endif>Revisado, quiero entrevistarlo</option>
                                                    <option value="Revisado, NO quiero entrevistarlo" @if($candidato->estatus == 'Revisado, NO quiero entrevistarlo') selected="selected" @endif>Revisado, NO quiero entrevistarlo</option>
                                                    <option value="Entrevistado, continúa" @if($candidato->estatus == 'Entrevistado, continúa') selected="selected" @endif>Entrevistado, continúa</option>
                                                    <option value="Entrevistado, rechazado" @if($candidato->estatus == 'Entrevistado, rechazado') selected="selected" @endif>Entrevistado, rechazado</option>
                                                </select>
                                                
                                                <input type="hidden" name="uid" value="{{$candidato->id}}">
                                                <input type="hidden" name="candidatoName" value="{{$candidato->name}}">
                                                <input type="hidden" name="candidatoLastName" value="{{$candidato->lastName}}">
                                                <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                <input type="hidden" name="vacanteTitulo" value="{{$vacante->titulo}}">
                                                <input type="hidden" name="idUserEmpresa" value="{{$vacante->idUserEmpresa}}">
                                                <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                            </form>
                                        </td>
                                    </tr>
                                {{-- @endif --}}
                            @endforeach
                            </table>
                            <div class="form-group mb-3 row">
                                <div class="col-md-12">
                                    <div class="col-md-6 text-left float-left">
                                        @if($vacante->resumenComparativo != NULL)
                                            <a class="btn btn-light" href="{{ asset('storage').'/'.$vacante->folder.'/'.$vacante->resumenComparativo }}" target="_blank">Descargar resumen comparativo</a>
                                        @endif
									</div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                            
                            <input id="vacanteId" type="hidden" value="{{$vacante->id}}">
                            <div class="col-md-12 bg-white p-4">
                                <div id="totalSecfitsError" style="text-align: center;"></div>
                                <div id="totalSecfits"></div>
                            </div>
                        </div>
                        {{-- END DESKTOP --}}
                        {{-- MOBILE --}}
                        <!--
                        <div class="mobileShow">
                            <div class="col-md-12 bg-whiteGrey p-4">
                                <h2>Candidatos</h2>
                            </div>
                            @foreach($candidatos as $candidato)
                                <div class="col-md-12 col-sm-12 col-xs-12 bg-white mb-3 pt-3 pb-3">
                                    <p><strong>{{$candidato->name}} {{$candidato->lastName}} ({{$candidato->email}})</strong></p>
                                    <div class="col-md-12 p-0">
                                        <table class="table">
                                            <tr>
                                                <td class="text-center">
                                                    <p><strong>CV:</strong></p>
                                                    <p>
                                                        @if($candidato->cv != NULL)
                                                            <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->cv }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                                        @else
                                                            <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></span>
                                                        @endif
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>SEC FITS:</strong></p>
                                                    <p>
                                                        @if($candidato->secFits != 0)
                                                            <a href="{{ url('/dashboard/vacantes/secFits/'.$candidato->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                                        @else
                                                            <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a>
                                                        @endif
                                                    </span>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>LTR:</strong></p>
                                                    <p>
                                                        @if($candidato->secFits != 0)
                                                            <a href="{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->ltr }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                                                        @else
                                                            <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a>
                                                        @endif
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-center">
                                                    <p><strong>COMP:</strong></p>
                                                    <p>
                                                        @if($candidato->compensaciones != 0)
                                                            <a href="{{ url('/dashboard/users/compensaciones/'.$candidato->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                                        @else
                                                            <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                                        @endif
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>REFS:</strong></p>
                                                    <p>
                                                        @if($candidato->referencias != 0)
                                                            <a href="{{ url('/dashboard/users/referencias/'.$candidato->id ) }}" target="_blank" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                                                        @else
                                                            <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                                        @endif
                                                    </p>
                                                </td>
                                                <td class="text-center">
                                                    <p><strong>VIDEO:</strong></p>
                                                    <p>
                                                        @if($candidato->video != NULL)
                                                            <button type="button" class="btn btn-xs bg-black color-white mr-1 font-14" data-title="Video {{$candidato->name}} {{$candidato->lastName}}" data-instructions="" data-submit-btn="Aceptar" onclick="showVideo('{{ asset('storage').'/'.$candidato->folder.'/'.$candidato->video }}'); toggleModal(this);">
                                                                <i class="fas fa-eye"></i>
                                                            </button>
                                                        @else
                                                            <span class="btn btn-xs bg-black color-white mr-1 font-14 btn-disabled" data-toggle="tooltip" data-placement="bottom" title="No disponible"><i class="fas fa-eye"></i></a></span>
                                                        @endif
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                            	{{-- ESTATUS --}}
                                                <td class="text-center">
                                                    <form id="estatusUser{{$candidato->id}}mobile" action="{{ url('/dashboard/vacantes/estatusUser') }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="actualizadorName" value="{{Auth::user()->name}} {{Auth::user()->lastName}}">
                                                        <select name="estatus" class="form-control" onChange="$('#estatusUser{{$candidato->id}}mobile').submit();">
                                                            <option value="No revisado" @if($candidato->estatus == 'No revisado') selected="selected" @endif>No revisado</option>
                                                            <option value="Revisado, quiero entrevistarlo" @if($candidato->estatus == 'Revisado, quiero entrevistarlo') selected="selected" @endif>Revisado, quiero entrevistarlo</option>
                                                            <option value="Revisado, NO quiero entrevistarlo" @if($candidato->estatus == 'Revisado, NO quiero entrevistarlo') selected="selected" @endif>Revisado, NO quiero entrevistarlo</option>
                                                            <option value="Entrevistado, continúa" @if($candidato->estatus == 'Entrevistado, continúa') selected="selected" @endif>Entrevistado, continúa</option>
                                                            <option value="Entrevistado, rechazado" @if($candidato->estatus == 'Entrevistado, rechazado') selected="selected" @endif>Entrevistado, rechazado</option>
                                                        </select>
                                                        
                                                        <input type="hidden" name="uid" value="{{$candidato->id}}">
                                                        <input type="hidden" name="candidatoName" value="{{$candidato->name}}">
                                                        <input type="hidden" name="candidatoLastName" value="{{$candidato->lastName}}">
                                                        <input type="hidden" name="idVacante" value="{{$vacante->id}}">
                                                        <input type="hidden" name="vacanteTitulo" value="{{$vacante->titulo}}">
                                                        <input type="hidden" name="idUserEmpresa" value="{{$vacante->idUserEmpresa}}">
                                                        <input type="hidden" class="_returnPath" name="_returnPath" value="{{Request::url()}}">
                                                    </form>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            @endforeach
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="col-md-6 text-left float-left">
                                    @if($vacante->resumenComparativo != NULL)
                                        <a class="btn btn-light" href="{{ asset('storage').'/'.$vacante->folder.'/'.$vacante->resumenComparativo }}" target="_blank">Descargar resumen comparativo</a>
                                    @endif
								</div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        -->
                        {{-- END MOBILE --}}
                    </div>
                @endif
        @endif
    </div>
</div>
<link rel="stylesheet" href="{{ asset('css/charts.css')}}">
<script type="text/javascript" src="{{ asset('js/chart-secfits.js')}}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script>
$('._video_file').each(function(index, element) {
	var _formID = 'videoUpload' + $(this).attr('title') + 'desktop';
	$(this).change(function () {
		var f = this.files[0]
		var _size = ((f.size||f.fileSize) / 1024) / 1024
		if(_size > 50){
			alert('El tamaño de tu archivo es: ' + _size.toFixed(2) + 'MB. El límite es de 50 MB, por favor intenta con otro video.');
		}else{
			$('#'+_formID).submit();
		}
	});
});

 var _hash = window.location.hash;
 //console.log('hash: '+_hash);*/
 if(_hash != ''){
	 //console.log(_hash);
	 $('.levuTabs').removeClass('active');
	 $(_hash+'Btn').addClass('active');
	 $(_hash).show();
 }else{
	 //console.log('NO HASH');
	 $('#tabDatosVacante').show();
 }
 $('input._returnPath').val('{{Request::url()}}'+_hash);
function limitText(limitField, limitNum) {
	if (limitField.value.length > limitNum) {
		limitField.value = limitField.value.substring(0, limitNum);
	}
	$('#charcount').html(limitField.value.length);
}
</script>
@endsection
