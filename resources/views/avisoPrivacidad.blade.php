@extends('layouts.siteTexts')

@section('title', "LEVU Talent")

@section('content')

<section id="avisoPrivacidad" class="site">
    <div class="container mb-5">
    	<h1 class="text-center mb-5">Aviso de privacidad</h1>
        <p>Levumex S.A. de C.V. (nombre comercial: Levu Talent Hunters) con domicilio en Aristóteles #81 Oficina 202, Polanco IV Sección, Miguel Hidalgo, Ciudad de México, C.P.11500, es el responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente:</p>
        <h2>Departamento de Privacidad</h2>
        <p>Para atender cualquier solicitud acerca del ejercicio de sus derechos ARCO, para la revocación del consentimiento que nos haya otorgado del tratamiento de sus datos personales, para limitar el uso o divulgación de sus datos, o acerca de las finalidades para el tratamiento de sus datos personales, ponemos a su disposición nuestro Departamento de Privacidad en los siguientes medios de contacto:</p>
        <p>Departamento de Privacidad<br>Correo electrónico: <a href="mailto:jobs@levutalent.com">jobs@levutalent.com</a></p>
        <p>Para poder comunicarnos con usted, necesitamos los siguientes datos personales y de contacto en su solicitud:</p>
        <ul>
        	<li>Nombre del titular</li>
            <li>Carta de autorización en caso de que la solicitud la haga otra persona que no sea el titular</li>
            <li>Copia de identificación del titular</li>
            <li>Correo electrónico</li>
            <li>Teléfono</li>
        </ul>
        <p>Después de recibir su solicitud, recibirá nuestra respuesta en un plazo máximo de veinte días hábiles por los medios de contacto que nos proporcione.</p>
        <h2>¿Para qué fines utilizaremos sus datos personales?</h2>
        <p>Los datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para el servicio que solicita:</p>
        <ul>
        	<li>Para fines de contacto</li>
            <li>Para permitir el registro en el sitio</li>
            <li>Para acceder al contenido del sitio</li>
            <li>Para reclutamiento</li>
            <li>Para postulaciones a vacantes con nuestros clientes</li>
        </ul>
        <p>De manera adicional, utilizaremos su información personal para las siguientes finalidades secundarias que no son necesarias para el servicio solicitado, pero que nos permiten y facilitan brindarle mejor atención:</p>
        <ul>
        	<li>Para envío de publicidad</li>
        </ul>
        <p>En caso de que no desee que sus datos personales sean tratados para estos fines adicionales, lo podrá indicar en el medio a través del cual nos proporcione sus datos personales, seleccionando la opción correspondiente.</p>
        <p>La negativa para el uso de sus datos personales para estas finalidades adicionales no podrá ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.</p>
        <h2>¿Qué datos personales utilizaremos para estos fines?</h2>
        <p>Para llevar a cabo las finalidades descritas en el presente Aviso de Privacidad, utilizaremos los siguientes datos personales:</p>
        <ul>
        	<li>Datos de contacto</li>
            <li>Datos de identificación</li>
            <li>Datos laborales</li>
            <li>Datos académicos</li>
            <li>Datos sobre referencias</li>
        </ul>
        <h2>¿Con quién compartimos su información personal y para qué fines?</h2>
        <p>Le informamos que sus datos personales son compartidos con las siguientes personas, empresas, organizaciones y autoridades distintas a nosotros, para los siguientes fines:</p>
        <table class="table">
        	<tr>
            	<td width="50%"><strong>Destinatario de los datos personales</strong></td>
                <td><strong>Finalidad</strong></td>
            </tr>
            <tr>
            	<td>Clientes</td>
                <td>Proceso de reclutamiento</td>
            </tr>
        </table>
        <p>Le informamos que para las transferencias mencionadas anteriormente no necesitamos de su consentimiento ya que son necesarias para poder cumplir con las obligaciones que tenemos con usted, para proveer algún servicio o un producto.</p>
        <h2>¿Cómo puede Acceder, Rectificar o Cancelar sus datos personales, u Oponerse a su uso?</h2>
        <p>Usted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada conforme a los principios, deberes y obligaciones previstas en la normativa (Cancelación); así como oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.</p>
        <p>Si usted desea ejercer sus derechos ARCO, lo podrá hacer contactándose con nuestro Departamento de Privacidad en los medios establecidos en el presente aviso.</p>
        <h2>¿Cómo puede revocar su consentimiento para el uso de sus datos personales?</h2>
        <p>Usted puede revocar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar qué para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de su relación con nosotros.</p>
        <p>Para conocer el procedimiento y requisitos para la revocación de su consentimiento, usted podrá ponerse en contacto con nuestro Departamento de Privacidad a través de los medios especificados en el presente aviso.</p>
        <h2>¿Cómo puede limitar el uso o divulgación de su información personal?</h2>
        <p>Si usted desea limitar el uso o divulgación de su información personal podrá solicitarlo a nuestro Departamento de Privacidad a través de los medios especificados en el presente aviso.</p>
        <p>Adicionalmente, podemos poner a su disposición procedimientos y mecanismos específicos mediante los cuales puede limitar el uso de su información personal. Estos procedimientos y mecanismos específicos se informarán a través de los medios que utilicemos para comunicarnos con usted u otros que consideremos adecuados.</p>
        <h2>Uso de Cookies y tecnologías de rastreo</h2>
        <p>Las cookies son pedazos de información en forma de pequeños archivos que se localizan en su disco duro y son generados por nuestro sitio web. Estos archivos hacen que la interacción con nuestro sitio sea más rápida y fácil, recordando preferencias, datos de comportamiento o datos de registro en nuestro sitio. La información que contienen puede ser utilizada cuando usted visita nuestro sitio y sitios externos.</p>
        <p>Algunas páginas o correos nuestros pueden contener pequeñas imágenes invisibles llamadas “web beacons” o “pixel tags”. Los web beacons rastrean su comportamiento en una página o correo de forma similar a las cookies.</p>
        <p>Le informamos que las cookies y otras tecnologías de rastreo las utilizamos para las siguientes finalidades:</p>
        <ul>
        	<li>Para permitir el registro e inicio de sesión en nuestro sitio</li>
        </ul>
        <p>Si usted prefiere deshabilitar las cookies completamente, lo puede hacer en la mayoría de los exploradores de internet. Tenga en cuenta que este sitio puede dejar de funcionar correctamente al hacerlo. En las versiones más recientes de los exploradores Mozilla Firefox, Internet Explorer y Google Chrome, la opción para deshabilitar las cookies se encuentra en la sección de “Privacidad” en las opciones de configuración de cada explorador.</p>
        <h2>¿Cómo puede conocer los cambios a este Aviso de Privacidad?</h2>
        <p>El presente Aviso de Privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.
Los cambios al Aviso de Privacidad se informarán a través de nuestro sitio web en la siguiente dirección: <a href="https://levutalent.com">www.levutalent.com</a></p>
    </div>
    <div class="clearfix"></div>
</section>
@endsection