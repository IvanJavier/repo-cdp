@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')
<div class="main-content bg-whiteGrey pt-5">
    <div class="col-md-12 mb-4">
        <h1 class="mt-0">Dashboard</h1>
        <h3>Bienvenido {{Auth::user()->name}}</h3>
    </div>
    <div class="col-md-12">
        <div class="row">
            @if(Auth::user()->hasRole('SuperAdmin'))
                <div class="col-md-6 mb-5">
                    <div class="">

                        <div class="bg-info bg-transparent card-header color-grey p-0">
                            <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>ADMINISTRADORES</strong></span>
                            <a class="btn bg-levuAzul float-right color-white btn-sm float-right" href="{{ url('/dashboard/users/registration/Admin') }}">
                                <span class="mobileHide">Nuevo admin</span><span class="mobileShow"><i class="fas fa-plus"></i>
                            </a>
                            <div class="clearfix"></div>
                        </div>

                        <div class="collapse show" id="collapseAdmin">
                            <div class="bg-white">
                                <table class="table table-striped table-hover mb-2 borderless">
                                    <tr class="border-bottom">
                                        <th>NOMBRE</th>
                                        <th class="mobileHide">CORREO</th>
                                        <th>TELÉFONO</th>
                                        <th></th>
                                    </tr>
                                    @foreach($users as $user)
                                        @if($user->hasRole('Admin'))
                                            <tr>
                                                <td>{{$user->name}} {{$user->lastName}} </td>
                                                <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                                                <td>{{$user->phone}}</td>
                                                <td class="text-right">
                                                    <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <button class="btn bg-black btn-sm color-white" data-title="Eliminar admin" data-instructions="¿Estás seguro/a de querer eliminar este admin?" data-submit-btn="Eliminar admin" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-deleteAdmin','{{ csrf_token() }}', '', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- VENDEDORES --}}
                <div class="col-md-6 mb-5">
                    <div class="">

                        <div class="bg-info bg-transparent card-header color-grey p-0">
                            <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>VENDEDORES</strong></span>
                            <a class="btn bg-levuAzul float-right color-white btn-sm float-right" href="{{ url('/dashboard/users/registration/Vendedor') }}">
                                <span class="mobileHide">Nuevo vendedor</span><span class="mobileShow"><i class="fas fa-plus"></i>
                            </a>
                            <div class="clearfix"></div>
                        </div>

                        <div class="collapse show" id="collapseVendedor">
                            <div class="bg-white">
                                <table class="table table-striped table-hover mb-2 borderless">
                                    <tr class="border-bottom">
                                        <th>NOMBRE</th>
                                        <th class="mobileHide">CORREO</th>
                                        <th>TELÉFONO</th>
                                        <th></th>
                                    </tr>
                                    @foreach($users as $user)
                                        @if($user->hasRole('Vendedor'))
                                            <tr>
                                                <td>{{$user->name}} {{$user->lastName}} </td>
                                                <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                                                <td>{{$user->phone}}</td>
                                                <td class="text-right">
                                                    <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <button class="btn bg-black btn-sm color-white" data-title="Eliminar vendedor" data-instructions="¿Estás seguro/a de querer eliminar este vendedor?" data-submit-btn="Eliminar vendedor" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-deleteAdmin','{{ csrf_token() }}', '', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            {{-- RECLUTADORES --}}
            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                <div class="col-md-6 mb-5">
                    <div class="">
    
                        <div class="bg-info bg-transparent card-header color-grey p-0">
                            <span class="mobileHide card-title pb-2 pl-3 pr-3 float-left m-0"><strong>VACANTES POR HEADHUNTER</strong></span>
                            <span class="mobileShow card-title pb-2 pl-3 pr-3 float-left m-0"><strong>VAC. x HEADHUNTER</strong></span>
                            
                            <a href="{{ url('/dashboard/users/registration/Reclutador') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right ml-2">
                                <span class="mobileHide">Nuevo HeadHunter</span><span class="mobileShow"><i class="fas fa-plus"></i></span>
                            </a>
                            <a href="{{ url('/dashboard/users/Reclutador') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right">
                                <span class="mobileHide">Ver todos</span><span class="mobileShow"><i class="fas fa-list-ul"></i></span>
                            </a>
                            <div class="clearfix"></div>
                        </div>
    
                        <div class="collapse show" id="collapseReclu">
                            <div class="">
                                <table class="table table-striped table-hover mb-2 borderless">
                                    <tr class="border-bottom">
                                        <th>NOMBRE</th>
                                        <th class="mobileHide">CORREO</th>
                                        <th>VAC.</th>
                                        <th></th>
                                    </tr>
                                    @php ($i = 0)
                                    @foreach($users as $user)
                                        @if($user->hasRole('Reclutador'))
                                            @if($i < 5)
                                                <tr>
                                                    <td>{{$user->name}} {{$user->lastName}}</td>
                                                    <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                                                    <td>{{$user->vacantes}}</td>
                                                    <td class="text-right">
                                                        <a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle de HeadHunter">
                                                            <i class="far fa-eye"></i>
                                                        </a>
                                                        <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar HeadHunter">
                                                            <i class="far fa-edit"></i>
                                                        </a>
                                                        <button class="btn bg-black btn-sm color-white" data-title="Eliminar HeadHunter" data-instructions="¿Estás seguro/a de querer eliminar este HeadHunter?" data-submit-btn="Eliminar HeadHunter" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-deleteReclutador','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar HeadHunter">
                                                            <i class="far fa-trash-alt"></i>
                                                        </button>
                                                    </td>
                                                </tr>
                                                @php ($i++)
                                            @else
                                                @break
                                            @endif
                                        @endif
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            {{-- VACANTES --}}
            <div class="col-md-6 mb-5">
                <div class="">

                    <div class="bg-info bg-transparent card-header color-grey p-0">
                    	@if(Auth::user()->hasRole('Reclutador'))
                        	<span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>MIS VACANTES ABIERTAS</strong></span>
                        @else
                        	<span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>VACANTES</strong></span>
                        @endif
                        <a href="{{ url('/dashboard/vacantes/create') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right ml-2">
                            <span class="mobileHide">Nueva vacante</span><span class="mobileShow"><i class="fas fa-plus"></i></span>
                        </a>
                        <a href="{{ url('/dashboard/vacantes') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right">
                            <span class="mobileHide">Ver todas</span><span class="mobileShow"><i class="fas fa-list-ul"></i></span>
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="collapse show" id="collapseVac">
                        <div class="">
                            <table class="table table-striped table-hover mb-2 borderless">
                                <tr class="border-bottom">
                                    <th>VACANTE</th>
                                    <th class="mobileHide">EMPRESA</th>
                                    <th class="text-center">CANDIDATOS</th>
                                    <th></th>
                                </tr>
                                @php ($i = 0)
                                @foreach($vacantes as $vacante)
                                    @if($i < 5 && $vacante->estatus == 'Abierta')
                                        <tr>
                                            <td>{{$vacante->titulo}}</td>
                                            <td class="mobileHide">{{$vacante->empresa}}</td>
                                            <td class="text-center">{{$vacante->candidatos}}</td>
                                            <td class="text-right">
                                                <a href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle de vacante">
                                                    <i class="far fa-eye"></i>
                                                </a>
                                                <a href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar vacante">
                                                    <i class="far fa-edit"></i>
                                                </a>
                                                <button class="btn bg-black btn-sm color-white" data-title="Eliminar vacante" data-instructions="¿Estás seguro/a de querer eliminar esta vacante?" data-submit-btn="Eliminar vacante" onclick="deleteRecord('{{$vacante->id}}', 'Vacante', '','form-deleteVacante','{{ csrf_token() }}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar vacante">
                                                    <i class="far fa-trash-alt"></i>
                                                </button>
                                            </td>
                                        </tr>
                                        @php($i++)
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            {{-- CANDIDATOS --}}
            <div class="col-md-6 mb-4 mt-5">
                <div class="">

                    <div class="bg-info bg-transparent card-header color-grey p-0">
                        <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>CANDIDATOS</strong></span>
                        
                        <a href="{{ url('/dashboard/users/registration/Candidato') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right ml-2">
                            <span class="mobileHide">Nuevo candidato</span><span class="mobileShow"><i class="fas fa-plus"></i></span>
                        </a>
                        <a href="{{ url('/dashboard/users/Candidato') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right">
                            <span class="mobileHide">Ver todos</span><span class="mobileShow"><i class="fas fa-list-ul"></i></span>
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="collapse show" id="collapseCandi">
                        <div class="">
                            <table class="table table-striped table-hover mb-2 borderless">
                                <tr class="border-bottom">
                                    <th>NOMBRE</th>
                                    <th class="mobileHide">CORREO</th>
                                    <th>POSTU.</th>
                                    <th></th>
                                </tr>
                                @php ($i = 0)
                                @foreach($users as $user)
                                    @if($user->hasRole('Candidato') && $user->bloqueado != 1)
                                        @if($i < 5)
                                            <tr>
                                                <td>{{$user->name}} {{$user->lastName}}</td>
                                                <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                                                <td>{{$user->postulaciones}}</td>
                                                <td class="text-right">
                                                    <a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle de candidato">
                                                        <i class="far fa-eye"></i>
                                                    </a>
                                                    <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar candidato">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <button class="btn bg-black btn-sm color-white" data-title="Eliminar candidato" data-instructions="¿Estás seguro/a de querer eliminar este candidato?" data-submit-btn="Eliminar candidatos" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-deleteCandidato','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar candidato">
                                                        <i class="far fa-trash-alt"></i>
                                                    </button>
                                                </td>
                                            </tr>
                                            @php ($i++)
                                        @else
                                        	@break
                                        @endif
                                    @endif
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {{-- EMPRESAS --}}
            <div class="col-md-6 mb-4 mt-5">
                <div class="">

                    <div class="bg-info bg-transparent card-header color-grey p-0">
                        <span class="card-title pb-2 pl-3 pr-3 float-left m-0"><strong>CLIENTES</strong></span>
                        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                            <a href="{{ url('/dashboard/users/registration/Empresa') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right ml-2">
                                <span class="mobileHide">Nueva empresa</span><span class="mobileShow"><i class="fas fa-plus"></i></span>
                            </a>
                        @endif
                        <a href="{{ url('/dashboard/users/Empresa') }}" class="btn bg-levuAzul float-right color-white btn-sm float-right">
                            <span class="mobileHide">Ver todos</span><span class="mobileShow"><i class="fas fa-list-ul"></i></span>
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    <div class="collapse show" id="collapseEmp">
                        <div class="">
                            <table class="table table-striped table-hover mb-2 borderless">
                                <tr class="border-bottom">
                                    <th>EMPRESA</th>
                                    <th class="mobileHide">CORREO</th>
                                    <th>VAC.</th>
                                    <th></th>
                                </tr>
                                @php ($i = 0)
                                @foreach($users as $user)
                                    @if($user->hasRole('Empresa'))
                                    
                                    	@if(Auth::user()->hasRole('Reclutador'))
                                        	@if($vacantes->contains('idUserEmpresa',$user->id))
                                            	{{-- --}}
                                                @if($i < 5)
                                                    <tr>
                                                        <td>{{$user->name}}</td>
                                                        <td class="mobileHide">{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                                                        <td>{{$user->vacantes}}</td>
                                                        <td class="text-right">
                                                            <a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle de empresa">
                                                                <i class="far fa-eye"></i>
                                                            </a>
                                                            @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                                                                <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar empresa">
                                                                    <i class="far fa-edit"></i>
                                                                </a>
                                                                <button class="btn bg-black btn-sm color-white" data-title="Eliminar empresa" data-instructions="¿Estás seguro/a de querer eliminar esta empresa?" data-submit-btn="Eliminar empresa" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-deleteEmpresa','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar empresa">
                                                                    <i class="far fa-trash-alt"></i>
                                                                </button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @php ($i++)
                                                @else
                                                    @break
                                                @endif
                                                {{-- --}}
                                            @endif
                                        @else
                                        	@if($i < 5)
                                                <tr>
                                                    <td>{{$user->name}}</td>
                                                    <td>{{ str_limit($user->email, $limit = 15, $end = '...') }}</td>
                                                    <td>{{$user->vacantes}}</td>
                                                    <td class="text-right">
                                                        <a href="{{ url('/dashboard/users/profile/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Detalle de empresa">
                                                            <i class="far fa-eye"></i>
                                                        </a>
                                                        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                                                            <a href="{{ url('/dashboard/users/edit/') }}/{{$user->id}}" class="btn bg-black btn-sm color-white" data-toggle="tooltip" data-placement="bottom" title="Editar empresa">
                                                                <i class="far fa-edit"></i>
                                                            </a>
                                                            <button class="btn bg-black btn-sm color-white" data-title="Eliminar empresa" data-instructions="¿Estás seguro/a de querer eliminar esta empresa?" data-submit-btn="Eliminar empresa" onclick="deleteRecord('{{$user->id}}', '{{$user->roles[0]->name}}', '{{$user->email}}','form-deleteEmpresa','{{ csrf_token() }}', '{{$user->folder}}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar empresa">
                                                                <i class="far fa-trash-alt"></i>
                                                            </button>
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php ($i++)
                                            @else
                                                @break
                                            @endif
                                        @endif
                                    
                                    	
                                        
                                        
                                    @endif
                               @endforeach 
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection
