@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<body onload="sellersInitialCharts()">
    <div class="main-content bg-whiteGrey">
        <div class="row justify-content-center">
            <div class="col-md-12 bg-white">
                <div class="col-md-12 mb-4">
                    <h1>Opportunities</h1>
                </div>
                <br><br>
                        <!-- vacantes abiertas div-->
                        <input type="text" id="inputSellers" hidden readonly>                
                    <div class="col-md-12 float-left">
                        <!-- vendedores monto x venta div-->
                        <div class="col-md-6 float-left">
                            <div class="col-md-3 float-left p-0">
                                <input class="form-control bg-white datepicker" type="text" id="fromDateSellers" placeholder="Desde" readonly>                  
                            </div>
                            <div class="col-md-3 float-left p-0">
                                <input class="form-control bg-white datepicker" type="text" id="toDateSellers" placeholder="Hasta" readonly>                    
                            </div>
                            <div class="col-md-3 float-left p-0">
                                <select class="form-control bg-white" name="status" id="status">
                                    <option value="0">Selecciona...</option>
                                    <option value="1">Abierta</option>
                                    <option value="2">Cerrada</option>
                                    <option value="3">Cerrada con contratación</option>
                                    <option value="4">Cerrada sin contratación</option>
                                    <option value="5">Todos los status</option>
                                </select>                  
                            </div>
                            <div class="col-md-3 float-left p-0">
                                <button class="form-control btn btn-primary" onclick="drawChartSellers()">Consultar</button>
                            </div>
                                
                            <div class="col-md-12 float-left p-0" id="sellers"></div>
                        </div>               
                    </div>
            </div>
        </div>
    </div>
</body>
<link rel="stylesheet" href="{{ asset('css/charts.css')}}">
<script type="text/javascript" src="{{ asset('js/charts.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/initial-chart.js') }}"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/html2canvas@1.0.0-rc.5/dist/html2canvas.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jspdf@1.5.3/dist/jspdf.min.js"></script>
@endsection
