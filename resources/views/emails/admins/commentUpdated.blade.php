<div style="padding:50px;">
    <table width="480" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
            	<p>Se ha actualizado los comentarios de la vacante {{$vacanteName}} perteciente a la empresa {{$empresaName}}.</p>
                <p>&nbsp;</p>
                <p><strong>Comentario:</strong></p>
                <p>{{$comments}}</p>
                <p>&nbsp;</p>
                <p>Puedes revisar o editar los comentarios a través de la siguiente URL: <a href="https://levutalent.com/dashboard/vacantes/view/{{$vacanteID}}#tabCandidatos">https://levutalent.com/dashboard/vacantes/view/{{$vacanteID}}#tabCandidatos</a>.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
