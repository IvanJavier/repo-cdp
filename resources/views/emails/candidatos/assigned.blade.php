<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Te hemos asignado a la vacante <strong>{{$vacanteTitulo}}</strong>. Puedes acceder a través de la siguiente URL: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a>.</p>
               <p>Gracias por formar parte de Levu Talent Hunters.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>You have been aasigned to the the next job vacancy: <strong>{{$vacanteTitulo}}</strong>. You can log in our Levu Dashboard from: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a>.</p>
               <p>Thanks in advance.</p>
               <p>The Levu Talent Hunters team.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>