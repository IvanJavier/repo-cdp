<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Te damos la bienvenida nuestra plataforma de reclutamiento. Puedes acceder a través de la siguiente URL: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
               <p>Tu datos de acceso son lo siguientes:</p>
               <p>&nbsp;</p>
               <p><strong>Correo electrónico:</strong> {{$candidatoEmail}}</p>
               <p><strong>Contraseña:</strong> {{$candidatoPwd}}</p>
               <p>&nbsp;</p>
               <p>Te recomendamos cambiar tu contraseña una vez que accedas al portal. Recuerda completar tu perfil para poder ser elegible a nuestras vacantes.</p>
               <p><strong>Adjunto en este mensaje, encontrarás las instrucciones para actualizar tu perfil en nuestra plataforma.</strong></p>
               <p>Gracias por formar parte de Levu Talent Hunters.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>Welcome to our Levu Dashboard, in here you may find greater details about the job vacancy we previously talked about. You can log in using the next credentials:</p>
               <p>Go to: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
               <p>&nbsp;</p>
               <p><strong>Username:</strong> {{$candidatoEmail}}</p>
               <p><strong>Password:</strong> {{$candidatoPwd}}</p>
               <p>&nbsp;</p>
               <p>We recommend you to change your password once you log in. Please find attached to this e-mail the instructions to complete your profile.</p>
               <p><strong>Thanks in advance.</strong></p>
               <p><strong>The Levu Talent Hunters team</strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>