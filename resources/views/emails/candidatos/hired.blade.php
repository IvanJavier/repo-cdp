<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>¡FELICIDADES!</p>
               <p>Has sido seleccionado para ocupar la vacante <strong>{{$vacanteTitulo}}</strong>.</p>
               <p>Gracias por formar parte de Levu Talent Hunters.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>¡CONGRATULATION!</p>
               <p>You have been selected for the <strong>{{$vacanteTitulo}}</strong> position.</p>
               <p>It was a pleasure meeting you, we wish you the best luck for this new opportunity in your career.</p>
               <p>Thank you for being part of our history</p>
               <p>The Levu Talent Hunters team</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
