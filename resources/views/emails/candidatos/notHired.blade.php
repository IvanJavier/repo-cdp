<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <h1>{{$candidatoName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Te compartimos que el proceso de selección a la vacante <strong>{{$vacanteTitulo}}</strong> en el que participaste ha sido cerrado. Agradecemos tu compromiso y seguimiento durante cada etapa en el mismo. Derivado de haber conocido tu perfil y experiencia, si estás de acuerdo, te mantendremos en nuestro Banco de Talento para futuras oportunidades y que puedas participar, previa autorización tuya.</p>
               <p>Nos encantó conocerte y seguiremos en contacto.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>Thank you very much for taking the time to interview with us for the <strong>{{$vacanteTitulo}}</strong> position We appreciate your interest in the process.</p>
               <p>We are writing to let you know that we have selected the candidate whom we believe most closely matches the job requirements of the position.</p>
               <p>We do appreciate you taking the time to interview with us and encourage you to listen for other openings in the future.</p>
                <p>The Levu Talent Hunters team</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>