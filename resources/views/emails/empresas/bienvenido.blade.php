<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$contactoName}}</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <h1>{{$contactoName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Se ha dado de alta la empresa <strong>{{$empresaName}}</strong> en nuestra plataforma de reclutamiento.</p>
               <p>Los datos de acceso para la empresa son lo siguientes:</p>
               <p>&nbsp;</p>
               <p><strong>Correo electrónico:</strong> {{$empresaEmail}}</p>
               <p><strong>Contraseña:</strong> {{$empresaPwd}}</p>
               <p>&nbsp;</p>
               <p>Por favor envía un correo de notificación al contacto de la empresa.</p>
               <p>Puedes acceder a través de la siguiente liga: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>The <strong>{{$empresaName}}</strong> company has been registered in our recruitment platform.</p>
               <p>You can log in using the next credentials:</p>
               <p>&nbsp;</p>
               <p><strong>Email:</strong> {{$empresaEmail}}</p>
               <p><strong>Password:</strong> {{$empresaPwd}}</p>
               <p>&nbsp;</p>
               <p>Please send a notification email to the company contact.</p>
               <p>You can access through the following link: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
