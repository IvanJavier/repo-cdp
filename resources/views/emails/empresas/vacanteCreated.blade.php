<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$contactoName}}</h1>
                <hr>
            </td>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HI</h1>
                <h1>{{$contactoName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Hemos dado de alta la vacante <strong>{{$vacanteName}}</strong> para la empresa <strong>{{$empresaName}}</strong>. Puedes acceder a través de la siguiente URL: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a> con tus datos de acceso y revisar el estatus de tu vacante.</p>
               <p>Gracias por formar parte de Levu Talent Hunters.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>We have created the job vacancy <strong>{{$vacanteName}}</strong> in our Levu Dashboard for the company <strong>{{$empresaName}}</strong>. You can log in using your credentials from URL: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a> with your access data and check the status of your vacancy.</p>
               <p>Thanks in advance</p>
               <p>The Levu Talent Hunters team.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
