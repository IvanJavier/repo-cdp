<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>La vacante <strong>{{$vacanteName}}</strong> ha sido dada de baja de tu perfil, ya no será posible continuar con el proceso de contratación a través de esta cuenta. Puedes seguir el proceso de tus demás vacantes en esta liga: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
               <p>Saludos de parte de Levu Talent Hunters.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>The <strong>{{$vacanteName}}</strong> vacancy has been removed from your profile, it will no longer be possible to continue with the hiring process through this account. You can follow the process of your other vacancies in this URL: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
               <p>Thanks in advance.</p>
               <p>The Levu Talent Hunters team</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
