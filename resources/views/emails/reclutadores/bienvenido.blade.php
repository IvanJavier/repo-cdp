<div style="padding:50px;">
    <table width="480" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$recluName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Te hemos dado de alta en la plataforma de reclutamiento. Puedes empezar a utilizarla en esta liga: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a></p>
               <p>Los datos de acceso son lo siguientes:</p>
               <p>&nbsp;</p>
               <p><strong>Correo electrónico:</strong> {{$recluEmail}}</p>
               <p><strong>Contraseña:</strong> {{$recluPwd}}</p>
               <p>&nbsp;</p>
               <p>Recomendamos cambiar la contraseña una vez que se acceda al portal.</p>
               <p>Saludos de parte de Levu Talent Hunters.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
