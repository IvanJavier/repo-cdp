<div style="padding:50px;">
    <table width="480" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <h1>{{$recluName}}</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>Hemos dado de alta la vacante <strong>{{$vacanteName}}</strong> para la empresa <strong>{{$empresaName}}</strong>. Puedes acceder a través de la siguiente URL: <a href="https://levutalent.com/dashboard">https://levutalent.com/dashboard</a> con tus datos de acceso y revisar el estatus de tu vacante.</p>
               <p>Gracias por formar parte de Levu Talent Hunters.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td>
        </tr>
    </table>
</div>
