<div class="container">
    <div class="row mt-xl-5">
        <table width="800" cellpadding="5" cellspacing="0" border="0" style="margin-bottom:50px; border-bottom:1px solid #CCCCCC;">
            <tr>
                <td colspan="2" align="center">
                    <img src="{{ asset('/img/logoHeader.png') }}" height="100" class="mb-3">
                    <h1>Nuevo mensaje</h1>
                </td>
            </tr>
            <tr>
                <td width="50%" align="center" valign="top">
                   <p>Hola <strong>Admin</strong>,</p>
                   <p>Un usuario ha enviado su CV:</p>
                   <p><strong>Datos del contacto</strong></p>
                   <p><strong>Nombre:</strong> {{$contactName}}</p>
                   <p><strong>Correo:</strong> {{$contactEmail}}</p>
                </td>
            </tr>
            <tr>
                <td bgcolor="#303048" align="center" style="color:#FFFFFF">
                    <p>©Levu Talent Hunters 2015</p>
                </td>
            </tr>
        </table>
    </div>
</div>
