<div>
    <table width="100%" cellpadding="5" cellspacing="0" border="0">
        <tr>
            <td align="left">
                <img src="{{ asset('/img/logoHeader.png') }}" height="75" style="margin-bottom:50px;">
                <h1 style="color:#00AEEF; margin:0;">HOLA</h1>
                <hr>
            </td>
            <td align="left">
                <div style="margin-bottom:125px;"></div>
                <h1 style="color:#00AEEF; margin:0;">HELLO</h1>
                <hr>
            </td>
        </tr>
        <tr>
            <td width="50%" align="left" valign="top">
               <p>{{$actualizadorName}} ha actualizado el estatus del candidato <strong>{{$candidatoName}}</strong> de la vacante <strong>{{$vacanteName}}.</strong></p>
               <p>Puedes acceder a través de la siguiente URL: <a href="https://levutalent.com/dashboard/vacantes/view/{{$vacanteID}}#tabCandidatos">https://levutalent.com/dashboard/vacantes/view/{{$vacanteID}}#tabCandidatos</a> con tus datos de acceso y revisar los datos del candidato y agendar una cita.</p>
               <p>Gracias por formar parte de Levu Talent Hunters.</p>
            </td>
            <td width="50%" align="left" valign="top">
               <p>{{$actualizadorName}} has updated the status of candidate <strong>{{$candidatoName}}</strong> of vacancy <strong>{{$vacanteName}}.</strong></p>
               <p>You can access it through the following URL: <a href="https://levutalent.com/dashboard/vacantes/view/{{$vacanteID}}#tabCandidatos">https://levutalent.com/dashboard/vacantes/view/{{$vacanteID}}#tabCandidatos</a> with your access data and review the candidate's data and schedule an appointment.</p>
               <p>Thanks in advance.</p>
               <p>The Levu Talent Hunters team.</p>
            </td>
        </tr>
        <tr>
            <td>
                <p>&nbsp;</p>
            </td>
            <td>
                <p>&nbsp;</p>
            </td>
        </tr>
        <tr>
            <td style="color:#00AEEF">
                <p><strong>©Levu Talent Hunters 2015</strong></p>
            </td> 
        </tr>
    </table>
</div>