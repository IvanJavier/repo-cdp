<table>
	<tr>
    	<td>Folio</td>
        <td>Título</td>
        <td>Tarifa</td>
        <td>Valor anticipo</td>
        <td>Valor cierre</td>
        <td>Bono al HeadHunter</td>
        <td>Jefe de vacante (nombre)</td>
        <td>Jefe de vacante (puesto)</td>
        <td>Empresa</td>
        <td>Contacto empresa</td>
        <td>Contacto email</td>
        <td>HeadHunter</td>
    </tr>
    @foreach($vacantes as $vacante)
    	<tr>
        	<td>{{$vacante->folio}}</td>
            <td>{{$vacante->titulo}}</td>
            <td>{{$vacante->tarifa}}</td>
            <td>{{$vacante->valorAnticipo}}</td>
            <td>{{$vacante->valorCierre}}</td>
            <td>{{$vacante->bonoReclutador}}</td>
            <td>{{$vacante->jefeVacantenombre}}</td>
            <td>{{$vacante->jefeVacantePuesto}}</td>
            <td>{{$vacante->empresa}}</td>
            <td>{{$vacante->contactoName}}</td>
            <td>{{$vacante->contactoEmail}}</td>
            <td>{{$vacante->reclutador}}</td>
            <td>{{$vacante->apellido}}</td>
        </tr>
    @endforeach
</table>