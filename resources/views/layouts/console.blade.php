<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script> 
    
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> 
    <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">
       
    <script type="text/javascript" src="{{ asset('js/functions.js?'.time()) }}"></script> 
    
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css?v=2.5') }}">
    
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    <!-- VIDEO RECORDER -->
    <link href="https://vjs.zencdn.net/7.0.5/video-js.min.css" rel="stylesheet">
    <link href="{{ asset('css/videojs.record.css') }}" rel="stylesheet">
    
    <script src="https://vjs.zencdn.net/7.0.5/video.min.js"></script>
    <script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
    <script src="https://webrtc.github.io/adapter/adapter-latest.js"></script>
    
    <script src="{{ asset('js/videojs.record.js') }}"></script>
    
    <style>
		.video-js{
			display:none;
		}
	</style>
</head>
<body class="bg-white">
    <header class="bg-levuGris">
        <nav class="bg-levuGris pr-4 pl-4 fixed">
            <a class="" href="{{ url('/') }}">
                <img src="{{ asset('img/logoWhiteBlue.png') }}" height="50">
            </a>

            <div class="pull-right" id="navbarSupportedContent">
                    <!-- Authentication Links -->
                    @guest
                        <div class="btn btn-light bg-transparent color-white pull-right"><a href="{{ route('login') }}">{{ __('Login') }}</a></div>
                    @else
                        <div class="pull-right p-3">
                            <a class="btn btn-light bg-transparent color-white" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </div>
                        <div class="pull-right p-3 color-white" style="padding-top: 18px !important;">
                        	{{-- @if( $user_data[0]->profilePic != NULL)
                        		<div style="float:left; width:30px; height:30px; border-radius:50%; background:url({{ asset('storage').'/'. session()->get('folder') .'/'.session()->get('profilePic') }}) no-repeat center; background-size:cover;"></div>
                            @else
                            	<div style="float:left; width:30px; height:30px; border-radius:50%; background:url({{ asset('img/no-img.jpg') }}) no-repeat center; background-size:cover;"></div>
                            @endif --}}
                                Hola {{ Auth::user()->name }}
                        </div>
                    @endguest
            </div>
            <div class="clearfix"></div>
        </nav>
    </header>
    <aside class="float-left bg-levuAzul pl-4 fixed">
        @if(Auth::user()->hasRole('SuperAdmin'))
    
            @include('partials/menus/superAdmin')
    
        @elseif(Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Vendedor'))
            
            @include('partials/menus/admin')
        
        @elseif(Auth::user()->hasRole('Reclutador'))
           
            @include('partials/menus/reclutador')
        
        @elseif(Auth::user()->hasRole('Candidato'))
           
            @include('partials/menus/candidato')
        
        @elseif(Auth::user()->hasRole('Empresa'))
           
            @include('partials/menus/empresa')
        
        @endif
    </aside>
    <main class="console">
        @yield('content')
    </main>
    @include('partials.modal')
    <div id="mainOverlay" class="middle middle-middle">
    	<img src="{{ asset('img/spinner.gif') }}" height="40">
    </div>
    <script>
		$( ".datepicker" ).datepicker({
			dateFormat: "yy-mm-dd"
		});
	</script>
</body>
</html>
