<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')</title>

    <!-- Scripts -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>
    <script type="text/javascript" src="{{ asset('js/functions.js?v=2.0') }}"></script> 
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/styles.css?v=2.4') }}">
</head>
<body class="bg-whiteGrey">
    <header>
        <nav class="bg-whiteGrey p-2">
            <a class="float-left" href="{{ url('/') }}">
                <img src="{{ asset('img/logoHeader.png') }}" height="75" id="logo">
            </a>    
            <button class="btn bg-transparent float-right font-16 relative" id="menu-bars" onClick="$('#main-menu').slideToggle();"><i class="fas fa-bars"></i></button>
            <ul class="nav navbar navbar-right pt-4 mb-0 pr-0 mr-0" id="main-menu">
                <li class="pr-4 pl-4"><a class="go-to" name="home" href="#home">Home</a></li>
                <li class="pr-4 pl-4"><a class="go-to" name="about-us" href="#about-us">About us</a></li>
                <li class="pr-4 pl-4"><a class="go-to" name="services" href="#services">Services</a></li>
                {{-- <li class="pr-4 pl-4"><a href="/blog">Blog & news</a></li> --}}
                <li class="pr-4 pl-4"><a class="go-to" name="contact-us" href="#contact-us">Contact us</a></li>
                <li class="pr-4 pl-4 mobileHide"><a href="#" id="aLogin">Login</a></li>
                <li class="pr-4 pl-4 mobileShow"><a href="{{ url('/login') }}">Login</a></li>
            </ul>
            <div class="clearfix"></div>        
        </nav>
        @include('partials.site.login')
    </header>
    <main>
        <div id="side-line" class="bg-transparent relative">
            <div id="side-line-inner" class="col-md-6 p0">
                <div id="welcome-side-bullet" class="side-bullet color-levuAzul"></div>
                <div id="our-mision-side-bullet" class="side-bullet color-levuAzul">
                    <span class="text-uppercase font-12"><strong>about&nbsp;us</strong></span>
                </div>
                <div id="services-side-bullet" class="side-bullet color-levuAzul">
                    <span class="text-uppercase font-12"><strong>services</strong></span>
                </div>
                <div id="contact-us-side-bullet" class="side-bullet color-white">
                    <span class="text-uppercase font-12"><strong>contact&nbsp;us</strong></span>
                </div>
            </div>
        </div>
        @yield('content')
    </main>
    <footer class="pb-3">
        <div class="container">
            <div class="">
                <div class="col-md-10 col-md-offset-1">
                    <div class="">
                        <div class="col-md-6 text-left">
                            <p class="m-0"><a href="mailto:jobs@levutalent.com" class="color-levuAzul font-20"><strong>jobs@levutalent.com</strong></a></p>
                            <p class="m-0"><small>© Levu Talent Hunters 2015 • Todos los derechos reservados</small></p>
                            <p class="m-0"><small><a href="/aviso-de-privacidad">Privacy Policy</a></small></p>
                        </div>
                        <div class="col-md-6 text-right">
                            <a href="/"><img src="{{ asset('img/logoHeader.png') }}" height="50"></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div> 
        </div>
        <div class="clearfix"></div> 
    </footer>
</body>
</html>
