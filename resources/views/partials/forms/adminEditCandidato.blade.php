<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>

@if($user_data->profilePic != NULL)
    <div class="form-group row">
        <label for="" class="col-md-4 col-form-label text-md-right">Foto de perfil</label>

        <div class="col-md-6">
            <img src="{{ asset('storage').'/'.$user_data->folder.'/'.$user_data->profilePic }}" width="100">
        </div>
    </div>
@endif
<div class="form-group row">
    <label for="profilePic" class="col-md-4 col-form-label text-md-right">¿Actualizar foto de perfil?</label>

    <div class="col-md-6">
        <input id="profilePic" type="file" name="profilePic">
        <input id="profilePicOld" type="hidden" name="profilePicOld" value="{{$user_data->profilePic}}">
    </div>
</div>

<div class="form-group row">
    <label for="puesto" class="col-md-4 col-form-label text-md-right">*Puesto</label>

    <div class="col-md-6">
        <input id="puesto" value="{{$user_data->puesto}}" type="text" class="form-control" name="puesto" required>
    </div>
</div>
<div class="form-group row">
    <label for="sector" class="col-md-4 col-form-label text-md-right">*Sector</label>

    <div class="col-md-6">
        <select class="form-control" name="idSector">
            <option value="0" disabled selected>Selecciona un sector</option>
            @foreach($sectores as $sector)
                <option value="{{$sector->id}}" @if($sector->id == $user_data->idSector) selected="selected" @endif>{{$sector->nombre}}</option>
            @endforeach
            <option value="newSector">Otro</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="escolaridad" class="col-md-4 col-form-label text-md-right">*Escolaridad</label>

    <div class="col-md-6">
        <select id="escolaridad" class="form-control" name="escolaridad">
            <option value="Preparatoria" @if($user_data->escolaridad == 'Preparatoria') selected="selected"  @endif>Preparatoria</option>
            <option value="Licenciatura" @if($user_data->escolaridad == 'Licenciatura') selected="selected"  @endif>Licenciatura</option>
            <option value="Maestría" @if($user_data->escolaridad == 'Maestría') selected="selected"  @endif>Maestría</option>
            <option value="Doctorado" @if($user_data->escolaridad == 'Doctorado') selected="selected" @endif>Doctorado</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="rangoSueldo" class="col-md-4 col-form-label text-md-right">*Rango de sueldo (desde - hasta)</label>

    <div class="col-md-2">
        <input id="sueldoDesde" value="{{$user_data->sueldoDesde}}" type="number" class="form-control" name="sueldoDesde" placeholder="Desde" onChange="checkSalaryRange();" required>
    </div>
    <div class="col-md-2">
        <input id="sueldoHasta" value="{{$user_data->sueldoHasta}}" type="number" class="form-control" name="sueldoHasta" placeholder="Hasta" onChange="checkSalaryRange();" required>
    </div>
    <div class="col-md-2">
        <select id="divisa" name="divisa" class="form-control float-left">
            <option value="MXN" @if($user_data->divisa == 'MXN') selected="selected" @endif>Pesos (MXN)</option>
            <option value="USD" @if($user_data->divisa == 'USD') selected="selected" @endif>Dolares (USD)</option>
            <option value="EUR" @if($user_data->divisa == 'EUR') selected="selected" @endif>Euros (EUR)</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Ubicación</label>

    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); loadSepomex();" @if($user_data->pais == 'México' || $user_data->pais == NULL || $user_data->pais == NULL) checked="checked" @endif> México</label>
    </div>
    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); loadSepomex();" @if($user_data->pais != 'México' && $user_data->pais != NULL) checked="checked" @endif> Fuera de México</label>
    </div>
</div>
<div class="form-group row">
    <label for="calleNumero" class="col-md-4 col-form-label text-md-right">Calle y número</label>

    <div class="col-md-6">
        <input id="calleNumero" value="{{$user_data->calleNumero}}" type="text" class="form-control" name="calleNumero">
    </div>
</div>
<div class="form-group row">
    <label for="cp" class="col-md-4 col-form-label text-md-right">CP</label>

    <div class="col-md-6">
        <input id="cp" value="{{$user_data->cp}}" type="text" class="form-control" name="cp" onchange="loadSepomex();">
    </div>
</div>
<div class="form-group row">
    <label for="col" class="col-md-4 col-form-label text-md-right">Colonia</label>

    <div class="col-md-6" id="sepomexCol">
        <input id="col" value="{{$user_data->col}}" type="text" class="form-control dirData" name="col" readonly>
    </div>
</div>

<div class="form-group row">
    <label for="delmpo" class="col-md-4 col-form-label text-md-right">Delegación / Municipio</label>

    <div class="col-md-6">
        <input id="delmpo" value="{{$user_data->delmpo}}" type="text" class="form-control dirData" name="delmpo" readonly>
    </div>
</div>
<div class="form-group row">
    <label for="edo" class="col-md-4 col-form-label text-md-right">Estado / Región</label>

    <div class="col-md-6">
        <input id="edo" value="{{$user_data->edo}}" type="text" class="form-control dirData" name="edo" readonly>
    </div>
</div>
<div class="form-group row">
    <label for="pais" class="col-md-4 col-form-label text-md-right">País</label>

    <div class="col-md-6">
        <input id="pais" value="{{$user_data->pais}}" type="text" class="form-control dirData" name="pais" readonly>
    </div>
</div>