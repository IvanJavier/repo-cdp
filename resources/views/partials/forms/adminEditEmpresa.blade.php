@if($user_data->profilePic != NULL)
    <div class="form-group row">
        <label for="" class="col-md-4 col-form-label text-md-right">Logotipo</label>

        <div class="col-md-6">
            <img src="{{ asset('storage').'/'.$user_data->folder.'/'.$user_data->profilePic }}" width="100">
        </div>
    </div>
@endif
<div class="form-group row">
    <label for="profilePic" class="col-md-4 col-form-label text-md-right">¿Actualizar logotipo?</label>

    <div class="col-md-6">
        <input id="profilePic" type="file" name="profilePic">
        <input id="profilePicOld" type="hidden" name="profilePicOld" value="{{$user_data->profilePic}}">
    </div>
</div>
@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Vendedor'))
    @if($user_data->contract != NULL)
        <div class="form-group row">
            <label for="contract" class="col-md-4 col-form-label text-md-right">Contrato</label>

            <div class="col-md-6">
                <a href="{{ asset('storage').'/'.$user_data->folder.'/'.$user_data->contract }}" target="_blank">Ver contrato</a>
            </div>
        </div>
    @endif
    <div class="form-group row">
        <label for="contract" class="col-md-4 col-form-label text-md-right">¿Actualizar contrato?</label>

        <div class="col-md-6">
            <input id="contract" type="file" name="contract">
            <input id="contractOld" type="hidden" name="contractOld" value="{{$user_data->contract}}">
        </div>
    </div>
@else
    <input id="contractOld" type="hidden" name="contractOld" value="{{$user_data->contract}}">
@endif

<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>

<div class="form-group row">
    <label for="contactoName" class="col-md-4 col-form-label text-md-right">*Nombre del contacto</label>

    <div class="col-md-6">
        <input value="{{$user_data->contactoName}}" id="contactoName" type="text" class="form-control" name="contactoName" required>
    </div>
</div>
<div class="form-group row">
    <label for="contactoEmail" class="col-md-4 col-form-label text-md-right">*Email del contacto</label>

    <div class="col-md-6">
        <input value="{{$user_data->contactoEmail}}" id="contactoEmail" type="text" class="form-control" name="contactoEmail" required>
    </div>
</div>
<div class="form-group row">
    <label for="contactoPuesto" class="col-md-4 col-form-label text-md-right">*Puesto del contacto</label>

    <div class="col-md-6">
        <input value="{{$user_data->contactoPuesto}}" id="contactoPuesto" type="text" class="form-control" name="contactoPuesto" required>
    </div>
</div>
<div class="form-group row">
    <label for="contactoPhone" class="col-md-4 col-form-label text-md-right">*Teléfono del contacto</label>

    <div class="col-md-6">
        <input value="{{$user_data->contactoPhone}}" id="contactoPhone" type="text" class="form-control" name="contactoPhone" required>
    </div>
</div>
<div class="form-group row">
    <label for="giroEmpresa" class="col-md-4 col-form-label text-md-right">*Sector de la empresa</label>

    <div class="col-md-6">
    	<select class="form-control" name="idSector">
        	<option value="0" disabled selected>Selecciona un sector</option>
            @foreach($sectores as $sector)
                <option value="{{$sector->id}}" @if($sector->id == $user_data->idSector) selected="selected" @endif>{{$sector->nombre}}</option>
            @endforeach
        	<option value="newSector">Otro</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Ubicación</label>

    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); loadSepomex();" @if($user_data->pais == 'México' || $user_data->pais == NULL) checked="checked" @endif> México</label>
    </div>
    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); loadSepomex();" @if($user_data->pais != 'México' && $user_data->pais != NULL) checked="checked" @endif> Fuera de México</label>
    </div>
</div>
<div class="form-group row">
    <label for="calleNumero" class="col-md-4 col-form-label text-md-right">Calle y número</label>

    <div class="col-md-6">
        <input value="{{$user_data->calleNumero}}" id="calleNumero" type="text" class="form-control" name="calleNumero">
    </div>
</div>
<div class="form-group row">
    <label for="cp" class="col-md-4 col-form-label text-md-right">CP</label>

    <div class="col-md-6">
        <input value="{{$user_data->cp}}" id="cp" type="text" class="form-control" name="cp" onchange="loadSepomex();">
    </div>
</div>
<div class="form-group row">
    <label for="col" class="col-md-4 col-form-label text-md-right">Colonia</label>

    <div class="col-md-6" id="sepomexCol">
        <input value="{{$user_data->col}}" id="col" type="text" class="form-control dirData" name="col" @if($user_data->pais == 'México' || $user_data->pais == NULL) readonly @endif>
    </div>
</div>

<div class="form-group row">
    <label for="delmpo" class="col-md-4 col-form-label text-md-right">Delegación / Municipio</label>

    <div class="col-md-6">
        <input value="{{$user_data->delmpo}}" id="delmpo" type="text" class="form-control dirData" name="delmpo"  @if($user_data->pais == 'México' || $user_data->pais == NULL) readonly @endif>
    </div>
</div>
<div class="form-group row">
    <label for="edo" class="col-md-4 col-form-label text-md-right">Estado / Región</label>

    <div class="col-md-6">
        <input value="{{$user_data->edo}}" id="edo" type="text" class="form-control dirData" name="edo"  @if($user_data->pais == 'México' || $user_data->pais == NULL) readonly @endif>
    </div>
</div>
<div class="form-group row">
    <label for="pais" class="col-md-4 col-form-label text-md-right">País</label>

    <div class="col-md-6">
        <input value="{{$user_data->pais}}" id="pais" type="text" class="form-control dirData" name="pais"  @if($user_data->pais == 'México' || $user_data->pais == NULL) readonly @endif>
    </div>
</div>