<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>
@if($user_data->profilePic != NULL)
    <div class="form-group row">
        <label for="" class="col-md-4 col-form-label text-md-right">Foto de perfil</label>

        <div class="col-md-6">
            <img src="{{ asset('storage').'/'.$user_data->folder.'/'.$user_data->profilePic }}" width="100">
        </div>
    </div>
@endif
<div class="form-group row">
    <label for="profilePic" class="col-md-4 col-form-label text-md-right">¿Actualizar foto de perfil?</label>

    <div class="col-md-6">
        <input id="profilePic" type="file" name="profilePic">
        <input id="profilePicOld" type="hidden" name="profilePicOld" value="{{$user_data->profilePic}}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Ubicación</label>

    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); loadSepomex();" @if($user_data->pais == 'México' || $user_data->pais == NULL) checked="checked" @endif> México</label>
    </div>
    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); loadSepomex();" @if($user_data->pais != 'México' && $user_data->pais != NULL) checked="checked" @endif> Fuera de México</label>
    </div>
</div>
<div class="form-group row">
    <label for="calleNumero" class="col-md-4 col-form-label text-md-right">Calle y número</label>

    <div class="col-md-6">
        <input id="calleNumero" type="text" class="form-control" name="calleNumero" value="{{$user_data->calleNumero}}">
    </div>
</div>
<div class="form-group row">
    <label for="cp" class="col-md-4 col-form-label text-md-right">CP</label>

    <div class="col-md-6">
        <input id="cp" type="text" class="form-control" name="cp" onchange="loadSepomex();" value="{{$user_data->cp}}">
    </div>
</div>
<div class="form-group row">
    <label for="col" class="col-md-4 col-form-label text-md-right">Colonia</label>

    <div class="col-md-6" id="sepomexCol">
        <input id="col" type="text" class="form-control dirData" name="col" readonly value="{{$user_data->col}}">
    </div>
</div>

<div class="form-group row">
    <label for="delmpo" class="col-md-4 col-form-label text-md-right">Delegación / Municipio</label>

    <div class="col-md-6">
        <input id="delmpo" type="text" class="form-control dirData" name="delmpo" readonly value="{{$user_data->delmpo}}">
    </div>
</div>
<div class="form-group row">
    <label for="edo" class="col-md-4 col-form-label text-md-right">Estado / Región</label>

    <div class="col-md-6">
        <input id="edo" type="text" class="form-control dirData" name="edo" readonly value="{{$user_data->edo}}">
    </div>
</div>
<div class="form-group row">
    <label for="pais" class="col-md-4 col-form-label text-md-right">'País</label>

    <div class="col-md-6">
        <input id="pais" type="text" class="form-control dirData" name="pais" readonly value="{{$user_data->pais}}">
    </div>
</div>