<div class="form-group row">
    <label for="profilePic" class="col-md-4 col-form-label text-md-right">Logotipo</label>

    <div class="col-md-6">
        <input id="profilePic" type="file" name="profilePic">
    </div>
</div>
@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Vendedor'))
    <div class="form-group row">
        <label for="contract" class="col-md-4 col-form-label text-md-right">Contrato</label>

        <div class="col-md-6">
            <input id="contract" type="file" name="contract">
        </div>
    </div>
@else
    <input id="contract" type="hidden" name="contract">
@endif

<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>

<div class="form-group row">
    <label for="contactoName" class="col-md-4 col-form-label text-md-right">*Nombre del contacto</label>

    <div class="col-md-6">
        <input id="contactoName" type="text" class="form-control" name="contactoName" required>
    </div>
</div>
<div class="form-group row">
    <label for="contactoEmail" class="col-md-4 col-form-label text-md-right">*Email del contacto</label>

    <div class="col-md-6">
        <input id="contactoEmail" type="text" class="form-control" name="contactoEmail" required>
    </div>
</div>
<div class="form-group row">
    <label for="contactoPuesto" class="col-md-4 col-form-label text-md-right">*Puesto del contacto</label>

    <div class="col-md-6">
        <input id="contactoPuesto" type="text" class="form-control" name="contactoPuesto" required>
    </div>
</div>
<div class="form-group row">
    <label for="contactoPhone" class="col-md-4 col-form-label text-md-right">*Teléfono del contacto</label>

    <div class="col-md-6">
        <input id="contactoPhone" type="text" class="form-control" name="contactoPhone" required>
    </div>
</div>
<div class="form-group row">
    <label for="giroEmpresa" class="col-md-4 col-form-label text-md-right">*Sector de la empresa</label>

    <div class="col-md-6">
        <select class="form-control" name="idSector">
            @foreach($sectores as $sector)
                <option value="{{$sector->id}}">{{$sector->nombre}}</option>
            @endforeach
        	<option value="newSector">Otro</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Ubicación</label>

    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); loadSepomex();" checked> México</label>
    </div>
    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); loadSepomex();"> Fuera de México</label>
    </div>
</div>
<div class="form-group row">
    <label for="calleNumero" class="col-md-4 col-form-label text-md-right">Calle y número</label>

    <div class="col-md-6">
        <input id="calleNumero" type="text" class="form-control" name="calleNumero">
    </div>
</div>
<div class="form-group row">
    <label for="cp" class="col-md-4 col-form-label text-md-right">CP</label>

    <div class="col-md-6">
        <input id="cp" type="text" class="form-control" name="cp" onchange="loadSepomex();">
    </div>
</div>
<div class="form-group row">
    <label for="col" class="col-md-4 col-form-label text-md-right">Colonia</label>

    <div class="col-md-6" id="sepomexCol">
        <input id="col" type="text" class="form-control dirData" name="col" readonly>
    </div>
</div>

<div class="form-group row">
    <label for="delmpo" class="col-md-4 col-form-label text-md-right">Delegación / Municipio</label>

    <div class="col-md-6">
        <input id="delmpo" type="text" class="form-control dirData" name="delmpo" readonly>
    </div>
</div>
<div class="form-group row">
    <label for="edo" class="col-md-4 col-form-label text-md-right">Estado / Región</label>

    <div class="col-md-6">
        <input id="edo" type="text" class="form-control dirData" name="edo" readonly>
    </div>
</div>
<div class="form-group row">
    <label for="pais" class="col-md-4 col-form-label text-md-right">País</label>

    <div class="col-md-6">
        <input id="pais" type="text" class="form-control dirData" name="pais" readonly>
    </div>
</div>