<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>
<div class="form-group row">
    <label class="col-md-4 col-form-label text-md-right">Ubicación</label>

    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion0" type="radio" name="ubicacion" value="local" onclick="$('.dirData').attr('readonly','readonly'); loadSepomex();" checked> México</label>
    </div>
    <div class="col-md-3">
        <label class="pointer"><input id="ubicacion1" type="radio" name="ubicacion" value="extranjero" onclick="$('.dirData').removeAttr('readonly'); loadSepomex();"> Fuera de México</label>
    </div>
</div>
<div class="form-group row">
    <label for="calleNumero" class="col-md-4 col-form-label text-md-right">Calle y número</label>

    <div class="col-md-6">
        <input id="calleNumero" type="text" class="form-control" name="calleNumero" value="@php if(isset($_GET['calleNumero'])){echo $_GET['calleNumero'];} @endphp">
    </div>
</div>
<div class="form-group row">
    <label for="cp" class="col-md-4 col-form-label text-md-right">CP</label>

    <div class="col-md-6">
        <input id="cp" type="text" class="form-control" name="cp" onchange="loadSepomex();" value="@php if(isset($_GET['cp'])){echo $_GET['cp'];} @endphp">
    </div>
</div>
<div class="form-group row">
    <label for="col" class="col-md-4 col-form-label text-md-right">Colonia</label>

    <div class="col-md-6" id="sepomexCol">
        <input id="col" type="text" class="form-control dirData" name="col" readonly value="@php if(isset($_GET['col'])){echo $_GET['col'];} @endphp">
    </div>
</div>

<div class="form-group row">
    <label for="delmpo" class="col-md-4 col-form-label text-md-right">Delegación / Municipio</label>

    <div class="col-md-6">
        <input id="delmpo" type="text" class="form-control dirData" name="delmpo" readonly value="@php if(isset($_GET['delmpo'])){echo $_GET['delmpo'];} @endphp">
    </div>
</div>
<div class="form-group row">
    <label for="edo" class="col-md-4 col-form-label text-md-right">Estado / Región</label>

    <div class="col-md-6">
        <input id="edo" type="text" class="form-control dirData" name="edo" readonly value="@php if(isset($_GET['edo'])){echo $_GET['edo'];} @endphp">
    </div>
</div>
<div class="form-group row">
    <label for="pais" class="col-md-4 col-form-label text-md-right">País</label>

    <div class="col-md-6">
        <input id="pais" type="text" class="form-control dirData" name="pais" readonly value="@php if(isset($_GET['pais'])){echo $_GET['pais'];} @endphp">
    </div>
</div>