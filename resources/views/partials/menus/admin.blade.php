    <ul class="navbar-nav">
        <li class="p-2"><strong><a href="{{ url('/dashboard/') }}"><i class="fas fa-home mr-3 font-20"></i> Home</a></strong></li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#reclutadoresMenu').slideToggle();"><i class="fas fa-user-tie mr-3 font-20"></i> HeadHunters <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="reclutadoresMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Reclutador') }}">Ver todos</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Reclutador') }}">Nuevo HeadHunter</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#candidatosMenu').slideToggle();"><i class="fas fa-user mr-3 font-20"></i> Candidatos <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="candidatosMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Candidato') }}">Ver todos</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Candidato') }}">Nuevo candidato</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#empresasMenu').slideToggle();"><i class="far fa-building mr-3 font-20"></i> Empresas <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="empresasMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Empresa') }}">Ver todas</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Empresa') }}">Nueva empresa</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#vacantesMenu').slideToggle();"><i class="fas fa-clipboard-list mr-3 font-20"></i> Vacantes <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="vacantesMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/vacantes') }}">Ver todas</a></li>
                <li><a href="{{ url('/dashboard/vacantes/create') }}">Nueva vacante</a></li>
            </ul>
        </li>
        @if(Auth::user()->hasRole('Vendedor'))
                <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#analyticsMenu').slideToggle();"><i class="fas fa-clipboard-list mr-3 font-20"></i> Analytics <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="analyticsMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/admin/opportunities') }}">Opportunities</a></li>
            </ul>
        </li>
        @endif
    </ul>