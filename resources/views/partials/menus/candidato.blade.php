    <ul class="navbar-nav">
        <li class="p-2"><strong><a href="{{ url('/dashboard/') }}"><i class="fas fa-user mr-3 font-20"></i><span class="mobileHide"> Mi perfil</span></a></strong></li>
        <li class="p-2"><strong><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-power-off mr-3 font-20"></i><span class="mobileHide"> Cerrar sesión</span></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/') }}"><i class="fas fa-question-circle mr-3 font-20"></i><span class="mobileHide">¿Quién es LEVU?</span></a></strong></li>
        <li class="p-2"><strong><h6><a href="{{ url('/aviso-de-privacidad')}}" target="_blank"><i class="fas fa-user-shield mr-3 font-20"></i><span class="mobileHide">Política de privacidad</span></a></h6></strong></li>
    </ul>