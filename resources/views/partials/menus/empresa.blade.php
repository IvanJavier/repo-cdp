    <ul class="navbar-nav">
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/profile/' . Auth::user()->id) }}"><i class="far fa-building mr-3 font-20"></i><span class="mobileHide"> Mi perfil</span></a></strong></li>
        <li class="p-2"><strong><a href="{{ route('enterprise.all.vacant', Auth::user()->id) }}"><i class="fas fa-clipboard-list mr-3 font-20"></i><span class="mobileHide">  Mis vacantes</span></a></strong></li>
    </ul>