    <ul class="navbar-nav mobileHide">
        <li class="p-2"><strong><a href="{{ url('/dashboard/') }}"><i class="fas fa-home mr-3 font-20"></i> Home</a></strong></li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#candidatosMenu').slideToggle();"><i class="fas fa-user mr-3 font-20"></i> Candidatos <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="candidatosMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Candidato') }}">Candidatos</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Candidato') }}">Nuevo candidato</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/Empresa') }}" class="pointer"><i class="far fa-building mr-3"></i> Clientes</strong></li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#vacantesMenu').slideToggle();"><i class="fas fa-clipboard-list mr-3 font-20"></i> Vacantes <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="vacantesMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/vacantes') }}">Mis vacantes</a></li>
                <li><a href="{{ url('/dashboard/vacantes/create') }}">Nueva vacante</a></li>
            </ul>
        </li>
    </ul>
    <ul class="navbar-nav mobileShow">
        <li class="p-2"><strong><a href="{{ url('/dashboard/') }}"><i class="fas fa-home mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/Candidato') }}" class="pointer" onclick="$('#candidatosMenu').slideToggle();"><i class="fas fa-user mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/Empresa') }}" class="pointer"><i class="far fa-building font-20 mr-3"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/vacantes') }}" class="pointer" onclick="$('#vacantesMenu').slideToggle();"><i class="fas fa-clipboard-list mr-3 font-20"></i></a></strong></li>
    </ul>
    