    <ul class="navbar-nav mobileHide">
        <li class="p-2"><strong><a href="{{ url('/dashboard/') }}"><i class="fas fa-home mr-3 font-20"></i> Home</a></strong></li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#adminMenu').slideToggle();"><i class="fas fa-user-secret mr-3 font-20"></i> Admins <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="adminMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/registration/Admin') }}">Nuevo admin</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#reclutadoresMenu').slideToggle();"><i class="fas fa-user-tie mr-3 font-20"></i> HeadHunters <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="reclutadoresMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Reclutador') }}">Ver todos</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Reclutador') }}">Nuevo HeadHunter</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#freelanceMenu').slideToggle();"><i class="fas fa-user-friends mr-3 font-20"></i> Freelance <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="freelanceMenu" style="display:none;">
                <li><a href="{{ route('freelance.index') }}">Ver todos</a></li>
                <li><a href="{{ route('freelances.create') }}">Nuevo freelance</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#candidatosMenu').slideToggle();"><i class="fas fa-user mr-3 font-20"></i> Candidatos <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="candidatosMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Candidato') }}">Ver todos</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Candidato') }}">Nuevo candidato</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#empresasMenu').slideToggle();"><i class="far fa-building mr-3 font-20"></i> Empresas <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="empresasMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/users/Empresa') }}">Ver todas</a></li>
                <li><a href="{{ url('/dashboard/users/registration/Empresa') }}">Nueva empresa</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#vacantesMenu').slideToggle();"><i class="fas fa-clipboard-list mr-3 font-20"></i> Vacantes <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="vacantesMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/vacantes') }}">Ver todas</a></li>
                <li><a href="{{ url('/dashboard/vacantes/create') }}">Nueva vacante</a></li>
            </ul>
        </li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/sucursalesSectores') }}"><i class="fas fa-industry mr-3 font-20"></i> Sucursales y sectores</a></strong></li>
        <li class="p-2"><strong><a href="#" class="pointer" onclick="$('#analiticosMenu').slideToggle();"><i class="fas fa-chart-bar mr-3 font-20"></i> Analíticos <i class="fas fa-chevron-down font-9"></i></a></strong>
            <ul class="nav-link pl-5 mb-3" id="analiticosMenu" style="display:none;">
                <li><a href="{{ url('/dashboard/admin/charts') }}">Consulta gráficas</a></li>
            </ul>
        </li>
    </ul>
    <ul class="navbar-nav mobileShow">
        <li class="p-2"><strong><a href="{{ url('/dashboard/') }}"><i class="fas fa-home mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/registration/Admin') }}"><i class="fas fa-user-secret mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/Reclutador') }}"><i class="fas fa-user-tie mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/Candidato') }}"><i class="fas fa-user mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/users/Empresa') }}"><i class="far fa-building mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/vacantes') }}"><i class="fas fa-clipboard-list mr-3 font-20"></i></a></strong></li>
        <li class="p-2"><strong><a href="{{ url('/dashboard/admin/charts') }}"><i class="fas fa-chart-bar mr-3 font-20"></i></a></strong></li>
    </ul>