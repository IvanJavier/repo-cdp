
<div class="main-content" id="mainModal" tabindex="-1" role="dialog" aria-labelledby="mainModalLabel">
    <div class="modal-dialog modal-dialog-centered d-flex" role="document">
        <div class="modal-content align-items-center">
            <div class="modal-header text-center">
                <button type="button" class="close absolute right20 top-5 font-48" onclick="$('#mainModal').fadeOut(); $('#modal-body').html('');">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modal-title"></h4>
                <p class="modal-title" id="modal-instructions"></p>
            </div>
            <div class="modal-title">
                <button type="button" class="btn btn-default" onclick="$('#mainModal').fadeOut(); $('#modal-body').html('');">Cerrar</button>
                <span class="pull-right">
               <button type="button" class="btn btn-primary" id="modal-submit-btn"></button>
                </span>
            </div>
            <div class="modal-body" id="modal-body">
            </div>
            <div class="clearfix"></div>

        </div>
    </div>
</div>