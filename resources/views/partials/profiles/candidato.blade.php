<div class="col-md-12 text-center clearboth">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>
<div class="">
	<div class="col-md-6 pull-center">
    	<table class="table table-condensed">
        	<tr>
            	<td width="50%"><strong>Puesto</strong></td>
                <td>{{$user_data[0]->puesto}}</td>
            </tr>
            <tr>
            	<td width="50%"><strong>Sector</strong></td>
                <td>{{$user_data[0]->sector}}</td>
            </tr>
            <tr>
            	<td width="50%"><strong>Escolaridad</strong></td>
                <td>{{$user_data[0]->escolaridad}}</td>
            </tr>
            <tr>
            	<td width="50%"><strong>Rango de sueldo</strong></td>
                <td>{{$user_data[0]->sueldoDesde}} hasta {{$user_data[0]->sueldoHasta}}</td>
            </tr>
        </table>
    </div>
</div>

<div class="text-center">
    <div class="col-md-12 text-center">
        <h4>Ubicación</h4>         
    </div>                       
</div>
<div class="">
	<div class="col-md-6 pull-center">
    	<table class="table table-condensed">
        	<tr>
            	<td><strong>Calle y número</strong></td>
                <td>{{$user_data[0]->calleNumero}}</td>
            </tr>
            <tr>
            	<td><strong>CP</strong></td>
                <td>{{$user_data[0]->cp}}</td>
            </tr>
            <tr>
            	<td><strong>Colonia</strong></td>
                <td>{{$user_data[0]->col}}</td>
            </tr>
            <tr>
            	<td><strong>Delegación / Municipio</strong></td>
                <td>{{$user_data[0]->delmpo}}</td>
            </tr>
            <tr>
            	<td><strong>Estado / Región</strong></td>
                <td>{{$user_data[0]->edo}}</td>
            </tr>
            <tr>
            	<td><strong>País</strong></td>
                <td>{{$user_data[0]->pais}}</td>
            </tr>
        </table>
    </div>
</div>

<div class="col-md-12 mt-5 mb-5 clearfix">
    <div class="col-md-12 text-center">
        <table class="table table-stripped font-12">
            <tr>
                <th class="text-center" width="20%">CV</th>
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                	<th class="text-center" width="20%">LTR</th>
                @endif
                <th class="text-center" width="20%">Video</th>
                <th class="text-center" width="20%">Referencias</th>
                <th class="text-center" width="20%">Compensaciones</th>
            </tr>
            <tr>
                {{-- CV --}}
                <td class="text-center">
                    @if($user_data[0]->cv != NULL)
                        <a href="{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->cv }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                    @endif
                    <button class="btn btn-xs bg-black color-white font-14" onClick="$('#cv_file{{$user->id}}').click()"><i class="fas fa-cloud-upload-alt"></i></button>
                    <form id="cvUpload{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                        {{ csrf_field() }}
                        <input type="file" id="cv_file{{$user->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#cvUpload{{$user->id}}').submit();">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <input type="hidden" name="folder" value="{{$user_data[0]->folder}}">
                        <input type="hidden" name="fileType" value="cv">
                        <input type="hidden" name="_returnPath" value="{{Request::url()}}">
                    </form>
                </td>
                {{-- LTR --}}
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                    <td class="text-center">
                        @if($user_data[0]->ltr != NULL)
                            <a href="{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->ltr }}" class="btn btn-xs bg-black color-white mr-1 font-14" target="_blank"><i class="fas fa-eye"></i></a>
                        @endif
                        <button class="btn btn-xs bg-black color-white font-14" onClick="$('#ltr_file{{$user->id}}').click()"><i class="fas fa-cloud-upload-alt"></i></button>
                        <form id="ltrUpload{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}
                            <input type="file" id="ltr_file{{$user->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#ltrUpload{{$user->id}}').submit();">
                            <input type="hidden" name="id" value="{{$user->id}}">
                            <input type="hidden" name="folder" value="{{$user_data[0]->folder}}">
                            <input type="hidden" name="fileType" value="ltr">
                            <input type="hidden" name="_returnPath" value="{{Request::url()}}">
                        </form>
                    </td>
                @endif
                {{-- VIDEO --}}
                <td class="text-center">
                    @if($user_data[0]->video != NULL)
                        <button type="button" class="btn btn-xs bg-black color-white mr-1 font-14" data-title="Video {{$user->name}} {{$user->lastName}}" data-instructions="" data-submit-btn="Aceptar" onclick="showVideo('{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->video }}'); toggleModal(this);">
                            <i class="fas fa-eye"></i>
                        </button>
                    @endif
                    <button class="btn btn-xs bg-black color-white font-14" data-title="Video CV" data-instructions="Selecciona una opción." data-submit-btn="Aceptar" onclick="videoCVmodal('{{$user->id}}'); toggleModal(this);"><i class="fas fa-cloud-upload-alt"></i></button>
                    
                    <form id="videoRecord{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                    	{{ csrf_field() }}
                        <input type="hidden" id="uid" name="id" value="{{$user->id}}">
                        <input type="hidden" id="folder" name="folder" value="{{$user_data[0]->folder}}">
                        <input type="hidden" id="fileType" name="fileType" value="video">
                        <input type="hidden" id="returnPath" name="_returnPath" value="{{Request::url()}}">
                    </form>
                    
                    <form id="videoUpload{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                        {{ csrf_field() }}
                        <input type="file" id="video_file{{$user->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#videoUpload{{$user->id}}').submit();">
                        <input type="hidden" name="id" value="{{$user->id}}">
                        <input type="hidden" name="folder" value="{{$user_data[0]->folder}}">
                        <input type="hidden" name="fileType" value="video">
                        <input type="hidden" name="_returnPath" value="{{Request::url()}}">
                    </form>
                </td>
                {{-- REFERENCIAS --}}
                <td class="text-center">
                    <a href="{{ url('/dashboard/users/referencias/'.$user->id.'/') }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                </td>
                {{-- COMPENSACIONES --}}
                <td class="text-center">
                	@if($user_data[0]->compensaciones == 1)
                    	<a href="{{ url('/dashboard/users/compensaciones/'.$user->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                    @endif
                    <a href="{{ url('/dashboard/users/compensaciones/edit/'.$user->id) }}" class="btn btn-xs bg-black color-white font-14"><i class="fas fa-edit"></i></a>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="text-center">
    <div class="col-md-12 text-center">
        <h3>Vacantes</h3>
    </div>                       
</div>

<div class="col-md-12 mb-5 clearfix">
    <div class="col-md-12 text-center">
        <table class="table table-stripped font-12">
            <tr>
                <th class="text-left" width="20%"><strong>Título</strong></th>
                <th class="text-left" width="20%"><strong>Empresa</strong></th>
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                	<th class="text-center" width="20%"><strong>SEC FITS</strong></th>
                @endif
                <th class="text-center" width="20%"><strong>REFERENCIAS</strong></th>
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                    <th class="text-center" width="20%"><strong>Bloqueado</strong></th>
                    <th class="text-center" width="20%"><strong>Colocado</strong></th>
                @endif
            </tr>
            @foreach($vacantes as $vacante)
                <tr>
                    {{-- CV --}}
                    <td class="text-left">
                        <a target="_blank" href="{{ url('/dashboard/vacantes/view/'.$vacante->id) }}">{{$vacante->titulo}}</a>
                    </td>
                    {{-- EMPRESA --}}
                    <td class="text-left">
                    	@if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                        	<a target="_blank" href="{{ url('/dashboard/users/profile/'.$vacante->idUserEmpresa) }}">{{$vacante->empresa}}</a>
                        @else
                        	{{$vacante->empresa}}
                        @endif
                    </td>
                    {{-- SEC FITS --}}
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                        <td class="text-center">
                            @if($vacante->secFits != 0)
                                <a href="{{ url('/dashboard/vacantes/secFits/'.$user->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white mr-1 font-14"><i class="fas fa-eye"></i></a>
                            @endif
                            <a href="{{ url('/dashboard/vacantes/secFits/edit/'.$user->id.'/'.$vacante->id) }}" class="btn btn-xs bg-black color-white font-14"><i class="fas fa-edit"></i></a>
                        </td>
                    @endif
                    @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin') || Auth::user()->hasRole('Reclutador'))
                        <td class="text-center">
                            @if($vacante->colocado == '1') <i class="fas fa-check font-22 text-success"></i> @elseif($vacante->bloqueado == '1') <i class="fas fa-exclamation font-22 text-warning"></i> @endif
                        </td>
                        <td class="text-center">
                            @if($vacante->colocado == '1') <i class="fas fa-check font-22 text-success"></i> @endif
                        </td>
                    @endif
                </tr>
            @endforeach
        </table>
    </div>
</div>