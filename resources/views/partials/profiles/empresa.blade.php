<div class="form-group text-center mt-4">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>
<div class="">
	<div class="col-md-6 pull-center">
    	<table class="table table-condensed">
        	<tr>
            	<td width="50%"><strong>Nombre del contacto</strong></td>
                <td>{{$user_data[0]->contactoName}}</td>
            </tr>
            <tr>
            	<td><strong>Email del contacto</strong></td>
                <td>{{$user_data[0]->contactoEmail}}</td>
            </tr>
            <tr>
            	<td><strong>Puesto del contacto</strong></td>
                <td>{{$user_data[0]->contactoPuesto}}</td>
            </tr>
            <tr>
            	<td><strong>Teléfono del contacto</strong></td>
                <td>{{$user_data[0]->contactoPhone}}</td>
            </tr>
            <tr>
            	<td><strong>Sector de la empresa</strong></td>
                <td>{{$user_data[0]->sector}}</td>
            </tr>
        </table>
    </div>
</div>

<div class="form-group text-center">
    <div class="col-md-12 text-center">
        <h4>Ubicación</h4>         
    </div>                       
</div>
<div class="">
	<div class="col-md-6 pull-center">
    	<table class="table table-condensed">
        	<tr>
            	<td><strong>Calle y número</strong></td>
                <td>{{$user_data[0]->calleNumero}}</td>
            </tr>
            <tr>
            	<td><strong>CP</strong></td>
                <td>{{$user_data[0]->cp}}</td>
            </tr>
            <tr>
            	<td><strong>Colonia</strong></td>
                <td>{{$user_data[0]->col}}</td>
            </tr>
            <tr>
            	<td><strong>Delegación / Municipio</strong></td>
                <td>{{$user_data[0]->delmpo}}</td>
            </tr>
            <tr>
            	<td><strong>Estado / Región</strong></td>
                <td>{{$user_data[0]->edo}}</td>
            </tr>
            <tr>
            	<td><strong>País</strong></td>
                <td>{{$user_data[0]->pais}}</td>
            </tr>
        </table>
    </div>
</div>
<hr>
<div class="col-md-12" id="misVacantes">
	<h3 class="text-center">Vacantes</h3>
</div>
<div class="col-md-12">
	<div class="row pt-3 pb-3 border-bottom">
        <form action="" method="get" id="vacantesFilter" class="col-md-12">
            <div class="col-md-1 float-left"><strong>Folio</strong></div>
            <div class="col-md-2 float-left"><strong>Estatus</strong></div>
            <div class="col-md-2 float-left"><strong>Fecha</strong></div>
            <div class="col-md-2 float-left"><strong>Vacante</strong></div>
            <div class="col-md-2 float-left"><strong>HeadHunter a cargo</strong></div>
            <div class="col-md-3 float-left"></div>
        </form>
    </div>
   @foreach($vacantes as $key=>$vacante)
        <div class="row pt-3 pb-3 border-bottom @if($key%2 == 0) bg-whiteGrey @endif">
            <div class="col-md-1 float-left">{{$vacante->id}} @if(count($vacante->candidatos) > 0) <button class="btn btn-xs font-12 btn-default" onclick="$('.tr-user-{{$vacante->id}}').slideToggle();" data-toggle="tooltip" data-placement="bottom" title="Candidatos"><i class="fas fa-users"></i> <i class="fas fa-caret-down"></i></button> @endif</div>
            <div class="col-md-2 float-left @if($vacante->estatus == 'Abierta') text-success @else text-danger @endif">{{$vacante->estatus}}</div>
            <div class="col-md-2 float-left">{{$vacante->created_at}}</div>
            <div class="col-md-2 float-left">{{$vacante->titulo}}</div>
            <div class="col-md-2 float-left">{{$vacante->recluName}} {{$vacante->recluLastName}}<br>{{$vacante->recluEmail}}</div>
            <div class="col-md-2 float-left">
            	@if(Auth::user()->hasRole('Reclutador'))
                	@if( Auth::user()->id == $vacante->ownerId)
                        <a href="{{ url('/dashboard/vacantes/view/') . '/' . $vacante->id }}" class="btn bg-black color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Detalle">
                            <i class="far fa-eye"></i>
                        </a>
                    @endif
                @endif
                @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                    <a href="{{ url('/dashboard/vacantes/edit/') . '/' . $vacante->id }}" class="btn bg-black color-white btn-sm" data-toggle="tooltip" data-placement="bottom" title="Editar">
                        <i class="far fa-edit"></i>
                    </a>
                    <button class="btn bg-black color-white btn-sm" data-title="Eliminar vacante" data-instructions="¿Estás seguro/a de querer eliminar esta vacante?" data-submit-btn="Eliminar vacante" onclick="deleteRecord('{{$vacante->id}}', 'Vacante', '','form-deleteVacante','{{ csrf_token() }}', '{{Request::url()}}'); toggleModal(this);" data-toggle="tooltip" data-placement="bottom" title="Eliminar">
                        <i class="far fa-trash-alt"></i>
                    </button>
                @endif
            </div>
            @if(count($vacante->candidatos) > 0)
                @foreach($vacante->candidatos as $candidato)
                    <div class="col-md-12 pt-3 pb-3 tr-user-{{$vacante->id}}" style="display:none;">
                        <div class="col-md-2"></div>
                        <div class="col-md-3">{{$candidato->name}} {{$candidato->lastName}}</div>
                        <div class="col-md-3">{{$candidato->email}}</div>
                        @if(Auth::user()->hasRole('SuperAdmin') || Auth::user()->hasRole('Admin'))
                            <div class="col-md-2">
                                <a target="_blank" href="{{ url('/dashboard/users/profile/') }}/{{$candidato->id}}" class="btn btn-outline-success btn-sm" >
                                    <i class="far fa-eye"></i>
                                </a>
                                <a target="_blank" href="{{ url('/dashboard/users/edit/') }}/{{$candidato->id}}" class="btn btn-outline-warning btn-sm">
                                    <i class="far fa-edit"></i>
                                </a>
                            </div>
                        @endif
                        <div class="col-md-2"></div>
                    </div>
                @endforeach
            @endif
        </div>
    @endforeach
</div>

<div class="clearfix"></div>