<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Datos adicionales</h4>         
    </div>                       
</div>
<div class="clearfix">
    <div class="col-md-6 float-left text-right"><strong>Foto de perfil</strong></div>

    <div class="col-md-6 float-left">
        @if($user_data[0]->profilePic != NULL)
            <img src="{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->profilePic }}" width="100">
        @endif
    </div>
</div>

<div class="form-group row text-center">
    <div class="col-md-12 text-center">
        <h4>Ubicación</h4>         
    </div>                       
</div>
<div class="">
	<div class="col-md-6 pull-center">
    	<table class="table table-condensed">
        	<tr>
            	<td><strong>Calle y número</strong></td>
                <td>{{$user_data[0]->calleNumero}}</td>
            </tr>
            <tr>
            	<td><strong>CP</strong></td>
                <td>{{$user_data[0]->cp}}</td>
            </tr>
            <tr>
            	<td><strong>Colonia</strong></td>
                <td>{{$user_data[0]->col}}</td>
            </tr>
            <tr>
            	<td><strong>Delegación / Municipio</strong></td>
                <td>{{$user_data[0]->delmpo}}</td>
            </tr>
            <tr>
            	<td><strong>Estado / Región</strong></td>
                <td>{{$user_data[0]->edo}}</td>
            </tr>
            <tr>
            	<td><strong>País</strong></td>
                <td>{{$user_data[0]->pais}}</td>
            </tr>
        </table>
    </div>
</div>