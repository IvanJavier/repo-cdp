
<div id="siteLogin" class="box-shadow absolute right0 bg-levuAzul">
    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

		<div class="form-group color-white">
			<h1 class="mt-0"><strong>LOGIN</strong></h1>
        </div>
        
        <div class="form-group">
            <input id="email" type="email" class="form-control bg-transparent color-white" name="email" required autofocus placeholder="E-mail">
        </div>

        <div class="form-group">
            <input id="password" type="password" class="form-control bg-transparent color-white" name="password" required placeholder="Password">
        </div>
        <div class="form-group mb-0">
            <button type="submit" class="btn bg-black color-white btn-block">
                Login
            </button>
        </div>
        <div class="form-group mb-0">
            <a class="btn btn-link color-white" href="{{ route('password.request') }}">
                {{ __('¿Olvidaste tu contraseña?') }}
            </a>
        </div>
    </form>
    <div class="clearfix"></div>
</div>