@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<div class="main-content bg-whiteGrey" style="background:url({{ asset('img/candidato.png') }}) no-repeat center; background-size:cover;">
    <div class="row justify-content-center">
    	{{-- --}}
        <div class="col-md-12">
        	<div class="col-md-12 pt-5 pb-5">
                <div class="col-md-6 float-left">
                    <div class="col-md-6 float-left">
                        @if( $user_data[0]->profilePic != NULL)
                            <div class="profilePic" style="background:url({{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->profilePic }}) no-repeat center; background-size:cover;">
                            </div>
                        @else
                            <div class="profilePic" style="background:url({{ asset('img/no-img.jpg') }}) no-repeat center; background-size:cover;">
                            </div>
                        @endif
                    </div>
                	<div>
                        <h2>{{ $user->name }} {{ $user->lastName }} <a href="{{ url('/dashboard/users/edit/'.$user->id) }}" class="btn btn-primary">Actualizar mis datos</a></h2>
                        <p><a href="{{ $user->email }}" target="_blank">{{ $user->email }}</a></p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 float-left border-bottom border-5 border-levuAzul pb-5">
                <div class="col-md-2"><strong>PUESTO ACTUAL</strong><br>{{$user_data[0]->puesto}}</div>
                <div class="col-md-2"><strong>SECTOR O INDUSTRIA ACTUAL</strong><br>{{$user_data[0]->sector}}</div>
                <div class="col-md-2"><strong>ESCOLARIDAD</strong><br>{{$user_data[0]->escolaridad}}</div>
                <!--<div class="col-md-3"><strong>RANGO DE SUELDO</strong><br>${{$user_data[0]->sueldoDesde}} a ${{$user_data[0]->sueldoHasta}}</div>-->
                <div class="col-md-2"><strong>CONTACTO</strong><br>{{$user->phone}}</div>           
            </div>
            <div class="col-md-12 float-left pt-5 pb-5">
                <div class="col-md-2"><strong>CALLE Y NÚMERO</strong><br>{{$user_data[0]->calleNumero}}</div>
                <div class="col-md-2"><strong>CP</strong><br>{{$user_data[0]->cp}}</div>
                <div class="col-md-2"><strong>COLONIA</strong><br>{{$user_data[0]->col}}</div>
                <div class="col-md-2"><strong>DEL./MPO.</strong><br>{{$user_data[0]->delmpo}}</div>
                <div class="col-md-2"><strong>ESTADO/REGIÓN</strong><br>{{$user_data[0]->edo}}</div>
                <div class="col-md-2"><strong>PAÍS</strong><br>{{$user_data[0]->pais}}</div>
            </div>
        </div>
        <div class="col-md-12 float-left pt-5 pl-0 pr-0">
            <div class="col-md-6 bg-levuAzul color-black pr-0 pl-0 pt-3 pb-3 candidatoInfo">
                <table width="100%" class="tableHead">
                	<tr>
                    	<td class="border-bottom p-2"><strong>TU INFORMACIÓN</strong></td>
                    </tr>
                    <tr>
                    	<td class="p-2 color-white">SUBE TUS ARCHIVOS</td>
                    </tr>
                </table>
                <div class="col-md-12 pl-0 pr-0 mt-3" style="height:150px;">
                    <table width="100%" class="mt-4">
                        <tr>
                            <td class="text-center" width="20%">
                                <p><strong>CV</strong></p>
                                <p>
                                    <div class="profileIcon bg-white pull-center text-center pt-3">
                                        <img src="{{ asset('img/icons/curriculum-vitae.svg') }}" class="img-fluid">
                                    </div>
                                </p>
                                <p>
                                    @if($user_data[0]->cv != NULL)
                                        <a href="{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->cv }}" class="btn btn-xs bg-white color-black font-10" target="_blank">VER</a>
                                    @endif
                                    <button class="btn btn-xs bg-black color-white font-10" onClick="$('#cv_file{{$user->id}}').click()">EDITAR</button>
                                    <form id="cvUpload{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                        {{ csrf_field() }}
                                        <input type="file" id="cv_file{{$user->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#cvUpload{{$user->id}}').submit();">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <input type="hidden" name="folder" value="{{$user_data[0]->folder}}">
                                        <input type="hidden" name="fileType" value="cv">
                                        <input type="hidden" name="_returnPath" value="{{Request::url()}}">
                                    </form>
                                </p>
                            </td>
                            <td class="text-center" width="20%">
                                <p><strong>VIDEO</strong></p>
                                <p>
                                    <div class="profileIcon bg-white pull-center text-center pt-3">
                                        <img src="{{ asset('img/icons/video-camera.svg') }}" class="img-fluid">
                                    </div>
                                </p>
                                <p>
                                    @if($user_data[0]->video != NULL)
                                        <button type="button" class="btn btn-xs bg-white color-black mr-1 font-10" data-title="Video {{$user->name}} {{$user->lastName}}" data-instructions="" data-submit-btn="Aceptar" onclick="showVideo('{{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->video }}'); toggleModal(this);">
                                            VER
                                        </button>
                                    @endif
                                    <button class="btn btn-xs bg-black color-white font-10" data-title="Video CV" data-instructions="Selecciona una opción." data-submit-btn="Aceptar" onclick="videoCVmodal('{{$user->id}}'); toggleModal(this);">EDITAR</button>
                                    
                                    <form id="videoRecord{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                        {{ csrf_field() }}
                                        <input type="hidden" id="uid" name="id" value="{{$user->id}}">
                                        <input type="hidden" id="folder" name="folder" value="{{$user_data[0]->folder}}">
                                        <input type="hidden" id="fileType" name="fileType" value="video">
                                        <input type="hidden" id="returnPath" name="_returnPath" value="{{Request::url()}}">
                                    </form>
                                    
                                    <form id="videoUpload{{$user->id}}" action="{{ url('/dashboard/vacantes/fileUpload') }}" enctype="multipart/form-data" method="POST">
                                        {{ csrf_field() }}
                                        <input type="file" id="video_file{{$user->id}}" name="_file" style="visibility:hidden; position:absolute;" onChange="$('#videoUpload{{$user->id}}').submit();">
                                        <input type="hidden" name="id" value="{{$user->id}}">
                                        <input type="hidden" name="folder" value="{{$user_data[0]->folder}}">
                                        <input type="hidden" name="fileType" value="video">
                                        <input type="hidden" name="_returnPath" value="{{Request::url()}}">
                                    </form>
                                </p>
                            </td>
                            <td class="text-center" width="20%">
                                <p class="mobileHide"><strong>COMPENSACIONES</strong></p>
                                <p class="mobileShow"><strong>COMP.</strong></p>
                                <p>
                                    <div class="profileIcon bg-white pull-center text-center pt-3">
                                        <img src="{{ asset('img/icons/money.svg') }}" class="img-fluid">
                                    </div>
                                </p>
                                <p>
                                	@if($user_data[0]->compensaciones == 1)
                                    <a href="{{ url('/dashboard/users/compensaciones/'.$user->id) }}" class="btn btn-xs bg-white color-black mr-1 font-10">VER</a>
                                    @endif
                                    <a href="{{ url('/dashboard/users/compensaciones/edit/'.$user->id) }}" class="btn btn-xs bg-black color-white font-10">EDITAR</a>
                                </p>  
                            </td>
                            <td class="text-center" width="20%">
                                <p class="mobileHide"><strong>REFERENCIAS</strong></p>
                                <p class="mobileShow"><strong>REF.</strong></p>
                                <p>
                                    <div class="profileIcon bg-white pull-center text-center pt-3">
                                        <img src="{{ asset('img/icons/add-comment.svg') }}" class="img-fluid">
                                    </div>
                                </p>
                                <p><a href="{{ url('/dashboard/users/referencias/'.$user->id.'/') }}" class="btn btn-xs bg-black color-white mr-1 font-10">EDITAR</a></p>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-md-6 bg-black color-white pr-0 pl-0 pt-3 pb-3 candidatoInfo">
                <table width="100%" class="tableHead">
                	<tr>
                    	<td class="border-bottom p-2 pl-5 color-levuAzul"><strong>TU VACANTE</strong></td>
                    </tr>
                </table>
                <div class="col-md-12 mt-3" style="height:182px; overflow:auto;">
                    <table width="100%" class="mt-4">
                        <tr>
                            <td class="text-left color-white" width="25%">VACANTE</td>
                            <td class="text-left color-white" width="25%">HEADHUNTER</td>
                            <td class="text-left color-white"width="25%">EMAIL</td>
                            <td width="25%"></td>
                        </tr>
                        @foreach($vacantes as $vacante)
                            <tr>
                                <td class="text-left color-white p-2">
                                    {{$vacante->titulo}}
                                </td>
                                <td class="text-left color-white p-2">{{$reclutador->name}} {{ $reclutador->lastName}}</td>
                                <td class="text-left color-white p-2">{{$reclutador->email}} </td>
                                <td>
                                    <a class="btn btn-xs btn-primary font-14" target="_blank" href="{{ url('/dashboard/vacantes/view/'.$vacante->id) }}">Detalle Vacante</a>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        
    </div>
</div>
@endsection