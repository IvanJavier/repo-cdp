@extends('layouts.console')

@section('title', "LEVU Talent")

@section('content')

<div class="main-content bg-whiteGrey" style="background:url({{ asset('img/bg-empresas.jpg') }}) no-repeat center; background-size:cover; min-height:calc(100vh - 56px);">
    <div class="row justify-content-center">
    	<div class="col-md-12">
        	<div class="col-md-12 pt-5 pb-5">
                <div class="col-md-12">
                    @if( $user_data[0]->profilePic != NULL)
                        <div class="enterprisePic float-left mr-3" style="background:url({{ asset('storage').'/'.$user_data[0]->folder.'/'.$user_data[0]->profilePic }}) no-repeat center; background-size:cover;">
                        </div>
                    @else
                        <div class="enterprisePic float-left mr-3" style="background:url({{ asset('img/no-img.jpg') }}) no-repeat center; background-size:cover;">
                        </div>
                    @endif
                    <h1>{{ $user->name }} <a href="{{ url('/dashboard/users/edit/'.$user->id) }}" class="btn btn-oval color-black bg-white"><img src="{{ asset('img/icons/pencil.svg') }}" width="25"></a></h1>
                    <p><a href="{{ $user->email }}" target="_blank">{{ $user->email }}</a> | {{ $user->phone }}</p>
                </div>
            </div>
            <div class="col-md-12 float-left">
            	<div class="col-md-12 color-levuAzul"><h4><strong>Datos del contacto / contact data</strong></h4></div>
                <div class="col-md-2"><strong>NOMBRE / NAME</strong><br>{{$user_data[0]->contactoName}}</div>
                <div class="col-md-2"><strong>EMAIL</strong><br>{{$user_data[0]->contactoEmail}}</div>
                <div class="col-md-2"><strong>PUESTO / JOB</strong><br>{{$user_data[0]->contactoPuesto}}</div>
                <div class="col-md-2"><strong>TELÉFONO / PHONE</strong><br>{{$user_data[0]->contactoPhone}}</div>
                <div class="col-md-2"><strong>SECTOR DE LA EMPRESA / INDUSTRY</strong><br>{{$user_data[0]->sector}}</div>
            </div>
            <div class="col-md-12 float-left border-bottom border-5 border-levuAzul pb-5">
            	<div class="col-md-12 color-levuAzul"><h4><strong>Dirección de la empresa / Address</strong></h4></div>
                <div class="col-md-2"><strong>CALLE Y NÚMERO / ST. & NUMBER</strong><br>{{$user_data[0]->calleNumero}}</div>
                <div class="col-md-2"><strong>CP / ZIPCODE</strong><br>{{$user_data[0]->cp}}</div>
                <div class="col-md-2"><strong>COLONIA / NEIGHTBORHOOD</strong><br>{{$user_data[0]->col}}</div>
                <div class="col-md-2"><strong>DEL./MPO. / COUNTY</strong><br>{{$user_data[0]->delmpo}}</div>
                <div class="col-md-2"><strong>ESTADO/REGIÓN / STATE</strong><br>{{$user_data[0]->edo}}</div>
                <div class="col-md-2"><strong>PAÍS / COUNTRY</strong><br>{{$user_data[0]->pais}}</div>
            </div>
        </div>
    </div>
</div>
@endsection