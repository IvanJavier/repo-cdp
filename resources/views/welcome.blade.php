@extends('layouts.site')

@section('title', "LEVU Talent")

@section('content')

<section id="home" class="site">
    <div id="home-bg" class="relative float-left w-adjust">
        <div id="welcome-text" class="anim absolute left0 top50pct bg-whiteGrey col-7 p-5">
            <h2 id="welcome_heading" class="text-uppercase color-levuAzul mt-0 relative">Headhunting based on skills, experience and culture fits<span class="absolute left0 pt-2 color-grey">___</span></h2>
            <p>Hiring is evolving, technology and social networks are the new tools for recruiters. Levu Talent Hunters can help you find and hit all the right pieces for your organizational structure and strategy needs with our unique sensitive and objective executives search procedure.</p>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="container pt-75 pb-75">
        <div class="row">
            <div id="videoPresetation" class="col-md-10 col-md-offset-1 pb-5 anim">
                <iframe style="width:100%; height:400px;" width="560" height="315" src="https://www.youtube.com/embed/6OxG1DKmpqo" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </div>
            <div class="col-md-10 col-md-offset-1 pt-75" id="videoTexts">
                <div class="pb-5 relative anim">
                    <div class="col-md-6 text-right pr-5">
                        <h2 class="m-0 text-uppercase relative"><strong><span class="color-levuAzul">what we</span><br>really want</strong><span class="absolute right0 pt-2 color-grey">___</span></h2>
                    </div>
                    <div class="col-md-6 pl-5">
                        When the need comes; be the first option for our clients and all the passive and active candidates to solve their recruitment issues and improve their lives.
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="pt-5 relative anim">
                    <div class="col-md-6 text-right pr-5">
                        <h2 class="m-0 text-uppercase relative"><strong><span class="color-levuAzul">our</span><br>principles</strong><span class="absolute right0 pt-2 color-grey">___</span></h2>
                    </div>
                    <div class="col-md-6 pl-5">
                        <ul class="pl-20">
                            <li>We are happy doing things the way we do.</li>
                            <li>We are focused on velocity and quality, we know our clients need different results.</li>
                            <li>We have and use integrity by conviction, everywhere.</li>
                            <li>We pursue our dreams.</li>
                            <li>We care about the world.</li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<section id="slogan1" class="bg-levuAzul color-white text-center text-uppercase pt-5 pb-5 relative">
    <h2><strong>We hit candidates whose experience was<br>achieved with events, not only with time.</strong></h2>
</section>
<section id="about-us" class="site">
    <div class="container pt-75 pb-75">
        <div class="">
            <div class="col-md-10 col-md-offset-1">
                <div class="">
                    <div class="col-md-4">
                        <h2 id="our_mision_heading" class="m-0 text-uppercase relative"><strong><span class="color-levuAzul">our</span> mission</strong><span class="absolute left0 pt-2 color-grey">___</span></h2>
                    </div>
                    <div class="col-md-8">
                        Have our happy and committed recruiters hit the ideal candidates for all our clients, by using a deep scoring and evaluation process that involves skills fits, experience fits and culture fits.
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="bg-black color-white text-center text-uppercase pt-5 pb-5 relative" id="slogan2">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h2>
                        <strong>
                            We provide absolutely all the information
                            required to make the final decision, we analyze
                            all this information and place it all together
                            in our clients web platform
                        </strong>
                    </h2>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
</section>
<section id="our-method" class="site d-flex middle">
    <div class="container pt-75">
        <div class="">
            <div class="col-md-10 col-md-offset-1 pb-5 relative" id="our-method-t1">
                <h2 class="m-0 text-uppercase relative"><strong><span class="color-levuAzul">our method</span> & deliverables</strong><span class="absolute left0 pt-2 color-grey">___</span></h2>
            </div>
            <div class="col-md-10 col-md-offset-1 pt-5 pl-0 pr-0">
                <div class="relative" id="our-method-t2">
                    <div class="col-md-6 pr-5">
                        <p>
                            <strong>Step 1:</strong><br>
                            Every job is related to a problem that our client needs to solve, therefore; we always have a meeting with the hiring manager in order to understand and get involved with the special requirements. This step of our process ensures a well focused search.
                        </p>
                    </div>
                    <div class="col-md-6 pl-5">
                        <strong>Step 2:</strong><br>
                        We have a special team to do the searches based on the previous position scan. We never post and pray. We look into the entire ocean to find the ideal candidates and contact them for a first filter. 
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="relative pt-5 pb-5" id="our-method-t3">
                    <div class="col-md-6 pr-5">
                        <strong>Step 3:</strong>
                        Once we have the first group of candidates: we meet them, we interview them, we make a 3 minutes video resume, we fill the SEC Fits (our tool designed to make sure the candidate matches with the skills, experience and culture required) and we evaluate them with our system Levu Talent Report (online tests).
                    </div>
                    <div class="col-md-6 pl-5">
                        <strong>Step 4:</strong>
                        With a time line of 2 to 3 weeks; we deliver a Comparison Report to our client. It includes the candidates ranking and results. We like to participate and help the hiring manager to choose the person he/she needs in the team. We really hunt.
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<section id="services" class="d-flex middle">
    <div class="container pt-75 pb-75">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 pb-5 text-center" id="services-t1">
                <h3 id="services_heading" class="m-0 text-uppercase relative font-22"><strong><span class="color-levuAzul">We launched a new technology based executives search model,</span><br>integrating Skills, Experience & Culture Fits of our candidates</strong></h3>
            </div>
            <div class="col-md-10 col-md-offset-1 pt-5">
                <div class="">
                    <div class="col-md-4 mb-3" id="services-t2">
                        <img src="/img/site/executivesSearch.jpg" class="img-fluid">
                        <div class="col-12 bg-white pt-3 pb-2 services-box">
                            <p class="m-0 text-uppercase relative font-20"><strong><span class="color-levuAzul">HEADHUNTING</span><br></strong></p>
                            <p class="mt-4 mb-4">We do head hunting, we are specialized in high level positions. We have different groups of recruiters asigned to specific industries in order to have well experienced head hunters for each customer.</p>                 
                        </div>
                    </div>
                    <div class="col-md-4 mb-3" id="services-t3">
                        <img src="/img/site/personnelEval.jpg" class="img-fluid">
                        <div class="col-12 bg-white pt-3 pb-2 services-box">
                            <p class="m-0 text-uppercase relative font-20"><strong><span class="color-levuAzul">TALENT MATURITY</span><br>REPORT</strong></p>
                            <p class="mt-4 mb-4">We offer our tool: Levu Talent Report. An online set of tests designed to analyze candidates intelligence, emotional control, skills and culture.</p>
                        </div>
                    </div>
                    <div class="col-md-4 mb-3" id="services-t4">
                        <img src="/img/site/humanRes.jpg" class="img-fluid">
                        <div class="col-12 bg-white pt-3 pb-2 services-box">
                            <p class="m-0 text-uppercase relative font-20"><strong><span class="color-levuAzul">LEVU DASHBOARD</span><br>LEVU TOGETHER</strong></p>
                            <p class="mt-4 mb-4">Our services are focused on helping all our clients through the technology that we have developed and supporting all Starups with Levu Together.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<section id="contact-us" class="d-flex middle bg-black">
    <div class="container pt-75 h-100">
        <div class="relative text-center">
            <div id="contact_us_heading" class="col-md-12 pb-5 text-center">
            </div>
            @include('partials.oficinas')
            <a class="btn bg-transparent color-white go-to" name="contact-form" href="#contact-form">
            	<small>Send us an e-mail</small><br>
            	<i class="fas fa-chevron-down"></i>
            </a>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
<section id="contact-form" class="d-flex middle">
    <div class="container pt-75">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 pb-5">
                <div class="">
                    <div class="col-md-6 pr-5 relative">
                        <h2 class="m-0 mb-4 text-uppercase relative"><strong><span class="color-levuAzul">send</span> your resume</strong></h2>
                        <form id="cvForm" method="POST" action="{{ url('/sendResume') }}" enctype="multipart/form-data" onSubmit="return checkBoxCV('cvForm');">
                            {{ csrf_field() }}
                            <p><input id="cv-name" type="text" class="form-control bg-transparent" name="cv-name" placeholder="Name*" required></p>
                            <p><input id="cv-email" type="text" class="form-control bg-transparent" name="cv-email" placeholder="E-mail*" required></p>
                            <p><small>Resume (PDF format, no more than 5MB)</small></p>
                            <p><input id="cv-file" type="file" class="form-control bg-transparent p-2" name="cv-file"></p>
                            <p><label class="small"><input class="privacyPolicy" type="checkbox"> I've read and accepted the <a href="{{ url('/aviso-de-privacidad') }}" target="_blank">Privacy policy</a></label></p>
                            <p><button type="submit" class="btn btn-block bg-levuAzul color-white">Send</button></p>
                            @if (app('request')->input('sent') == 'r')
                            	<small>Thanks for sending your resume.</small>
                            @endif
                        </form>
                    </div>
                    <div class="col-md-6 pl-5 relative">
                        <h2 class="m-0 mb-4 text-uppercase relative"><strong><span class="color-levuAzul">send</span> a message</strong></h2>
                        <form id="contactForm" method="POST" action="{{ url('/sendContact') }}" onSubmit="return checkBoxContact('contactForm');">
                            {{ csrf_field() }}
                            <p><input id="contact-name" type="text" class="form-control bg-transparent" name="contact-name" placeholder="Name*" required></p>
                            <p><input id="contact-email" type="text" class="form-control bg-transparent" name="contact-email" placeholder="E-mail*" required></p>
                            <p><input id="contact-phone" type="text" class="form-control bg-transparent" name="contact-phone" placeholder="Phone number*" required></p>
                            <p><textarea id="contact-message" class="form-control bg-transparent" name="contact-message" placeholder="Message*"></textarea></p>
                            {{-- <p><label><input type="checkbox"> I wish to receive news and updates in my e-mail.</label></p> --}}
                            <p><label class="small"><input class="privacyPolicy" type="checkbox"> I've read and accepted the <a href="{{ url('/aviso-de-privacidad') }}" target="_blank">Privacy policy</a></label></p>
                            <p><button type="submit" class="btn btn-block bg-levuAzul color-white">Send</button></p>
                        	@if (app('request')->input('sent') == 'c')
                            	<small>Thanks for contacting us.</small>
                            @endif
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</section>
@endsection