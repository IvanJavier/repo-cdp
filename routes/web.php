<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// MAIN SITE
Route::get('/', function () {
    return view('welcome');
});
Route::get('/aviso-de-privacidad', function () {
    return view('avisoPrivacidad');
});
// SEND EMAIL
Route::post('/sendResume', 'SiteController@sendResume' );
Route::post('/sendContact', 'SiteController@sendContact' );

Auth::routes();
//AJAX ROUTES
Route::get('/ajax/csrf/', 'CsrfController@csrf_');
Route::get('/ajax/sepomex/{cp}', 'AjaxController@sepomex');
Route::get('/ajax/empresasData/{id}', 'AjaxController@empresasData');
Route::get('/ajax/candidatosList/{_userId}/{_roleId}/{_ownerId}/{_vacanteId}', 'AjaxController@candidatosList');
Route::get('/ajax/reclutadorRelations/{id}', 'AjaxController@reclutadorRelations');
Route::get('/ajax/empresasRelations/{id}', 'AjaxController@empresasRelations');

// ADMINS
Route::post('/dashboard/user/delete', 'AdminController@userDelete');
Route::post('/dashboard/users/edit', 'AdminController@update');
Route::post('/dashboard/users/compensaciones/update', 'AdminController@updateCompensaciones');

Route::get('/dashboard/users/profile/{id}', 'AdminController@userProfile');
Route::get('/dashboard/users/edit/{id}', 'AdminController@showEditForm');
Route::get('/dashboard/users/compensaciones/{id}', 'AdminController@showCompensaciones');
Route::get('/dashboard/users/compensaciones/edit/{id}', 'AdminController@editCompensaciones');
Route::get('/dashboard/users/{role_type}', 'AdminController@userList');

Route::get('/dashboard/users/referencias/new/{uid}', 'AdminController@newReferencias');
Route::get('/dashboard/users/referencias/{uid}', 'AdminController@showReferencias');
Route::post('/dashboard/users/referencias/create', 'AdminController@createReferencias');

Route::get('/dashboard/users/referencias/view/{rid}/{uid}', 'AdminController@viewReferencias');
Route::get('/dashboard/users/referencias/edit/{rid}/{uid}', 'AdminController@editReferencias');
Route::post('/dashboard/users/referencias/update', 'AdminController@updateReferencias');
Route::post('/dashboard/users/referencias/delete', 'AdminController@deleteReferencias');

// Registration Routes...
Route::get('/dashboard/users/registration/{role_type}', 'AdminController@showRegistrationForm');
Route::post('/dashboard/users/registration', 'AdminController@create');
// Vacantes
Route::get('/dashboard/vacantes/', 'VacantesController@index');
Route::get('/dashboard/vacantes/export', 'VacantesController@indexExport');

Route::get('/dashboard/vacantes/view/{id}', 'VacantesController@view');
Route::get('/dashboard/vacantes/print/{id}', 'VacantesController@imprimir');
Route::get('/dashboard/vacantes/create/', 'VacantesController@showCreationForm');
Route::post('/dashboard/vacantes/create/', 'VacantesController@create');
Route::get('/dashboard/vacantes/edit/{id}', 'VacantesController@showEditForm');
Route::post('/dashboard/vacantes/edit/', 'VacantesController@update');
Route::post('/dashboard/vacantes/updateComments/', 'VacantesController@updateComments');
Route::post('/dashboard/vacantes/delete/', 'VacantesController@delete');
Route::post('/dashboard/vacantes/addCandidate/', 'VacantesController@addCandidate');
Route::post('/dashboard/vacantes/fileUpload/', 'VacantesController@fileUpload');

Route::get('/dashboard/vacantes/secFits/{uid}/{vid}', 'VacantesController@showSecFits');
Route::get('/dashboard/vacantes/secFits/edit/{uid}/{vid}', 'VacantesController@editSecFits');
Route::get('/dashboard/vacantes/secFits/print/{uid}/{vid}', 'VacantesController@printSecFits');
Route::post('/dashboard/vacantes/secFits/update', 'VacantesController@updateSecFits');

Route::post('/dashboard/vacantes/estatusUser', 'VacantesController@estatusUser');
Route::post('/dashboard/vacantes/blockUser', 'VacantesController@blockUser');
Route::post('/dashboard/vacantes/hireUser', 'VacantesController@hireUser');
Route::post('/dashboard/vacantes/hideCandidate', 'VacantesController@hideCandidate');
Route::post('/dashboard/vacantes/showCandidate', 'VacantesController@showCandidate');

Route::get('/dashboard/sucursalesSectores', 'AdminController@sucursalesSectores');
Route::post('/dashboard/sucursalesSectores/sucursalUpdate', 'AdminController@sucursalesUpdate');
Route::post('/dashboard/sucursalesSectores/sucursalUpload', 'AdminController@sucursalesUpload');
Route::post('/dashboard/sucursalesSectores/sectorUpload', 'AdminController@sectorUpload');
Route::post('/dashboard/sucursalesSectores/sectorUpdate', 'AdminController@sectorUpdate');

// DASHBOARD
Route::get('/dashboard', 'DashboardController@dashboard');
//STORAGE RESTRICTION
Route::get('storage/{folder}/{filename}', 'DashboardController@getFile')->where('filename', '^[^/]+$');

// MI PERFIL
Route::get('/dashboard/my-profile/', 'UserController@myProfile');
// RECLUTADORES

// CANDIDATOS

// EMPRESAS

//Analytics

Route::group(['prefix' => 'dashboard/admin/', 'middleware'=>['auth', 'role:SuperAdmin,Vendedor']], function () {
    
    Route::get('charts', 'AnalyticsController@index');
    Route::get('getOpenVacants', 'AnalyticsController@getOpenVacants');
    Route::get('getClosedVacants', 'AnalyticsController@getClosedVacants');
    Route::get('getVacantsClosedWithHiring', 'AnalyticsController@getVacantsClosedWithHiring');
    Route::get('getVacantsClosedWithOutHiring', 'AnalyticsController@getVacantsClosedWithOutHiring');
    Route::get('getSellers', 'AnalyticsController@getSaleAmount');
    Route::get('opportunities', 'AnalyticsController@viewChart');
    Route::get('getVacantsByEnterprise', 'AnalyticsController@getVacantsByEnterprise');
    Route::get('getVacantsWithAssurance', 'AnalyticsController@getVacantsWithAssurance');

});

// FREELANCE

Route::group(['prefix' => '/dashboard/users', 'middleware' => ['auth','role:SuperAdmin']], function () {
    
    Route::get('/freelances/index', 'FreelanceController@index')->name('freelance.index');
    Route::resource('freelances', 'FreelanceController');

});

Route::get('/dashboard/admin/getTotalSecfits', 'AnalyticsController@getTotalSecfits');
//EMPRESAS-CLIENTES
Route::get('/dashboard/users/profile/{id}/vacantes', 'EnterpriseController@show')->middleware(['auth','role:Empresa'])->name('enterprise.all.vacant');
